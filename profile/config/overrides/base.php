<?php
/**
 * Base overrides for frontend application
 */
return [
    // So our relative path aliases will resolve against the `/frontend` subdirectory and not nonexistent `/protected`
    'basePath' => 'profile',
    'import' => [
        'application.controllers.*',
        'application.controllers.actions.site.*',
        'common.actions.*',
    ],
    'controllerMap' => [
        // Overriding the controller ID so we have prettier URLs without meddling with URL rules
        'site' => 'ProfileSiteController'
    ],
    'modules' => [
        'users' => [
            'class' => '\profile\modules\users\UsersModule'
        ],
        'gallery' => [
            'class' => '\profile\modules\gallery\GalleryModule'
        ],
        'message' => [
            'class' => '\profile\modules\message\MessageModule'
        ],
        'pocket' => [
            'class' => '\profile\modules\pocket\PocketModule'
        ],
        'saver' => [
            'class' => '\profile\modules\saver\SaverModule'
        ],
        'filter' => [
            'class' => '\profile\modules\filter\FilterModule'
        ],
    ],
    'theme' => 'sport',
    'components' => [
        'errorHandler' => [
            'errorAction' => 'site/error'
        ],
        'urlManager' => [
            'rules' => [
                //Карман
                ['pocket/image/add',        'pattern' => '/<conference:.*>/pocket/image/<image_dir:.*>/add', 'urlSuffix' => '.ajax'],
                ['pocket/video/add',        'pattern' => '/<conference:.*>/pocket/video/<video_dir:.*>/add', 'urlSuffix' => '.ajax'],

                //Галерея
                ['gallery/image/update',    'pattern' => '/gallery/image/<image_dir:.*>/update'],
                ['gallery/video/update',    'pattern' => '/gallery/video/<video_dir:.*>/update'],
                ['gallery/image/delete',    'pattern' => '/gallery/image/<image_dir:.*>/delete'],
                ['gallery/video/delete',    'pattern' => '/gallery/video/<video_dir:.*>/delete'],
                ['gallery/image/index',     'pattern' => '/images'],
                ['gallery/video/index',     'pattern' => '/videos'],

                //Сообщения
                ['message/profile/view',    'pattern' => '/im<id:\w+>/view',            'sitePart' => 'profile'],
                ['message/profile/add',     'pattern' => '/im<id:\w+>/add',             'sitePart' => 'profile', 'urlSuffix' => '.ajax'],
                ['message/profile/get',     'pattern' => '/im/view',                    'sitePart' => 'profile', 'urlSuffix' => '.ajax'],
            ]
        ],
    ],
    'params' => [
        'sitePart' => 'profile'
    ]
];