<?php
namespace profile\components;
/**
 * Base class for controllers at frontend.
 *
 * Includes all assets required for frontend and also registers Google Analytics widget if there's code specified.
 *
 * @package YiiBoilerplate\Frontend
 */


use common\components\Controller;
use Yii;
class ProfileController extends Controller
{
    public $layout = 'profile';
    public $showUploadImage = false;
    public $showAddVideo = false;

    /**
     * What to do before rendering the view file.
     *
     * We include Google Analytics code if ID was specified and register the frontend assets.
     *
     * @param string $view
     * @return bool
     */
    public function beforeRender($view)
    {
        $result = parent::beforeRender($view);
        $this->addGoogleAnalyticsCode();
        return $result;
    }

    private function addGoogleAnalyticsCode()
    {
        $gaid = @Yii::app()->params['google.analytics.id'];
        if ($gaid)
            $this->widget('frontend.widgets.GoogleAnalytics.GoogleAnalyticsWidget', compact('gaid'));
    }

    private function registerAssets()
    {
        Yii::app()->static->setOwn()
            ->registerCssFile('form.css')
            ->registerCssFile('ajax.css')
            ->registerCssFile('style.css')
            ->registerScriptFile('ajax.js')
            ->registerScriptFile('autoNumeric.js');

        Yii::app()->static->setWww()
            ->registerCssFile('style.css')
            ->registerCssFile('idangerous.swiper.css')
            ->registerScriptFile('idangerous.swiper.js')
            ->registerScriptFile('jquery.suggest.js')
            ->registerScriptFile('main.js')
            ->registerScriptFile('plugins.js');
    }

    public function beforeAction($action)
    {
        $result = parent::beforeAction($action);
        $this->registerAssets();

        return $result;
    }
}
