<?php
namespace profile\controllers\actions\site;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */

use CAction;
use Yii;
use EMongoCriteria;
use HistoryMessage;
class InfoAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new EMongoCriteria();
        $criteria->addCondition('user_receiver_id', Yii::app()->user->getId());
        $criteria->addCondition('receiver_view', false);
        $count = HistoryMessage::model()->count($criteria);

        $message = ['count' => $count];


        Yii::app()->ajax->addOther([
            'message' => $message
        ])->send();
    }
} 