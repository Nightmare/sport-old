<?php
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use \profile\components\ProfileController;
class ProfileSiteController extends ProfileController
{
	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'error' 	=> ['class' => 'SimpleErrorAction'],
			'info' 		=> ['class' => 'profile\controllers\actions\site\InfoAction'],
		);
	}
}