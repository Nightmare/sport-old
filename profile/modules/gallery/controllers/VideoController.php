<?php
namespace profile\modules\gallery\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use profile\modules\gallery\components\GalleryBaseController;
use Yii;

class VideoController extends GalleryBaseController
{
    public function filters() {
        return array(
            'ajaxOnly + create',
        );
    }

    public function beforeRender($view)
    {
        $this->showAddVideo = true;

        Yii::app()->static->setOwn()->registerScriptFile('video.js');

        $result = parent::beforeRender($view);
        return $result;
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'index' => ['class' => 'profile\modules\gallery\controllers\actions\video\IndexAction'],
            'create' => ['class' => 'profile\modules\gallery\controllers\actions\video\CreateAction'],
		);
	}
}