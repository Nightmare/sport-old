<?php
namespace profile\modules\gallery\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use profile\modules\gallery\components\GalleryBaseController;
use Yii;

class ImageController extends GalleryBaseController
{
    public function filters() {
        return array(
            'ajaxOnly + upload, update, create',
        );
    }

    public function beforeRender($view)
    {
        $this->showUploadImage = true;

        Yii::app()->static->setOwn()->registerScriptFile('image.js');

        $result = parent::beforeRender($view);
        return $result;
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'index' => ['class' => 'profile\modules\gallery\controllers\actions\image\IndexAction'],
            'upload' => ['class' => 'profile\modules\gallery\controllers\actions\image\UploadAction'],
            'update' => ['class' => 'profile\modules\gallery\controllers\actions\image\UpdateAction'],
            'create' => ['class' => 'profile\modules\gallery\controllers\actions\image\CreateAction'],
		);
	}
}