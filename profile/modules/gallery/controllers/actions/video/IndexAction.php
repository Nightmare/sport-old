<?php
namespace profile\modules\gallery\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use User;
use GalleryVideo;
use CPagination;
use CDbCacheDependency;
use CDbCriteria;
class IndexAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->scopes = ['own', 'enable'];

        $dependency = new CDbCacheDependency('SELECT count(*) FROM {{gallery_video}} where user_id = :user_id');
        $dependency->params = [':user_id' => Yii::app()->user->id];
        $dependency->reuseDependentData = true;

        $pages = new CPagination(GalleryVideo::model()->cache(10000, $dependency)->count($criteria));
        $pages->pageSize = Yii::app()->params['pages']['video'];
        $pages->applyLimit($criteria);

        /** @var GalleryVideo[] $Models */
        $Models = GalleryVideo::model()->cache(10000, $dependency)->findAll($criteria);

        $params = [
            'models' => $Models,
            'pages' => $pages
        ];
        if(Yii::app()->request->isAjaxRequest) {
            Yii::app()->ajax->addOther([
                'ok' => true,
                'page' => $pages->getCurrentPage(),
                'request' => $_REQUEST,
                'selector' => '#page_content',
                'view' => $this->controller->renderPartial('_index', $params, true),
            ])->send();
        } else {
            Yii::app()->breadcrumbs
                ->addBreadcrumb('Профиль', Yii::app()->createUrl('/users/profile/index'))
                ->addBreadcrumb('Видеозаписи');

            $this->controller->render('index', $params);
        }
    }
}