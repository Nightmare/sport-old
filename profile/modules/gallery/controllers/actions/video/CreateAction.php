<?php
namespace profile\modules\gallery\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use GalleryVideo;
use Muscle;
use Simulator;
use Items;

class CreateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $galleryPost = Yii::app()->request->getPost('GalleryVideo');
        $filterPost = Yii::app()->request->getPost('filter', []);

        /** @var GalleryVideo $GalleryVideo */
        $GalleryVideo = new GalleryVideo('createAction');

        if(!empty($galleryPost)) {
            $filter = [];
            foreach ($filterPost as $filter_id) {
                $Filter = Yii::app()->categories->getFilter($filter_id);
                if($Filter !== null) {
                    $filter[] = $Filter->getId();
                    $GalleryVideo->addVideoFilterInfo($Filter->getId(), $Filter->getTitle());
                }
            }

            $GalleryVideo->setFilterIds(serialize($filter))
                ->setUserId(Yii::app()->user->id)
                ->setAttributes($galleryPost);

            if(!$GalleryVideo->createAction())
                Yii::app()->ajax->addErrors($GalleryVideo);
            else {
                Yii::app()->ajax->addMessage(Yii::t('labels', 'Видео добавлено'));
                Yii::app()->ajax->addOther([
                    'ok' => true,
                    'runJS' => [
                        'functionName' => 'updatePage',
                        'functionParams' => [Yii::app()->createUrl('/gallery/video/index')]
                    ]
                ]);
            }
        } else {
            /** @var Items $Item */
            $Item = Items::model()->findOne(['image_id' => $GalleryVideo->getId()]);
            if(!$Item)
                $Item = new Items();

            $params = [
                'model' => $GalleryVideo,
                'itemMongo' => $Item,
                'link' => Yii::app()->createUrl('/gallery/video/create')
            ];
            Yii::app()->ajax
                ->addReplace('_form', '#customModal #replacement', $params)
                ->runJS('updateVideo')
                ->addOther(['ok' => true]);
        }

        Yii::app()->ajax->send();
    }
}