<?php
namespace profile\modules\gallery\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use Items;
use CDbCriteria;
use GalleryImage;
use MException;
use Muscle;
use Simulator;

class CreateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $galleryPost = Yii::app()->request->getPost('GalleryImage');
        $filterPost = Yii::app()->request->getPost('filter', []);
        $minImage = Yii::app()->request->getPost('min');
        $originImage = Yii::app()->request->getPost('origin');

        $GalleryImage = new GalleryImage('createAction');
        $GalleryImage->min = Yii::app()->static->staticPath().'/'.$minImage;
        $GalleryImage->original = Yii::app()->static->staticPath().'/'.$originImage;

        if(!empty($galleryPost)) {
            $filter = [];
            foreach ($filterPost as $filter_id) {
                $Filter = Yii::app()->categories->getFilter($filter_id);
                if($Filter !== null) {
                    $filter[] = $Filter->getId();
                    $GalleryImage->addImageFilterInfo($Filter->getId(), $Filter->getTitle());
                }
            }

            $GalleryImage->setAttributes($galleryPost);
            $GalleryImage->setFilterIds(serialize($filter));

            if(!$GalleryImage->createAction()) {
                Yii::app()->ajax->addErrors($GalleryImage);
            } else {
                Yii::app()->ajax->addMessage(Yii::t('labels', 'Изображение обновленно'));
                Yii::app()->ajax->addOther([
                    'ok' => true,
                    'runJS' => [
                        'functionName' => 'updatePage',
                        'functionParams' => [Yii::app()->createUrl('/gallery/image/index')]
                    ]
                ]);
            }
        } else {
            $params = [
                'model' => $GalleryImage,
                'itemMongo' => new Items(),
                'image' => $minImage,
                'min' => $minImage,
                'origin' => $originImage,
            ];
            Yii::app()->ajax
                ->addReplace('_form', '#customModal #replacement', $params)
                ->runJS('updateImage')
                ->addOther(['ok' => true]);
        }

        Yii::app()->ajax->send();
    }
}