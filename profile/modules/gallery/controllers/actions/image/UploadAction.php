<?php
namespace profile\modules\gallery\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\components\VarDumper;
use Yii;
use GalleryImage;

class UploadAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        if(!isset($_FILES['image']) || !isset($_FILES['image']['tmp_name']) || count($_FILES['image']['tmp_name']) != 2)
            Yii::app()->ajax->send();

        $imageNameWithoutExt = GalleryImage::generateImageName();
        foreach ($_FILES['image']['tmp_name'] as $key => $item) {
            foreach ($item as $type => $path) {
                switch ($type) {
                    case 'thumb':
                        $type = GalleryImage::SIZE_TYPE_MIN;
                        break;
                    case 'original':
                        $type = GalleryImage::SIZE_TYPE_ORIGIN;
                        break;
                }
                /** @var \common\components\CImageHandler $ih */
                $ih = new \common\components\CImageHandler();
                $ih->load($path);

                $imagePath = sprintf('%s/%s.%s', GalleryImage::getTempFilePath($type), $imageNameWithoutExt, $ih->getFormatType());
                $ih->save(Yii::app()->static->staticPath().'/'.$imagePath);

                Yii::app()->ajax->addOther([
                    $type => $imagePath
                ]);
            }
        }

        Yii::app()->ajax->addMessage(Yii::t('messages', 'Изображение добавлено, заполните инфомрацию'))
            ->addOther(['ok' => true])->send();
    }
}