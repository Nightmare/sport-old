<?php
namespace profile\modules\gallery\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use Items;
use CDbCriteria;
use GalleryImage;
use MException;
use Muscle;
use Simulator;

class UpdateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $galleryPost = Yii::app()->request->getPost('GalleryImage');
        $filterPost = Yii::app()->request->getPost('filter', []);

        $criteria = new CDbCriteria();
        $criteria->addCondition('`t`.dir = :image_dir');
        $criteria->params = [':image_dir' => Yii::app()->request->getParam('image_dir')];

        /** @var GalleryImage $GalleryImage */
        $GalleryImage = GalleryImage::model()->find($criteria);
        if(!$GalleryImage)
            MException::ShowError(Yii::t('errors', 'Изображение не найдено'));
        $GalleryImage->scenario = 'updateAction';

        $filter = [];
        foreach ($filterPost as $filter_id) {
            $Filter = Yii::app()->categories->getFilter($filter_id);
            if($Filter !== null) {
                $filter[] = $Filter->getId();
                $GalleryImage->addImageFilterInfo($Filter->getId(), $Filter->getTitle());
            }
        }
        if(!empty($galleryPost)) {
            $GalleryImage->setAttributes($galleryPost);
            $GalleryImage->setFilterIds(serialize($filter));

            if(!$GalleryImage->updateAction()) {
                Yii::app()->ajax->addErrors($GalleryImage);
            } else {
                Yii::app()->ajax->addMessage(Yii::t('labels', 'Изображение обновленно'));
                Yii::app()->ajax->addOther([
                    'ok' => true,
                    'runJS' => [
                        'functionName' => 'updatePage',
                        'functionParams' => [Yii::app()->createUrl('/gallery/image/index')]
                    ]
                ]);
            }
        } else {
            /** @var Items $Item */
            $Item = Items::model()->findOne(['image_id' => $GalleryImage->getId()]);
            if(!$Item)
                $Item = new Items();

            $params = [
                'model' => $GalleryImage,
                'itemMongo' => $Item,
                'image' => $GalleryImage->getMinImage(),
                'min' => null,
                'origin' => null,
            ];

            Yii::app()->ajax
                ->addReplace('_form', '#customModal #replacement', $params)
                ->runJS('updateImage')
                ->addOther(['ok' => true]);
        }

        Yii::app()->ajax->send();
    }
}