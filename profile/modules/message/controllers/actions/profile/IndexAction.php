<?php
namespace profile\modules\message\controllers\actions\profile;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CPagination;
class IndexAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        Yii::app()->breadcrumbs
            ->addBreadcrumb('Профиль', Yii::app()->createUrl('/users/profile/index'))
            ->addBreadcrumb('Сообщения');

        $this->controller->render('index', [
            'models' => [],
            'Me' => Yii::app()->user->getModel(),
            'UserDialog' => null,
            'pages' => null
        ]);
    }
}