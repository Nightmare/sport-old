<?php
namespace profile\modules\message\controllers\actions\profile;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use EMongoCriteria;
use HistoryMessage;
class MessageAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new EMongoCriteria();
        $criteria->addCondition('user_receiver_id', Yii::app()->user->getId());
        $criteria->addCondition('receiver_view', false);
        $count = HistoryMessage::model()->count($criteria);

        Yii::app()->ajax->addOther(['ok' => true, 'count' => $count])->send();
    }
}