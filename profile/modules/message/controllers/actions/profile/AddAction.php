<?php
namespace profile\modules\message\controllers\actions\profile;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use HistoryMessage;
use User;
use UserMessageList;
class AddAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $id = Yii::app()->request->getPost('id');
        $body = Yii::app()->request->getPost('body');
        if(empty($body)) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Текст сообщения не может быть пустым'))->send();
        }

        /** @var User $UserDialog */
        $UserDialog = User::model()->with('userProfile')->findByPk($id);
        if(!$UserDialog || $UserDialog->getId() == Yii::app()->user->id) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Пользователь не найден'))->send();
        }

        if($UserDialog->getId() == Yii::app()->user->id) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Вы не можете отправлять сообщения сами себе'))->send();
        }

        $HistoryMessage = new HistoryMessage();
        $HistoryMessage->setUserSenderId(Yii::app()->user->id)
            ->setUserReceiverId($UserDialog->getId())
            ->setBody($body);
        $save = $HistoryMessage->save();
        if(!$save)
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Неудалось отправить сообщение'));
        else {
            Yii::app()->ajax->addOther(['ok' => true]);

            $criteria = new \CDbCriteria();
            $criteria->addCondition('owner_user_id = :owner_user_id');
            $criteria->addCondition('dialog_user_id = :dialog_user_id');
            $criteria->params = [':owner_user_id' => Yii::app()->user->id, ':dialog_user_id' => $UserDialog->getId()];
            if(UserMessageList::model()->count($criteria) == 0) {
                $model = new UserMessageList();
                $model->setOwnerUserId(Yii::app()->user->id)
                    ->setDialogUserId($UserDialog->getId())
                    ->save(false);
            }
        }

        Yii::app()->ajax->send();
    }
}