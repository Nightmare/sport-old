<?php
namespace profile\modules\message\controllers\actions\profile;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use HistoryMessage;
use EMongoCriteria;
use User;
use EMongoPagination;
class ViewAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $id = Yii::app()->request->getParam('id');
        /** @var User $UserDialog */
        $UserDialog = User::model()->with('userProfile')->findByPk($id);
        if(!$UserDialog) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Пользователь не найден'))
                ->setUrl(['route' => '/message/profile/index', 'params' => []])
                ->send();
        }

        if($UserDialog->getId() == Yii::app()->user->id) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Вы не можете отправлять сообщения сами себе'))
                ->setUrl(['route' => '/message/profile/index', 'params' => []])
                ->send();
        }

        /** @var User $User */
        $User = Yii::app()->user->getModel();

        $criteria = new EMongoCriteria();
        $condition = [
            '$or' => [
                [
                    '$and' => [
                        ['user_sender_id' => Yii::app()->user->id, 'sender_delete' => false],
                        ['user_receiver_id' => $UserDialog->getId()],
                    ]
                ],
                [
                    '$and' => [
                        ['user_sender_id' => $UserDialog->getId()],
                        ['user_receiver_id' => Yii::app()->user->id, 'receiver_delete' => false],
                    ]
                ]
            ]
        ];
        $criteria->setCondition($condition);
        $criteria->sort = ['create_at' => 'desc'];

        $Pages = new EMongoPagination(HistoryMessage::model()->count($criteria));
        $Pages->pageSize = 30;
        $Pages->applyLimit($criteria);
        $Pages->params = ['id' => $UserDialog->getId()];

        /** @var HistoryMessage[] $HistoryMessages */
        $HistoryMessages = HistoryMessage::model()->find($criteria);

        $ids = [];
        foreach ($HistoryMessages as $model) {
            if($model->user_receiver_id == Yii::app()->user->id && $model->receiver_view === false)
                $ids[] = $model->getPrimaryKey();
        }
        if(!empty($ids))
            HistoryMessage::readMessages($ids);

        Yii::app()->breadcrumbs
            ->addBreadcrumb('Профиль', Yii::app()->createUrl('/users/profile/index'))
            ->addBreadcrumb('Сообщения', Yii::app()->createUrl('/message/profile/index'))
            ->addBreadcrumb($UserDialog->getUsername());

        $this->controller->render('index', [
            'models' => $HistoryMessages,
            'Me' => $User,
            'UserDialog' => $UserDialog,
            'pages' => $Pages
        ]);
    }
}