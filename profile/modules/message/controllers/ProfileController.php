<?php
namespace profile\modules\message\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use profile\modules\message\components\MessageBaseController;
use Yii;

class ProfileController extends MessageBaseController
{
    public function filters() {
        return array(
            'ajaxOnly + get, add',
        );
    }

    public function beforeRender($view)
    {
        $result = parent::beforeRender($view);
        return $result;
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'index' => ['class' => 'profile\modules\message\controllers\actions\profile\IndexAction'],
            'get' => ['class' => 'profile\modules\message\controllers\actions\profile\GetAction'],
            'add' => ['class' => 'profile\modules\message\controllers\actions\profile\AddAction'],
            'view' => ['class' => 'profile\modules\message\controllers\actions\profile\ViewAction'],
            'message' => ['class' => 'profile\modules\message\controllers\actions\profile\MessageAction'],
		);
	}
}