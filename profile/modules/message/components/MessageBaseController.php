<?php
namespace profile\modules\message\components;
/**
 * Created by PhpStorm.
 * User: me
 * Date: 16.09.2014
 * Time: 14:09
 */

use profile\components\ProfileController;
use Yii;
use UserMessageList;
use User;
use CDbCriteria;
use CDbCacheDependency;
class MessageBaseController extends ProfileController
{
    /**
     * @var User[]
     */
    public $userMessageList = [];

    public function beforeAction($action)
    {
        if(!\Yii::app()->request->isAjaxRequest) {
            $criteria = new CDbCriteria();
            $criteria->scopes = ['own'];
            $criteria->with = [
                'dialogUser' => [
                    'with' => 'userProfile'
                ]
            ];

            $dependency = new CDbCacheDependency('SELECT count(*) FROM {{user_message_list}} where owner_user_id = :owner_user_id');
            $dependency->params = [':owner_user_id' => Yii::app()->user->id];
            $dependency->reuseDependentData = true;

            /** @var UserMessageList $model */
            foreach (UserMessageList::model()->cache(1000, $dependency)->findAll($criteria) as $model)
                $this->userMessageList[] = $model->dialogUser;
        }

        $result = parent::beforeAction($action);
        $this->registerAssets();

        return $result;
    }

    private function registerAssets()
    {
        Yii::app()->static->setWww()->registerCssFile('messages.css');
        Yii::app()->static->setOwn()
            ->registerScriptFile('messages.js')
            ->registerCssFile('messages.css');

        Yii::app()->static->setLibrary('history')
            ->registerScriptFile('jquery.history.js');
    }
} 