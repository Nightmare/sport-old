<?php
namespace profile\modules\filter\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use profile\modules\filter\components\FilterBaseController;
class GroupController extends FilterBaseController
{
    /**
     * Actions attached to this controller
     *
     * @return array
     */
    public function actions()
    {
        return array(
            'filter' => ['class' => 'common\modules\filter\controllers\actions\group\FilterAction'],
        );
    }
}