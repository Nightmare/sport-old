<?php
namespace profile\modules\users\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use \profile\components\ProfileController as BaseProfileController;
class ProfileController extends BaseProfileController
{
    public $layout = 'profile';

    public function filters() {
        return array(
            'ajaxOnly + image, update, panel',
        );
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'index' => ['class' => 'profile\modules\users\controllers\actions\profile\IndexAction'],
            'image' => ['class' => 'profile\modules\users\controllers\actions\profile\ImageAction'],
            'update' => ['class' => 'profile\modules\users\controllers\actions\profile\UpdateAction'],
            'panel' => ['class' => 'profile\modules\users\controllers\actions\profile\PanelAction'],
		);
	}
}