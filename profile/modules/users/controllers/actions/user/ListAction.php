<?php
namespace profile\modules\users\controllers\actions\user;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use User;
use CDbCacheDependency;

class ListAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $term = Yii::app()->request->getParam('term');

        $criteria = new \CDbCriteria();
        $criteria->addCondition('`t`.id != :id');
        $criteria->params = [':id' => Yii::app()->user->id];
        $criteria->with = ['userProfile'];
        $criteria->addSearchCondition('username', $term);

        $dependency = new CDbCacheDependency('SELECT AVG (update_at) FROM {{user}}');
        $dependency->reuseDependentData = true;

        /** @var User[] $Users */
        $Users = User::model()->cache(1000,$dependency)->findAll($criteria);

        $result = [];
        foreach ($Users as $User) {
            $result[] = [
                'id' => $User->getId(),
                'label' => $User->getUsername(),
                'value' => $User->getId(),
                'thumb_mini' => Yii::app()->static->imageLink($User->userProfile->getThumbsImageLink())
            ];
        }

        Yii::app()->ajax->addOther($result)->send();
    }
}