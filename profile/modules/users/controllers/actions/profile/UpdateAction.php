<?php
namespace profile\modules\users\controllers\actions\profile;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use User;

class UpdateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        /** @var User $User */
        $User = Yii::app()->user->getModel();
        $UserProfile = $User->userProfile;

        $User->scenario = 'updateAction';
        $UserProfile->scenario = 'updateAction';

        $postUser = Yii::app()->request->getPost('User');
        $postUserProfile = Yii::app()->request->getPost('UserProfile');
        if($postUser || $postUserProfile) {
            $t = Yii::app()->db->beginTransaction();
            try {
                if(isset($postUserProfile['birthday']))
                    $postUserProfile['birthday'] = strtotime($postUserProfile['birthday'].' 00:00:00');

                $User->setAttributes($postUser);
                if(isset($postUser['password']) && $postUser['password'] != '' && isset($postUser['newPassword']) && isset($postUser['passwordConfirm'])) {
                    $User->scenario = 'change_pass';
                    if(!$User->verifyPassword($postUser['password'])) {
                        Yii::app()->ajax->addErrors(Yii::t('errors', 'Неправельный текущий пароль'));
                        throw new \Exception();
                    }

                    if($postUser['newPassword'] != isset($postUser['passwordConfirm'])) {
                        Yii::app()->ajax->addErrors(Yii::t('errors', 'Пароли не совпадают'));
                        throw new \Exception();
                    }

                    $User->password = trim($postUser['newPassword']);
                }

                if(!$User->save()) {
                    Yii::app()->ajax->addErrors($User);
                    throw new \Exception();
                }

                $UserProfile->setAttributes($postUserProfile);
                if(!$UserProfile->save()) {
                    Yii::app()->ajax->addErrors($UserProfile);
                    throw new \Exception();
                }

                $t->commit();

                Yii::app()->ajax
                    ->runJS('clearForm')
                    ->addOther(['ok' => true]);

                Yii::app()->ajax->addMessage(Yii::t('validate', 'Данные успешно обновленны'));
            } catch (\Exception $ex) {
                $t->rollback();
            }

            Yii::app()->ajax->send();
        }
    }
}