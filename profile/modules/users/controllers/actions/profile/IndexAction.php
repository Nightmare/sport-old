<?php
namespace profile\modules\users\controllers\actions\profile;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use User;

class IndexAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $User = Yii::app()->user->getModel();

        $User->password = '';
        if(!empty($User->userProfile->birthday))
            $User->userProfile->birthday = date('d.m.Y', $User->userProfile->birthday);

        Yii::app()->breadcrumbs
            ->addBreadcrumb('Профиль');

        $this->controller->render('index', ['model' => $User, 'modelProfile' => $User->userProfile]);
    }
}