<?php
namespace profile\modules\users\controllers\actions\profile;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\components\CImageHandler;
use Yii;
use User;
use UserProfile;
use EasyImage;

class ImageAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $imageTemp = \CUploadedFile::getInstanceByName('filedata');
        if($imageTemp === null)
            return;

        $User = Yii::app()->user->getModel();

        /** @var UserProfile $UserProfile */
        $UserProfile = $User->userProfile;
        
        $t = Yii::app()->db->beginTransaction();
        try {
            $title = \UserProfile::generateName('avatar-'.$User->getUsername().'-'.rand(0, 1000));

            /** @var CImageHandler $image */
            $image = Yii::app()->ih->load($imageTemp->getTempName());
            $origin = sprintf('%s/%s.%s', UserProfile::generateAvatarPath(UserProfile::SIZE_TYPE_ORIGIN, true), $title, $image->getFormatType());

            $UserProfile->setImageName(sprintf('%s.%s', $title, $image->getFormatType()));
            if(!$UserProfile->save())
                throw new \Exception();

            $image->save($origin);
            foreach (UserProfile::getSizes() as $type => $info) {
                $image
                    ->reload()
                    ->resize($info['w'], $info['h'], false)
                    ->save(sprintf('%s/%s.%s', UserProfile::generateAvatarPath($type, true), $title, $image->getFormatType()));
            };

            $t->commit();
            Yii::app()->ajax->addOther([
                'ok' => true,
                'link' => Yii::app()->static->imageLink(sprintf(
                        '%s/%s.%s',
                        UserProfile::generateAvatarPath(UserProfile::SIZE_TYPE_MAX),
                        $title,
                        $image->getFormatType())
                ),
            ]);

        } catch (\Exception $ex) {
            $t->rollback();

            Yii::app()->ajax->addOther([
                'debug' => [
                    $ex->getMessage()
                ]
            ]);
        }

        Yii::app()->ajax->send();
    }
}