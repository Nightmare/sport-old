<?php
namespace profile\modules\users\controllers\actions\profile;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use UserProfile;

class PanelAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $status = Yii::app()->request->getPost('status', true);
        if(!$status) $status = false;
        else $status = true;

        \Yii::app()->user->setState('panelStatus', $status);

        $User = Yii::app()->user->getModel();

        /** @var UserProfile $UserProfile */
        $UserProfile = $User->userProfile;
        $UserProfile->setPanelStatus($status);
        $UserProfile->save(false);

        Yii::app()->ajax->send();
    }
}