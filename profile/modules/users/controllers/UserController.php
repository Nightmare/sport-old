<?php
namespace profile\modules\users\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use \profile\components\ProfileController as BaseProfileController;
class UserController extends BaseProfileController
{
    public $layout = 'profile';

    public function filters() {
        return array(
            'ajaxOnly + list',
        );
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'list' => ['class' => 'profile\modules\users\controllers\actions\user\ListAction'],
		);
	}
}