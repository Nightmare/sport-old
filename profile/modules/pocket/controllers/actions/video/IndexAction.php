<?php
namespace profile\modules\pocket\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCacheDependency;

class IndexAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->scopes = ['own', 'enable'];
        $criteria->with = ['video' => ['scopes' => ['enable'], 'with' => ['channel']]];

        $dependency = new CDbCacheDependency('SELECT MAX(pv.update_at + gv.update_at) FROM {{pocket_video}} pv, {{gallery_video}} gv where pv.user_id = :user_id and pv.video_id = gv.id');
        $dependency->params = [':user_id' => Yii::app()->user->id];
        $dependency->reuseDependentData = true;

        $pages = new \CPagination(\PocketVideo::model()->cache(10000, $dependency)->count($criteria));
        $pages->applyLimit($criteria);

        /** @var \PocketVideo $PocketList */
        $PocketList = \PocketVideo::model()->cache(10000, $dependency)->findAll($criteria);

        $params = ['models' => $PocketList, 'pages' => $pages];
        if(Yii::app()->request->isAjaxRequest) {
            Yii::app()->ajax->addOther([
                'ok' => true,
                'page' => $pages->getCurrentPage(),
                'selector' => '#page_content',
                'view' => $this->controller->renderPartial('_index', $params, true),
            ])->send();
        } else {

            Yii::app()->static->setLibrary('moment')
                ->registerScriptFile('moment.min.js');
            Yii::app()->static->setLibrary('timepicker')
                ->registerScriptFile('script.min.js')
                ->registerCssFile('style.min.css');
            Yii::app()->static->setOwn()
                ->registerScriptFile('pocket.js');
            Yii::app()->breadcrumbs
                ->addBreadcrumb('Профиль', Yii::app()->createUrl('/users/profile/index'))
                ->addBreadcrumb('Карман - видеозаписи');

            $this->controller->render('index', $params);
        }
    }
}