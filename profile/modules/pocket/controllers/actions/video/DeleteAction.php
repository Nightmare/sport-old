<?php
namespace profile\modules\pocket\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use User;

class DeleteAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->scopes = ['own'];
        $criteria->with = ['pocketVideoInfos' => ['scopes' => 'enable']];
        $criteria->addCondition('id = :id');
        $criteria->params = [':id' => Yii::app()->request->getParam('pocket_id')];
        /** @var \PocketVideo $PocketVideo */
        $PocketVideo = \PocketVideo::model()->find($criteria);
        if(!$PocketVideo)
            \MException::ShowError(Yii::t('error', 'Метка не найдена'));

        $t = Yii::app()->db->beginTransaction();
        try {
            foreach ($PocketVideo->pocketVideoInfos as $Item)
                $Item->setIsDeleted(1)->save(false);

            if(!$PocketVideo->setIsDeleted(1)->save())
                Yii::app()->ajax->addErrors($PocketVideo);
            else
                Yii::app()->ajax->addMessage(Yii::t('errors', 'Видео удалено из кармана'));

            $t->commit();

        } catch (\Exception $ex) {
            $t->rollback();
        }

        Yii::app()->ajax
            ->runJS('updatePage', [Yii::app()->createUrl('/pocket/video/index')])
            ->addOther(['ok' => true])->send();
    }
}