<?php
namespace profile\modules\pocket\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use User;

class ManageAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->scopes = ['own'];
        $criteria->with = ['pocketVideoInfos' => ['scopes' => 'enable']];
        $criteria->addCondition('`t`.id = :id');
        $criteria->params = [':id' => Yii::app()->request->getParam('pocket_id')];
        /** @var \PocketVideo $Pocket */
        $Pocket = \PocketVideo::model()->find($criteria);
        if(!$Pocket)
            \MException::ShowError(Yii::t('error', 'Видео в кармане не найдено!'));

        $post = Yii::app()->request->getParam('PocketVideo', array());

        /** @var \PocketVideoInfo[] $PocketInfo */
        $PocketInfo = [];
        if(!empty($post) || Yii::app()->request->getParam('form')) {
            foreach ($Pocket->pocketVideoInfos as $Info)
                $PocketInfo[$Info->getId()] = $Info;

            foreach ($post as $item) {
                try {
                    if(isset($item['id']) && isset($PocketInfo[$item['id']])) {
                        $PocketVideoInfo = $PocketInfo[$item['id']];
                        unset($PocketInfo[$item['id']]);
                    } else
                        $PocketVideoInfo = new \PocketVideoInfo();

                    $PocketVideoInfo->setPocketId($Pocket->getId())
                        ->setDescription($item['description'])
                        ->setStartTime($item['time'], true);
                    if(!$PocketVideoInfo->createAction())
                        Yii::app()->ajax->addErrors($PocketVideoInfo);
                } catch (\Exception $ex) {

                }
            }

            foreach ($PocketInfo as $model) {
                $model->setIsDeleted(true);
                $model->save();
            }

            if(!Yii::app()->ajax->hasErrors()) {
                Yii::app()->ajax->addMessage(Yii::t('labels', 'Метки обновлены'));
                Yii::app()->ajax->addOther([
                    'ok' => true,
                    'runJS' => [
                        'functionName' => 'updatePage',
                        'functionParams' => [Yii::app()->createUrl('/pocket/video/index')]
                    ]
                ]);
            }
        } else {
            Yii::app()->ajax
                ->addReplace('_manage', '#customModal #replacement', ['model' => $Pocket])
                ->runJS('openCustom')
                ->runJS('setTimepicker', ['#customModal #replacement'])
                ->addOther(['ok' => true]);
        }

        Yii::app()->ajax->send();
    }
}