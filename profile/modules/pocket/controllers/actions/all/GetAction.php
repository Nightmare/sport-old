<?php
namespace profile\modules\pocket\controllers\actions\all;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCacheDependency;
use PocketImage;
use PocketVideo;

class GetAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $dependency = new CDbCacheDependency('SELECT MAX(pi.update_at + gi.update_at) FROM {{pocket_image}} pi, {{gallery_image}} gi where pi.user_id = :user_id and pi.image_id = gi.id');
        $dependency->params = [':user_id' => Yii::app()->user->id];
        $dependency->reuseDependentData = true;

        $criteria = new \CDbCriteria();
        $criteria->with = ['image' => ['scopes' => 'enable']];
        $criteria->limit = 20;
        $criteria->order = '`t`.update_at desc';
        $criteria->scopes = ['enable', 'own'];
        /** @var PocketImage $PocketImage */
        $PocketImage = PocketImage::model()->cache(10000, $dependency)->findAll($criteria);

        $dependency = new CDbCacheDependency('SELECT MAX(pv.update_at + gv.update_at) FROM {{pocket_video}} pv, {{gallery_video}} gv where pv.user_id = :user_id and pv.video_id = gv.id');
        $dependency->params = [':user_id' => Yii::app()->user->id];
        $dependency->reuseDependentData = true;

        $criteria->with = ['video' => ['scopes' => 'enable', 'with' => ['channel']]];
        /** @var PocketVideo $PocketVideo */
        $PocketVideo = PocketVideo::model()->cache(10000, $dependency)->findAll($criteria);

        $params = [
            'images' => $PocketImage,
            'videos' => $PocketVideo,
        ];

        Yii::app()->ajax
            ->addReplace('_pocket', '.panel [data-tab-in="pocket"]', $params)
            ->send();
    }
}