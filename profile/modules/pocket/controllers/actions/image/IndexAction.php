<?php
namespace profile\modules\pocket\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCacheDependency;

class IndexAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->scopes = ['own', 'enable'];
        $criteria->with = ['image' => ['scopes' => ['enable']]];

        $dependency = new CDbCacheDependency('SELECT MAX(pi.update_at + gi.update_at) FROM {{pocket_image}} pi, {{gallery_image}} gi where pi.user_id = :user_id and pi.image_id = gi.id');
        $dependency->params = [':user_id' => Yii::app()->user->id];
        $dependency->reuseDependentData = true;

        $pages = new \CPagination(\PocketImage::model()->cache(10000, $dependency)->count($criteria));
        $pages->applyLimit($criteria);

        /** @var \PocketImage $PocketList */
        $PocketList = \PocketImage::model()->cache(10000, $dependency)->findAll($criteria);

        $params = ['models' => $PocketList, 'pages' => $pages];
        if(Yii::app()->request->isAjaxRequest) {
            Yii::app()->ajax->addOther([
                'ok' => true,
                'page' => $pages->getCurrentPage(),
                'selector' => '#page_content',
                'view' => $this->controller->renderPartial('_index', $params, true),
            ])->send();
        } else {
            Yii::app()->breadcrumbs
                ->addBreadcrumb('Профиль', Yii::app()->createUrl('/users/profile/index'))
                ->addBreadcrumb('Карман - фотографии');

            $this->controller->render('index', $params);
        }
    }
}