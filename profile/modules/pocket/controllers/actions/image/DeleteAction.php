<?php
namespace profile\modules\pocket\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use User;

class DeleteAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->scopes = ['own'];
        $criteria->addCondition('id = :id');
        $criteria->params = [':id' => Yii::app()->request->getParam('pocket_id')];
        /** @var \PocketImage $PocketImage */
        $PocketImage = \PocketImage::model()->find($criteria);
        if($PocketImage && !$PocketImage->setIsDeleted(1)->save())
            Yii::app()->ajax->addErrors($PocketImage);
        else
            Yii::app()->ajax->addMessage(Yii::t('errors', 'Фото удалено из кармана'));

        Yii::app()->ajax
            ->runJS('updatePage', [Yii::app()->createUrl('/pocket/image/index')])
            ->addOther(['ok' => true])->send();
    }
}