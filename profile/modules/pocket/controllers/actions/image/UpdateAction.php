<?php
namespace profile\modules\pocket\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use User;

class UpdateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->scopes = ['own'];
        $criteria->addCondition('id = :id');
        $criteria->params = [':id' => Yii::app()->request->getParam('pocket_id')];
        /** @var \PocketImage $Pocket */
        $Pocket = \PocketImage::model()->find($criteria);

        $post = Yii::app()->request->getParam('PocketImage');

        if(!empty($post)) {
            $Pocket->setAttributes($post);

            if(!$Pocket->updateAction()) {
                Yii::app()->ajax->addErrors($Pocket);
            } else {
                Yii::app()->ajax->addMessage(Yii::t('labels', 'Метка обновлена'));
                Yii::app()->ajax->addOther([
                    'ok' => true,
                    'runJS' => [
                        'functionName' => 'updatePage',
                        'functionParams' => [Yii::app()->createUrl('/pocket/image/index')]
                    ]
                ]);
            }
        } else {
            Yii::app()->ajax
                ->addReplace('_form', '#customModal #replacement', ['model' => $Pocket])
                ->runJS('openCustom')
                ->addOther(['ok' => true]);
        }

        Yii::app()->ajax->send();
    }
}