<?php
namespace profile\modules\pocket\controllers;
use profile\modules\pocket\components\PocketBaseController;

/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

class ImageController extends PocketBaseController
{
    public $layout = 'profile';

	public function filters() {
		return array(
			'ajaxOnly + update, delete',
		);
	}

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'index' => ['class' => 'profile\modules\pocket\controllers\actions\image\IndexAction'],
            'update' => ['class' => 'profile\modules\pocket\controllers\actions\image\UpdateAction'],
            'delete' => ['class' => 'profile\modules\pocket\controllers\actions\image\DeleteAction'],
		);
	}
}