<?php
namespace profile\modules\pocket\controllers;
use profile\modules\pocket\components\PocketBaseController;

/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

class AllController extends PocketBaseController
{
    public $layout = 'profile';

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'get' => ['class' => 'profile\modules\pocket\controllers\actions\all\GetAction'],
		);
	}
}