<?php
namespace profile\modules\saver\controllers;
use profile\modules\saver\components\SaverBaseController;

/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

class AllController extends SaverBaseController
{
    public $layout = 'profile';

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'get' => ['class' => 'profile\modules\saver\controllers\actions\all\GetAction'],
		);
	}
}