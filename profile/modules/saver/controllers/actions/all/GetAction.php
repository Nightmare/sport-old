<?php
namespace profile\modules\saver\controllers\actions\all;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCacheDependency;
use SaverImage;
use SaverVideo;

class GetAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $dependency = new CDbCacheDependency('SELECT MAX(si.view_at + gi.update_at) FROM {{saver_image}} si, {{gallery_image}} gi where si.user_id = :user_id and si.image_id = gi.id');
        $dependency->params = [':user_id' => Yii::app()->user->id];
        $dependency->reuseDependentData = true;

        $criteria = new \CDbCriteria();
        $criteria->with = ['image' => ['scopes' => 'enable']];
        $criteria->scopes = ['own'];
        $criteria->limit = 20;
        $criteria->order = '`t`.view_at desc';
        /** @var SaverImage $SaverImage */
        $SaverImage = SaverImage::model()->cache(10000, $dependency)->findAll($criteria);

        $dependency = new CDbCacheDependency('SELECT MAX(sv.view_at + gv.update_at) FROM {{saver_video}} sv, {{gallery_video}} gv where sv.user_id = :user_id and sv.video_id = gv.id');
        $dependency->params = [':user_id' => Yii::app()->user->id];
        $dependency->reuseDependentData = true;

        $criteria->with = ['video' => ['scopes' => 'enable', 'with' => ['channel']]];
        /** @var SaverVideo $SaverVideo */
        $SaverVideo = SaverVideo::model()->cache(10000, $dependency)->findAll($criteria);

        Yii::app()->ajax
            ->addReplace('_saver', '.panel [data-tab-in="saver"]', ['images' => $SaverImage, 'videos' => $SaverVideo])
            ->send();
    }
}