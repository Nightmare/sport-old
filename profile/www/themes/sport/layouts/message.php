<?php
/**
 * @var $this \profile\modules\message\components\MessageBaseController
 * @var string $content
 */
?>
<?php $this->beginContent('profile.www.themes.'.Yii::app()->theme->name.'.layouts.template'); ?>
<?php
$autocompleteConfig = [
    'name' => 'user-name', // атрибут модели
    'source' => Yii::app()->createUrl('/users/user/list'),
    'options' => [
        'minLength'=>'2',
        'showAnim'=>'fold',
        'select' => 'js: function(event, ui) {
            this.value = ui.item.label;
            updateUserMessageList(ui.item);
            return false;
        }',
    ],
    'htmlOptions' => [
        'maxlength' => 50,
        'placeholder' => 'Найти пользователя…'
    ],
];
?>
<div class="middle" id="middle">
    <div class="container">
        <?= $content; ?>
    </div>
    <aside class="left-sidebar">
        <div class="box box-left">
            <form action="" method="post" id="message_user_search" class="form-user-search">
                <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', $autocompleteConfig); ?>
                <input type="submit" value="" class="btn search-friend-list" name="user-friends" title="title#1">
                <input type="submit" value="" class="btn search-user-list-online" name="user-online" title="title#2">
            </form>
            <ul class="user-list" id="user-list">
                <?php foreach ($this->userMessageList as $model): ?>
                    <li class="user-item" data-username="<?= $model->getUsername(); ?>">
                        <img src="<?= $model->userProfile->getAvatar(UserProfile::SIZE_TYPE_MIN); ?>" alt="<?= $model->getUsername(); ?>" class="user-item-photo">
                        <div class="user-item-info user-item-info-arrow">
                            <a href="javascript:void(0);" class="username gender-<?= Yii::app()->user->getGender(); ?>"><?= $model->getUsername(); ?></a>
                            <span class="user-item-status <?= $model->getOnline(); ?>"><?= $model->getOnline(); ?></span>
                            <div class="user-item-action-list">
                                <a href="javascript:void(0);" data-id="<?= $model->getId() ?>" class="friends" title="<?= Yii::t('labels', 'Добавить в друзья'); ?>"></a>
                                <a href="javascript:void(0);" data-id="<?= $model->getId() ?>" class="message" title="<?= Yii::t('labels', 'Отправить сообщение'); ?>"></a>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
            <!--<a class="btn btn-user-list-more" href="javascript:void(0)">Показать ещё пользователей</a>-->
        </div>
    </aside>
    <aside class="right-sidebar">
        <div class="box">
            <?php
            $this->widget('zii.widgets.CMenu', [
                'htmlOptions' => ['class' => 'user-menu'],
                'items' => [
                    [
                        'label' => Yii::t('labels' , 'личные данные'),
                        'itemOptions' => ['class' => 'profile'],
                        'url' => ['/users/profile/index']
                    ],
                    [
                        'label' => Yii::t('labels', 'галерея'),
                        'itemOptions' => ['class' => 'gallery'],
                        'url' => 'javascript:void(0);',
                        'items' => [
                            [
                                'label' => Yii::t('labels' , 'фото'),
                                'itemOptions' => ['class' => 'photo'],
                                'url' => ['/gallery/image/index']
                            ],
                            [
                                'label' => Yii::t('labels' , 'видео'),
                                'itemOptions' => ['class' => 'video'],
                                'url' => ['/gallery/video/index']
                            ]
                        ]
                    ],
                    [
                        'label' => Yii::t('labels' , 'карман'),
                        'itemOptions' => ['class' => 'pocket'],
                        'url' => 'javascript:void(0);',
                        'items' => [
                            [
                                'label' => Yii::t('labels' , 'фото'),
                                'itemOptions' => ['class' => 'photo'],
                                'url' => ['/pocket/image/index']
                            ],
                            [
                                'label' => Yii::t('labels' , 'видео'),
                                'itemOptions' => ['class' => 'video'],
                                'url' => ['/pocket/video/index']
                            ]
                        ]
                    ],
                    [
                        'label' => Yii::t('labels' , 'сообщения'),
                        'itemOptions' => ['class' => 'message'],
                        'url' => ['/message/profile/index']
                    ],
                    [
                        'label' => Yii::t('labels' , 'темы'),
                        'itemOptions' => ['class' => 'theme'],
                        'url' => ['/post/profile/index']
                    ]
                ]
            ]);
            ?>
            <?php if($this->showUploadImage): ?>
                <?php $this->widget('\common\extensions\fileapi\FileApi', [
                    'url' => Yii::app()->createUrl('/gallery/image/upload'),
                    'view' => 'image',
                    'is_crop' => true,
                    'preview_height' => Yii::app()->params['sizes']['image']['preview']['height'],
                    'preview_width' => Yii::app()->params['sizes']['image']['preview']['width'],
                ]); ?>
            <?php endif; ?>

            <?php if($this->showAddVideo): ?>
                <?php $this->widget('\common\modules\gallery\widgets\video\FormWidget'); ?>
            <?php endif; ?>
        </div>
    </aside>
</div>
<?php $this->endContent(); ?>