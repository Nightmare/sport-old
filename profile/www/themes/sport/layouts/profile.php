<?php
/**
 * @var $this \profile\components\ProfileController
 * @var string $content
 */
?>
<?php $this->beginContent('profile.www.themes.'.Yii::app()->theme->name.'.layouts.template'); ?>
<div class="middle no-left-side profile" id="middle">
    <div class="container">
        <?= $content; ?>
    </div>
    <aside class="right-sidebar">
        <div class="box">
            <?php
            $this->widget('zii.widgets.CMenu', [
                'htmlOptions' => ['class' => 'user-menu'],
                'items' => [
                    [
                        'label' => Yii::t('labels' , 'личные данные'),
                        'itemOptions' => ['class' => 'profile'],
                        'url' => ['/users/profile/index']
                    ],
                    [
                        'label' => Yii::t('labels', 'галерея'),
                        'itemOptions' => ['class' => 'gallery'],
                        'url' => 'javascript:void(0);',
                        'items' => [
                            [
                                'label' => Yii::t('labels' , 'фото'),
                                'itemOptions' => ['class' => 'photo'],
                                'url' => ['/gallery/image/index']
                            ],
                            [
                                'label' => Yii::t('labels' , 'видео'),
                                'itemOptions' => ['class' => 'video'],
                                'url' => ['/gallery/video/index']
                            ]
                        ]
                    ],
                    [
                        'label' => Yii::t('labels' , 'карман'),
                        'itemOptions' => ['class' => 'pocket'],
                        'url' => 'javascript:void(0);',
                        'items' => [
                            [
                                'label' => Yii::t('labels' , 'фото'),
                                'itemOptions' => ['class' => 'photo'],
                                'url' => ['/pocket/image/index']
                            ],
                            [
                                'label' => Yii::t('labels' , 'видео'),
                                'itemOptions' => ['class' => 'video'],
                                'url' => ['/pocket/video/index']
                            ]
                        ]
                    ],
                    [
                        'label' => Yii::t('labels' , 'сообщения'),
                        'itemOptions' => ['class' => 'message'],
                        'url' => ['/message/profile/index']
                    ],
                    [
                        'label' => Yii::t('labels' , 'темы'),
                        'itemOptions' => ['class' => 'theme'],
                        'url' => ['/post/profile/index']
                    ]
                ]
            ]);
            ?>
            <?php if($this->showUploadImage): ?>
                <?php $this->widget('\common\extensions\fileapi\FileApi', [
                    'url' => Yii::app()->createUrl('/gallery/image/upload'),
                    'view' => 'image',
                    'is_crop' => true,
                    'preview_height' => Yii::app()->params['sizes']['image']['preview']['height'],
                    'preview_width' => Yii::app()->params['sizes']['image']['preview']['width'],
                ]); ?>
            <?php endif; ?>

            <?php if($this->showAddVideo): ?>
                <?php $this->widget('\common\modules\gallery\widgets\video\FormWidget'); ?>
            <?php endif; ?>
        </div>
    </aside>
</div>
<?php $this->endContent(); ?>