<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 07.10.2014
 * Time: 0:18
 *
 * @var \profile\modules\message\components\MessageBaseController $this
 * @var HistoryMessage[] $models
 * @var User $Me
 * @var User $UserDialog
 * @var EMongoPagination $pages
 */ ?>
<ul class="comment-list" id="message_list">
    <?php if(empty($models)): ?>
    <li class="empty_text">
        Сообщения отсутствуют, выбирите или найдите пользователя в левом меню.
    </li>
    <?php endif; ?>
    <li class="pagerWrapper">
        <?php $this->widget('\common\extensions\pagination\Pagination', [
            'pages' => $pages,
            'cssFile' => Yii::app()->static->setOwn()->getLink('css/paging.css')
        ]); ?>
    </li>
    <?php foreach ($models as $model): ?>
        <?php
        $user = $Me;
        if($model->getUserSenderId() == $UserDialog->getId())
            $user = $UserDialog;
        ?>
        <?= $this->renderPartial('_item', ['model' => $model, 'user' => $user], true); ?>
    <?php endforeach; ?>
    <li class="pagerWrapper">
        <?php $this->widget('\common\extensions\pagination\Pagination', [
            'pages' => $pages,
            'cssFile' => Yii::app()->static->setOwn()->getLink('css/paging.css')
        ]); ?>
    </li>
</ul>