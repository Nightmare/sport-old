<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 06.10.2014
 * Time: 23:48
 *
 * @var HistoryMessage $model
 * @var User $user
 */ ?>

<li class="comment-item">
    <img src="<?= $user->userProfile->getAvatar(UserProfile::SIZE_TYPE_MIN); ?>" alt="<?= $user->getUsername() ?>">
    <div class="comment-info">
        <a href="javascript:void(0);" class="username"><?= $user->getUsername() ?></a>
        <time class="time" datetime="<?= date('Y-m-d H:i:s', $model->getCreateAt()); ?>"><?= date('H:i d.m.Y', $model->getCreateAt()); ?></time>

        <div class="text"><?= $model->getBody() ?></div>
    </div>
</li>