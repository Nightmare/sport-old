<?php
/**
 * @var \profile\modules\message\components\MessageBaseController $this
 * @var HistoryMessage[] $models
 * @var User $Me
 * @var User $UserDialog
 * @var EMongoPagination $pages
 */
?>
<main class="content">
    <div class="box" id="message_box">
        <?= $this->renderPartial('_list', [
            'models' => $models,
            'Me' => $Me,
            'UserDialog' => $UserDialog,
            'pages' => $pages
        ]); ?>
    </div>
</main>
<script>
    var messageGet = '<?= Yii::app()->createUrl('/message/profile/get'); ?>';
    var messageAdd = '<?= Yii::app()->createUrl('/message/profile/add', ['id' => 'null']); ?>';
    var messageHistory = '<?= Yii::app()->createUrl('/message/profile/view', ['id' => 'null']); ?>';
    $(function(){
        <?php if($UserDialog !== null): ?>
            internalDialog(<?= $UserDialog->getId(); ?>);
        <?php endif; ?>
    });
</script>