<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 23.01.2015
 * Time: 0:35
 *
 * @var SaverImage[] $models
 */ ?>
<div id="saver-slider-image" class="panel-slider-container-wrapper">
    <a href="#" class="panel-slider-arrow prev"></a>
    <a href="#" class="panel-slider-arrow next"></a>
    <div class="panel-slider-container swiper-container">
        <!--<div class="panel-slider-pagination"></div>-->
        <ul class="swiper-wrapper image-list">
            <?php foreach($models as $model): ?>
                <li class="swiper-slide">
                    <div class="image-item image-item-adv">
                        <div class="image-item-inner standard">
                            <a href="<?= Yii::app()->createUrl('/gallery/image/view', [
                                'image_dir' => $model->image->getDir(),
                                'conference' => $model->image->getConferenceString()
                            ]); ?>">
                                <img src="<?= Yii::app()->static->imageLink($model->image->getMinImage()) ?>" alt=""/>
                            </a>
                        </div>
                        <div class="image-item-bottom">
                            <div class="title">
                                <?= $model->image->getTitle() ?>
                            </div>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>