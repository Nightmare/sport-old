<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 16.09.2014
 * Time: 14:48
 *
 * @var $this \common\components\Controller
 * @var GalleryVideo[] $models
 * @var CPagination $pages
 */ ?>
<main class="content video">
    <div class="box" id="page_content">
        <?= $this->renderPartial('_index', ['models' => $models, 'pages' => $pages], true) ?>
    </div>
</main>