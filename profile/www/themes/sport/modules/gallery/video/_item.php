<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 02.10.2014
 * Time: 14:43
 *
 * @var string $logo
 * @var GalleryVideo $model
 */ ?>

<li class="video-item video-item-adv">
    <div class="video-item-title">
        <?= $model->getTitle(); ?>
    </div>
    <div class="video-item-inner standard">
        <a href="<?= Yii::app()->createUrl('/gallery/video/view', ['video_dir' => $model->getDir()]) ?>">
            <img src="<?= Yii::app()->static->imageLink($model->getMinImage()); ?>" alt="">
            <span class="time-line"><?= $model->getDuration(true); ?></span>
            <span class="brand"><img src="http://static.sport.loc/www/img/video/adidas.jpg" alt=""></span>
        </a>
    </div>
    <a href="<?= Yii::app()->createUrl('/gallery/video/view', ['video_dir' => $model->getDir(), 'conference' => $model->getConferenceString()]) ?>" class="video-item-bottom">
        <span class="video-item-watch-site-lite"><?= $model->getViewCount() ?></span>
        <span class="video-item-watch-youtube"><?= $model->getHostView() ?></span>
    </a>
</li>