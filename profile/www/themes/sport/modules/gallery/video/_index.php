<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 03.10.2014
 * Time: 15:13
 *
 * @var GalleryVideo $models
 * @var Controller $this
 * @var CPagination $pages
 */ ?>

<ul class="video-list bottom-margin side-margin">
    <?php foreach($models as $model): ?>
        <?= $this->renderPartial('_item', ['model' => $model], true); ?>
    <?php endforeach; ?>
</ul>
<div class="pagerWrapper">
    <?php $this->widget('\common\extensions\pagination\Pagination', [
        'pages' => $pages,
        'cssFile' => Yii::app()->static->setOwn()->getLink('css/paging.css')
    ]); ?>
</div>