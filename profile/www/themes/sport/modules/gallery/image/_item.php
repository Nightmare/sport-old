<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 02.10.2014
 * Time: 14:43
 *
 * @var string $logo
 * @var GalleryImage $model
 */ ?>

<li class="image-item image-item-adv">
    <div class="image-item-title clearfix">
        <div class="control">
            <span
                class="glyphicon glyphicon-pencil pointer"
                data-type="ajax"
                data-link="<?= Yii::app()->createUrl('/gallery/image/update', ['image_dir' => $model->getDir()]) ?>"
                data-title="<?= Yii::t('labels', 'Редактировать') ?>"
                data-toggle="tooltip"></span>
            <span
                class="glyphicon glyphicon-remove pointer"
                data-type="ajax"
                data-link="<?= Yii::app()->createUrl('/gallery/image/delete', ['image_dir' => $model->getDir()]) ?>"
                data-title="<?= Yii::t('labels', 'Удалить') ?>"
                data-toggle="tooltip"></span>
        </div>
    </div>
    <div class="image-item-inner standard">
        <a href="<?= Yii::app()->createUrl('/gallery/image/view', ['image_dir' => $model->getDir()]); ?>">
            <img class="preview" src="<?= Yii::app()->static->imageLink($model->getMinImage()); ?>" alt="">
            <span class="brand"><img src="<?= Yii::app()->static->imageLink('www/img/video/adidas.jpg'); ?>" alt=""></span>
        </a>
    </div>
    <div class="image-item-bottom">
        <div class="title">
            <?= CHtml::link($model->getTitle(), Yii::app()->createUrl('/gallery/image/view', ['image_dir' => $model->getDir(), 'conference' => $model->getConferenceString()])); ?>
        </div>
    </div>
</li>