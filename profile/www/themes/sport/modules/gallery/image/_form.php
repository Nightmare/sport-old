<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 02.10.2014
 * Time: 0:00
 *
 * @var GalleryImage $model
 * @var Controller $this
 * @var array $muscle
 * @var array $simulator
 * @var Items $itemMongo
 * @var string $image
 * @var string $min
 * @var string $origin
 */ ?>
<div class="modal-dialog form" id="replacement">
    <?php /** @var TbActiveForm $form */
    $form = $this->beginWidget(
        '\common\widgets\booster\ActiveForm',
        [
            'id' => 'image_form',
            'action' => $model->isNewRecord ? Yii::app()->createUrl('/gallery/image/create') : Yii::app()->createUrl('/gallery/image/update', ['image_dir' => $model->getDir()]),
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            'htmlOptions' => [
                'class' => 'ajax'
            ],
            'clientOptions' => [
                'validateOnChange' => false
            ]
        ]
    ); ?>
    <input type="hidden" class="field" name="min" value="<?= $min; ?>">
    <input type="hidden" class="field" name="origin" value="<?= $origin; ?>">
    <div class="modal-content" style="width: 700px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel"><?= Yii::t('labels', 'Информация'); ?></h4>
        </div>
        <div class="modal-body">
            <table>
                <tr>
                    <td>
                        <ul>
                            <li>
                                <?= $form->textFieldGroup(
                                    $model,
                                    'title',
                                    [
                                        'wrapperHtmlOptions' => ['class' => 'col-sm-8'],
                                        'widgetOptions' => [
                                            'htmlOptions' => [
                                                'class' => 'field',
                                                'placeholder' => '',
                                            ],
                                        ],
                                        'labelClass' => 'col-sm-3',
                                    ]
                                ); ?>
                            </li>
                            <li>
                                <?= $form->textAreaGroup(
                                    $model,
                                    'description',
                                    [
                                        'wrapperHtmlOptions' => ['class' => 'col-sm-8'],
                                        'widgetOptions' => [
                                            'htmlOptions' => [
                                                'class' => 'field',
                                                'placeholder' => '',
                                            ],
                                        ],
                                        'labelClass' => 'col-sm-3',
                                    ]
                                ); ?>
                            </li>
                            <li>
                                <?= $form->dropDownListGroup(
                                    $model,
                                    'image_filter_group_id',
                                    [
                                        'wrapperHtmlOptions' => ['class' => 'col-sm-8'],
                                        'widgetOptions' => [
                                            'data' => CMap::mergeArray(['Выберите группу'], CHtml::listData(FilterGroup::model()->enable()->findAll(), 'id', 'title')),
                                            'htmlOptions' => [
                                                'data-group-filter-link' => Yii::app()->createUrl('/filter/group/filter'),
                                                'class' => 'groupSelect'
                                            ]
                                        ],
                                        'labelOptions' => ['label' => '']
                                    ]
                                ); ?>
                            </li>
                            <li>
                                <?= $form->dropDownListGroup(
                                    $model,
                                    'image_filter_id',
                                    [
                                        'wrapperHtmlOptions' => ['class' => 'col-sm-8'],
                                        'widgetOptions' => [
                                            'data' => [],
                                            'htmlOptions' => ['class' => 'field filterSelect']
                                        ],
                                        'labelOptions' => ['label' => '']
                                    ]
                                ); ?>
                                <div class="buttons" style="display: inline-block;">
                                    <?= CHtml::link(
                                        '+',
                                        'javascript:void(0);',
                                        [
                                            'class' => 'btn btn-success btn-sm sq',
                                            'id' => 'media-filter-add',
                                            'data-block-out' => 'media-assign-items'
                                        ]
                                    ); ?>
                                </div>
                            </li>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <li>
                                <?= CHtml::image(Yii::app()->static->imageLink($image), $model->getTitle()); ?>
                            </li>
                            <li>
                                <ul id="media-assign-items">
                                    <?php foreach ($model->getFilterIds(true) as $filter_id): ?>
                                        <?php $_item = Yii::app()->categories->getFilter($filter_id); if($_item === null) continue; ?>
                                        <li class="item" data-id="filter_<?= $_item->getId(); ?>" data-type="filter">
                                            <span class="label label-default"><?= $_item->getTitle(); ?> <span class="media-filter-delete glyphicon glyphicon-remove delete pointer"></span></span>
                                            <input type="hidden" value="<?= $_item->getId(); ?>" name="filter[<?= $_item->getId(); ?>]" class="field">
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
        <div class="modal-footer buttons">
            <button type="button" class="btn btn-default sq" data-dismiss="modal"><?= Yii::t('labels', 'Отмена') ?></button>
            <button type="button" data-modal-selector="#customModal" class="btn btn-primary sq" data-submit="ajax" data-for="image_form"><?= Yii::t('labels', 'Сохранить') ?></button>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>