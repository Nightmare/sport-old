<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 23.01.2015
 * Time: 0:35
 *
 * @var PocketImage[] $images
 * @var PocketVideo[] $videos
 * @var \profile\components\ProfileController $this
 */ ?>
<div class="tab_content" data-tab-in="pocket">
    <div data-type="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible">
        <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
            <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active">
                <a href="javascript:void(0)" data-tab-for="pocket-video-tab" class="ui-tabs-anchor">Видео</a>
            </li>
            <li class="ui-state-default ui-corner-top">
                <a href="javascript:void(0)" data-tab-for="pocket-image-tab" class="ui-tabs-anchor">Фото</a>
            </li>
        </ul>
        <div data-tab-in="pocket-video-tab" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <?php if(!empty($videos)): ?>
                <?= $this->renderPartial('_video', ['models' => $videos], true) ?>
            <?php else: ?>
                <i>Пусто</i>
            <?php endif; ?>
        </div>
        <div data-tab-in="pocket-image-tab" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <?php if(!empty($images)): ?>
                <?= $this->renderPartial('_image', ['models' => $images], true) ?>
            <?php else: ?>
                <i>Пусто</i>
            <?php endif; ?>
        </div>
    </div>
</div>