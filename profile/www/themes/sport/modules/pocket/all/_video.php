<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 23.01.2015
 * Time: 0:35
 *
 * @var SaverVideo[] $models
 */ ?>
<div id="pocket-slider-video" class="panel-slider-container-wrapper">
    <a href="#" class="panel-slider-arrow prev"></a>
    <a href="#" class="panel-slider-arrow next"></a>
    <div class="panel-slider-container swiper-container">
        <!--<div class="panel-slider-pagination"></div>-->
        <ul class="swiper-wrapper video-list">
            <?php foreach($models as $model): ?>
                <li class="swiper-slide">
                    <div class="video-item video-item-adv">
                        <div class="video-item-title">
                            <?= $model->video->getTitle() ?>
                        </div>
                        <div class="video-item-inner standard">
                            <a href="<?= Yii::app()->createUrl('/gallery/video/view', [
                                'video_dir' => $model->video->getDir(),
                                'conference' => $model->video->getConferenceString()
                            ]) ?>">
                                <img src="<?= Yii::app()->static->imageLink($model->video->getMinImage()); ?>" alt=""/>
                                <span class="time-line"><?= $model->video->getDuration(true); ?></span>
                                <span class="brand"><?= $model->video->getChannelImage() ?></span>
                            </a>
                        </div>
                        <a href="#" class="video-item-bottom">
                            <span class="video-item-watch-site-lite"><?= $model->video->getViewCount() ?></span>
                            <span class="video-item-watch-youtube"><?= $model->video->getHostView() ?></span>
                        </a>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>