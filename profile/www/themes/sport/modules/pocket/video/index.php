<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 24.10.2014
 * Time: 17:57
 *
 * @var PocketVideo[] $models
 * @var \common\components\Controller $this
 * @var CPagination $pages
 */ ?>

<main class="content video">
    <div class="box" id="page_content">
        <?= $this->renderPartial('_index', ['models' => $models, 'pages' => $pages], true) ?>
    </div>
</main>