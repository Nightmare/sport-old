<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 02.10.2014
 * Time: 14:43
 *
 * @var string $logo
 * @var PocketVideo $model
 * @var \profile\components\ProfileController $this
 */ ?>

<li class="video-item video-item-adv">
    <div class="video-item-title">
        <?= $model->video->getTitle(); ?>
        <div class="control">
            <span
                class="glyphicon glyphicon-cog pointer"
                data-type="ajax"
                data-link="<?= Yii::app()->createUrl('/pocket/video/manage', ['pocket_id' => $model->getId()]) ?>"
                data-title="<?= Yii::t('labels', 'Управление временными метками') ?>"
                data-toggle="tooltip"></span>
            <span
                class="glyphicon glyphicon-remove pointer"
                data-type="ajax"
                data-link="<?= Yii::app()->createUrl('/pocket/video/delete', ['pocket_id' => $model->getId()]) ?>"
                data-title="<?= Yii::t('labels', 'Удалить видео из кармана') ?>"
                data-toggle="tooltip"></span>
        </div>
    </div>
    <div class="video-item-inner standard">
        <a href="<?= Yii::app()->createUrl('/gallery/video/view', ['id' => $model->video->getId()]) ?>">
            <img src="<?= Yii::app()->static->imageLink($model->video->getMinImage()); ?>" alt="">
            <span class="time-line"><?= $model->video->getDuration(true); ?></span>
            <span class="brand"><?= $model->video->getChannelImage(true) ?></span>
        </a>
    </div>
    <a href="<?= Yii::app()->createUrl('/gallery/video/view', ['id' => $model->video->getId()]) ?>" class="video-item-bottom">
        <span class="video-item-watch-site-lite"><?= $model->video->getViewCount() ?></span>
        <span class="video-item-watch-youtube"><?= $model->video->getHostView() ?></span>
    </a>
</li>