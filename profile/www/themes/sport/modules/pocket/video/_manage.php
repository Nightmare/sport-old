<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 25.10.2014
 * Time: 15:37
 *
 * @var PocketVideo $model
 * @var \profile\components\ProfileController $this
 */ ?>

<div class="modal-dialog form pocket-video" id="replacement" style="width: 500px;">
    <?php /** @var TbActiveForm $form */
    $form = $this->beginWidget(
        '\common\widgets\booster\ActiveForm',
        [
            'id' => 'video_form',
            'action' => Yii::app()->createUrl('/pocket/video/manage', ['pocket_id' => $model->getId()]),
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            'htmlOptions' => [
                'class' => 'ajax'
            ],
            'clientOptions' => [
                'validateOnChange' => false
            ]
        ]
    ); ?>
    <input type="hidden" name="form" class="field" value="true">
    <input type="hidden" id="source_link" value="<?= $model->video->getSourceLink() ?>">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel"><?= Yii::t('labels', 'Управление метками'); ?></h4>
        </div>
        <div class="modal-body">
            <table id="murk_list">
                <?php $k = 1; foreach ($model->pocketVideoInfos as $murk): ?>
                    <tr class="pocket-item" data-number="<?= $k; ?>">
                        <td>
                            <textarea class="form-control field" name="PocketVideo[<?= $k; ?>][description]"><?= $murk->getDescription() ?></textarea>
                        </td>
                        <td>
                            <div class="buttons">
                                <?= CHtml::link('<span class="glyphicon glyphicon-eye-open"></span>', 'javascript:void(0);', ['class' => 'view btn btn-info sq']); ?>
                                <?= CHtml::link('-', 'javascript:void(0);', ['class' => 'take btn btn-danger sq']); ?>
                            </div>
                            <div class="input-group date timepicker">
                                <input type="text" class="form-control field time" value="<?= $murk->getTimeView(); ?>" name="PocketVideo[<?= $k; ?>][time]" />
                                <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                            </div>
                            <input type="hidden" class="field" name="PocketVideo[<?= $k; ?>][id]" value="<?= $murk->getId() ?>">
                        </td>
                    </tr>
                <?php $k++; endforeach; ?>
            </table>
            <?= CHtml::link('Добавить метку', 'javascript:void(0);', ['id' => 'add_murk', 'class' => 'btn btn-success sq']); ?>
        </div>
        <div class="modal-footer buttons">
            <button type="button" class="btn btn-default sq" data-dismiss="modal"><?= Yii::t('labels', 'Отмена') ?></button>
            <button type="button" data-modal-selector="#customModal" class="btn btn-primary sq" data-submit="ajax" data-for="video_form"><?= Yii::t('labels', 'Сохранить') ?></button>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
