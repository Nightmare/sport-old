<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 02.10.2014
 * Time: 14:43
 *
 * @var string $logo
 * @var PocketImage $model
 * @var \profile\components\ProfileController $this
 */ ?>

<li class="image-item image-item-adv">
    <div class="image-item-title clearfix">
        <div class="control">
            <span
                class="glyphicon glyphicon-pencil pointer"
                data-type="ajax"
                data-link="<?= Yii::app()->createUrl('/pocket/image/update', ['pocket_id' => $model->getId()]) ?>"
                data-title="<?= Yii::t('labels', 'Редактировать метку') ?>"
                data-toggle="tooltip"></span>
            <span
                class="glyphicon glyphicon-remove pointer"
                data-type="ajax"
                data-link="<?= Yii::app()->createUrl('/pocket/image/delete', ['pocket_id' => $model->getId()]) ?>"
                data-title="<?= Yii::t('labels', 'Удалить метку') ?>"
                data-toggle="tooltip"></span>
        <span
            class="glyphicon glyphicon-info-sign"
            data-title="<?= $model->getTitle() ?><br><?= $model->getDescription(); ?>"
            data-toggle="tooltip"
            data-html="true"></span>
        </div>
    </div>
    <div class="image-item-inner">
        <a href="<?= Yii::app()->createUrl('/gallery/image/view', ['id' => $model->image->getId()]); ?>">
            <img class="preview" src="<?= Yii::app()->static->imageLink($model->image->getMinImage()); ?>" alt="">
            <span class="brand"><img src="<?= Yii::app()->static->imageLink('www/img/video/adidas.jpg'); ?>" alt=""></span>
        </a>
    </div>
    <div class="image-item-bottom">
        <div class="title">
            <?= $model->image->getTitle(); ?>
        </div>
    </div>
</li>