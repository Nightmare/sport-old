<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 24.10.2014
 * Time: 17:57
 *
 * @var PocketImage[] $models
 * @var \common\components\Controller $this
 * @var CPagination $pages
 */ ?>

<ul class="image-list clearfix bottom-margin side-margin">
    <?php foreach($models as $model): ?>
        <?= $this->renderPartial('_item', ['model' => $model], true); ?>
    <?php endforeach; ?>
</ul>
<div class="pagerWrapper">
    <?php $this->widget('\common\extensions\pagination\Pagination', [
        'pages' => $pages,
        'cssFile' => Yii::app()->static->setOwn()->getLink('css/paging.css')
    ]); ?>
</div>