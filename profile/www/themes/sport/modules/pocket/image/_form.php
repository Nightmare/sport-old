<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 25.10.2014
 * Time: 15:37
 *
 * @var PocketImage $model
 * @var \profile\components\ProfileController $this
 */ ?>

<div class="modal-dialog form" id="replacement">
    <?php /** @var TbActiveForm $form */
    $form = $this->beginWidget(
        '\common\widgets\booster\ActiveForm',
        [
            'id' => 'image_form',
            'action' => Yii::app()->createUrl('/pocket/image/update', ['pocket_id' => $model->getId()]),
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            'htmlOptions' => [
                'class' => 'ajax'
            ],
            'clientOptions' => [
                'validateOnChange' => false
            ]
        ]
    ); ?>
    <div class="modal-content" style="width: 700px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel"><?= Yii::t('labels', 'Информация'); ?></h4>
        </div>
        <div class="modal-body">
            <table>
                <tr>
                    <td>
                        <ul>
                            <li>
                                <?= $form->textFieldGroup(
                                    $model,
                                    'title',
                                    [
                                        'wrapperHtmlOptions' => ['class' => 'col-sm-8'],
                                        'widgetOptions' => [
                                            'htmlOptions' => [
                                                'class' => 'field',
                                                'placeholder' => '',
                                            ],
                                        ],
                                        'labelClass' => 'col-sm-3',
                                    ]
                                ); ?>
                            </li>
                            <li>
                                <?= $form->textAreaGroup(
                                    $model,
                                    'description',
                                    [
                                        'wrapperHtmlOptions' => ['class' => 'col-sm-8'],
                                        'widgetOptions' => [
                                            'htmlOptions' => [
                                                'class' => 'field',
                                                'placeholder' => '',
                                            ],
                                        ],
                                        'labelClass' => 'col-sm-3',
                                    ]
                                ); ?>
                            </li>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <li>
                                <?= CHtml::image(Yii::app()->static->imageLink($model->image->getMinImage()), $model->getTitle()); ?>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
        <div class="modal-footer buttons">
            <button type="button" class="btn btn-default sq" data-dismiss="modal"><?= Yii::t('labels', 'Отмена') ?></button>
            <button type="button" data-modal-selector="#customModal" class="btn btn-primary sq" data-submit="ajax" data-for="image_form"><?= Yii::t('labels', 'Сохранить') ?></button>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>

