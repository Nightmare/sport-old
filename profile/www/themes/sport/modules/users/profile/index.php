<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 16.09.2014
 * Time: 14:48
 *
 * @var $this Controller
 * @var User $model
 * @var UserProfile $modelProfile
 */ ?>
<main class="content profile">
<div class="box">
<?php /** @var TbActiveForm $form */
$form = $this->beginWidget(
    '\common\widgets\booster\ActiveForm',
    [
        'id' => 'form-profile',
        'action' => Yii::app()->createUrl('/users/profile/update'),
        'type' => 'horizontal',
        'enableAjaxValidation' => true,
        'htmlOptions' => [
            'class' => 'ajax'
        ],
        'clientOptions' => [
            'validateOnChange' => false
        ]
    ]
); ?>
<table class="user_info form">
    <tr>
        <td class="avatar">
            <ul>
                <li>
                    <div class="form-group">
                        <?php $this->widget('\common\extensions\fileapi\FileApi', [
                            'url' => Yii::app()->createUrl('/users/profile/image'),
                            'view' => 'avatar',
                            'is_crop' => true,
                            'preview_height' => UserProfile::SIZE_MAX_HEIGHT,
                            'preview_width' => UserProfile::SIZE_MAX_WIDTH,
                        ]); ?>
                    </div>
                </li>
            </ul>
        </td>
        <td>
            <ul>
                <li>
                    <?= $form->textFieldGroup(
                        $model,
                        'username',
                        [
                            'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'class' => 'field',
                                    'placeholder' => '',
                                ],
                            ],
                            'labelClass' => 'col-sm-4',
                        ]
                    ); ?>
                </li>
                <li>
                    <?= $form->textFieldGroup(
                        $model,
                        'email',
                        [
                            'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'class' => 'disable',
                                    'placeholder' => '',
                                ],
                            ],
                            'labelClass' => 'col-sm-4',
                        ]
                    ); ?>
                </li>
                <li>
                    <?= $form->textFieldGroup(
                        $modelProfile,
                        'first_name',
                        [
                            'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'class' => 'field',
                                    'placeholder' => '',
                                ],
                            ],
                            'labelClass' => 'col-sm-4',
                        ]
                    ); ?>
                </li>
                <li>
                    <?= $form->textFieldGroup(
                        $modelProfile,
                        'middle_name',
                        [
                            'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'class' => 'field',
                                    'placeholder' => '',
                                ],
                            ],
                            'labelClass' => 'col-sm-4',
                        ]
                    ); ?>
                </li>
                <li>
                    <?= $form->textFieldGroup(
                        $modelProfile,
                        'last_name',
                        [
                            'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'class' => 'field',
                                    'placeholder' => '',
                                ],
                            ],
                            'labelClass' => 'col-sm-4',
                        ]
                    ); ?>
                </li>
                <li>
                    <?= $form->radioButtonListGroup(
                        $modelProfile,
                        'sex',
                        [
                            'wrapperHtmlOptions' => [
                                'class' => 'col-sm-6',
                                'style' => 'margin-left: 20px;'
                            ],
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'class' => 'field',
                                    'placeholder' => '',
                                ],
                                'data' => UserProfile::getSexList(),
                            ],
                            'labelClass' => 'col-sm-4',
                        ]
                    ); ?>
                </li>
                <li>
                    <div class="form-group">
                        <?= $form->label($modelProfile, 'birthday', ['class' => 'col-sm-4 control-label']); ?>
                        <div class="col-sm-6">
                            <?php
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model' => $modelProfile,
                                'attribute' => 'birthday',
                                'options' => [
                                    'changeYear' => true,
                                    'changeMonth' => true,
                                    'yearRange' => '-70:-15',
                                    'dateFormat' => 'dd.mm.yy',
                                    'orientation' => 'auto',
                                ],
                                'htmlOptions' => [
                                    'class' => 'field form-control',
                                    'placeholder' => '',
                                ]
                            ));
                            ?>
                        </div>
                    </div>
                </li>
            </ul>
        </td>
        <td>
            <ul>
                <li>
                    <?= $form->passwordFieldGroup(
                        $model,
                        'password',
                        [
                            'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'class' => 'field clear',
                                    'placeholder' => '',
                                ]
                            ],
                            'labelOptions' => ['label' => Yii::t('labels', 'Текущий пароль')],
                            'labelClass' => 'col-sm-5',
                        ]
                    ); ?>
                </li>
                <li>
                    <?= $form->passwordFieldGroup(
                        $model,
                        'newPassword',
                        [
                            'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'class' => 'field clear',
                                    'placeholder' => '',
                                ]
                            ],
                            'labelClass' => 'col-sm-5',
                        ]
                    ); ?>
                </li>
                <li>
                    <?= $form->passwordFieldGroup(
                        $model,
                        'passwordConfirm',
                        [
                            'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'class' => 'field clear',
                                    'placeholder' => '',
                                ]
                            ],
                            'labelClass' => 'col-sm-5',
                        ]
                    ); ?>
                </li>
                <li>
                    <div class="buttons center">
                        <?= CHtml::link(Yii::t('labels', 'Редактировать'), '', ['class' => 'enable-edit btn btn-danger sq']) ?>
                        <?= CHtml::link(Yii::t('labels', 'Сохранить'), '', [
                            'class' => 'btn blocked btn-primary sq',
                            'data-submit' => 'ajax',
                            'data-for' => 'form-profile'
                        ]) ?>
                    </div>
                </li>
            </ul>
        </td>
    </tr>
</table>
<?php $this->endWidget(); ?>
</div>
</main>
<script>
    $(function(){
        var disabled = true;
        $('input.field, a.blocked, input.disable').attr('disabled', true);
        $(document.body).on('click', '.enable-edit', function(event){
            event.preventDefault();

            disabled = !disabled;

            $('input.field, a.blocked').attr('disabled', disabled);
        });
    });
</script>