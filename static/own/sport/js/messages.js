/**
 * Created by me on 06.10.2014.
 */
$(function(){
    $(document.body).on('click', '#user-list li.user-item a.message', function(event){
        var $self = $(this);

        beginDialog($self.attr('data-id'));
    });
});
function internalDialog(id)
{
    $message = null;
    $message = new Message();
    $message.setLinkHistory(messageHistory);
    $message.setLinkGet(messageGet);
    $message.setLinkAdd(messageAdd);
    $message.setUserDialog(id);
    $message.activeForm();
}

function beginDialog(id)
{
    $message = null;
    $message = new Message();
    $message.setLinkHistory(messageHistory);
    $message.setLinkGet(messageGet);
    $message.setLinkAdd(messageAdd);
    $message.setUserDialog(id);
    $message.activeForm();
    $message.getHistory(id);
}

var $message = null;
function Message()
{
    var _that = this;
    var _linkHistory = null;
    var _linkHistoryAdd = null;
    var _linkHistoryGet = null;
    var _userDialog = null;
    this.timer = null;

    this.time = 0;
    this.inProcess = false;

    this.setLinkHistory = function(link){
        _linkHistory = link;
    };

    this.setLinkAdd = function(link) {
        _linkHistoryAdd = link;
    };

    this.setLinkGet = function(link) {
        _linkHistoryGet = link;
    };

    this.clear = function(){
        $('#message_box #message_list').html('');
    };

    this.setUserDialog = function(id){
        _userDialog = id;
    };

    this.getHistory = function(){
        if($message.inProcess) {
            $message.timer = setTimeout(function () {
                $message.getHistory();
            }, 1000);

            return;
        }

        var callback = function(response){
            if(response.time !== undefined)
                $message.time = response.time;

            if(response.messages === true)
                $('#message_box .empty_text').hide();
            else
                $('#message_box .empty_text').show().html(response.text);

            $message.inProcess = false;
        };

        _linkHistory = _linkHistory.replace(/null/, _userDialog);
        $message.inProcess = true;
        $ajax.send(
            _linkHistoryGet,
            {'time': $message.time, 'id': _userDialog},
            null, null, null, _linkHistory, null, callback, function(){ $message.inProcess = false; });
    };

    this.addMessage = function() {
        if($message.inProcess) {
            setTimeout(function () {
                $message.addMessage();
            }, 1000);

            return;
        }

        var callback = function(response){
            $message.inProcess = false;

            $('#bottom_form_body').val('');
            clearTimeout($message.timer);
            $message.getHistory();
        };

        $message.inProcess = true;
        $ajax.send(
            _linkHistoryAdd,
            {'id': _userDialog, 'body': $('#bottom_form_body').val()},
            null, null, null, null, null, callback, function(){ $message.inProcess = false; });
    };

    this.activeForm = function(){
        var $form = $('#panel_bottom [data-tab-in="form"]');

        $form.find('.form-item-group').hide();
        $form.find('textarea').attr('placeholder', 'Напишите тут свое сообщение...');
        $form.find('input[type="submit"]').val('Отправить сообщение');
        $panel.showPanelForm();

        $(document.body).off('click', '#bottom_form_submit');
        $(document.body).on('click', '#bottom_form_submit', function(event){
            event.preventDefault();

            _that.addMessage();
        });
    };
}

/*
 <li class="user-item">
 <img src="img/avatars/avatar1.jpg" alt="Name last-name" class="user-item-photo">
 <div class="user-item-info user-item-info-arrow">
    <a href="javascript:void(0);" class="username gender-man">Arnold Schwarzenegger</a>
    <span class="user-item-status online">online</span>
    <div class="user-item-action-list">
        <a href="javascript:void(0);" class="friends" title="title#1"></a>
        <a href="javascript:void(0);" class="message" title="title#2"></a>
    </div>
 </div>
 </li>
 */
function updateUserMessageList(user) {
    beginDialog(user.id);
    if($('[data-username="'+user.label+'"]').length > 0) return;

    var $li = $('<li>', {'class': 'user-item', 'data-username': user.label});
    $('<img>', {'alt': user.label, 'class': 'user-item-photo', 'src': user.thumb_mini}).appendTo($li);

    var $div = $('<div>', {'class': 'user-item-info user-item-info-arrow'}).appendTo($li);
    $('<a>', {'href': 'javascript:void(0);', 'class': 'username gender-man', 'text': user.label}).appendTo($div);
    $('<span>', {'class': 'user-item-status online', 'text': 'online'}).appendTo($div);

    var $div2 = $('<div>', {'class': 'user-item-action-list'}).appendTo($div);
    $('<a>', {'href': 'javascript:void(0);', 'data-id': user.id, 'class': 'friends', 'title': 'Добавить в друзья'}).appendTo($div2);
    $('<a>', {'href': 'javascript:void(0);', 'data-id': user.id, 'class': 'message', 'title': 'Отправить сообщение'}).appendTo($div2);

    $('ul#user-list').prepend($li);
}