/**
 * Created by me on 09.10.2014.
 */
function elementFocus (selector, scrollto){
    $(selector).scrollTo($(scrollto), 1000);
    //$('#focused_'+id).scrollTo('focus');
}

function openCustom()
{
    console.log('openCustom');

    $('.modal').modal('hide');
    $('#customModal').modal('show');
}
function closeCustom()
{
    $('.modal').modal('hide');
}

function removeElement(selector)
{
    console.log('removeElement', selector);

    $(selector).remove();
}

function setTimepicker(selector)
{
    console.log('setTimepicker', selector);

    $(selector + ' .timepicker').datetimepicker({
        format: 'HH:mm:ss',
        defaultDate: '2014-08-10 00:00:00',
        widgetPositioning: {'horizontal':'right', 'vertical':'auto'}
    });
}
function clearForm(){
    $('form .clear').val('');
}

function updatePage(link)
{
    closeCustom();
    $ajax.send(link);
}

function updateGrid(gridId, data, callback)
{
    if(data === undefined)
        data = {};
    if(callback === undefined)
        callback = function(){};


    var grid = $('#'+gridId) ;
    $.fn.yiiGridView.update(gridId, {
        data: data,
        complete:function(){
            grid.removeClass('grid-view-loading');
            callback();
        }
    });
}

$(function(){
    $(document.body).on('blur', '.point_input', function(event){
        if($(this).val() == "")
            $(this).val('0.00');
    });

    $(document.body).on('click', 'a.image_fancy', function(event){
        event.preventDefault();

        var $self = $(this);
        $.fancybox({
            type        : 'image',
            href        : $self.attr('href'),
            fitToView	: false,
            width		: '70%',
            height		: '70%',
            autoSize	: false,
            closeClick	: false,
            openEffect	: 'none',
            closeEffect	: 'none'
        });
    });

    $(document.body).on('click', 'a.video_fancy', function(event){
        event.preventDefault();

        var $self = $(this);
        $.fancybox({
            type        : 'iframe',
            href        : $self.attr('href'),
            maxWidth	: 800,
            maxHeight	: 600,
            fitToView	: false,
            width		: '70%',
            height		: '70%',
            autoSize	: false,
            closeClick	: false,
            openEffect	: 'none',
            closeEffect	: 'none',
            helpers : {
                media : {}
            }
        });
    });

    $(document.body).on('click', '[data-type="tabs"] > ul > li > a', function(event){
        event.preventDefault();

        var $self = $(this);
        $self.closest('ul').find('li').removeClass('ui-tabs-active ui-state-active');
        $self.closest('li').addClass('ui-tabs-active ui-state-active');
        $self.closest('[data-type="tabs"]').find('[data-tab-in]').hide();
        $('[data-tab-in="'+$self.attr('data-tab-for')+'"]').show();
    });

    $(document.body).on('click', '#conference .conf-list .conf-item', function(){
        var $self = $(this);
        var callback = function(response) {
            $('#pin').masonry({
                itemSelector: '.box-item',
                isFitWidth: true,
                isResizeBound: true,
                columnWidth: $('#pin').width()/5
            });

            $('#conference .conf-list .conf-item').removeClass('active');
            params['conf_category'].addClass('active');
        };

        var params = {'conf_category': $self};
        $ajax.send($self.data('link'), {}, null, null, true, $self.data('link'),  null, callback, null, null, params);
    });

    $(document.body).on('mouseover', '[data-toggle="popover"]', function(){
        $(this).popover('show');
    });
    $(document.body).on('mouseover', '[data-toggle="tooltip"]', function(){
        $(this).tooltip('show');
    });

    $(document.body).on('click', '[data-type="file"]', function(event){
        event.preventDefault();

        var $self = $(this);
        var name = $self.attr('data-for-name');

        $('[name="'+name+'"]').trigger('click');
    });

    $(document.body).on('click', '[data-type="file2"]', function(event){
        event.preventDefault();

        var $self = $(this);
        var name = $self.attr('data-for-name');

        $('#' + $self.data('file-block')).fileapi({
            url: $self.attr('href'),
            accept: 'image/*',
            autoUpload: true,
            onFileComplete: function(evt, uiEvt){
                var error = uiEvt.error;
                var result = uiEvt.result; // server response

                if(result.ok !== undefined) {
                    $(this).css({"background": "url("+result.link+")"});
                }
                $ajax.hideLoader();
            },
            onUpload: function(evt, uiEvt) {
                $ajax.showLoader();
            }
        });

        $('[name="'+name+'"]').trigger('click');
    });

    $(document.body).on('click', '[data-type="file-crop"]', function(event){
        event.preventDefault();

        var $self = $(this);

        var name = $self.attr('data-for-name');
        var preview_width = parseInt($self.data('preview-width'));
        var preview_height = parseInt($self.data('preview-height'));

        var id = '#' + $self.data('file-block');
        $(id).fileapi({
            url: $self.attr('href'),
            accept: 'image/*',
            imageSize: { minWidth: preview_width, minHeight: preview_height },
            imageTransform: {
                'thumb': { width: preview_width, height: preview_height }
            },
            onSelect: function (evt, ui){
                var file = ui.files[0];
                $('#crop-dark, #crop-block').show();
                var $crop_block = $('#crop-block').show();

                $(document.body).off('click', '#crop-block #crop-control #crop-save');
                $(document.body).on('click', '#crop-block #crop-control #crop-save', function(event){
                    event.preventDefault();

                    $(id).fileapi('upload');
                });
                $(document.body).off('click', '#crop-block #crop-control #crop-cancel');
                $(document.body).on('click', '#crop-block #crop-control #crop-cancel', function(event){
                    event.preventDefault();
                    $('#crop-dark, #crop-block').hide().find('#image-crop').html('');
                });

                var width = $crop_block.width();
                $('#crop-block #image-crop').cropper({
                    file: file,
                    bgColor: '#fff',
                    maxSize: [width, width],
                    minSize: [preview_width, preview_height],
                    allowSelect: false,
                    allowResize: true,
                    allowMove : true,
                    aspectRatio: preview_width / preview_height,
                    setSelect: [ 0, 0, preview_width, preview_height ],
                    keySupport: false,
                    onSelect: function (coords){
                        console.log(coords);
                        $(id).fileapi('crop', file, coords);
                    }
                });
            },
            onFileComplete: function(evt, uiEvt){
                var error = uiEvt.error;
                var result = uiEvt.result; // server response

                if(result.ok !== undefined) {
                    var randomId = new Date().getTime();

                    var origin_name = 'origin';
                    var min_name = 'min';
                    if($self.data('for-name-origin') !== undefined)
                        origin_name = $self.data('for-name-origin');
                    if($self.data('for-name-min') !== undefined)
                        min_name = $self.data('for-name-min');

                    var $origin = $self.closest('form').find('[name="'+origin_name+'"]');
                    var $min = $self.closest('form').find('[name="'+min_name+'"]');

                    var origin_value = result.origin;
                    var min_value = result.min;
                    if(result.original !== undefined)
                        origin_value = result.original;
                    if(result.thumb !== undefined)
                        min_value = result.thumb;

                    $(this).css({"background": "url('"+result.minLink+"?r="+randomId+"')"});
                    $origin.val(origin_value);
                    $min.val(min_value);
                }
                $('#crop-block, #crop-dark').hide();
                $ajax.hideLoader();
            },
            onUpload: function(evt, uiEvt) {
                $ajax.showLoader();
            }
        });

        $('[name="'+name+'"]').trigger('click');
    });

    $(document.body).on('click', '#media-filter-add', function(){
        var $self = $(this);
        var id = $self.closest('li').find('.filterSelect').val();
        var text = $self.closest('li').find('.filterSelect option:selected').text();

        var $block = $('#'+$self.data('block-out'));

        if($block.find('li[data-id="filter_'+id+'"]').length)
            return true;

        var $li = $('<li>', {'class': 'item', 'data-id': 'filter_'+id, 'data-type': 'filter'}).appendTo($block);
        $li.append('<span class="label label-default">'+text+' <span class="media-filter-delete glyphicon glyphicon-remove delete pointer"></span></span>');
        $li.append('<input type="hidden" value="'+id+'" name="filter['+id+']" class="field">');
    });

    $(document.body).on('click', '.media-filter-delete', function(){
        var $self = $(this);
        $self.closest('li').remove();
    });

    var select_group_id = {};
    $(document.body).on('change', 'select.groupSelect', function(event){
        var $self = $(this);
        var group_id = $self.val();

        var $block = $('select.filterSelect');
        $block.empty();
        if(group_id == 0) return false;

        if(select_group_id[group_id] !== undefined) {
            $.each(select_group_id[group_id], function(id, value){
                $block.append('<option value="'+id+'">'+value+'</option>');
            });
            return true;
        }

        var callback = function(response){
            select_group_id[response.group_id] = response.filters;
            $.each(response.filters, function(id, value){
                params['$block'].append('<option value="'+id+'">'+value+'</option>');
            });
        };
        var params = {'$block': $block};
        var data = {'group_id': group_id};
        $ajax.send($self.data('group-filter-link'), data, null, null, true, null, null, callback, null, null, params);
    });

    $(document.body).on('click', 'img[data-channel-link]', function(event){
        event.preventDefault();

        var win = window.open($(this).data('channel-link'), '_blank');
        win.focus();
    });
});