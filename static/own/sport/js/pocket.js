/**
 * Created by me on 04.02.2015.
 */
$(function(){
    $(document.body).on('click', '.pocket-video #add_murk', function(){
        var count = 0;
        if($('.pocket-video #murk_list tr').length > 0)
            count = parseInt($('.pocket-video #murk_list tr:last-child').data('number'));
        var $tr = createBlockPocket(count + 1);

        $('.pocket-video #murk_list').append($tr);
    });

    $(document.body).on('click', '.pocket-video a.take', function(){
        $(this).closest('tr').remove();
    });

    $(document.body).on('click', '.pocket-video .buttons a.view', function(event){
        event.preventDefault();

        var $self = $(this);

        var link = $('.pocket-video #source_link').val();

        var time = $self.closest('tr').find('.time').val();
        var time_arr = time.split(':');
        var t = parseInt(time_arr[0]) * 3600 + parseInt(time_arr[1]) * 60 + parseInt(time_arr[2]);

        console.log(time, time_arr, t, link);
        $.fancybox({
            'href'			: link.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type'			: 'swf',
            'swf'			: {
                'wmode'				: 'transparent',
                'allowfullscreen'	: 'true'
            },
            maxWidth	: 800,
            maxHeight	: 600,
            fitToView	: false,
            width		: '70%',
            height		: '70%',
            autoSize	: false,
            closeClick	: false,
            openEffect	: 'none',
            closeEffect	: 'none',
            helpers : {
                media : {
                    youtube : {
                        params : {
                            autoplay : 1,
                            start: t
                        }
                    }
                }
            }
        });
    });
});

function createBlockPocket(number)
{
    var $tr = $('<tr>', {'data-number': number, 'class': 'pocket-item'});

    var $td1 = $('<td>', {}).appendTo($tr);
    $('<textarea>', {'class': 'form-control field', 'name': 'PocketVideo['+number+'][description]'}).appendTo($td1);

    var $td2 = $('<td>', {}).appendTo($tr);
    var $buttons = $('<div>', {'class':'buttons'}).appendTo($td2);
    $('<a>', {'href':'javascript:void(0);', 'class':'view btn btn-info sq', 'html':'<span class="glyphicon glyphicon-eye-open"></span>'}).appendTo($buttons);
    $buttons.append(' ');
    $('<a>', {'href':'javascript:void(0);', 'class':'take btn btn-danger sq', 'html':'<span class="glyphicon glyphicon-remove"></span>'}).appendTo($buttons);

    var $time = $('<div>', {'class':'input-group date timepicker'}).appendTo($td2);
    $('<input>', {'type':'text', 'class':'form-control field time', 'name':'PocketVideo['+number+'][time]'}).appendTo($time);
    $('<span>', {'class':'input-group-addon', 'html':'<span class="glyphicon glyphicon-time"></span>'}).appendTo($time);

    $time.datetimepicker({
        format: 'HH:mm:ss',
        defaultDate: '2014-08-10 00:00:00',
        widgetPositioning: {'horizontal':'left', 'vertical':'bottom'}
    });

    return $tr;
}