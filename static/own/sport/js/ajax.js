/**
 * Created by Ice on 13.07.14.
 */
$.fn.exists = function(){return this.length>0;}

var $ajax = new AJAX();
var HistoryCallback = {};
$(function(){
    $.ajaxSetup({
        xhrFields: {
            withCredentials: true
        },
        data: {'request_width':true}
    });
    try {
        var History = window.History;
        History.Adapter.bind(window, 'statechange', function() {
            var state = History.getState();
            var index = state.data._indexState;

            if(HistoryCallback[index] !== undefined) {
                $.each(HistoryCallback[index], function(el, fn){
                    fn.apply();
                });
            } else
                window.location.reload();
        });
        $ajax.flag = true;
    } catch (ex) {
        console.log(ex);
    }

    $( document ).ajaxError(function(error, response) {
        //$.fancybox.hideLoading();
        //$.notify(response.responseText, 'info');

        console.log(error);
        $ajax.hideLoader();
    });

    $ajax.events();
});

function AJAX()
{
    var _that = this;
    var _CSRF = null;
    var _debug = true;
    var _inProcess = false;

    this.flag = false;

    this.afterResponse = function(response) {
        console.log('Begin afterResponse');
        if(response.replaceList !== undefined) {
            $.each(response.replaceList, function(i, item){
                $(item.selector).replaceWith(item.view);
            });
        }

        if(response.htmlList !== undefined) {
            $.each(response.htmlList, function(i, item){
                $(item.selector).html(item.view);
            });
        }

        if(response.runJS !== undefined)
            $ajax.runJS(response.runJS);

        if(response.redirectLink !== undefined)
            window.location = response.redirectLink;

        _that.hideLoader();
        console.log('End afterResponse');
    };

    this.beforeValidate = function(formId){

    };

    this.afterValidate = function(data){
        if(data === null)
            return true;

        var string = '';
        var formId = null;
        if(data.formId !== undefined)
            formId = data.formId;


        $('#'+formId).find('input, select, textarea').attr('readonly', false).removeClass('error');
        $('#' + formId + ' div.error').html('').hide();
        if(data.errors !== undefined) {
            $.each(data.errors, function(type, errorList) {
                $.each(errorList, function(field, text){
                    if($('#' + formId + ' #' + field).exists()) {
                        $('#' + formId + ' #' + field).addClass('error');
                        $('#' + formId + ' #' + field+'_em_').html(text).show();
                    } else
                        _that.message(type, text);
                    //string += text + '<br>';
                });
            });
            return false;
        } else {
            if(data.close !== undefined && data.close === true)
                $.fancybox.close();

            if(data.messages !== undefined) {
                $.each(data.messages, function(type, message){
                    $.each(message, function(i, text){
                        _that.message(type, text);
                    });
                });
            }

            return true;
        }
    };

    this.setCSRF = function(value){
        _CSRF = value;
    };

    this.message = function(type, text){
        var n = noty({
            text: text,
            layout: 'topCenter',
            type: type,
            theme: 'relax',
            timeout: 5000
        });
    };

    this.showLoader = function(showLoader){
        switch(showLoader) {
            case 'panel':
                $('#panel-page-dark').fadeIn(100);
                break;
            default:
                $('#page-dark, #page-dark #page-loader').fadeIn(100);
                break;
        }
    };

    this.hideLoader = function() {
        $('#page-dark, #panel-page-dark').fadeOut(150);
        //$('#page-dark, #panel-page-dark').hide();
    };

    this.events = function(){
        $(document.body).on('click', 'form.ajax [data-submit="ajax"]', function(event){
            event.preventDefault();

            var $self = $(this);
            _submitForm($self);
        });

        $(document.body).on('click', '.ajax-link, [data-type="ajax"]', function(event){
            event.preventDefault();

            var $self = $(this);
            var historyLink = null;
            var link = $self.attr('href');
            if($self.attr('data-link') !== undefined)
                link = $self.attr('data-link');

            if($self.attr('data-history') !== undefined)
                historyLink = link;

            _that.send(link, {}, null, null, null, historyLink);
        });
    };

    //data-modal-selector
    var _submitForm = function($obj){
        var id = $obj.attr('data-for');
        var $form = $('#'+id);

        var data = _prepareFormData($form.find('.field').serializeArray());
        var callback = function(response){
            if(response !== null && response.ok !== undefined) {

                if(params.close !== undefined)
                    _closePopup(params.close);
            }
        };

        var params = {};
        if($obj.attr('data-modal-selector') !== undefined)
            params['close'] = $obj.attr('data-modal-selector');

        _that.send($form.attr('action'), data, null, $form.attr('method'), null, null, null, callback, null, null, params);
    };

    this.runJS = function(params){
        if(params[0] === undefined)
            params = {0: params};

        $.each(params, function(i, call){
            try {
                var fnstring = call.name;
                var fnparams = call.params;

                var fn;
                if(fnstring.indexOf('.') < 1)
                    fn = window[fnstring];
                else {
                    var arr = fnstring.split('.');
                    if(window[arr[0]] !== undefined && window[arr[0]][arr[1]] !== undefined)
                        fn = window[arr[0]][arr[1]];
                }

                if (typeof fn === "function") fn.apply(null, fnparams);
            } catch(e) {

            }
        });

    };

    this.send = function(link,data,dataType,requestType,showLoader,historyUrl,callbackBefore,callbackSuccess,callbackError,callbackHistory,params){
        data             = (data              === undefined || data              === null ) ? {}            : data;
        dataType         = (dataType          === undefined || dataType          === null ) ? 'json'        : dataType;
        requestType      = (requestType       === undefined || requestType       === null ) ? 'post'        : requestType;
        showLoader       = (showLoader        === undefined || showLoader        === null ) ? true          : showLoader;
        historyUrl       = (historyUrl        === undefined || historyUrl        === null ) ? ''            : historyUrl;
        callbackBefore   = (callbackBefore    === undefined || callbackBefore    === null ) ? function(){}  : callbackBefore;
        callbackSuccess  = (callbackSuccess   === undefined || callbackSuccess   === null ) ? function(){}  : callbackSuccess;
        callbackError    = (callbackError     === undefined || callbackError     === null ) ? function(){}  : callbackError;
        callbackHistory  = (callbackHistory   === undefined || callbackHistory   === null ) ? function(){}  : callbackHistory;
        params           = (params            === undefined || params            === null ) ? {}            : params;

        if(_inProcess === true) {
            setTimeout(function(){
                $ajax.send(link,data,dataType,requestType,showLoader,historyUrl,callbackBefore,callbackSuccess,callbackError,callbackHistory,params);
            }, 2000);
            return;
        }

        if(historyUrl != '' && historyUrl != window.location.pathname) {
            if(!History || !History.pushState || !window.history.pushState || !_that.flag) {
                window.location = link;
                return;
            }

            var sendRequest = function() { $ajax.send(link,data,dataType,requestType,showLoader,null,callbackBefore,callbackSuccess,callbackError,null,params); };
            HistoryCallback[History.getCurrentIndex()] = {
                'callbackHistory'  : callbackHistory,
                'sendRequest'      : sendRequest
            };

            History.pushState({'_indexState' : History.getCurrentIndex(), 'link' : historyUrl}, null, historyUrl);
            return;
        }

        if(requestType === 'post')
            data['YII_CSRF_TOKEN'] = _that.CSRF;

        $.ajax({
            url         : link,
            data        : data,
            type        : requestType,
            dataType    : dataType,
            beforeSend  : function(){
                _inProcess = true;
                if(showLoader !== false)
                    _that.showLoader(showLoader);

                callbackBefore();
                return _that.beforeValidate();
            },
            success     : function(response){
                console.log('Begin ajax.success');
                _inProcess = false;
                $ajax.afterResponse(response);
                $ajax.afterValidate(response);

                callbackSuccess(response);

                if(response.error !== undefined && response.error == true) {
                    callbackError();
                }
                console.log('End ajax.success');
            },
            error       : function() {
                callbackError();

                _inProcess = false;
            }
        });
    };

    var _closePopup = function(selector){
        $(selector).modal('hide');
    };

    var _prepareFormData = function(data) {
        var returned = {};
        $.each(data, function(i, info){
            returned[info['name']] = info['value'];
        });

        return returned;
    };
}