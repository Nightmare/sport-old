/**
 * Created by me on 06.10.2014.
 */
$(function(){
    $(document.body).on('click', '#user-list li.user-item a.message', function(event){
        var $self = $(this);

        beginDialog($self.attr('data-id'));
    });
});

var $conference = new Conference();
function Conference()
{
    var _that = this;
    var _linkAdd = null;
    this.timer = null;

    this.inProcess = false;

    this.setLinkAdd = function(link) {
        _linkAdd = link;
    };
    this.addMessage = function() {
        if($conference.inProcess) {
            setTimeout(function () {
                $conference.addMessage();
            }, 1000);

            return;
        }

        var callback = function(response){
            $conference.inProcess = false;

            $('#bottom_form_body').val('');
        };

        $conference.inProcess = true;
        $ajax.send(
            _linkAdd,
            {'body': $('#bottom_form_body').val()},
            null, null, null, null, null, callback, function(){ $conference.inProcess = false; });
    };

    this.activeForm = function(){
        var $form = $('#panel_bottom [data-tab-in="form"]');

        $form.find('.form-item-group').hide();
        $form.find('textarea').attr('placeholder', 'Напишите тут свое сообщение...');
        $form.find('input[type="submit"]').val('Отправить сообщение');
        $panel.showPanelForm();

        $(document.body).off('click', '#bottom_form_submit');
        $(document.body).on('click', '#bottom_form_submit', function(event){
            event.preventDefault();

            _that.addMessage();
        });
    };
}