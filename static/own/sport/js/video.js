/**
 * Created by me on 02.10.2014.
 */
$(function(){
    $(document.body).on('click', 'form#video_form #muscle .add', function(){
        assignMuscle();
    });

    $(document.body).on('click', 'form#video_form #simulator .add', function(){
        assignSimulator();
    });

    $(document.body).on('click', 'form#video_form #assign_items .delete', function(){
        $(this).closest('li').remove();
    });
});

function assignMuscle()
{
    var $item = $('#GalleryVideo_muscle :selected');
    if($('li.item[data-id="muscle_'+$item.val()+'"]').length > 0 || $item.val() == '0')
        return;

    var $block = $('#assign_items');
    var $li = $('<li>', {'class':'item', 'data-id':'muscle_'+$item.val(), 'data-type':'muscle'}).appendTo($block);
    var $span = $('<span>', {'class':'label label-primary', 'text':$item.text()}).appendTo($li);
    $('<span>', {'class':'glyphicon glyphicon-remove delete pointer'}).appendTo($span);

    $('<input>', {'type':'hidden', 'value':$item.val(), 'name':'muscle['+$item.val()+']', 'class':'field'}).appendTo($li);
}

function assignSimulator()
{
    var $item = $('#GalleryVideo_simulator :selected');
    if($('li.item[data-id="simulator_'+$item.val()+'"]').length > 0 || $item.val() == '0')
        return;

    var $block = $('#assign_items');
    var $li = $('<li>', {'class':'item', 'data-id':'simulator_'+$item.val(), 'data-type':'simulator'}).appendTo($block);
    var $span = $('<span>', {'class':'label label-default', 'text':$item.text()}).appendTo($li);
    $('<span>', {'class':'glyphicon glyphicon-remove delete pointer'}).appendTo($span);

    $('<input>', {'type':'hidden', 'value':$item.val(), 'name':'simulator['+$item.val()+']', 'class':'field'}).appendTo($li);
}

function updateVideo()
{
    $('.modal').modal('hide');
    $('#customModal').modal('show');
}