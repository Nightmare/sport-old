/**
 * Created by me on 08.10.2014.
 */
var $comment = new Comment();
$(function(){
    $(document.body).on('click', 'a#add_comment.btn-action', function(event){
        event.preventDefault();

        $comment.openPanel();
    });

    $(document.body).on('click', 'ul.comment-list li a.reply', function(event){
        event.preventDefault();

        var $self = $(this);
        var $li = $self.closest('li');

        $('ul.comment-list li').removeClass('active-replay');
        $li.addClass('active-replay');

        $comment.setParent($self.attr('data-id'));
        $comment.openPanel();
    });

    $(document.body).on('click', '#ajax_upload_video .start_upload_video', function(event){
        event.preventDefault();

        var $form = $('#panel_bottom form#bottom_form');
        var data = {
            'link': $form.find('#ajax_upload_video input').val()
        };
        var callback = function(response){
            $comment.addVideo(response);

            $('#panel_bottom form#bottom_form #ajax_upload_video input').val('');
        };

        $ajax.send(videoCommentTemp, data, null, null, null, null, null, callback);
    });

    $(document.body).on('click', 'ul#comment_list li span.attach', function(event){
        event.preventDefault();

        var $self = $(this);
        $('[data-unique-source="'+$self.attr('data-unique-link')+'"] a').trigger('click');
    });
    /** image click */
    $(document.body).on('click', 'ul.media-list li.image-item:not(.active) a', function(event){
        event.preventDefault();

        var $self = $(this);
        $comment.uncheckedMedia();
        $self.closest('li').addClass('active');
        $comment.showComment($self.closest('li').attr('data-unique-source'));
    });


    $(document.body).on('click', 'ul.media-list li.image-item.active a', function(event){
        event.preventDefault();

        var $self = $(this);
        $.fancybox({
            type        : 'image',
            href        : $self.attr('href'),
            maxWidth	: 800,
            maxHeight	: 600,
            fitToView	: false,
            width		: '70%',
            height		: '70%',
            autoSize	: false,
            closeClick	: false,
            openEffect	: 'none',
            closeEffect	: 'none',
            afterClose  : function() {
                $comment.uncheckedMedia();
            }
        });
    });
    /** end image click */
    /** video click */
    $(document.body).on('click', 'ul.media-list li.video-item:not(.active) a', function(event){
        event.preventDefault();

        var $self = $(this);
        $comment.uncheckedMedia();
        $self.closest('li').addClass('active');
        $self.closest('div').switchClass('image-item-inner', 'video-item-inner');
        $comment.showComment($self.closest('li').attr('data-unique-source'));
    });


    $(document.body).on('click', 'ul.media-list li.video-item.active a', function(event){
        event.preventDefault();

        var $self = $(this);
        $.fancybox({
            type        : 'iframe',
            href        : $self.attr('href'),
            maxWidth	: 800,
            maxHeight	: 600,
            fitToView	: false,
            width		: '70%',
            height		: '70%',
            autoSize	: false,
            closeClick	: false,
            openEffect	: 'none',
            closeEffect	: 'none',
            afterClose  : function() {
                $comment.uncheckedMedia();
            },
            helpers : {
                media : {}
            }
        });
    });
    /** end video click */

    $(document.body).on('click', '#ajax_upload_image .add_image', function(){
        $('#ajax_upload_image [type="file"]').trigger('click');
    });

    $(document.body).on('click', '#ajax_upload_image .start_upload_image', function(){
        if($('#ajax_upload_image input[type="text"]').val().length > 0)
            $('#ajax_upload_image').fileapi('upload');
        else
            $ajax.message('error', 'Сначала вставьте ссылку');
    });

    $('#ajax_upload_image').fileapi({
        url: uploadCommentTemp,
        multiple: true,
        maxSize: 2 * FileAPI.MB,
        maxFiles: 3,
        onSelect: function (evt, ui) {
            var name = '';
            $.each(ui.files, function(i, file){
                if(name != '') name += ', ';
                name += file.name;
            });
            $.each(ui.other, function(i, file){
                $.each(file.errors, function(type, value){
                    switch (type) {
                        case 'maxSize':
                            $ajax.message('error', 'Максимальный размер изображения 2MB');
                            break;
                        case 'maxFiles':
                            $ajax.message('error', 'Максимальное кол-во файлов 3');
                            break;
                    }
                });
            });
            $('#ajax_upload_image input[type="text"]').val(name);
        },
        onFileComplete: function(evt, uiEvt){
            var error = uiEvt.error;
            var result = uiEvt.result; // server response

            $comment.addImage(result);
            $ajax.hideLoader();
        },
        onUpload: function(evt, uiEvt) {
            $ajax.showLoader();
        }
    });

    $(document.body).on('click', '.comment-item .comment-info span.show_tree', function(){
        var $self = $(this);

        if($self.hasClass('active')) {
            $self.removeClass('active');
            $comment.get();
        } else {
            $self.addClass('active');
            $comment.getTree($self.attr('data-main'));
        }
    });

    $(document.body).on('click', '.comment-item .comment-info span.tree_top', function(){
        var $self = $(this);

        var id = $self.attr('data-source');
        console.log(id);
        elementFocus('body', '[data-unique="'+id+'"]');
    });
});

function Comment()
{
    var _that = this;
    var _images = {};
    var _videos = {};

    this.timer = null;
    this.parent = 0;

    this.getImages = function(){
        return _images;
    };
    this.getVideos = function(){
        return _videos;
    };

    this.showComment = function(unique){

        var $chooseComment = $('[data-unique-link="'+unique+'"]').closest('li');
        $('ul.comment-list li').removeClass('active-replay');
        $chooseComment.addClass('active-replay');
        elementFocus('body', '[data-unique="'+$chooseComment.attr('data-unique')+'"]');
    };

    this.openPanel = function(){
        $panel.showPanelForm();

        var $form = $('#panel_bottom form#bottom_form');
        $form.find('textarea').attr('placeholder', 'Напишите тут свое сообщение...');
        $form.find('input[type="submit"]').val('Отправить сообщение');

        $(document.body).off('click', '#bottom_form_submit');
        $(document.body).on('click', '#bottom_form_submit', function(event){
            event.preventDefault();

            $comment.send();
        });

        $(document.body).on('click', '#panel_bottom form#bottom_form ul.form-items .delete', function(){
            var $self = $(this);
            var $li = $self.closest('li');
            var data_id = $li.attr('data-id');

            switch ($li.attr('data-type')) {
                case 'image':
                    delete _images[data_id];
                    break;
                case 'video':
                    delete _videos[data_id];
                    break;
                default:
                    break;
            }

            $li.remove();
        });
    };

    this.setParent = function(id) {
        $comment.parent = id;
    };

    this.getTree = function(id){
        if($comment.inProcess)
        {
            $comment.timer = setTimeout(function () {
                $comment.getTree();
            }, 1000);

            return;
        }

        var callback = function(response)
        {
            $comment.inProcess = false;
            if(response.comment_tree !== undefined) {
                setTimeout(function(){
                    $('[data-main="' + response.comment_tree + '"]').addClass('active');
                }, 1000);
            }
        };

        $ajax.send(getCommentLink, {'comment_tree':id}, null, null, null, null, null, callback);
    };

    this.get = function() {

        if($comment.inProcess)
        {
            $comment.timer = setTimeout(function () {
                $comment.get();
            }, 1000);

            return;
        }

        var callback = function(response)
        {
            if(response.ok !== undefined) {
                if(response.messages === true)
                    $('#comment_box .empty_text').hide();
                else
                    $('#comment_box .empty_text').show().html(response.text);
            }

            if(response.time !== undefined)
                $comment.time = response.time;

            $comment.inProcess = false;
        };

        $ajax.send(getCommentLink, {}, null, null, null, null, null, callback);
    };

    this.clear = function(){
        var $form = $('form#bottom_form');
        $form.find('ul.form-items li').remove();
        $form.find('#bottom_form_body').val('');
        $form.find('input[type="text"]').val('');

        clearTimeout($comment.timer);
        _images = {};
        _videos = {};
    };

    this.send = function() {
        if($comment.inProcess) {
            setTimeout(function () {
                $comment.send();
            }, 1000);

            return;
        }

        var callback = function(response){
            $comment.inProcess = false;
            $comment.parent = 0;
            if(response.ok !== undefined) {
                $comment.clear();
                $comment.get();
            }
        };
        var callbackError = function(){
            $comment.inProcess = false;
            $comment.parent = 0;
        };

        var data = {
            'parent': $comment.parent,
            'body': $('form#bottom_form').find('#bottom_form_body').val(),
            'images': _images,
            'videos': _videos
        };

        $comment.inProcess = true;
        $ajax.send(sendCommentLink, data, null, null, null, null, null, callback, callbackError);
    };

    var _imgCount = 0;
    this.addImage = function (response) {
        _images[_imgCount] = response.link;
        var $form = $('form#bottom_form');

        var $block = $form.find('ul.form-items');
        var $li = $('<li>', {'class':'item', 'data-id':_imgCount, 'data-type':'image'}).appendTo($block);
        var $span = $('<span>', {'class':'label label-primary', 'text':response.name}).appendTo($li);
        $('<span>', {'class':'glyphicon glyphicon-remove delete pointer'}).appendTo($span);

        _imgCount++;
    };

    var _videoCount = 0;
    this.addVideo = function (response) {
        if(response.ok === undefined)
            return;

        _videos[_videoCount] = {
            'video_link' : response.video_link
        };
        var $form = $('form#bottom_form');

        var $block = $form.find('ul.form-items');
        var $li = $('<li>', {'class':'item', 'data-id':_videoCount, 'data-type':'video'}).appendTo($block);
        var $span = $('<span>', {'class':'label label-primary', 'text':response.name}).appendTo($li);
        $('<span>', {'class':'glyphicon glyphicon-remove delete pointer'}).appendTo($span);

        _videoCount++;
    };

    this.uncheckedMedia = function(){
        $('ul.media-list li').removeClass('active');
        $('ul.media-list li.video-item div.video-item-inner').switchClass('video-item-inner', 'image-item-inner');
        $('ul.comment-list li').removeClass('active-replay');
    };
}
