/**
 * Created by me on 11.10.2014.
 */
$(function(){
    $(document.body).on('click', '#filter-filter li a', function(event){
        event.preventDefault();

        var reloadFilter = false;
        var $self = $(this);
        var filter = $self.data('filter');

        if($self.closest('li').hasClass('active'))
            reloadFilter = true;

        var $preview = $('#filter-filter .preview-box');
        $('#filter-filter li').removeClass('active');
        if(reloadFilter) {
            $preview.find('img').attr('src', $preview.data('img'));
            $ajax.send($('#data-filter-link').data('filter-link'));
            return;
        }

        $preview.attr('data-filter', filter)
            .find('img').attr('src', $self.data('image'));

        $self.closest('li').addClass('active');

        var callback = function(){
            elementFocus('body', '.wrapper');
        };

        var data = {'filter': filter};
        $ajax.send($self.attr('href'), data, null, null, null, null, null, callback);
    });

    $(document.body).on('click', '#filter-filter .preview-box[data-filter]', function(event){
        event.preventDefault();

        var $self = $(this);

        $self.find('img').attr('src', $self.data('img'));
        $ajax.send($('#data-filter-link').data('filter-link'));
    });
});