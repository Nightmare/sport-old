/**
 * Created by me on 06.10.2014.
 */
var $panel = new Panel();
var $slider = new Slider();
$(function(){
    $('#panel_bottom .tab_content:not(.site-rules)').hide();
    $panel.hidePanelForm();
    $(document.body).on('click', '#panel_bottom a.tab_buttons', function(event){
        event.preventDefault();

        var $self = $(this);
        var selector = $self.attr('data-tab-for');

        var $show =  $('[data-tab-in="'+selector+'"]');
        if($show.length > 0) {
            $('#panel_bottom .tab_content').hide();
            $show.show();
        }

        switch (selector){
            case 'saver':
                $slider.saver();
                break;
            case 'pocket':
                $slider.pocket();
                break;
        }
    });

    setInterval(function(){
        checkInfo();
    }, 30000);
});

function Panel()
{
    var _that = this;

    this.showPanelForm = function() {
        $('#panel_bottom [data-tab-for="form"]').trigger('click')
            .closest('li').show()
            .closest('ul').find('li.empty:not(.last)').hide();

        _that.show();
    };

    this.hidePanelForm = function() {
        $('#panel_bottom [data-tab-for="form"]')
            .closest('li').hide()
            .closest('ul').find('li.empty:not(.last)').show();
    };

    this.hide = function(){
        $('#panel_bottom.panel.open .top-panel .btn-nav').trigger('click');
    };

    this.show = function(){
        $('#panel_bottom.panel:not(.open) .top-panel .btn-nav').trigger('click');
    };

    this.showStartup = function(){
        $('#panel_bottom.panel').addClass('open');
        $('#middle').css('padding-bottom', $('#footer').height() + 9);
    };
}

function Slider()
{
    var _that = this;

    this.saver_image = null;
    this.saver_video = null;

    this.pocket_image = null;
    this.pocket_video = null;

    this.saver = function(){
        if(_that.saver_image === null || _that.saver_video === null) {
            var callback = function(response){
                if($slider.saver_image === null) {
                    $slider.saver_image = new Swiper('.panel #saver-slider-image .swiper-container', {
                        createPagination: false,
                        slidesPerView: parseInt($('.panel .center-content-wrapper').width() / 245, 10),
                        speed: 600,
                        calculateHeight: true
                    });
                    $(document.body).on('click', '.panel #saver-slider-image .panel-slider-arrow', function (e) {
                        e.preventDefault();
                        $slider.saver_image['swipe' + ($(this).hasClass('next') ? 'Next' : 'Prev')]();
                    });
                }

                if($slider.saver_video === null) {
                    $slider.saver_video = new Swiper('.panel #saver-slider-video .swiper-container', {
                        createPagination: false,
                        slidesPerView: parseInt($('.panel .center-content-wrapper').width() / 245, 10),
                        speed: 600,
                        calculateHeight: true
                    });
                    $(document.body).on('click', '.panel #saver-slider-video .panel-slider-arrow', function (e) {
                        e.preventDefault();
                        $slider.saver_video['swipe' + ($(this).hasClass('next') ? 'Next' : 'Prev')]();
                    });
                }

                $('.panel [data-tab-in="saver"] [data-tab-in]').hide();
                $('.panel [data-tab-in="saver"] [data-tab-in]:first').show();
            };

            $ajax.send(saverLink, null, null, null, 'panel', null, null, callback);
        }
    };

    this.pocket = function(){
        if(_that.pocket_image === null || _that.pocket_video === null) {
            var callback = function(response){
                if($slider.pocket_image === null) {
                    $slider.pocket_image = new Swiper('.panel #pocket-slider-image .swiper-container', {
                        createPagination: false,
                        slidesPerView: parseInt($('.panel .center-content-wrapper').width() / 245, 10),
                        speed: 600,
                        calculateHeight: true
                    });
                    $(document.body).on('click', '.panel #pocket-slider-image .panel-slider-arrow', function (e) {
                        e.preventDefault();
                        $slider.pocket_image['swipe' + ($(this).hasClass('next') ? 'Next' : 'Prev')]();
                    });
                }

                if($slider.pocket_video === null) {
                    $slider.pocket_video = new Swiper('.panel #pocket-slider-video .swiper-container', {
                        createPagination: false,
                        slidesPerView: parseInt($('.panel .center-content-wrapper').width() / 245, 10),
                        speed: 600,
                        calculateHeight: true
                    });
                    $(document.body).on('click', '.panel #pocket-slider-video .panel-slider-arrow', function (e) {
                        e.preventDefault();
                        $slider.pocket_video['swipe' + ($(this).hasClass('next') ? 'Next' : 'Prev')]();
                    });
                }

                $('.panel [data-tab-in="pocket"] [data-tab-in]').hide();
                $('.panel [data-tab-in="pocket"] [data-tab-in]:first').show();
            };

            $ajax.send(pocketLink, null, null, null, 'panel', null, null, callback);
        }
    };
}

function checkInfo() {
    var callback = function(response){
        var message = response.message;
        if(message !== undefined) {
            var $el = $('#message_link');
            if(message.count > 0)
                $el.html('Сообщения (' + message.count + ')');
            else
                $el.html('Сообщения');
        }
    };

    $ajax.send(infoUpdateLink, null, null, null, false, null, null, callback);
}