<?php
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use \frontend\components\FrontendController;
class FrontendLocationController extends FrontendController
{
    public function filters() {
        return array(
            'ajaxOnly + registration, restore',
        );
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
    public function actions()
    {
        return array(
            'country'   => ['class' => '\frontend\controllers\actions\location\CountryAction'],
            'region'    => ['class' => '\frontend\controllers\actions\location\RegionAction'],
            'city'      => ['class' => '\frontend\controllers\actions\location\CityAction'],
        );
    }

    public function actionFlush()
    {
        Yii::app()->cache->flush();
    }
}