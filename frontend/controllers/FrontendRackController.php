<?php
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use \frontend\components\FrontendController;
class FrontendRackController extends FrontendController
{
    public function filters() {
        return array(
            //'ajaxOnly + registration, restore',
        );
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'street_workout'    => ['class' => '\frontend\controllers\actions\rack\StreetWorkoutAction'],
            'turnik'            => ['class' => '\frontend\controllers\actions\rack\TurnikAction'],
            'brusya'            => ['class' => '\frontend\controllers\actions\rack\BrusyaAction'],
            'index'             => ['class' => '\frontend\controllers\actions\rack\IndexAction'],
		);
	}

    public function actionFlush()
    {
        Yii::app()->cache->flush();
    }
}