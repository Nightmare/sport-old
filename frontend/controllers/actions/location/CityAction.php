<?php
namespace frontend\controllers\actions\location;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
class CityAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $data = [];
        $region = Yii::app()->request->getParam('region_id');
        /** @var \SxgeoRegions $Region */
        $Region = \SxgeoRegions::model()->findByPk($region);
        if($Region) {
            $query = Yii::app()->request->getParam('q');
            $criteria_search = new \CDbCriteria();
            $criteria_search->addSearchCondition('name_ru', $query, true, 'OR');
            $criteria_search->addSearchCondition('name_en', $query, true, 'OR');

            $criteria = new \CDbCriteria();
            $criteria->addCondition('region_id = :region_id');
            $criteria->mergeWith($criteria_search);
            $criteria->params = \CMap::mergeArray($criteria->params, [':region_id' => $Region->getId()]);

            /** @var \SxgeoCities $model */
            foreach (\SxgeoCities::model()->findAll($criteria) as $model) {
                $data[] = [
                    'id'            => $model->getId(),
                    'text'          => $model->getNameRu(),
                    'lat'           => str_replace(',', '.', $model->getLat()),
                    'lon'           => str_replace(',', '.', $model->getLon()),
                    'country'       => strtolower($Region->getCountry()),
                ];
            }
        }

        Yii::app()->ajax->addOther(['locations' => $data])->send();
    }
} 