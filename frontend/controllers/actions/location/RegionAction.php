<?php
namespace frontend\controllers\actions\location;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
class RegionAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $data = [];
        $country = Yii::app()->request->getParam('country_id');
        /** @var \SxgeoCountry $Country */
        $Country = \SxgeoCountry::model()->findByPk($country);
        if($Country) {
            $query = Yii::app()->request->getParam('q');
            $criteria_search = new \CDbCriteria();
            $criteria_search->addSearchCondition('name_ru', $query, true, 'OR');
            $criteria_search->addSearchCondition('name_en', $query, true, 'OR');

            $criteria = new \CDbCriteria();
            $criteria->addCondition('country = :country');
            $criteria->mergeWith($criteria_search);
            $criteria->params = \CMap::mergeArray($criteria->params, [':country' => $Country->getIso()]);

            /** @var \SxgeoRegions $model */
            foreach (\SxgeoRegions::model()->findAll($criteria) as $model) {
                $data[] = [
                    'id' => $model->getId(),
                    'text' => $model->getNameRu(),
                ];
            }
        }

        Yii::app()->ajax->addOther(['locations' => $data])->send();
    }
} 