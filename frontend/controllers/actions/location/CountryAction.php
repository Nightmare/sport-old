<?php
namespace frontend\controllers\actions\location;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
class CountryAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $query = Yii::app()->request->getParam('q');
        $criteria = new \CDbCriteria();
        $criteria->addSearchCondition('name_ru', $query, true, 'OR');
        $criteria->addSearchCondition('name_en', $query, true, 'OR');

        $data = [];
        /** @var \SxgeoCountry $model */
        foreach (\SxgeoCountry::model()->findAll($criteria) as $model) {
            $data[] = [
                'id' => $model->getId(),
                'text' => $model->getNameRu(),
                'lat' => str_replace(',', '.', $model->getLat()),
                'lon' => str_replace(',', '.', $model->getLon()),
            ];
        }

        Yii::app()->ajax->addOther(['locations' => $data])->send();
    }
} 