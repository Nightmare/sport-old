<?php
namespace frontend\controllers\actions\rack;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
class IndexAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        Yii::app()->static->setWww()
            ->registerCssFile('reception.css');

        Yii::app()->breadcrumbs->addBreadcrumb('Турники');

        $this->controller->render('index');
    }
} 