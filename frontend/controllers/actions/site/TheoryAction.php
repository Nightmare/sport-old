<?php
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use \common\helpers\StreetHelper;
class TheoryAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $this->controller->layout = 'noleft';

        $filter_list = [];
        $criteria = new CDbCriteria();
        $criteria->addCondition('dir = :dir');
        $criteria->params = [':dir' => Yii::app()->request->getParam('filter')];
        /** @var \Filter $Filter */
        $Filter = \Filter::model()->find($criteria);
        if($Filter)
            $filter_list[] = (int)$Filter->getId();

        $criteria = new EMongoCriteria();
        $criteria->sort = ['host_create_at' => 'desc', 'create_at' => 'desc'];
        $condition = [
            'is_deleted' => 0,
            'conference_id' => (int)StreetHelper::getConferenceByName(StreetHelper::STREET_STADIUM_TEORIA)
        ];
        if($filter_list)
            $condition['filter'] = ['$all' => $filter_list];
        $criteria->setCondition($condition);

        //\common\components\VarDumper::dump($criteria);die;

        $Pages = new EMongoPagination(Items::model()->count($criteria));
        $Pages->pageSize = 5;
        $Pages->applyLimit($criteria);

        $items = [];
        /** @var Items[] $Items */
        $Items = Items::model()->find($criteria, ['_id']);
        foreach ($Items as $Item)
            $items[$Item->getPrimaryKey()] = $Item->getPrimaryKey();
        unset($Items);

        //echo '<pre>';
        //var_dump($items);die;
        $criteria = new CDbCriteria();
        $criteria->addInCondition('unique_id', array_keys($items));
        $criteria->addCondition('`t`.conference_id = :conference_id');
        $criteria->with = ['user', 'userProfile'];
        $criteria->scopes = ['enable'];
        $criteria->params = \CMap::mergeArray($criteria->params, [':conference_id' => (int)StreetHelper::getConferenceByName(StreetHelper::STREET_STADIUM_TEORIA)]);

        $dependency = new CDbCacheDependency('SELECT MAX(update_at) FROM {{gallery_image}}');
        $dependency->reuseDependentData = true;
        /** @var GalleryImage[] $Images */
        $Images = GalleryImage::model()->cache(10000, $dependency)->findAll($criteria);
        foreach ($Images as $Image)
            $items[$Image->getUniqueId()] = $Image;

        $criteria->with[] = 'channel';

        $dependency = new CDbCacheDependency('SELECT MAX(update_at) FROM {{gallery_video}}');
        $dependency->reuseDependentData = true;
        /** @var GalleryVideo[] $Videos */
        $Videos = GalleryVideo::model()->cache(10000, $dependency)->findAll($criteria);
        foreach ($Videos as $Video)
            $items[$Video->getUniqueId()] = $Video;

        $params = [
            'models' => $items,
            'pages' => $Pages,
        ];
        if(Yii::app()->request->isAjaxRequest)
            Yii::app()->ajax->addHtml('_theory', '[data-js="replace-items-list"]', $params)->send();
        else {
            Yii::app()->breadcrumbs
                ->addBreadcrumb('СпортКомплекс', Yii::app()->createUrl('/site/reception', ['conference' => StreetHelper::STREET_STADIUM]))
                ->addBreadcrumb('Теория');

            $this->controller->render('theory', $params);
        }
    }
} 