<?php
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
class StreetAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        Yii::app()->static->setWww()->registerCssFile('street.css');
        Yii::app()->static->setOwn()->registerCssFile('street.css');

        Yii::app()->breadcrumbs->addBreadcrumb('Улица');

        $this->controller->render('index');
    }
} 