<?php
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
class FixAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        /** @var GalleryImage[] $Items */
        $Items = GalleryImage::model()->findAll();
        foreach ($Items as $Item)
            $Item->updateAction();

        /** @var GalleryVideo[] $Items */
        $Items = GalleryVideo::model()->findAll();
        foreach ($Items as $Item)
            $Item->updateAction();
    }
} 