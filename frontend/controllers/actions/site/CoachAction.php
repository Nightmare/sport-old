<?php
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
class CoachAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        Yii::app()->static->setWww()
            ->registerCssFile('mannequin.css')
            ->registerScriptFile('jquery.suggest.js');

        Yii::app()->breadcrumbs->addBreadcrumb('СпортКомплекс', Yii::app()->createUrl('/site/reception', ['conference' => \common\helpers\StreetHelper::STREET_STADIUM]))
            ->addBreadcrumb('Тренер');

        $this->controller->render('coach');
    }
} 