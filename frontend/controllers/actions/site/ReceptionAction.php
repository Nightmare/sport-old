<?php
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
class ReceptionAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        Yii::app()->static->setWww()
            ->registerCssFile('reception.css');

        Yii::app()->breadcrumbs->addBreadcrumb('СпортКомплекс');

        $this->controller->render('reception');
    }
} 