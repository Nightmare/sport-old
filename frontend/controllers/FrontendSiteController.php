<?php
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use \frontend\components\FrontendController;
class FrontendSiteController extends FrontendController
{
    public function filters() {
        return array(
            'ajaxOnly + registration, restore',
        );
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'index'      => ['class' => 'StreetAction'],
            'reception'  => ['class' => 'ReceptionAction'],
            'coach'      => ['class' => 'CoachAction'],
            'error'      => ['class' => 'SimpleErrorAction'],
            'simulators' => ['class' => 'SimulatorsAction'],
            'fix'        => ['class' => 'FixAction'],
            'cache'      => ['class' => 'CacheAction'],
            'info'       => ['class' => 'InfoAction'],
            'theory'     => ['class' => 'TheoryAction'],
		);
	}

    public function actionFlush()
    {
        Yii::app()->cache->flush();
    }
}