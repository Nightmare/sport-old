<?php
/**
 * @var $this \frontend\modules\conference\components\ConferenceBaseController
 * @var string $content
 */
?>
<?php $this->beginContent('frontend.www.themes.'.Yii::app()->theme->name.'.layouts.noright'); ?>
    <div id="conference">
        <div class="container">
            <?= $content; ?>
        </div>
        <aside class="left-sidebar">
            <div class="box conf-list" id="conf-category-list">
                <ul>
                    <?php foreach ($this->categories as $category): ?>
                        <li
                            class="conf-item"
                            data-dir="<?= $category->getDir(); ?>"
                            data-link="<?= Yii::app()->createUrl('/conference/post/list', ['conf_cat_dir' => $category->getDir(), 'conference' => $category->getConferenceString()]) ?>">

                            <div class="title"><?= $category->getTitle(); ?></div>
                            <div class="hint"><?= $category->getDescription(); ?></div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </aside>
    </div>
<?php $this->endContent(); ?>