<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <script type="text/javascript" src="<?= Yii::app()->static->setLibrary('jquery')->getLink('js/jquery-1.10.2.min.js'); ?>"></script>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <script>
        $(function(){$ajax.setCSRF(<?= CJavaScript::encode(Yii::app()->request->csrfToken); ?>);});
    </script>
</head>

<body>
<div class="wrapper" id="login">
    <?php echo $content; ?>
</div>
<div id="page-dark">
    <div id="page-loader">

    </div>
</div>
</body>
</html>