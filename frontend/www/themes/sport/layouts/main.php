<?php
/**
 * @var $this Controller
 * @var string $content
 */
?>
<?php $this->beginContent('frontend.www.themes.'.Yii::app()->theme->name.'.layouts.template'); ?>
    <div class="middle" id="middle">
        <?= $content; ?>
    </div>
<?php $this->endContent(); ?>