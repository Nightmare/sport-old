<?php
/**
 * @var \common\components\Controller $this
 * @var string $content
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7 <?= \common\helpers\SiteHelper::getBG() ?>"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8 <?= \common\helpers\SiteHelper::getBG() ?>"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 <?= \common\helpers\SiteHelper::getBG() ?>"> <![endif]-->
<!--[if gt IE 8]><!
<html class="no-js <?= \common\helpers\SiteHelper::getBG() ?>"> <![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script type="text/javascript" src="<?= Yii::app()->static->setLibrary('jquery')->getLink('js/jquery-1.10.2.min.js'); ?>"></script>
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
</head>
<body class="page-street <?= \common\helpers\SiteHelper::getBG() ?>">
<div class="page-wrapper <?= \common\helpers\SiteHelper::getBG() ?>">
    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
        your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="wrapper">
        <header class="header">
            <div class="header-content">
                <div class="logo">
                    <a href="#"><img src="<?= Yii::app()->static->imageLink('www/img/logo.png'); ?>" alt=""></a>
                </div>
                <?php $this->widget('\common\widgets\breadcrumb\BreadcrumbWidget'); ?>
            </div>
        </header>
        <?= $content; ?>
    </div>
    <footer class="footer" id="footer">
        <?php $this->widget('\common\widgets\panel\Panel'); ?>
    </footer>
</div>

<div id="page-dark">
    <div id="page-loader">

    </div>
</div>
<div class="modal fade" id="customModal" tabindex="-1" role="dialog" aria-labelledby="customModalLabel" aria-hidden="true">
    <div class="modal-dialog" id="replacement"></div>
</div>
</body>
</html>
