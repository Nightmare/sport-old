<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 08.02.2015
 * Time: 4:11
 *
 * @var ConferencePost[] $models
 * @var ConferenceCategory $conference_category
 */ ?>

<div id="replacement">
    <div id="pin" class="clearfix">
        <?php foreach ($models as $model):
            $classes = ['w', 'w2', 'w3'];
            $v =  rand(0, 2);

            ?>
            <div class="box-item">
                <div
                    data-type="ajax"
                    data-history="true"
                    data-link="<?= Yii::app()->createUrl('/conference/post/view', [
                        'post_dir' => $model->getDir(),
                        'conference' => $model->getConferenceString(),
                        'conf_cat_dir' => $conference_category->getDir()
                    ]) ?>"
                    class="item <?= $classes[$v]; ?>">
                    <div class="title"><?= $model->getTitle(); ?></div>
                    <div class="description"><?= $model->getDescription(); ?></div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>