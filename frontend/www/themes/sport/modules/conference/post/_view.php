<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 08.02.2015
 * Time: 4:11
 *
 * @var ConferencePost $model
 * @var \frontend\modules\conference\components\ConferenceBaseController $this
 */ ?>

<div id="replacement" class="clearfix">
    <?php $this->widget('\common\modules\comment\widgets\ListViewWidget', [
        'model' => $model,
        'view' => 'conference_post'
    ]); ?>
</div>