<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 08.02.2015
 * Time: 4:11
 *
 * @var \frontend\modules\conference\components\ConferenceBaseController $this
 * @var ConferenceCategory $conference_category
 * @var ConferencePost[] $models
 */ ?>
<main class="content conference">
    <div class="box" id="page_content">
        <?php $this->renderPartial('_list', $data) ?>
    </div>
</main>
<script>
    $(function(){
        $('#pin').masonry({
            itemSelector: '.box-item',
            isFitWidth: true,
            isResizeBound: true,
            columnWidth: $('#pin').width()/5
        });

        $('[data-dir="<?= Yii::app()->request->getParam('conf_cat_dir') ?>"]').addClass('active');
    });
</script>