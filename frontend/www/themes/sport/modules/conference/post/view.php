<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 08.02.2015
 * Time: 4:11
 *
 * @var \frontend\modules\conference\components\ConferenceBaseController $this
 * @var array $data
 * @var string $conf_cat_dir
 */ ?>
<main class="content">
    <div class="box" id="page_content">
        <?php $this->renderPartial('_view', $data) ?>
    </div>
</main>
<script>
    $(function(){
        $('[data-dir="<?= $conf_cat_dir ?>"]').addClass('active');
    });
</script>