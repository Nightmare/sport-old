<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 09.02.2015
 * Time: 2:52
 *
 * @var ConferenceCategory[] $categories
 */ ?>

<div class="box">
    <ul class="conf-list">
        <?php
        foreach ($categories as $category): ?>
            <li
                class="conf-item"
                data-dir="<?= $category->getDir(); ?>"
                data-link="<?= Yii::app()->createUrl('/conference/post/list', ['conf_cat_dir' => $category->getDir(), 'conference' => $category->getConferenceString()]) ?>">

                <div class="title"><?= $category->getTitle(); ?></div>
                <div class="hint"><?= $category->getDescription(); ?></div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>