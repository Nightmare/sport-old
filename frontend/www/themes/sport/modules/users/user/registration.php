<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 02.12.13
 * Time: 16:44
 *
 * @var User $model
 * @var TbActiveForm $form
 * @var Controller $this
 */ ?>
<div class="form shadow border-radius" data-replace="true" id="registration">
    <?php /** @var TbActiveForm $form */
    $form = $this->beginWidget(
        'common\widgets\booster\ActiveForm',
        [
            'id' => 'form-registration',
            'action' => Yii::app()->createUrl('/registration'),
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            'htmlOptions' => [
                'class' => 'ajax'
            ],
            'clientOptions' => [
                'validateOnChange' => false
            ]
        ]
    ); ?>
    <div class="form-title">
        <h2><?= Yii::t('labels', 'Регистрация'); ?></h2>
    </div>

    <div class="form-body">
        <fieldset>
            <?= $form->textFieldGroup(
                $model,
                'username',
                [
                    'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                    'widgetOptions' => [
                        'htmlOptions' => ['class' => 'field']
                    ],
                    'labelClass' => 'col-sm-5',
                ]
            ); ?>
            <?= $form->textFieldGroup(
                $model,
                'email',
                [
                    'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                    'widgetOptions' => [
                        'htmlOptions' => ['class' => 'field']
                    ],
                    'labelClass' => 'col-sm-5',
                ]
            ); ?>
            <?= $form->passwordFieldGroup(
                $model,
                'password',
                [
                    'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                    'widgetOptions' => [
                        'htmlOptions' => ['class' => 'field']
                    ],
                    'labelClass' => 'col-sm-5',
                ]
            ); ?>
            <?= $form->passwordFieldGroup(
                $model,
                'passwordConfirm',
                [
                    'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                    'widgetOptions' => [
                        'htmlOptions' => ['class' => 'field']
                    ],
                    'labelClass' => 'col-sm-5',
                ]
            ); ?>
        </fieldset>
    </div>

    <div class="form-actions">
        <div>
            <?php $this->widget(
                'booster.widgets.TbButton',
                [
                    'context' => 'primary',
                    'label' => Yii::t('labels', 'Зарегистрироваться'),
                    'htmlOptions' => [
                        'data-for' => 'form-registration',
                        'data-submit' => 'ajax'
                    ],
                ]
            ); ?>
            <?php $this->widget(
                'booster.widgets.TbButton',
                [
                    'label' => Yii::t('labels', 'Отмена'),
                    'htmlOptions' => [
                        'data-link' => Yii::app()->createUrl('/login'),
                        'class' => 'ajax-link'
                    ],
                ]
            ); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>