<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 02.12.13
 * Time: 16:44
 *
 * @var User $model
 * @var TbActiveForm $form
 * @var Controller $this
 */ ?>
<div class="form shadow border-radius" data-replace="true" id="login">
    <?php /** @var TbActiveForm $form */
    $form = $this->beginWidget(
        '\common\widgets\booster\ActiveForm',
        [
            'id' => 'form-login',
            'action' => Yii::app()->createUrl('/login'),
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            'htmlOptions' => [
                'class' => 'ajax'
            ],
            'clientOptions' => [
                'validateOnChange' => false
            ]
        ]
    ); ?>
    <div class="form-title">
        <h2><?= Yii::t('labels', 'Авторизоваться'); ?></h2>
    </div>

    <div class="form-body">
        <fieldset>
            <?= $form->textFieldGroup(
                $model,
                'username',
                [
                    'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                    'widgetOptions' => [
                        'htmlOptions' => ['class' => 'field']
                    ],
                    'labelClass' => 'col-sm-5',
                ]
            ); ?>
            <?= $form->passwordFieldGroup(
                $model,
                'password',
                [
                    'wrapperHtmlOptions' => ['class' => 'col-sm-6'],
                    'widgetOptions' => [
                        'htmlOptions' => ['class' => 'field']
                    ],
                    'labelClass' => 'col-sm-5',
                ]
            ); ?>
        </fieldset>
    </div>

    <div class="form-actions">
        <div>
            <?php $this->widget(
                'booster.widgets.TbButton',
                [
                    'context' => 'primary',
                    'label' => Yii::t('labels', 'Войти'),
                    'htmlOptions' => [
                        'data-for' => 'form-login',
                        'data-submit' => 'ajax'
                    ],
                ]
            ); ?>
            <?php $this->widget(
                'booster.widgets.TbButton',
                [
                    'label' => Yii::t('labels', 'Регистрация'),
                    'htmlOptions' => [
                        'data-link' => Yii::app()->createUrl('/registration'),
                        'class' => 'ajax-link'
                    ],
                ]
            ); ?>
        </div>
        <div class="restore">
            <?= CHtml::link(Yii::t('labels', 'Восстановить пароль')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>