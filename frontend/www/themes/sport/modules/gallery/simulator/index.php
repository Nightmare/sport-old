<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 16.09.2014
 * Time: 14:48
 *
 * @var $this \common\components\Controller
 * @var GalleryImage[] $models
 * @var CPagination $pages
 * @var Filter $filter
 */ ?>
<div class="container">
    <main class="content" id="items_list" data-js="replace-items-list">
        <ul class="item-list clearfix bottom-margin side-margin">
            <?= $this->renderPartial('_index', ['models' => $models, 'pages' => $pages], true) ?>
        </ul>
        <div class="pagerWrapper">
            <?php $this->widget('\common\extensions\pagination\Pagination', [
                'pages' => $pages,
                'cssFile' => Yii::app()->static->setOwn()->getLink('css/paging.css')
            ]); ?>
        </div>
    </main>
</div>
<aside class="right-sidebar">
    <?php $this->widget('\common\widgets\filters\AssignWidget', [
        'parent_id' => $filter->getId(),
        'link' => Yii::app()->createUrl('/gallery/simulator/list', ['filter_dir' => $filter->getDir(), 'conference' => \common\helpers\StreetHelper::STREET_STADIUM]),
    ]) ?>
</aside>
<script>
    $('[data-js="replace-items-list"]').infinitescroll({
        navSelector  : ".pager-list",
        nextSelector : ".pager-list .next-item a",
        dataType: 'json',
        appendCallback: false,
        maxPage: <?= $pages->getPageCount() ?>
    }, function(json, opts){
        var $ul = $('[data-js="replace-items-list"]').find('ul.item-list');
        $.each(json.htmlList, function(i, el){
            $ul.append(el.view);
        });

        return false;
    });
</script>