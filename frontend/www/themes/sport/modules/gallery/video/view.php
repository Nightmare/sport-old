<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 08.10.2014
 * Time: 15:55
 *
 * @var GalleryVideo $model
 * @var \frontend\components\FrontendController $this
 * @var boolean $isPocket
 * @var boolean $isReport
 */ ?>

<?php $this->widget('\common\modules\comment\widgets\ListViewWidget', [
    'model' => $model
]); ?>
<aside class="left-sidebar">
    <div class="box clearfix">
        <div class="video-content">
            <div class="video-item video-item-adv" id="sticker">
                <div class="video-item-title"><?= $model->getTitle(); ?></div>
                <div class="video-item-inner standard">
                    <a class="video_fancy" href="<?= $model->getSourceLink(); ?>">
                        <img src="<?= Yii::app()->static->imageLink($model->getMinImage()); ?>" alt="<?= $model->getTitle() ?>">
                    </a>
                </div>
                <div class="video-item-bottom">
                    <span class="video-item-watch-site-lite"><?= $model->getViewCount() ?></span>
                    <span class="video-item-watch-youtube"><?= $model->getHostView() ?></span>
                </div>
            </div>
            <div class="tag-list">
                <!--Теги: грудь, плечи, трицепс, пресс-->
            </div>
        </div>
        <div class="video-info">
            <div class="info">
                <div class="pull-left avatar">
                    <img width="54" height="54" src="<?= $model->getChannelImage(false) ?>" alt="">
                </div>
                <a target="_blank" href="<?= $model->channel_id ? $model->channel->getLink() : 'javascript:void(0);';  ?>" class="username"><?= $model->channel_id ? $model->channel->getTitle() : $model->user->getUsername(); ?></a>
                <img class="brand-list" src="<?= Yii::app()->static->imageLink('www/img/video/logos.png') ?>" alt="logos">
            </div>
            <div class="description">
                <?= $model->getDescription() ?>
            </div>
            <a id="add_comment" class="btn-action comment" href=""><?= Yii::t('labels', 'Добавить комментарий') ?></a>
            <?php if(!$isPocket): ?><a href="javascript:void(0);" id="add_pocket" data-link="<?=  Yii::app()->createUrl('/pocket/video/add', ['video_dir' => $model->getDir()])?>" class="btn-action pocket"><?= Yii::t('labels', 'Добавить в карман') ?></a><?php endif; ?>
            <?php if(!$isReport): ?>
                <a id="report" href="javascript:void(0);" class="btn-action report" data-type="ajax" data-link="<?= Yii::app()->createUrl('/report/video/add', ['video_dir' => $model->getDir()]) ?>">
                    <?= Yii::t('labels', 'Пожаловаться') ?>
                </a>
            <?php endif; ?>
        </div>

        <!--<ul class="video-list col-3">
            <li class="video-item">
                <div class="video-item-inner">
                    <a href="#">
                        <img src="<?= Yii::app()->static->imageLink('www/img/video/holder.jpg') ?>" alt="holder">
                        <span class="time-line">12:12</span>
                        <span class="brand"><img src="<?= Yii::app()->static->imageLink('www/img/video/adidas.jpg') ?>" alt=""></span>
                    </a>
                </div>
            </li>
            <li class="video-item">
                <div class="video-item-inner">
                    <a href="#">
                        <img src="<?= Yii::app()->static->imageLink('www/img/video/holder.jpg') ?>" alt="holder">
                        <span class="time-line">12:12</span>
                        <span class="brand"><img src="<?= Yii::app()->static->imageLink('www/img/video/adidas.jpg') ?>" alt=""></span>
                    </a>
                </div>
            </li>
            <li class="video-item">
                <div class="video-item-inner">
                    <a href="#"><img src="<?= Yii::app()->static->imageLink('www/img/video/holder.jpg') ?>" alt="holder"></a>
                </div>
            </li>
            <li class="video-item">
                <div class="video-item-inner">
                    <a href="#"><img src="<?= Yii::app()->static->imageLink('www/img/video/holder.jpg') ?>" alt="holder"></a>
                </div>
            </li>
            <li class="video-item">
                <div class="video-item-inner">
                    <a href="#"><img src="<?= Yii::app()->static->imageLink('www/img/video/holder.jpg') ?>" alt="holder"></a>
                </div>
            </li>
            <li class="video-item">
                <div class="video-item-inner">
                    <a href="#"><img src="<?= Yii::app()->static->imageLink('www/img/video/holder.jpg') ?>" alt="holder"></a>
                </div>
            </li>
        </ul>-->
    </div>
</aside><!-- .left-sidebar -->
<script>
    $(function(){
        $(document).ready(function(){
            $("#sticker").sticky({topSpacing:0});
        });
        $(document.body).on('click', '#add_pocket', function(event){
            event.preventDefault();

            var $self = $(this);
            var callback = function(response){
                if(response.ok !== undefined)
                    $('#add_pocket').remove();
            };

            $ajax.send($self.data('link'), null, null, null, null, null, null, callback);
        });
    });
</script>