<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 02.10.2014
 * Time: 16:04
 *
 * @var GalleryVideo $models
 * @var Controller $this
 * @var CPagination $pages
 */ ?>

<?php foreach($models as $model): ?>
    <li class="video-item video-item-adv">
        <?= $this->renderPartial('_item', ['model' => $model], true); ?>
    </li>
<?php endforeach; ?>