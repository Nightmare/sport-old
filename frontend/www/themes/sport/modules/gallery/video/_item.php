<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 02.10.2014
 * Time: 14:43
 *
 * @var string $logo
 * @var GalleryVideo $model
 */ ?>

<div class="video-item-title"><?= $model->getTitle(); ?></div>
<div class="video-item-inner standard">
    <a href="<?= Yii::app()->createUrl('/gallery/video/view', ['video_dir' => $model->getDir(), 'conference' => $model->getConferenceString()]); ?>">
        <img src="<?= Yii::app()->static->imageLink($model->getMinImage()); ?>" alt="">
        <span class="brand"><?= $model->getChannelImage(true); ?></span>
    </a>
</div>
<div class="video-item-bottom">
    <span class="video-item-watch-site-lite"><?= $model->getViewCount() ?></span>
    <span class="video-item-watch-youtube"><?= $model->getHostView() ?></span>
</div>