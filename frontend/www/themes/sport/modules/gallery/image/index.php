<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 16.09.2014
 * Time: 14:48
 *
 * @var $this \common\components\Controller
 * @var GalleryImage[] $models
 * @var CPagination $pages
 */ ?>
<main class="content" data-js="replace-items-list">
    <ul class="image-list bottom-margin side-margin">
        <?= $this->renderPartial('_index', ['models' => $models, 'pages' => $pages], true) ?>
    </ul>
    <div class="pagerWrapper">
        <?php $this->widget('\common\extensions\pagination\Pagination', [
            'pages' => $pages,
            'cssFile' => Yii::app()->static->setOwn()->getLink('css/paging.css')
        ]); ?>
    </div>
</main>
<script>
    $('[data-js="replace-items-list"]').infinitescroll({
        navSelector  : ".pager-list",
        nextSelector : ".pager-list .next-item a",
        dataType: 'json',
        appendCallback: false,
        maxPage: <?= $pages->getPageCount() ?>
    }, function(json, opts){
        var $ul = $('[data-js="replace-items-list"]').find('ul.image-list');
        $.each(json.htmlList, function(i, el){
            $ul.append(el.view);
        });

        return false;
    });
</script>