<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 02.10.2014
 * Time: 14:43
 *
 * @var string $logo
 * @var GalleryImage $model
 */ ?>

<div class="image-item-inner standard">
    <a href="<?= Yii::app()->createUrl('/gallery/image/view', ['image_dir' => $model->getDir(), 'conference' => $model->getConferenceString()]); ?>">
        <img src="<?= Yii::app()->static->imageLink($model->getMinImage()); ?>" alt="">
        <span class="brand"><img src="<?= Yii::app()->static->imageLink('www/img/video/adidas.jpg'); ?>" alt=""></span>
    </a>
</div>
<div class="image-item-bottom">
    <div class="title">
        <?= CHtml::link($model->getTitle(), Yii::app()->createUrl('/gallery/image/view', ['image_dir' => $model->getDir(), 'conference' => $model->getConferenceString()])); ?>
    </div>
</div>