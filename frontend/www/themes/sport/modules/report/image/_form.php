<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 02.10.2014
 * Time: 0:00
 *
 * @var ReportImage $model
 * @var Controller $this
 * @var GalleryImage $image
 */ ?>
<div class="modal-dialog" id="replacement">
    <?php /** @var TbActiveForm $form */
    $form = $this->beginWidget(
        '\common\widgets\booster\ActiveForm',
        [
            'id' => 'report_form',
            'action' => Yii::app()->createUrl('/report/image/add', ['image_dir' => $image->getDir()]),
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            'htmlOptions' => [
                'class' => 'ajax'
            ],
            'clientOptions' => [
                'validateOnChange' => false
            ]
        ]
    ); ?>
    <div class="modal-content" style="">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel"><?= Yii::t('labels', 'Жалоба на изображение'); ?> "<?= $image->getTitle() ?>"</h4>
        </div>
        <div class="modal-body">
            <?= $form->textAreaGroup(
                $model,
                'text',
                [
                    'wrapperHtmlOptions' => ['class' => 'col-sm-12'],
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'class' => 'field',
                        ],
                    ],
                    'labelOptions' => ['label' => false],
                ]
            ); ?>
        </div>
        <div class="modal-footer buttons">
            <button type="button" class="btn btn-default sq" data-dismiss="modal"><?= Yii::t('labels', 'Отмена') ?></button>
            <button type="button" data-modal-selector="#customModal" class="btn btn-primary sq" data-submit="ajax" data-for="report_form"><?= Yii::t('labels', 'Сохранить') ?></button>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>