<?php
use \common\helpers\StreetHelper;
/**
 * Landing page view file
 *
 * @var FrontendSiteController $this
 */
?>
<div class="img-holder">
    <img src="<?= Yii::app()->static->imageLink('www/img/t.gif'); ?>" width="1600" height="580" alt="" usemap="#map"/>
    <ul class="action-list">
        <li class="arena-hover"></li>
        <li class="horizontal-bars-hover"></li>
        <li class="shop-hover"></li>
        <li class="stadium-hover"></li>
    </ul>
</div>

<map name="map">
    <area href="#" class="area-arena" shape="poly" coords="1129, 305, 1128, 400, 1204, 419, 1254, 429, 1285, 428, 1365, 419, 1368, 412, 1457, 396, 1444, 384, 1443, 276, 1372, 273, 1352, 274, 1352, 284, 1309, 272, 1250, 271, 1197, 277, 1153, 291" />
    <area href="#" class="area-shop" shape="poly" coords="915, 360, 913, 278, 936, 255, 963, 246, 995, 246, 1027, 251, 1053, 251, 1081, 247, 1092, 247, 1090, 397, 1079, 390" />
    <area href="<?= StreetHelper::getConferenceLink(StreetHelper::STREET_RACK); ?>" class="area-horizontal-bars" shape="poly" coords="566, 363, 568, 340, 588, 340, 588, 346, 600, 346, 603, 350, 620, 350, 614, 270, 705, 266, 743, 274, 743, 352, 708, 361, 658, 374, 659, 360, 631, 364, 610, 370, 599, 371, 586, 375" />
    <area href="<?= StreetHelper::getConferenceLink(StreetHelper::STREET_STADIUM); ?>" class="area-stadium" shape="poly" coords="287, 295, 285, 396, 288, 398, 295, 398, 309, 405, 323, 406, 371, 410, 385, 410, 411, 410, 434, 406, 444, 406, 464, 412, 497, 405, 556, 395, 557, 334, 550, 333, 550, 325, 475, 325, 471, 332, 463, 332, 464, 295, 459, 295, 449, 272, 329, 270, 301, 272, 296, 286" />
</map>

<script>
    $(document).ready(function () {
        var areaAction = function ($target, action) {
            var areaClass = $.trim($target.attr('class')).replace(/^area\-/i, '');
            $('.' + areaClass + '-hover')[action]('fast');
        };

        $('map').hover(
            function (e) {
                areaAction($(e.target), 'fadeIn');
            },
            function (e) {
                areaAction($(e.target), 'fadeOut');
            }
        );
    });
</script>

