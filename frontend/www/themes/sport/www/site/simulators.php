<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 23.10.2014
 * Time: 17:40
 */ ?>
<main class="content">
    <ul class="image-list bottom-margin side-margin">
        <?php foreach(Yii::app()->categories->getFilterByPage(\common\helpers\StreetHelper::PAGE_SIMULATORS) as $filter): ?>
            <li class="image-item">
                <div class="image-item-inner">
                    <a href="<?= Yii::app()->createUrl('/gallery/simulator/list', ['filter_dir' => $filter->getDir(), 'conference' => \common\helpers\StreetHelper::STREET_STADIUM]) ?>">
                        <img src="<?= Yii::app()->static->imageLink($filter->getHImageLink()) ?>" alt="<?= $filter->getTitle() ?>">
                    </a>
                </div>
                <div class="image-item-title">
                    <a href="<?= Yii::app()->createUrl('/gallery/simulator/list', ['filter_dir' => $filter->getDir(), 'conference' => \common\helpers\StreetHelper::STREET_STADIUM]) ?>">
                        <?= $filter->getTitle() ?>
                    </a>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</main>