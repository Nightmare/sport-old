<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 10.10.2014
 * Time: 20:13
 */ ?>

<div class="img-holder">
    <img src="<?= Yii::app()->static->imageLink('www/img/t.gif'); ?>" width="1800" height="600" alt="" usemap="#map">
    <ul class="action-list">
        <li class="simulators-hover" style="display: none;"></li>
        <li class="trainer-hover" style="display: none;"></li>
        <li class="theoretical-knowledge-hover" style="display: none;"></li>
    </ul>
</div>

<map name="map">
    <area href="<?= Yii::app()->createUrl('/site/simulators', ['conference' => \common\helpers\StreetHelper::STREET_STADIUM]); ?>" class="area-simulators" shape="poly" coords="1410, 428, 1327, 403, 1329, 217, 1410, 216">
    <area href="<?= Yii::app()->createUrl('/site/coach', ['conference' => \common\helpers\StreetHelper::STREET_STADIUM]); ?>" class="area-trainer" shape="poly" coords="1055, 338, 1056, 225, 1126, 227, 1127, 265, 1121, 286, 1124, 300, 1126, 316, 1127, 339">
    <area href="<?= Yii::app()->createUrl('/site/theory', ['conference' => \common\helpers\StreetHelper::STREET_STADIUM]); ?>" class="area-theoretical-knowledge" shape="poly" coords="388, 380, 388, 176, 500, 198, 498, 337, 484, 339, 493, 367">
</map>

<script>
    $(document).ready(function () {
        var areaAction = function ($target, action) {
            var areaClass = $.trim($target.attr('class')).replace(/^area\-/i, '');
            $('.' + areaClass + '-hover')[action]('fast');
        };
        $('map').hover(
            function (e) {
                areaAction($(e.target), 'fadeIn');
            },
            function (e) {
                areaAction($(e.target), 'fadeOut');
            }
        );
    });
</script>
