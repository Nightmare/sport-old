<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 10.10.2014
 * Time: 20:29
 */ ?>

<div class="container">
    <main class="content">
        <div class="img-holder">
            <img src="<?= Yii::app()->static->imageLink('www/img/t.gif'); ?>" width="900" height="700" alt="" usemap="#map">
            <ul class="action-list">
                <li class="breast-hover" style="display: none;"></li>
                <li class="abdominal-hover" style="display: none;"></li>
                <li class="trapezius-hover" style="display: none;"></li>
                <li class="dorsi-hover" style="display: none;"></li>

                <li class="triceps-hover frontal" style="display: none;"></li>
                <li class="triceps-hover back" style="display: none;"></li>

                <li class="delta-hover frontal" style="display: none;"></li>
                <li class="delta-hover back" style="display: none;"></li>

                <li class="forearm-hover frontal" style="display: none;"></li>
                <li class="forearm-hover back" style="display: none;"></li>

                <li class="biceps-hover frontal" style="display: none;"></li>
                <li class="biceps-hover back" style="display: none;"></li>

                <li class="breech-hover" style="display: none;"></li>
                <li class="large-circular-hover"></li>

                <li class="latissimus-hover frontal" style="display: none;"></li>
                <li class="latissimus-hover back" style="display: none;"></li>

                <li class="calf-hover frontal" style="display: none;"></li>
                <li class="calf-hover back" style="display: none;"></li>

                <li class="hamstrings-hover" style="display: none;"></li>

                <li class="quadriceps-hover" style="display: none;"></li>
            </ul>
        </div>
        <map name="map">
            <!-- triceps -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 4]) ?>" class="area-triceps" shape="poly" coords="648, 212, 661, 206, 676, 206, 684, 209, 692, 220, 699, 234, 708, 21, 697, 243, 686, 243, 672, 239, 666, 230, 64, 223, 649, 221">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 4]) ?>" class="area-triceps" shape="poly" coords="08, 227, 01, 234, 486, 237, 467, 244, 476, 222, 470, 220, 486, 199, 48, 210, 11, 198, 23, 199, 33, 204, 27, 218">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 4]) ?>" class="area-triceps" shape="poly" coords="36, 189, 369, 198, 392, 227, 399, 228, 389, 21, 382, 203">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 4]) ?>" class="area-triceps" shape="poly" coords="179, 196, 178, 202, 164, 218, 10, 233, 14, 221, 162, 208">

            <!-- biceps -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 6]) ?>" class="area-biceps" shape="poly" coords="331, 203, 348, 220, 37, 241, 383, 236, 373, 223, 36, 210, 347, 206, 337, 199">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 6]) ?>" class="area-biceps" shape="poly" coords="211, 199, 198, 208, 183, 212, 17, 222, 167, 239, 171, 24, 184, 239, 196, 230, 20, 21, 214, 207">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 6]) ?>" class="area-biceps" shape="poly" coords="670, 243, 664, 232, 646, 220, 68, 234">

            <!-- delta -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 2]) ?>" class="area-delta" shape="poly" coords="238, 172, 218, 181, 218, 196, 214, 208, 201, 20, 182, 214, 179, 19, 187, 178, 204, 164, 220, 164">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 2]) ?>" class="area-delta" shape="poly" coords="330, 19, 34, 208, 364, 212, 369, 202, 369, 192, 360, 179, 30, 167, 328, 164, 321, 162, 309, 170, 323, 182">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 2]) ?>" class="area-delta" shape="poly" coords="617, 190, 632, 190, 646, 187, 63, 186, 667, 191, 662, 180, 60, 172, 638, 167, 628, 167, 628, 177">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 2]) ?>" class="area-delta" shape="poly" coords="492, 194, 14, 184, 34, 184, 4, 187, 61, 190, 3, 182, 42, 171, 2, 167, 12, 171, 499, 181">

            <!-- abdominal muscles -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 5]) ?>" class="area-abdominal" shape="poly" coords="260, 221, 23, 233, 233, 246, 233, 266, 233, 287, 239, 308, 20, 326, 26, 343, 269, 39, 277, 366, 282, 38, 289, 334, 292, 311, 292, 289, 292, 269, 292, 21, 289, 23, 278, 224">

            <!-- breast -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 3]) ?>" class="area-breast" shape="poly" coords="269, 217, 282, 229, 299, 230, 316, 223, 329, 193, 314, 170, 298, 173, 283, 17, 271, 176, 268, 198">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 3]) ?>" class="area-breast" shape="poly" coords="29, 216, 249, 22, 228, 230, 220, 224, 212, 207, 214, 193, 219, 180, 237, 17, 260, 174, 263, 194">

            <!-- forearm -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 7]) ?>" class="area-forearm" shape="poly" coords="397, 236, 379, 238, 366, 247, 361, 261, 33, 274, 366, 290, 377, 276, 391, 264, 394, 20, 399, 24">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 7]) ?>" class="area-forearm" shape="poly" coords="1, 231, 12, 241, 169, 243, 183, 26, 191, 274, 182, 292, 163, 278, 13, 29, 146, 248">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 7]) ?>" class="area-forearm" shape="poly" coords="486, 248, 492, 269, 499, 297, 10, 29, 00, 278, 498, 27">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 7]) ?>" class="area-forearm" shape="poly" coords="677, 23, 664, 267, 6, 288, 61, 292, 68, 297, 668, 290, 683, 273, 693, 24">

            <!-- calf -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 14]) ?>" class="area-calf" shape="poly" coords="614, 30, 608, 42, 99, 23, 602, 01, 607, 478, 608, 474, 613, 482, 622, 469, 633, 480, 640, 0, 641, 27, 637, 40, 618, 40">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 14]) ?>" class="area-calf" shape="poly" coords="77, 28, 77, 37, 6, 41, 3, 37, 48, 19, 46, 484, , 469, 6, 47, 69, 489, 72, 472, 79, 481, 91, 10, 93, 23, 88, 42">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 14]) ?>" class="area-calf" shape="poly" coords="272, 40, 266, 49, 269, 64">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 14]) ?>" class="area-calf" shape="poly" coords="211, 1, 219, 62, 220, 72, 213, 92, 210, 68">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 14]) ?>" class="area-calf" shape="poly" coords="271, 488, 267, 04, 264, 16, 270, 26, 277, 24, 281, 13, 280, 01">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 14]) ?>" class="area-calf" shape="poly" coords="238, 492, 229, 499, 217, 06, 213, 1, 211, 29, 211, 33, 219, 41, 229, 47, 23, 36, 240, 27, 240, 16, 237, 03">

            <!-- hamstrings -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 13]) ?>" class="area-hamstrings" shape="poly" coords="602, 481, 610, 468, 616, 49, 626, 474, 637, 484, 632, 49, 627, 44, 629, 426, 627, 412, 619, 39, 609, 388, 98, 391, 96, 407, 97, 439, 601, 46">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 13]) ?>" class="area-hamstrings" shape="poly" coords="48, 483, 4, 470, 61, 449, 66, 46, 80, 48, 82, 474, 79, 40, 79, 427, 80, 407, 77, 390, 70, 374, 4, 374, 42, 380, 38, 399, 37, 419, 40, 441, 44, 47">

            <!-- quadriceps -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 12]) ?>" class="area-quadriceps" shape="poly" coords="632, 438, 628, 418, 622, 399, 622, 371, 633, 390, 637, 413, 637, 43, 633, 40">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 12]) ?>" class="area-quadriceps" shape="poly" coords="39, 446, 34, 428, 34, 40, 38, 391, 28, 367, 24, 384, 23, 399, 27, 419">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 12]) ?>" class="area-quadriceps" shape="poly" coords="300, 442, 288, 447, 281, 460, 271, 46, 273, 427, 283, 397, 298, 362, 311, 338, 318, 36, 330, 371, 332, 399, 327, 42, 317, 44, 30, 461, 297, 449">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 12]) ?>" class="area-quadriceps" shape="poly" coords="206, 443, 219, 469, 229, 468, 232, 43, 23, 426, 230, 393, 232, 367, 238, 348, 214, 360, 200, 38, 193, 411, 194, 429">

            <!-- latissimus -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 9]) ?>" class="area-latissimus" shape="poly" coords="214, 211, 218, 217, 21, 22, 218, 238, 221, 21, 209, 23, 208, 21">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 9]) ?>" class="area-latissimus" shape="poly" coords="332, 202, 32, 219, 326, 224, 318, 242, 319, 24, 312, 270, 330, 242, 33, 226, 337, 210">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 9]) ?>" class="area-latissimus" shape="poly" coords="91, 271, 606, 282, 611, 290, 60, 303, 608, 311, 616, 299, 624, 292, 619, 279, 634, 22, 641, 231, 614, 233, 604, 243, 602, 24">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 9]) ?>" class="area-latissimus" shape="poly" coords="76, 273, , 282, 3, 304, 37, 28, 30, 272, 30, 264, 16, 236, 16, 221, 24, 228, 1, 232, 8, 233, 64, 21, 71, 260">

            <!-- trapezius -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 1]) ?>" class="area-trapezius" shape="poly" coords="73, 266, 66, 23, 60, 238, , 222, 62, 206, 62, 190, 38, 169, 64, 12, 79, 10, 96, 149, 602, 13, 616, 166, 626, 170, 634, 169, 629, 182, 613, 190, 616, 20, 613, 219, 616, 227, 609, 238, 606, 247, 91, 270, 91, 242, 92, 219, 92, 193, 90, 174, 92, 16, 82, 14, 78, 169, 82, 187, 82, 208, 80, 229, 77, 22">

            <!-- dorsi -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 8]) ?>" class="area-dorsi" shape="poly" coords="8, 208, 81, 222, 76, 20, 7, 262, 6, 26, 71, 272, 67, 289, 70, 310, 77, 338, 83, 336, 86, 318, 89, 291, 91, 277, 601, 28, 91, 267, 92, 248, 91, 22, 91, 208">

            <!-- large-circular -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 10]) ?>" class="area-large-circular" shape="poly" coords="613, 230, 626, 228, 63, 219, 636, 206, 643, 193, 64, 197, 648, 208, 646, 222, 641, 23, 62, 23">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 10]) ?>" class="area-large-circular" shape="poly" coords="24, 182, 36, 186, 42, 199, 43, 206, 39, 212, 40, 222, 49, 231, 4, 233, 37, 23, 23, 229, 16, 221, 28, 21, 34, 20, 31, 193">

            <!-- breech -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 11]) ?>" class="area-breech" shape="poly" coords="83, 340, 97, 323, 607, 319, 61, 334, 618, 39, 611, 389, 88, 390, 76, 373, 78, 31">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 11]) ?>" class="area-breech" shape="poly" coords="67, 374, 1, 372, 3, 362, 29, 341, 32, 323, 0, 309, 62, 318, 74, 340, 74, 31">
        </map>
    </main><!-- .content -->
</div>

<aside class="left-sidebar">
    <div class="box box-left">
        <form action="" class="form-search">
            <input type="text" style="height: 22px;" name="" placeholder="Какую мышцу ищите?">
            <input type="submit" value="тренировать">
        </form>

        <div class="holder">
            <img src="<?= Yii::app()->static->imageLink('www/img/mannequin/img-help.jpg'); ?>" alt="">
            <p>
                Наведите на интересующую вас групу мышщ, затем <strong>для выбора кликните по ней</strong>, либо
                воспользуйтесь списком мышц в правой части экрана
            </p>
        </div>
    </div>
</aside>

<aside class="right-sidebar">
    <div class="box box-right">
        <ul class="muscles-list">
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 2]) ?>" class="link-delta">дельты</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 4]) ?>" class="link-triceps">трицепс</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 6]) ?>" class="link-biceps">бицепс</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 3]) ?>" class="link-breast">мышци груди</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 5]) ?>" class="link-abdominal">мышци пресса</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 7]) ?>" class="link-forearm">предплечье</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 14]) ?>" class="link-calf">икры</a></li>
            <li class="separator">&nbsp;</li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 12]) ?>" class="link-quadriceps">квадрицепс</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 9]) ?>" class="link-latissimus">широчайшая</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 1]) ?>" class="link-trapezius">трапеция</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 8]) ?>" class="link-dorsi">длынные</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 10]) ?>" class="link-large-circular">большая круглая</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 11]) ?>" class="link-breech">ягодици</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['muscle' => 13]) ?>" class="link-hamstrings">бедра</a></li>
        </ul>
    </div>
</aside>

<script>
    $(document).ready(function () {
        var areaAction = function ($target, action) {
            var areaClass = $.trim($target.attr('class')).replace(/^(area|link)\-/i, '');
            $('.' + areaClass + '-hover')[action]('fast');
        };

        $(document)
            .on('mouseover', 'area, .muscles-list a', function(e) {
                areaAction($(e.target), 'fadeIn');
            })
            .on('mouseleave', 'area, .muscles-list a', function(e) {
                areaAction($(e.target), 'fadeOut');
            });

        $('.form-search :text').suggest($('.muscles-list a').map(function () { return $(this).text(); }), {
            suggestionColor : '#ccc',
            moreIndicatorClass: 'suggest-more',
            moreIndicatorText : '&hellip;'
        }).bind('keyup.suggest', function (e) {
            $('ul.muscles-list li a').trigger('mouseleave');
            var suggestions = $(this).data('suggestions');
            if(suggestions !== undefined && suggestions.all !== undefined) {
                $.each(suggestions.all, function(i, value){
                    $('ul.muscles-list li a:contains('+value+')').trigger('mouseenter');
                });
            }

            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 9 || code == 13) {
                console.log($(this).val());
            }
        });
    });
</script>
