<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 16.09.2014
 * Time: 14:48
 *
 * @var $this \common\components\Controller
 * @var GalleryImage[] $models
 * @var CPagination $pages
 * @var Filter $filter
 */ ?>
<div class="container">
    <main class="content" id="items_list" data-js="replace-items-list">
        <?= $this->renderPartial('_theory', ['models' => $models, 'pages' => $pages], true) ?>
    </main>
</div>
<aside class="right-sidebar">
    <?php $this->widget('\common\widgets\filters\PageFilterWidget', [
        'page_id' => \common\helpers\StreetHelper::PAGE_STADIUM_TEORIA,
        'link' => Yii::app()->createUrl('/site/theory', ['conference' => \common\helpers\StreetHelper::STREET_STADIUM]),
    ]) ?>
</aside>