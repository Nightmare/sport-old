<?php
use \common\helpers\StreetHelper;
/**
 * Created by PhpStorm.
 * User: me
 * Date: 10.10.2014
 * Time: 20:29
 */ ?>

<div class="container">
    <main class="content">
        <div class="img-holder">
            <img src="<?= Yii::app()->static->imageLink('www/img/t.gif'); ?>" width="900" height="700" alt="" usemap="#map">
            <ul class="action-list">
                <li class="breast-hover" style="display: none;"></li>
                <li class="abdominal-hover" style="display: none;"></li>
                <li class="trapezius-hover" style="display: none;"></li>
                <li class="dorsi-hover" style="display: none;"></li>

                <li class="triceps-hover frontal" style="display: none;"></li>
                <li class="triceps-hover back" style="display: none;"></li>

                <li class="delta-hover frontal" style="display: none;"></li>
                <li class="delta-hover back" style="display: none;"></li>

                <li class="forearm-hover frontal" style="display: none;"></li>
                <li class="forearm-hover back" style="display: none;"></li>

                <li class="biceps-hover frontal" style="display: none;"></li>
                <li class="biceps-hover back" style="display: none;"></li>

                <li class="breech-hover" style="display: none;"></li>
                <li class="large-circular-hover"></li>

                <li class="latissimus-hover frontal" style="display: none;"></li>
                <li class="latissimus-hover back" style="display: none;"></li>

                <li class="calf-hover frontal" style="display: none;"></li>
                <li class="calf-hover back" style="display: none;"></li>

                <li class="hamstrings-hover" style="display: none;"></li>

                <li class="quadriceps-hover" style="display: none;"></li>
            </ul>
        </div>
        <map name="map">
            <!-- triceps -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'triceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-triceps" shape="poly" coords="648, 212, 661, 206, 676, 206, 684, 209, 692, 220, 699, 234, 708, 251, 697, 243, 686, 243, 672, 239, 666, 230, 654, 223, 649, 221">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'triceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-triceps" shape="poly" coords="508, 227, 501, 234, 486, 237, 467, 244, 476, 222, 470, 220, 486, 199, 485, 210, 511, 198, 523, 199, 533, 204, 527, 218">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'triceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-triceps" shape="poly" coords="365, 189, 369, 198, 392, 227, 399, 228, 389, 215, 382, 203">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'triceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-triceps" shape="poly" coords="179, 196, 178, 202, 164, 218, 150, 233, 154, 221, 162, 208">

            <!-- biceps -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'biceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-biceps" shape="poly" coords="331, 203, 348, 220, 375, 241, 383, 236, 373, 223, 365, 210, 347, 206, 337, 199">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'biceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-biceps" shape="poly" coords="211, 199, 198, 208, 183, 212, 175, 222, 167, 239, 171, 245, 184, 239, 196, 230, 205, 215, 214, 207">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'biceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-biceps" shape="poly" coords="670, 243, 664, 232, 646, 220, 658, 234">

            <!-- delta -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'plechi', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-delta" shape="poly" coords="238, 172, 218, 181, 218, 196, 214, 208, 201, 205, 182, 214, 179, 195, 187, 178, 204, 164, 220, 164">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'plechi', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-delta" shape="poly" coords="330, 195, 354, 208, 364, 212, 369, 202, 369, 192, 360, 179, 350, 167, 328, 164, 321, 162, 309, 170, 323, 182">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'plechi', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-delta" shape="poly" coords="617, 190, 632, 190, 646, 187, 653, 186, 667, 191, 662, 180, 650, 172, 638, 167, 628, 167, 628, 177">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'plechi', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-delta" shape="poly" coords="492, 194, 514, 184, 534, 184, 545, 187, 561, 190, 553, 182, 542, 171, 525, 167, 512, 171, 499, 181">

            <!-- abdominal muscles -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'press', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-abdominal" shape="poly" coords="260, 221, 235, 233, 233, 246, 233, 266, 233, 287, 239, 308, 250, 326, 256, 343, 269, 359, 277, 366, 282, 358, 289, 334, 292, 311, 292, 289, 292, 269, 292, 251, 289, 235, 278, 224">

            <!-- breast -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'grud', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-breast" shape="poly" coords="269, 217, 282, 229, 299, 230, 316, 223, 329, 193, 314, 170, 298, 173, 283, 175, 271, 176, 268, 198">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'grud', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-breast" shape="poly" coords="259, 216, 249, 225, 228, 230, 220, 224, 212, 207, 214, 193, 219, 180, 237, 175, 260, 174, 263, 194">

            <!-- forearm -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'predpleche', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-forearm" shape="poly" coords="397, 236, 379, 238, 366, 247, 361, 261, 353, 274, 366, 290, 377, 276, 391, 264, 394, 250, 399, 245">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'predpleche', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-forearm" shape="poly" coords="155, 231, 152, 241, 169, 243, 183, 256, 191, 274, 182, 292, 163, 278, 153, 259, 146, 248">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'predpleche', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-forearm" shape="poly" coords="486, 248, 492, 269, 499, 297, 510, 295, 500, 278, 498, 257">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'predpleche', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-forearm" shape="poly" coords="677, 253, 664, 267, 655, 288, 651, 292, 658, 297, 668, 290, 683, 273, 693, 254">

            <!-- calf -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'ikry', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-calf" shape="poly" coords="614, 530, 608, 542, 599, 523, 602, 501, 607, 478, 608, 474, 613, 482, 622, 469, 633, 480, 640, 505, 641, 527, 637, 540, 618, 540">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'ikry', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-calf" shape="poly" coords="577, 528, 577, 537, 565, 541, 553, 537, 548, 519, 546, 484, 555, 469, 565, 475, 569, 489, 572, 472, 579, 481, 591, 510, 593, 523, 588, 542">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'ikry', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-calf" shape="poly" coords="272, 540, 266, 549, 269, 564">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'ikry', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-calf" shape="poly" coords="211, 551, 219, 562, 220, 572, 213, 592, 210, 568">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'ikry', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-calf" shape="poly" coords="271, 488, 267, 504, 264, 516, 270, 526, 277, 524, 281, 513, 280, 501">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'ikry', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-calf" shape="poly" coords="238, 492, 229, 499, 217, 506, 213, 515, 211, 529, 211, 533, 219, 541, 229, 547, 235, 536, 240, 527, 240, 516, 237, 503">

            <!-- hamstrings -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'biceps-bedra', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-hamstrings" shape="poly" coords="602, 481, 610, 468, 616, 459, 626, 474, 637, 484, 632, 459, 627, 445, 629, 426, 627, 412, 619, 395, 609, 388, 598, 391, 596, 407, 597, 439, 601, 456">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'biceps-bedra', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-hamstrings" shape="poly" coords="548, 483, 554, 470, 561, 449, 566, 465, 580, 485, 582, 474, 579, 450, 579, 427, 580, 407, 577, 390, 570, 374, 554, 374, 542, 380, 538, 399, 537, 419, 540, 441, 544, 457">

            <!-- quadriceps -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'kvadriceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-quadriceps" shape="poly" coords="632, 438, 628, 418, 622, 399, 622, 371, 633, 390, 637, 413, 637, 435, 633, 450">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'kvadriceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-quadriceps" shape="poly" coords="539, 446, 534, 428, 534, 405, 538, 391, 528, 367, 524, 384, 523, 399, 527, 419">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'kvadriceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-quadriceps" shape="poly" coords="300, 442, 288, 447, 281, 460, 271, 456, 273, 427, 283, 397, 298, 362, 311, 338, 318, 356, 330, 371, 332, 399, 327, 425, 317, 445, 305, 461, 297, 449">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'kvadriceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-quadriceps" shape="poly" coords="206, 443, 219, 469, 229, 468, 232, 453, 235, 426, 230, 393, 232, 367, 238, 348, 214, 360, 200, 385, 193, 411, 194, 429">

            <!-- latissimus -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'shirochajshaya-myshca', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-latissimus" shape="poly" coords="214, 211, 218, 217, 215, 225, 218, 238, 221, 251, 209, 235, 208, 215">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'shirochajshaya-myshca', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-latissimus" shape="poly" coords="332, 202, 325, 219, 326, 224, 318, 242, 319, 254, 312, 270, 330, 242, 335, 226, 337, 210">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'shirochajshaya-myshca', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-latissimus" shape="poly" coords="591, 271, 606, 282, 611, 290, 605, 303, 608, 311, 616, 299, 624, 292, 619, 279, 634, 252, 641, 231, 614, 233, 604, 243, 602, 254">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'shirochajshaya-myshca', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-latissimus" shape="poly" coords="576, 273, 555, 282, 553, 304, 537, 285, 530, 272, 530, 264, 516, 236, 516, 221, 524, 228, 551, 232, 558, 233, 564, 251, 571, 260">

            <!-- trapezius -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'trapeciya', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-trapezius" shape="poly" coords="573, 266, 566, 253, 560, 238, 555, 222, 562, 206, 562, 190, 538, 169, 564, 152, 579, 150, 596, 149, 602, 153, 616, 166, 626, 170, 634, 169, 629, 182, 613, 190, 616, 205, 613, 219, 616, 227, 609, 238, 606, 247, 591, 270, 591, 242, 592, 219, 592, 193, 590, 174, 592, 156, 582, 154, 578, 169, 582, 187, 582, 208, 580, 229, 577, 252">

            <!-- dorsi -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'dlinnye-spiny', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-dorsi" shape="poly" coords="585, 208, 581, 222, 576, 250, 575, 262, 565, 256, 571, 272, 567, 289, 570, 310, 577, 338, 583, 336, 586, 318, 589, 291, 591, 277, 601, 258, 591, 267, 592, 248, 591, 225, 591, 208">

            <!-- large-circular -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'bolshaya-kruglaya', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-large-circular" shape="poly" coords="613, 230, 626, 228, 635, 219, 636, 206, 643, 193, 654, 197, 648, 208, 646, 222, 641, 235, 625, 235">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'bolshaya-kruglaya', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-large-circular" shape="poly" coords="524, 182, 536, 186, 542, 199, 543, 206, 539, 212, 540, 222, 549, 231, 554, 233, 537, 235, 523, 229, 516, 221, 528, 215, 534, 205, 531, 193">

            <!-- breech -->
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'yagodicy', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-breech" shape="poly" coords="583, 340, 597, 323, 607, 319, 615, 334, 618, 359, 611, 389, 588, 390, 576, 373, 578, 351">
            <area href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'yagodicy', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="area-breech" shape="poly" coords="567, 374, 551, 372, 535, 362, 529, 341, 532, 323, 550, 309, 562, 318, 574, 340, 574, 351">
        </map>
    </main><!-- .content -->
</div>

<aside class="left-sidebar">
    <div class="box box-left">
        <form action="" class="form-search">
            <input type="text" style="height: 22px;" name="" placeholder="Какую мышцу ищите?">
            <input type="submit" value="тренировать">
        </form>

        <div class="holder">
            <img src="<?= Yii::app()->static->imageLink('www/img/mannequin/img-help.jpg'); ?>" alt="">
            <p>
                Наведите на интересующую вас групу мышщ, затем <strong>для выбора кликните по ней</strong>, либо
                воспользуйтесь списком мышц в правой части экрана
            </p>
        </div>
    </div>
</aside>

<aside class="right-sidebar">
    <div class="box box-right">
        <ul class="muscles-list">
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'plechi', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-delta">дельты</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'triceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-triceps">трицепс</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'biceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-biceps">бицепс</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'grud', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-breast">мышци груди</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'press', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-abdominal">мышци пресса</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'predpleche', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-forearm">предплечье</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'ikry', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-calf">икры</a></li>
            <li class="separator">&nbsp;</li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'kvadriceps', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-quadriceps">квадрицепс</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'shirochajshaya-myshca', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-latissimus">широчайшая</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'trapeciya', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-trapezius">трапеция</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'dlinnye-spiny', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-dorsi">длынные</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'bolshaya-kruglaya', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-large-circular">большая круглая</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'yagodicy', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-breech">ягодици</a></li>
            <li><a href="<?= Yii::app()->createUrl('/gallery/muscle/list', ['filter_dir' => 'biceps-bedra', 'conference' => StreetHelper::STREET_STADIUM]) ?>" class="link-hamstrings">бедра</a></li>
        </ul>
    </div>
</aside>

<script>
    $(document).ready(function () {
        var areaAction = function ($target, action) {
            var areaClass = $.trim($target.attr('class')).replace(/^(area|link)\-/i, '');
            $('.' + areaClass + '-hover')[action]('fast');
        };

        $(document)
            .on('mouseover', 'area, .muscles-list a', function(e) {
                areaAction($(e.target), 'fadeIn');
            })
            .on('mouseleave', 'area, .muscles-list a', function(e) {
                areaAction($(e.target), 'fadeOut');
            });

        $('.form-search :text').suggest($('.muscles-list a').map(function () { return $(this).text(); }), {
            suggestionColor : '#ccc',
            moreIndicatorClass: 'suggest-more',
            moreIndicatorText : '&hellip;'
        }).bind('keyup.suggest', function (e) {
            $('ul.muscles-list li a').trigger('mouseleave');
            var suggestions = $(this).data('suggestions');
            if(suggestions !== undefined && suggestions.all !== undefined) {
                $.each(suggestions.all, function(i, value){
                    $('ul.muscles-list li a:contains('+value+')').trigger('mouseenter');
                });
            }

            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 9 || code == 13) {
                console.log($(this).val());
            }
        });
    });
</script>
