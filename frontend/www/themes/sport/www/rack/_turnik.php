<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 02.10.2014
 * Time: 16:04
 *
 * @var GalleryImage $models
 * @var Controller $this
 * @var CPagination $pages
 */ ?>

<?php if(empty($models)): ?>
    <li class="empty_text">
        Материалы отсутствуют
    </li>
<?php endif; ?>
<?php foreach($models as $model):
    $type = null;
    if($model instanceof GalleryImage) $type = 'image';
    elseif($model instanceof GalleryVideo) $type = 'video';
    ?>
    <li class="<?= $type ?>-item <?= $type ?>-item-adv">
        <?= $this->renderPartial('frontend.www.themes.'.Yii::app()->theme->name.'.modules.gallery.'.$type.'._item', ['model' => $model], true); ?>
    </li>
<?php endforeach; ?>