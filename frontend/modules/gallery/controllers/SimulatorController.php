<?php
namespace frontend\modules\gallery\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use frontend\modules\gallery\components\GalleryBaseController;
use Yii;

class SimulatorController extends GalleryBaseController
{
    public function beforeRender($view)
    {
        $result = parent::beforeRender($view);
        return $result;
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'list' => ['class' => 'frontend\modules\gallery\controllers\actions\simulator\ListAction'],
		);
	}
}