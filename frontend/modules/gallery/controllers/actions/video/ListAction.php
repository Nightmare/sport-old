<?php
namespace frontend\modules\gallery\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\helpers\StreetHelper;
use Yii;
use CDbCriteria;
use GalleryVideo;
use CDbCacheDependency;
use CPagination;
class ListAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $this->controller->layout = 'fullsize';

        $conference = Yii::app()->request->getParam('conference');
        $conference_ids = StreetHelper::getConferenceTreeId($conference);

        $criteria = new CDbCriteria();
        $criteria->addInCondition('conference_id', $conference_ids);
        $criteria->with = ['user', 'userProfile', 'channel'];
        $criteria->scopes = ['enable'];
        $criteria->order = '`t`.host_create_at desc';

        $dependency = new CDbCacheDependency('SELECT MAX(update_at) FROM {{gallery_video}} where conference_id IN (:conference_ids)');
        $dependency->params = [':conference_ids' => implode($conference_ids)];
        $dependency->reuseDependentData = true;

        $pages = new CPagination(GalleryVideo::model()->cache(10000, $dependency)->count($criteria));
        $pages->pageSize = Yii::app()->params['pages']['video'];
        $pages->applyLimit($criteria);

        /** @var GalleryVideo[] $Videos */
        $Videos = GalleryVideo::model()->cache(10000, $dependency)->findAll($criteria);

        $params = ['models' => $Videos, 'pages' => $pages];
        if(Yii::app()->request->isAjaxRequest)
            Yii::app()->ajax->addHtml('_index', '[data-js="replace-items-list"]', $params)->send();
        else {
            Yii::app()->breadcrumbs
                ->addBreadcrumb(StreetHelper::getConferenceTitle($conference), StreetHelper::getConferenceLink($conference))
                ->addBreadcrumb('Видеозаписи');

            $this->controller->render('index', $params);
        }
    }
}