<?php
namespace frontend\modules\gallery\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCriteria;
use GalleryVideo;
use CDbCacheDependency;
use PocketVideo;
use ReportVideo;
class ViewAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $dir = Yii::app()->request->getParam('video_dir');
        $criteria = new CDbCriteria();
        $criteria->addCondition('`t`.dir = :dir');
        $criteria->with = ['user', 'userProfile', 'template', 'channel'];
        $criteria->scopes = ['enable'];
        $criteria->params = [':dir' => $dir];

        $dependency = new CDbCacheDependency('SELECT MAX(update_at) FROM {{gallery_video}} where dir = :dir');
        $dependency->params = [':dir' => $dir];
        $dependency->reuseDependentData = true;

        /** @var GalleryVideo $Video */
        $Video = GalleryVideo::model()->cache(10000, $dependency)->find($criteria);
        if(!$Video)
            \MException::ShowError(Yii::t('error', 'Видео не найдено'));

        $criteria = new CDbCriteria();
        $criteria->addCondition('`t`.user_id = :user_id');
        $criteria->addCondition('`t`.video_id = :video_id');
        $criteria->params = [':user_id' => Yii::app()->user->id, ':video_id' => $Video->getId()];
        $isPocket = PocketVideo::model()->count($criteria);
        $isReport = ReportVideo::model()->count($criteria);

        Yii::app()->breadcrumbs
            ->addBreadcrumb('Видеозаписи', Yii::app()->createUrl('/gallery/video/list', ['conference' => $Video->getConferenceString()]))
            ->addBreadcrumb($Video->getTitle());

        Yii::app()->static->setLibrary('stickyjs')
            ->registerScriptFile('jquery.sticky.js');

        $criteria = new CDbCriteria();
        $criteria->addCondition('video_id = :video_id');
        $criteria->params = [':video_id' => $Video->getId()];
        /** @var \SaverVideo $SaverVideo */
        $SaverVideo = \SaverVideo::model()->find($criteria);
        if(!$SaverVideo)
            $SaverVideo = new \SaverVideo();
        $SaverVideo->setVideoId($Video->getId())->setUserId(Yii::app()->user->id)->save();

        $this->controller->render('view', ['model' => $Video, 'isPocket' => $isPocket, 'isReport' => $isReport]);
    }
}