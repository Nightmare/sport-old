<?php
namespace frontend\modules\gallery\controllers\actions\muscle;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\components\VarDumper;
use common\helpers\StreetHelper;
use Yii;
use CDbCriteria;
use GalleryVideo;
use CDbCacheDependency;
use EMongoPagination;
use Items;
use EMongoCriteria;
use GalleryImage;
class ListAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $this->controller->layout = 'noleft';

        $criteria = new CDbCriteria();
        $criteria->addCondition('dir = :dir');
        $criteria->params = [':dir' => Yii::app()->request->getParam('filter_dir')];
        /** @var \Filter $FilterModel */
        $FilterModel = \Filter::model()->find($criteria);
        if(!$FilterModel)
            \MException::ShowError(Yii::t('error', 'Страница не найдена'), 404);

        $filter_list = [(int)$FilterModel->getId()];

        $criteria->params = [':dir' => Yii::app()->request->getParam('filter')];
        /** @var \Filter $Filter */
        $Filter = \Filter::model()->find($criteria);
        if($Filter)
            $filter_list[] = (int)$Filter->getId();

        $criteria = new EMongoCriteria();
        $criteria->sort = ['host_create_at' => 'desc', 'create_at' => 'desc'];
        $condition = [
            'is_deleted' => 0,
            'filter' => ['$all' => $filter_list],
            'conference_id' => (int)StreetHelper::getConferenceByName(StreetHelper::STREET_STADIUM)
        ];
        $criteria->setCondition($condition);

        $Pages = new EMongoPagination(Items::model()->count($criteria));
        $Pages->pageSize = 40;
        $Pages->applyLimit($criteria);

        $items = [];
        /** @var Items[] $Items */
        $Items = Items::model()->find($criteria, ['_id']);
        foreach ($Items as $Item)
            $items[$Item->getPrimaryKey()] = $Item->getPrimaryKey();
        unset($Items);

        $criteria = new CDbCriteria();
        $criteria->addInCondition('unique_id', array_keys($items));
        $criteria->addCondition('`t`.conference_id = :conference_id');
        $criteria->with = ['user', 'userProfile'];
        $criteria->scopes = ['enable'];
        $criteria->params = \CMap::mergeArray($criteria->params, [':conference_id' => (int)StreetHelper::getConferenceByName(StreetHelper::STREET_STADIUM)]);

        $dependency = new CDbCacheDependency('SELECT MAX(update_at) FROM {{gallery_image}}');
        $dependency->reuseDependentData = true;
        /** @var GalleryImage[] $Images */
        $Images = GalleryImage::model()->cache(10000, $dependency)->findAll($criteria);
        foreach ($Images as $Image)
            $items[$Image->getUniqueId()] = $Image;

        $criteria->with[] = 'channel';

        $dependency = new CDbCacheDependency('SELECT MAX(update_at) FROM {{gallery_video}}');
        $dependency->reuseDependentData = true;
        /** @var GalleryVideo[] $Videos */
        $Videos = GalleryVideo::model()->cache(10000, $dependency)->findAll($criteria);
        foreach ($Videos as $Video)
            $items[$Video->getUniqueId()] = $Video;

        $params = [
            'models' => $items,
            'pages' => $Pages,
            'filter' => $FilterModel
        ];
        if(Yii::app()->request->isAjaxRequest)
            Yii::app()->ajax->addHtml('_index', '[data-js="replace-items-list"]', $params)->send();
        else {
            Yii::app()->breadcrumbs
                ->addBreadcrumb('СпортКомплекс', Yii::app()->createUrl('/site/reception', ['conference' => StreetHelper::STREET_STADIUM]))
                ->addBreadcrumb('Тренер', Yii::app()->createUrl('/site/coach', ['conference' => StreetHelper::STREET_STADIUM]))
                ->addBreadcrumb($FilterModel->getTitle());

            $this->controller->render('index', $params);
        }
    }
}