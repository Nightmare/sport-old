<?php
namespace frontend\modules\gallery\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\components\VarDumper;
use common\helpers\StreetHelper;
use Yii;
use CDbCriteria;
use GalleryImage;
use CDbCacheDependency;
use CPagination;
class ListAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $this->controller->layout = 'fullsize';
        $conference = Yii::app()->request->getParam('conference');
        $conference_ids = StreetHelper::getConferenceTreeId($conference);

        $criteria = new CDbCriteria();
        $criteria->addInCondition('conference_id', $conference_ids);
        $criteria->with = ['user', 'userProfile'];
        $criteria->scopes = ['enable'];

        $dependency = new CDbCacheDependency('SELECT MAX(update_at) FROM {{gallery_image}} where conference_id IN (:conference_ids)');
        $dependency->params = [':conference_ids' => implode($conference_ids)];
        $dependency->reuseDependentData = true;

        $pages = new CPagination(GalleryImage::model()->cache(10000, $dependency)->count($criteria));
        $pages->pageSize = Yii::app()->params['pages']['image'];
        $pages->applyLimit($criteria);

        /** @var GalleryImage[] $Images */
        $Images = GalleryImage::model()->cache(10000, $dependency)->findAll($criteria);

        $params = ['models' => $Images, 'pages' => $pages];
        if(Yii::app()->request->isAjaxRequest)
            Yii::app()->ajax->addHtml('_index', '[data-js="replace-items-list"]', $params)->send();
        else {
            Yii::app()->breadcrumbs
                ->addBreadcrumb(StreetHelper::getConferenceTitle($conference), StreetHelper::getConferenceLink($conference))
                ->addBreadcrumb('Фотографии');

            $this->controller->render('index', $params);
        }
    }
}