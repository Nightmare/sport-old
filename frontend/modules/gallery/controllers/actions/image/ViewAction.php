<?php
namespace frontend\modules\gallery\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCriteria;
use GalleryImage;
use CDbCacheDependency;
use PocketImage;
use ReportImage;
use common\helpers\StreetHelper;
class ViewAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $dir = Yii::app()->request->getParam('image_dir');
        $criteria = new CDbCriteria();
        $criteria->addCondition('`t`.dir = :dir');
        $criteria->with = ['user', 'userProfile'];
        $criteria->scopes = ['enable'];
        $criteria->params = [':dir' => $dir];

        $dependency = new CDbCacheDependency('SELECT MAX(update_at) FROM {{gallery_image}} where dir = :dir');
        $dependency->params = [':dir' => $dir];
        $dependency->reuseDependentData = true;

        /** @var GalleryImage $Image */
        $Image = GalleryImage::model()->cache(10000, $dependency)->find($criteria);
        if(!$Image)
            \MException::ShowError(Yii::t('error', 'Фотография не найдена'));

        $criteria = new CDbCriteria();
        $criteria->addCondition('`t`.image_id = :image_id');
        $criteria->scopes = ['own'];
        $criteria->params = [':image_id' => $Image->getId()];
        $isReport = ReportImage::model()->count($criteria);

        $criteria->scopes = ['own', 'enable'];
        $isPocket = PocketImage::model()->count($criteria);

        Yii::app()->breadcrumbs
            ->addBreadcrumb('Фотографии', Yii::app()->createUrl('/gallery/image/list', ['conference' => $Image->getConferenceString()]))
            ->addBreadcrumb($Image->getTitle());

        $criteria = new CDbCriteria();
        $criteria->addCondition('image_id = :image_id');
        $criteria->params = [':image_id' => $Image->getId()];
        /** @var \SaverImage $SaverImage */
        $SaverImage = \SaverImage::model()->find($criteria);
        if(!$SaverImage)
            $SaverImage = new \SaverImage();
        $SaverImage->setImageId($Image->getId())->setUserId(Yii::app()->user->id)->save();

        $this->controller->render('view', ['model' => $Image, 'isPocket' => $isPocket, 'isReport' => $isReport]);
    }
}