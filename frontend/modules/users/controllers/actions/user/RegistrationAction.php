<?php
namespace frontend\modules\users\controllers\actions\user;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use User;
class RegistrationAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
//        Yii::app()->ajax->addErrors('Регистрация временно отключена')->send();

        $model = new User('registration');

        $post = Yii::app()->request->getPost('User');
        if($post) {
            $model->setAttributes($post);
            if($model->registrationUser()) {
                Yii::app()->ajax->addMessage(Yii::t('validation', 'Вы успешно зарегистрировались'));
            }
        } else {
            Yii::app()->ajax
                ->addReplace('registration', '[data-replace="true"]', ['model' => $model])
                ->addOther(['ok' => true]);
        }

        \Yii::app()->ajax->send();
    }
}