<?php
namespace frontend\modules\users\controllers\actions\user;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use User;
class LoginAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $this->controller->layout = 'loginlay';

        $model = new \LoginFormFrontend('login');
        $post = Yii::app()->request->getPost('LoginFormFrontend');
        if(Yii::app()->request->isAjaxRequest) {
            if($post) {
                $model->setAttributes($post);
                if($model->validate() && $model->login()) {
                    Yii::app()->ajax->addMessage(Yii::t('validation', 'Вы успешно авторизовались'))
                        ->setUrl(Yii::app()->createUrl('/site/index'));
                } else
                    Yii::app()->ajax->addErrors($model);
            } else {
                Yii::app()->ajax
                    ->addReplace('login', '[data-replace="true"]', ['model' => $model])
                    ->addOther(['ok' => true]);
            }

            \Yii::app()->ajax->send();
        } else
            $this->controller->render('login', ['model' => $model ]);
    }
}