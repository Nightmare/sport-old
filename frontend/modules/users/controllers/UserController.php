<?php
namespace frontend\modules\users\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use frontend\modules\users\components\UserBaseController;
use Yii;
class UserController extends UserBaseController
{
    public function filters() {
        return array(
            'ajaxOnly + registration, restore',
        );
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'login' => ['class' => 'frontend\modules\users\controllers\actions\user\LoginAction'],
            'registration' => ['class' => 'frontend\modules\users\controllers\actions\user\RegistrationAction'],
            'logout' => ['class' => 'LogoutAction'],
		);
	}

    public function beforeAction($action)
    {
        Yii::app()->static->setOwn()
            ->registerScriptFile('script.js');

        Yii::app()->static->setLibrary('needim-noty')
            ->registerScriptFile('jquery.noty.packaged.min.js');

        Yii::app()->static->setLibrary('fancybox')
            ->registerCssFile('jquery.fancybox.css')
            ->registerScriptFile('jquery.fancybox.pack.js')
            ->registerScriptFile('jquery.fancybox-media.js');

        Yii::app()->static->setLibrary('jquery')
            ->registerScriptFile('jquery-ui.min.js');

        Yii::app()->static->setLibrary('scroll')
            ->registerScriptFile('scrollTo.js');

        Yii::app()->static->setOwn()
            ->registerCssFile('form.css')
            ->registerCssFile('ajax.css')
            ->registerCssFile('style.css')
            ->registerScriptFile('ajax.js')
            ->registerScriptFile('autoNumeric.js');

        return true;
    }
}