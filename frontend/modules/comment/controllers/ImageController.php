<?php
namespace frontend\modules\comment\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use frontend\modules\comment\components\CommentBaseController;
use Yii;
class ImageController extends CommentBaseController
{
    public function filters() {
        return array(
            'ajaxOnly + add, get, upload, video',
        );
    }

    public function beforeRender($view)
    {
        $result = parent::beforeRender($view);
        return $result;
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'add'       => ['class' => 'frontend\modules\comment\controllers\actions\image\AddAction'],
            'get'       => ['class' => 'frontend\modules\comment\controllers\actions\image\GetAction'],
            'upload'    => ['class' => 'frontend\modules\comment\actions\UploadAction'],
            'video'     => ['class' => 'frontend\modules\comment\actions\VideoAction'],
		);
	}
}