<?php
namespace frontend\modules\comment\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCriteria;
use GalleryVideo;
use EMongoCriteria;
use CommentItems;
use ImageMedia;
use VideoMedia;
class AddAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $parent = Yii::app()->request->getPost('parent');
        $body = Yii::app()->request->getPost('body');
        $images = Yii::app()->request->getPost('images', []);
        $videos = Yii::app()->request->getPost('videos', []);

        $criteria = new CDbCriteria();
        $criteria->addCondition('`t`.dir = :video_dir');
        $criteria->params = [':video_dir' => Yii::app()->request->getParam('video_dir')];

        /** @var GalleryVideo $Video */
        $Video = GalleryVideo::model()->find($criteria);
        if(!$Video) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Видео не найдено'))
                ->send();
        }

        $NewComment = new CommentItems();
        $NewComment->setUserSenderId(Yii::app()->user->id)
            ->setVideoId($Video->getId())
            ->setBody($body)
            ->setPrimaryKey(null)
            ->setCreateAt(time());

        if(!$NewComment->validate()) {
            Yii::app()->ajax->addErrors($NewComment)
                ->send();
        }

        foreach ($images as $image) {
            $media = new ImageMedia();
            $media->setPath(Yii::app()->static->staticPath().'/'.ltrim($image, '/'))
                ->setParentId($NewComment->getPrimaryKey());

            if($media->createAction())
                $NewComment->addMedia($media->getRawDocument());
        }

        foreach ($videos as $video) {
            $media = new VideoMedia();
            $media->setAttributes($video, false);
            $media->setParentId($NewComment->getPrimaryKey());

            if($media->createAction())
                $NewComment->addMedia($media->getRawDocument());
        }
        //echo '<pre>';var_dump($NewComment);die;
        $criteria = new EMongoCriteria();
        $criteria->addCondition('_id', CommentItems::model()->getMongoId($parent));
        $criteria->addCondition('video_id', $Video->getId());
        /** @var CommentItems $Comment */
        $Comment = CommentItems::model()->findOne($criteria);
        if($Comment) {
            $main = $Comment->getUnique();
            if($Comment->getParent() !== null)
                $main = $Comment->getMainParent();

            $parents = $Comment->getParents();
            $parents[] = $Comment->getPrimaryKey();
            $NewComment->setParent($Comment->getPrimaryKey())
                ->setMainParent($main)
                ->setParents($parents);
        }
        if(!$NewComment->save())
            Yii::app()->ajax->addErrors($NewComment);

        if(!Yii::app()->ajax->hasErrors()) {
            Yii::app()->ajax->addOther(['ok' => true])
                ->addMessage(Yii::t('labels', 'Комментарий добавлен'));
        }

        Yii::app()->ajax->send();
    }
}