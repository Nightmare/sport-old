<?php
namespace frontend\modules\comment\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCriteria;
use EMongoCriteria;
use CommentItems;
use EMongoPagination;
use GalleryVideo;
class GetAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('`t`.dir = :video_dir');
        $criteria->params = [':video_dir' => Yii::app()->request->getParam('video_dir')];

        /** @var GalleryVideo $Video */
        $Video = GalleryVideo::model()->find($criteria);
        if(!$Video) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Изображение не найдено'))->send();
        }

        $criteria = new EMongoCriteria();
        $criteria->addCondition('video_id', $Video->getId());
        $criteria->sort = ['create_at' => 'desc'];

        $Pages = new EMongoPagination(CommentItems::model()->count($criteria));
        $Pages->pageSize = 30;
        $Pages->applyLimit($criteria);

        $view = $this->controller->widget('\common\modules\comment\widgets\ListViewWidget', [
            'model' => $Video,
            'controller' => true,
            'comment_id' => Yii::app()->request->getPost('comment_tree'),
        ], true);

        Yii::app()->ajax
            ->addReplace($view, '.comment_container#replacement', [], false)
            ->addOther(['comment_tree' => Yii::app()->request->getPost('comment_tree')])->send();
    }
}