<?php
namespace frontend\modules\comment\actions;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\components\video\iVideo;
use Yii;
use VideoTemplate;
class VideoAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $link = Yii::app()->request->getParam('link');
        /** @var iVideo $linkInfo */
        $linkInfo = VideoTemplate::checkLink($link);
        if($linkInfo === false)
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Видео ресурс не поддерживается'));
        else
            Yii::app()->ajax->addOther([
                'ok' => true,
                'name' => $linkInfo->getTitle(),
                'video_link' => $link
            ]);

        Yii::app()->ajax->send();
    }
}