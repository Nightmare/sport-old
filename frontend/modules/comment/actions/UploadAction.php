<?php
namespace frontend\modules\comment\actions;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\components\CImageHandler;
use common\components\VarDumper;
use Yii;
use CUploadedFile;
use CommentItems;
class UploadAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $link = null;
        /** @var CUploadedFile[] $files */
        $files = CUploadedFile::getInstancesByName('files');
        foreach ($files as $file) {
            /** @var CImageHandler $image */
            $image = Yii::app()->ih->load($file->getTempName());
            $link = sprintf('%s/%s.%s', CommentItems::getTempImagePath(), CommentItems::generateImageName(), $image->getFormatType());

            $image->save(Yii::app()->static->staticPath().'/'.$link);

            Yii::app()->ajax->addOther([
                'link' => $link,
                'name' => $file->getName()
            ]);

            break;
        }

        Yii::app()->ajax->send();
    }
}