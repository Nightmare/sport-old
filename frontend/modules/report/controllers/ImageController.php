<?php
namespace frontend\modules\report\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use frontend\modules\report\components\ReportBaseController;
class ImageController extends ReportBaseController
{
    public function filters() {
        return array(
            'ajaxOnly + add',
        );
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'add' => ['class' => 'frontend\modules\report\controllers\actions\image\AddAction'],
		);
	}
}