<?php
namespace frontend\modules\report\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCriteria;
use GalleryImage;
use ReportImage;
class AddAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $dir = Yii::app()->request->getParam('image_dir');
        $criteria = new CDbCriteria();
        $criteria->addCondition('dir = :dir');
        $criteria->params = [':dir' => $dir];
        /** @var GalleryImage $Image */
        $Image = GalleryImage::model()->find($criteria);
        if(!$Image) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Изображение не найдено'))->send();
        }

        $criteria = new CDbCriteria();
        $criteria->addCondition('user_id = :user_id');
        $criteria->addCondition('image_id = :image_id');
        $criteria->params = [
            ':user_id' => Yii::app()->user->id,
            ':image_id' => $Image->getId()
        ];
        /** @var ReportImage $Report */
        $Report = ReportImage::model()->find($criteria);
        if($Report) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Вы уже оставляли жалобу на это изображение'))->send();
        }

        $Report = new ReportImage('createAction');
        $post = Yii::app()->request->getPost('ReportImage');
        if($post) {
            $Report->setAttributes($post);
            $Report->setUserId(Yii::app()->user->id)
                ->setImageId($Image->getId());
            if(!$Report->createAction())
                Yii::app()->ajax->addErrors($Report);
            else {
                Yii::app()->ajax->addMessage(Yii::t('labels', 'Вы оставили жалобу на изображение'));
                Yii::app()->ajax->addOther([
                    'ok' => true,
                    'runJS' => [
                        'functionName' => 'removeElement',
                        'functionParams' => ['a#report.report']
                    ]
                ]);
            }
        } else {
            Yii::app()->ajax
                ->addReplace('_form', '#customModal #replacement', ['model' => $Report, 'image' => $Image])
                ->runJS('openCustom')
                ->addOther(['ok' => true]);
        }

        Yii::app()->ajax->send();
    }
}