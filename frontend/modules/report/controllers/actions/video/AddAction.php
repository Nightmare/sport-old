<?php
namespace frontend\modules\report\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCriteria;
use GalleryVideo;
use ReportVideo;
class AddAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $dir = Yii::app()->request->getParam('video_dir');
        $criteria = new CDbCriteria();
        $criteria->addCondition('dir = :dir');
        $criteria->params = [':dir' => $dir];
        /** @var GalleryVideo $Video */
        $Video = GalleryVideo::model()->find($criteria);
        if(!$Video) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Изображение не найдено'))->send();
        }

        $criteria = new CDbCriteria();
        $criteria->addCondition('user_id = :user_id');
        $criteria->addCondition('video_id = :video_id');
        $criteria->params = [
            ':user_id' => Yii::app()->user->id,
            ':video_id' => $Video->getId()
        ];
        /** @var ReportVideo $Report */
        $Report = ReportVideo::model()->find($criteria);
        if($Report) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Вы уже оставляли жалобу на это видео'))->send();
        }

        $Report = new ReportVideo('createAction');
        $post = Yii::app()->request->getPost('ReportImage');
        if($post) {
            $Report->setAttributes($post);
            $Report->setUserId(Yii::app()->user->id)
                ->setVideoId($Video->getId());
            if(!$Report->createAction())
                Yii::app()->ajax->addErrors($Report);
            else {
                Yii::app()->ajax->addMessage(Yii::t('labels', 'Вы оставили жалобу на видео'));
                Yii::app()->ajax->addOther([
                    'ok' => true,
                    'runJS' => [
                        'functionName' => 'removeElement',
                        'functionParams' => ['a#report.report']
                    ]
                ]);
            }
        } else {
            Yii::app()->ajax
                ->addReplace('_form', '#customModal #replacement', ['model' => $Report, 'image' => $Video])
                ->runJS('openCustom')
                ->addOther(['ok' => true]);
        }

        Yii::app()->ajax->send();
    }
}