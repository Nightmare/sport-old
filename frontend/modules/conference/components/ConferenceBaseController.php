<?php
namespace frontend\modules\conference\components;
/**
 * Created by PhpStorm.
 * User: me
 * Date: 16.09.2014
 * Time: 14:09
 */

use frontend\components\FrontendController;
use Yii;
class ConferenceBaseController extends FrontendController
{
    public $conference;
    public $conference_id;
    /** @var \ConferenceCategory[] */
    public $categories;

    public function beforeAction($action)
    {
        $result = parent::beforeAction($action);
        $this->registerAssets();

        return $result;
    }

    private function registerAssets()
    {

    }
} 