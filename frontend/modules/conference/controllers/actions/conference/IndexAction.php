<?php
namespace frontend\modules\conference\controllers\actions\conference;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use common\helpers\StreetHelper;

class IndexAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $conference = Yii::app()->request->getParam('conference');
        Yii::app()->breadcrumbs
            ->addBreadcrumb(StreetHelper::getConferenceTitle($conference), StreetHelper::getConferenceLink($conference))
            ->addBreadcrumb('Конференция');

        $this->controller->render('index');
    }
}