<?php
namespace frontend\modules\conference\controllers\actions\post;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCriteria;
use ConferenceCategory;
use ConferencePost;
use common\helpers\StreetHelper;

class ViewAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $this->controller->layout = 'conference_post';

        $conference = Yii::app()->request->getParam('conference');
        $conf_cat_dir = Yii::app()->request->getParam('conf_cat_dir');
        $post_dir = Yii::app()->request->getParam('post_dir');

        $criteria = new CDbCriteria();
        $criteria->addCondition('dir = :post_dir');
        $criteria->params = [':post_dir' => $post_dir];
        /** @var ConferencePost $Post */
        $Post = ConferencePost::model()->find($criteria);
        if(!$Post)
            \MException::ShowError(Yii::t('error', 'Пост не найден'));

        $params = [
            'model' => $Post,
        ];
        if(Yii::app()->request->isAjaxRequest) {
            Yii::app()->ajax
                ->addReplace('_view', '#page_content #replacement', $params)
                ->addOther(['ok' => true])->send();
        } else {
            $this->controller->render('view', ['data' => $params, 'conference' => $conference, 'conf_cat_dir' => $conf_cat_dir]);
        }
    }
}