<?php
namespace frontend\modules\conference\controllers\actions\post;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCriteria;
use ConferenceCategory;
use ConferencePost;
use common\helpers\StreetHelper;

class ListAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $conference = Yii::app()->request->getParam('conference');

        $criteria = new CDbCriteria();
        $criteria->addCondition('dir = :dir');
        $criteria->params = [':dir' => Yii::app()->request->getParam('conf_cat_dir')];
        /** @var ConferenceCategory $ConfCategory */
        $ConfCategory = ConferenceCategory::model()->find($criteria);
        if(!$ConfCategory)
            \MException::ShowError(Yii::t('error', 'Категория не найдена'));

        $criteria = new CDbCriteria();
        $criteria->addCondition('conference_id = :id');
        $criteria->params = [':id' => $ConfCategory->getId()];
        /** @var ConferencePost[] $Posts */
        $Posts = ConferencePost::model()->findAll($criteria);

        $params = [
            'models' => $Posts,
            'conference_category' => $ConfCategory
        ];
        if(Yii::app()->request->isAjaxRequest) {
            Yii::app()->ajax
                ->addReplace('_list', '#page_content #replacement', $params)
                ->addOther(['ok' => true])->send();
        } else {
            $this->controller->render('list', ['data' => $params]);
        }
    }
}