<?php
namespace frontend\modules\conference\controllers;
use frontend\modules\conference\components\ConferenceBaseController;

/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

class ConferenceController extends ConferenceBaseController
{
    public function filters() {
        return array(
            'ajaxOnly + add',
        );
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'index' => ['class' => 'frontend\modules\conference\controllers\actions\conference\IndexAction'],
		);
	}
}