<?php
namespace frontend\modules\conference\controllers;
use frontend\modules\conference\components\ConferenceBaseController;

/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

class PostController extends ConferenceBaseController
{
    public function filters() {
        return array(
            'ajaxOnly + add',
        );
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'list' => ['class' => 'frontend\modules\conference\controllers\actions\post\ListAction'],
            'view' => ['class' => 'frontend\modules\conference\controllers\actions\post\ViewAction'],
		);
	}
}