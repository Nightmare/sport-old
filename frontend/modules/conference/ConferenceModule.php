<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 16.09.2014
 * Time: 2:02
 */
namespace frontend\modules\conference;

use common\helpers\StreetHelper;
use frontend\modules\conference\components\ConferenceBaseController;
use Yii;
use CDbCriteria;
use ConferenceCategory;
use common\modules\conference\ConferenceModule as BaseConferenceModule;
class ConferenceModule extends BaseConferenceModule
{
    public $controllerNamespace = '\frontend\modules\conference\controllers';


    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        // import the module-level models and components
        $this->setImport([
            'conference.models.*',
            'conference.components.*'
        ]);
    }

    /**
     * @param ConferenceBaseController $controller
     * @param \CAction $action
     * @return bool
     */
    public function beforeControllerAction($controller, $action)
    {
        if(parent::beforeControllerAction($controller, $action))
        {
            $controller->layout = 'conference';

            if(!($conference = Yii::app()->request->getParam('conference')) || !StreetHelper::getConferenceByName($conference))
                return true;
            $controller->conference = $conference;
            $controller->conference_id = StreetHelper::getConferenceByName($conference);

            $criteria = new CDbCriteria();
            $criteria->addCondition('conference_id = :id');
            $criteria->scopes = ['enable'];
            $criteria->params = [':id' => $controller->conference_id];
            $controller->categories = ConferenceCategory::model()->findAll($criteria);

            Yii::app()->static->setLibrary('masonry')
                ->registerScriptFile('masonry.pkgd.min.js');
            Yii::app()->static->setOwn()
                ->registerScriptFile('conference.js');

            return true;
        }
        else
            return false;
    }
}