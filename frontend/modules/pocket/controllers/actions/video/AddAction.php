<?php
namespace frontend\modules\pocket\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCriteria;
use GalleryVideo;
use PocketVideo;
class AddAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $dir = Yii::app()->request->getParam('video_dir');
        $criteria = new CDbCriteria();
        $criteria->addCondition('dir = :dir');
        $criteria->params = [':dir' => $dir];
        /** @var GalleryVideo $Video */
        $Video = GalleryVideo::model()->find($criteria);
        if(!$Video) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Видео не найдено'))->send();
        }

        $criteria = new CDbCriteria();
        $criteria->addCondition('user_id = :user_id');
        $criteria->addCondition('video_id = :video_id');
        $criteria->params = [
            ':user_id' => Yii::app()->user->id,
            ':video_id' => $Video->getId()
        ];
        /** @var PocketVideo $Pocket */
        $Pocket = PocketVideo::model()->find($criteria);
        if($Pocket) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Видео уже у вас в кармане'))->send();
        }

        $Pocket = new PocketVideo();
        $Pocket->setUserId(Yii::app()->user->id)
            ->setVideoId($Video->getId());
        if(!$Pocket->save())
            Yii::app()->ajax->addErrors($Video);
        else {
            Yii::app()->ajax->addMessage(Yii::t('labels', 'Видео добавлено в карман'));
            Yii::app()->ajax->addOther(['ok' => true]);
        }

        Yii::app()->ajax->send();
    }
}