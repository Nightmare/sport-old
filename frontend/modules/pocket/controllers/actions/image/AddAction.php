<?php
namespace frontend\modules\pocket\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use Yii;
use CDbCriteria;
use GalleryImage;
use PocketImage;
class AddAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $dir = Yii::app()->request->getParam('image_dir');
        $criteria = new CDbCriteria();
        $criteria->addCondition('dir = :dir');
        $criteria->params = [':dir' => $dir];
        /** @var GalleryImage $Image */
        $Image = GalleryImage::model()->find($criteria);
        if(!$Image) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Изображение не найдено'))->send();
        }

        $criteria = new CDbCriteria();
        $criteria->addCondition('user_id = :user_id');
        $criteria->addCondition('image_id = :image_id');
        $criteria->params = [
            ':user_id' => Yii::app()->user->id,
            ':image_id' => $Image->getId()
        ];
        /** @var PocketImage $Pocket */
        $Pocket = PocketImage::model()->find($criteria);
        if($Pocket) {
            Yii::app()->ajax->addErrors(Yii::t('errors', 'Изображение уже у вас в кармане'))->send();
        }

        $Pocket = new PocketImage();
        $Pocket->setUserId(Yii::app()->user->id)
            ->setImageId($Image->getId());
        if(!$Pocket->save())
            Yii::app()->ajax->addErrors($Pocket);
        else {
            Yii::app()->ajax->addMessage(Yii::t('labels', 'Изображение добавлено в карман'));
            Yii::app()->ajax->addOther(['ok' => true]);
        }

        Yii::app()->ajax->send();
    }
}