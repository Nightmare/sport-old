<?php
namespace frontend\modules\pocket\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use frontend\modules\pocket\components\PocketBaseController;
class VideoController extends PocketBaseController
{
    public function filters() {
        return array(
            'ajaxOnly + add',
        );
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'add' => ['class' => 'frontend\modules\pocket\controllers\actions\video\AddAction'],
		);
	}
}