<?php
/**
 * Base overrides for frontend application
 */
return [
    // So our relative path aliases will resolve against the `/frontend` subdirectory and not nonexistent `/protected`
    'basePath' => 'frontend',
    'import' => [
        'application.controllers.*',
        'application.controllers.actions.site.*',
        'common.actions.*',
    ],
    'controllerMap' => [
        // Overriding the controller ID so we have prettier URLs without meddling with URL rules
        'site' => 'FrontendSiteController',
        'rack' => 'FrontendRackController',
        'location' => 'FrontendLocationController',
    ],
    'modules' => [
        'users' => [
            'class' => 'frontend\modules\users\UsersModule'
        ],
        'gallery' => [
            'class' => 'frontend\modules\gallery\GalleryModule'
        ],
        'comment' => [
            'class' => 'frontend\modules\comment\CommentModule'
        ],
        'pocket' => [
            'class' => 'frontend\modules\pocket\PocketModule'
        ],
        'report' => [
            'class' => 'frontend\modules\report\ReportModule'
        ],
        'conference' => [
            'class' => 'frontend\modules\conference\ConferenceModule'
        ]
    ],
    'theme' => 'sport',
    'components' => [
        'errorHandler' => [
            'errorAction' => 'site/error'
        ],
        'urlManager' => [
            'rules' => [
                //Город
                ['site/reception',             'pattern' => '/<conference:.*>/reception'],
                ['site/coach',                 'pattern' => '/<conference:.*>/coach'],
                ['site/simulators',            'pattern' => '/<conference:.*>/simulators'],
                ['site/theory',                'pattern' => '/<conference:.*>/theory'],

                ['rack/index',                 'pattern' => '/<conference:.*>/index'],
                ['rack/street_workout',        'pattern' => '/<conference:.*>/street_workout'],
                ['rack/turnik',                'pattern' => '/<conference:.*>/turnik'],
                ['rack/brusya',                'pattern' => '/<conference:.*>/brusya'],

                //Пожаловаться
                ['report/image/add',           'pattern' => '/report/image/<image_dir:.*>'],
                ['report/video/add',           'pattern' => '/report/video/<video_dir:.*>'],

                //Галерея
                ['gallery/simulator/list',     'pattern' => '/<conference:.*>/gallery/simulator/<filter_dir:.*>'],
                ['gallery/muscle/list',        'pattern' => '/<conference:.*>/gallery/muscle/<filter_dir:.*>'],

                //Конференции
                ['conference/conference/<_a>', 'pattern' => '/<conference:.*>/conference/<_a>'],
                ['conference/post/<_a>',       'pattern' => '/<conference:.*>/conference/<conf_cat_dir:.*>/post/<post_dir:.*>/<_a>'],
                ['conference/post/<_a>',       'pattern' => '/<conference:.*>/conference/post/<post_dir:.*>/<_a>'],
                ['conference/post/<_a>',       'pattern' => '/<conference:.*>/conference/<conf_cat_dir:.*>/post/<_a>'],

                //Комментарии
                ['comment/image/get',           'pattern' => '/<conference:.*>/comment/image/<image_dir:.*>/get',       'urlSuffix' => '.ajax'],
                ['comment/video/get',           'pattern' => '/<conference:.*>/comment/video/<video_dir:.*>/get',       'urlSuffix' => '.ajax'],
                ['comment/image/add',           'pattern' => '/<conference:.*>/comment/image/<image_dir:.*>/add',       'urlSuffix' => '.ajax'],
                ['comment/video/add',           'pattern' => '/<conference:.*>/comment/video/<video_dir:.*>/add',       'urlSuffix' => '.ajax'],
                ['comment/image/upload',        'pattern' => '/<conference:.*>/comment/image/upload',                   'urlSuffix' => '.ajax'],
                ['comment/video/upload',        'pattern' => '/<conference:.*>/comment/video/upload',                   'urlSuffix' => '.ajax'],
            ]
        ],
    ],
    'params' => [
        'sitePart' => 'frontend'
    ]
];