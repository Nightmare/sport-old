<?php
/**
 * Controller action representing the act of home page rendering on backend.
 *
 * Everything which should be done when user opens the backend landing page is here.
 *
 * @package YiiBoilerplate\Backend
 */
class BackendHomePageAction extends CAction
{
    /**
     * We render the homepage as a controller action here.
     */
    public function run()
    {
        $user_count = User::model()->count();

        $criteria = new CDbCriteria();
        $criteria->scopes = ['enable', 'admin'];
        $image_count = GalleryImage::model()->count($criteria);

        $video_count = GalleryVideo::model()->count($criteria);

        $criteria = new EMongoCriteria();
        $condition = [
            'video_id' => ['$ne' => null]
        ];
        $criteria->setCondition($condition);
        $video_comment_count = CommentItems::model()->count($criteria);

        $criteria = new EMongoCriteria();
        $condition = [
            'image_id' => ['$ne' => null]
        ];
        $criteria->setCondition($condition);
        $image_comment_count = CommentItems::model()->count($criteria);

        $criteria = new EMongoCriteria();
        $condition = [
            'create_at' => [
                '$gt' => strtotime('today')
            ]
        ];
        $criteria->setCondition($condition);
        $today_comment_count = CommentItems::model()->count($criteria);

        $this->controller->render('index', [
            'user_count'            => $user_count,
            'image_count'           => $image_count,
            'video_count'           => $video_count,
            'video_comment_count'   => $video_comment_count,
            'image_comment_count'   => $image_comment_count,
            'today_comment_count'   => $today_comment_count,
        ]);
    }
} 