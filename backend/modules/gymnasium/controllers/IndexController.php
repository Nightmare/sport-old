<?php
namespace backend\modules\gymnasium\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use backend\modules\gymnasium\components\GymnasiumBaseController;
class IndexController extends GymnasiumBaseController
{
    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => [
                    'list',
                ],
                'roles' => ['gymnasiumView']
            ],
            [
                'allow',
                'actions' => [
                    'update', 'create', 'image',
                ],
                'roles' => ['gymnasiumControl']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'list'      => ['class' => 'backend\modules\gymnasium\controllers\actions\index\ListAction'],
            'create'    => ['class' => 'backend\modules\gymnasium\controllers\actions\index\CreateAction'],
            'update'    => ['class' => 'backend\modules\gymnasium\controllers\actions\index\UpdateAction'],
            'image'     => ['class' => 'backend\modules\gymnasium\controllers\actions\index\ImageAction'],
		);
	}
}