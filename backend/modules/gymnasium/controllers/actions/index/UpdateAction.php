<?php
namespace backend\modules\gymnasium\controllers\actions\index;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\extensions\egmap\MEGMap;

class UpdateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        \Yii::import('vendor.2amigos.egmap.*');

        $criteria = new \CDbCriteria();
        $criteria->addCondition('id = :id');
        $criteria->params = [':id' => \Yii::app()->request->getParam('gymnasium_id')];
        /** @var \Gymnasium $model */
        $model = \Gymnasium::model()->find($criteria);
        if(!$model)
            \MException::ShowError('Спорт-зал не найден');

        $post = \Yii::app()->request->getPost('Gymnasium');
        if($post) {
            $model->setAttributes($post);
            if($model->updateAction())
                \Yii::app()->ajax->setUrl(\Yii::app()->createUrl('/gymnasium/index/list'));

            \Yii::app()->ajax->send();
        }

        $gMap = new MEGMap();
        $gMap->setWidth('100%');
        $gMap->setHeight(550);
        $gMap->zoom = 8;
        $mapTypeControlOptions = array(
            'position' => \EGMapControlPosition::RIGHT_TOP,
            'style' => \EGMap::MAPTYPECONTROL_STYLE_HORIZONTAL_BAR
        );

        $gMap->mapTypeId = \EGMap::TYPE_ROADMAP;
        $gMap->mapTypeControlOptions = $mapTypeControlOptions;

        $gMap->setCenter($model->getLat(), $model->getLon());
        $gMap->addAutocomplete();

        // Setting up new event for user click on map, so marker will be created on place and respectful event added.
        $Event = new \EGMapEvent('click', 'function (event) { moveMarker(event.latLng.lat(), event.latLng.lng()); }', false, \EGMapEvent::TYPE_EVENT_DEFAULT);
        $gMap->addEvent($Event);

        $this->controller->render('form', ['model' => $model, 'gMap' => $gMap]);
    }
}