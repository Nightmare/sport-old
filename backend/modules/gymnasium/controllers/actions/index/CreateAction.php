<?php
namespace backend\modules\gymnasium\controllers\actions\index;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\extensions\egmap\MEGMap;

class CreateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        \Yii::import('vendor.2amigos.egmap.*');

        $model = new \Gymnasium('createAction');
        $post = \Yii::app()->request->getPost('Gymnasium');
        if($post) {
            $model->setAttributes($post);
            $model->setUserId(\Yii::app()->user->getId());
            if($model->createAction())
                \Yii::app()->ajax->setUrl(\Yii::app()->createUrl('/gymnasium/index/list'));

            \Yii::app()->ajax->send();
        }

        $gMap = new MEGMap();
        $gMap->setWidth('100%');
        $gMap->setHeight(550);
        $gMap->zoom = 8;
        $mapTypeControlOptions = array(
            'position' => \EGMapControlPosition::RIGHT_TOP,
            'style' => \EGMap::MAPTYPECONTROL_STYLE_HORIZONTAL_BAR
        );

        $gMap->mapTypeId = \EGMap::TYPE_ROADMAP;
        $gMap->mapTypeControlOptions = $mapTypeControlOptions;

        $gMap->setCenter(47.25, 28.58);
        $gMap->addAutocomplete();

        // Setting up new event for user click on map, so marker will be created on place and respectful event added.
        $Event = new \EGMapEvent('click', 'function (event) { moveMarker(event.latLng.lat(), event.latLng.lng()); }', false, \EGMapEvent::TYPE_EVENT_DEFAULT);
        $gMap->addEvent($Event);

        $this->controller->render('form', ['model' => $model, 'gMap' => $gMap]);
    }
}