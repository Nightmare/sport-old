<?php
namespace backend\modules\gymnasium\controllers\actions\index;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\components\CImageHandler;
use Yii;
use CUploadedFile;
use UserProfile;
class ImageAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        if(!isset($_FILES['Image']))
            Yii::app()->end();

        $files = isset($_FILES['Image']['tmp_name']['main']) ? $_FILES['Image']['tmp_name']['main'] : $_FILES['Image']['tmp_name']['logo'];
        $linkToView = null;
        $image_name = \Gymnasium::generateImageName();
        foreach ($files as $type => $file) {
            /** @var CImageHandler $image */
            $image = Yii::app()->ih->load($file);

            $link = sprintf('%s/%s.%s', \Gymnasium::getTempImagePath($type), $image_name, $image->getFormatType());
            if($type == 'thumb')
                $linkToView = $link;

            $image->save(Yii::app()->static->staticPath() . '/' .$link);
            Yii::app()->ajax->addOther([$type => sprintf('%s.%s', $image_name, $image->getFormatType())]);
        }

        Yii::app()->ajax->addOther([
            'ok' => true,
            'minLink' => Yii::app()->static->imageLink($linkToView),
            'name' => $image_name
        ]);

        Yii::app()->ajax->send();
    }
}