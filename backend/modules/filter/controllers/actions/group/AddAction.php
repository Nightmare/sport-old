<?php
namespace backend\modules\filter\controllers\actions\group;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class AddAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $model = new \FilterGroup('createAction');
        $post = \Yii::app()->request->getParam('FilterGroup');
        if($post) {
            $model->setAttributes($post);
            if(!$model->save())
                \Yii::app()->ajax->addErrors($model)->send();

            \Yii::app()->ajax
                ->addMessage('Группа создана')
                ->runJS('closeCustom')
                ->runJS('updateGrid', ['filter-group-list'])->send();
        } else
            \Yii::app()->ajax
                ->addHtml('_form', '#customModal #replacement', ['model' => $model])
                ->runJS('openCustom')->send();
    }
}