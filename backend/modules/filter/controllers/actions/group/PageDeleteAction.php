<?php
namespace backend\modules\filter\controllers\actions\group;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class PageDeleteAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->addCondition('filter_group_id = :filter_group_id');
        $criteria->addCondition('page_id = :page_id');
        $criteria->params = [
            ':filter_group_id' => \Yii::app()->request->getParam('filter_group_id'),
            ':page_id' => \Yii::app()->request->getParam('page_id'),
        ];
        \FilterGroupPage::model()->deleteAll($criteria);

        \Yii::app()->ajax
            ->addMessage('Привязка удалена')
            ->runJS('closeCustom')
            ->runJS('updateGrid', ['filter-group-list'])->send();
    }
}