<?php
namespace backend\modules\filter\controllers\actions\group;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class DeleteAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        /** @var \FilterGroup $Group */
        $Group = \FilterGroup::model()->findByPk(\Yii::app()->request->getParam('group_id'));
        if(!$Group)
            \MException::ShowError('Группа не найдена');

        $Group->setIsDeleted(true)->save();
    }
}