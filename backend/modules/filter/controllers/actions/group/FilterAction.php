<?php
namespace backend\modules\filter\controllers\actions\group;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class FilterAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        /** @var \FilterGroup $model */
        $model = \FilterGroup::model()->findByPk(\Yii::app()->request->getParam('group_id'));
        if (!$model)
            \MException::ShowError('Группа не найдена');

        $criteria = new \CDbCriteria();
        $criteria->addCondition('status != :deleted');
        $criteria->addCondition('group_id = :group_id');
        $criteria->params = [
            ':group_id' => $model->getId(),
            ':deleted' => \Filter::STATUS_DELETE
        ];
        /** @var \Filter[] $Filters */
        $Filters = \Filter::model()->findAll($criteria);

        $filters = [];
        foreach ($Filters as $filter)
            $filters[$filter->getId()] = $filter->getTitle();

        \Yii::app()->ajax->addOther([
            'filters' => $filters,
            'group_id' => $model->getId()
        ])->send();
    }
}