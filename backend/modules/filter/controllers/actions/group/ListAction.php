<?php
namespace backend\modules\filter\controllers\actions\group;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class ListAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $model = new \FilterGroup('search');
        $model->unsetAttributes();
        $post = \Yii::app()->request->getParam('FilterGroup');
        if($post)
            $model->setAttributes($post);

        $criteria = new \CDbCriteria();
        $criteria->scopes = ['enable'];
        $criteria->with = [
            'filters' => [
                'condition' => 'filters.status != :deleted',
                'params' => [':deleted' => \Filter::STATUS_DELETE]
            ],
            'pages'
        ];
        $dataProvider = new \CActiveDataProvider($model, [
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        \Yii::app()->breadcrumbs->addBreadcrumb('Группы фильтров');

        $this->controller->render('list', ['dataProvider' => $dataProvider]);
    }
}