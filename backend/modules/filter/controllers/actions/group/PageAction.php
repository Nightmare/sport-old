<?php
namespace backend\modules\filter\controllers\actions\group;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;

class PageAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        /** @var \FilterGroup $Group */
        $Group = \FilterGroup::model()->with('pages')->findByPk(\Yii::app()->request->getParam('group_id'));
        if(!$Group)
            \MException::ShowError('Группа не найдена');


        $model = new \FilterGroupPage('createAction');
        $model->setFilterGroupId($Group->getId());

        $post = \Yii::app()->request->getParam('FilterGroupPage');
        if($post) {
            $model->setAttributes($post);
            foreach ($Group->pages as $p) {
                if($p->getPageId() == $model->getPageId())
                    \Yii::app()->ajax->addErrors('Эта страница уже привязана, выберите другую')->send();
            }
            if(!$model->save())
                \Yii::app()->ajax->addErrors($model)->send();

            \Yii::app()->ajax
                ->addMessage('Страница привязана')
                ->runJS('closeCustom')
                ->runJS('updateGrid', ['filter-group-list'])->send();
        } else
            \Yii::app()->ajax
                ->addHtml('_page', '#customModal #replacement', ['model' => $model])
                ->runJS('openCustom')->send();
    }
}