<?php
namespace backend\modules\filter\controllers\actions\filter;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class UpdateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        /** @var \Filter $model */
        $model = \Filter::model()->findByPk(\Yii::app()->request->getParam('filter_id'));
        if(!$model)
            \MException::ShowError('Фильтр не найден');
        $model->scenario = 'updateAction';

        $post = \Yii::app()->request->getParam('Filter');
        if($post) {
            $model->setAttributes($post);
            if(!$model->updateAction())
                \Yii::app()->ajax->addErrors($model)->send();

            \Yii::app()->ajax
                ->addMessage('Фильтр обновлен')
                ->runJS('closeCustom')
                ->runJS('updateGrid', ['filter-list'])->send();
        } else
            \Yii::app()->ajax
                ->addHtml('_form', '#customModal #replacement', ['model' => $model])
                ->runJS('openCustom')->send();
    }
}