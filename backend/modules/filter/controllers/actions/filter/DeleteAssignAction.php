<?php
namespace backend\modules\filter\controllers\actions\filter;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class DeleteAssignAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->addCondition('filter_id = :filter_id and filter_assign_id = :filter_assign_id');
        $criteria->addCondition('filter_id = :filter_assign_id and filter_assign_id = :filter_id', 'OR');
        $criteria->params = [
            ':filter_id' => \Yii::app()->request->getParam('filter_id'),
            ':filter_assign_id' => \Yii::app()->request->getParam('filter_assign_id'),
        ];
        \FilterFilter::model()->deleteAll($criteria);

        \Yii::app()->ajax
            ->addMessage('Связь удалена')
            ->runJS('closeCustom')
            ->runJS('updateGrid', ['filter-list'])->send();
    }
}