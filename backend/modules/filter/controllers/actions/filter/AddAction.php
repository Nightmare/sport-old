<?php
namespace backend\modules\filter\controllers\actions\filter;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class AddAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        /** @var \FilterGroup $group */
        $group = \FilterGroup::model()->findByPk(\Yii::app()->request->getParam('group_id'));
        if(!$group)
            \MException::ShowError('Группа не найдена');

        $model = new \Filter('createAction');
        $model->setGroupId($group->getId());

        $post = \Yii::app()->request->getParam('Filter');
        if($post) {
            $model->setAttributes($post);
            if(!$model->createAction())
                \Yii::app()->ajax->addErrors($model)->send();

            \Yii::app()->ajax
                ->addMessage('Фильтр создан')
                ->runJS('closeCustom')
                ->runJS('updateGrid', ['filter-list'])->send();
        } else
            \Yii::app()->ajax
                ->addHtml('_form', '#customModal #replacement', ['model' => $model, 'group' => $group])
                ->runJS('openCustom')->send();
    }
}