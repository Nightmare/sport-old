<?php
namespace backend\modules\filter\controllers\actions\filter;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class ListAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        /** @var \FilterGroup $group */
        $group = \FilterGroup::model()->findByPk(\Yii::app()->request->getParam('group_id'));
        if(!$group)
            \MException::ShowError('Группа не найдена');

        $model = new \Filter('search');
        $model->unsetAttributes();
        $post = \Yii::app()->request->getParam('Filter');
        if($post)
            $model->setAttributes($post);

        $criteria = new \CDbCriteria();
        $criteria->addCondition('`t`.status != :deleted');
        $criteria->addCondition('`t`.group_id = :group_id');
        $criteria->with = [
            'filters' => [
                'with' => [
                    'filterAssign' => [
                        'on' => 'filterAssign.status != :filterAssign_deleted',
                        'params' => [':filterAssign_deleted' => \Filter::STATUS_DELETE]
                    ]
                ]
            ]
        ];
        $criteria->params = [':deleted' => \Filter::STATUS_DELETE, ':group_id' => $group->getId()];
        $dataProvider = new \CActiveDataProvider($model, [
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        \Yii::app()->breadcrumbs->addBreadcrumb('Группы фильтров', \Yii::app()->createUrl('/filter/group/list'))
            ->addBreadcrumb($group->getTitle());

        $model->status = \Filter::STATUS_DELETE;
        $this->controller->render('list', ['dataProvider' => $dataProvider, 'group' => $group]);
    }
}