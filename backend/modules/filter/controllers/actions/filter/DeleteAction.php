<?php
namespace backend\modules\filter\controllers\actions\filter;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class DeleteAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        /** @var \Filter $filter */
        $filter = \Filter::model()->findByPk(\Yii::app()->request->getParam('filter_id'));
        if(!$filter)
            \MException::ShowError('Фильтр не найден');

        $filter->setStatus(\Filter::STATUS_DELETE)
            ->save();
    }
}