<?php
namespace backend\modules\filter\controllers\actions\filter;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class CopyAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        /** @var \FilterGroup $Group */
        $Group = \FilterGroup::model()->findByPk(\Yii::app()->request->getParam('group_id'));
        if(!$Group)
            \MException::ShowError('Группа не найдена');

        $model = new \Filter('copyAction');
        $model->setGroupId($Group->getId());

        $post = \Yii::app()->request->getParam('Filter');
        if($post && isset($post['filter_id'])) {
            $criteria = new \CDbCriteria();
            $criteria->addCondition('id = :id');
            $criteria->params = [':id' => $post['filter_id']];
            $Filter = \Filter::model()->find($criteria);
            if(!$Filter)
                \Yii::app()->ajax->addErrors('Копируемый фильтр не найден')->send();

            $CopyFilter = new \Filter();
            foreach ($Filter->getAttributes() as $name => $value)
                $CopyFilter->setAttribute($name, $value);
            $CopyFilter
                ->setId(null)
                ->setGroupId($Group->getId());

            if(!$CopyFilter->save())
                \Yii::app()->ajax->addErrors($model)->send();

            \Yii::app()->ajax
                ->addMessage('Фильтр скопирован')
                ->runJS('closeCustom')
                ->runJS('updateGrid', ['filter-list'])->send();
        } else {
            $criteria = new \CDbCriteria();
            $criteria->addCondition('id != :id');
            $criteria->scopes = ['enable'];
            $criteria->params = [':id' => $Group->getId()];
            /** @var \FilterGroup[] $GroupList */
            $GroupList = \FilterGroup::model()->findAll($criteria);

            \Yii::app()->ajax
                ->addHtml('_copy', '#customModal #replacement', [
                    'model' => $model,
                    'group' => $Group,
                    'groupList' => $GroupList
                ])
                ->runJS('openCustom')->send();
        }
    }
}