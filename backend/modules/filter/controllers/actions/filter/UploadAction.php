<?php
namespace backend\modules\filter\controllers\actions\filter;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\components\CImageHandler;
use common\components\VarDumper;
use Yii;
use CUploadedFile;
use CommentItems;
class UploadAction extends CAction
{
    public $filterField;

    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->addCondition('id = :id');
        $criteria->params = [':id' => Yii::app()->request->getParam('filter_id')];
        /** @var \Filter $Filter */
        $Filter = \Filter::model()->find($criteria);
        if(!$Filter)
            Yii::app()->ajax->addErrors('Фильтр не существует')->send();

        $link = null;
        /** @var CUploadedFile[] $files */
        $files = CUploadedFile::getInstancesByName('Image');
        foreach ($files as $file) {
            $title = \Filter::generateImageName($this->filterField.'-'.$Filter->getTitle().'-'.rand(0, 1000));
            /** @var CImageHandler $image */
            $image = Yii::app()->ih->load($file->getTempName());
            $link = sprintf('%s/%s.%s', \Filter::getImagePath(),$title , $image->getFormatType());

            $Filter->setAttribute($this->filterField.'_link', $link);
            $Filter->setAttribute($this->filterField.'_path', Yii::app()->static->staticPath().'/'.$link);
            if(!$Filter->save())
                Yii::app()->ajax->addErrors($Filter)->send();

            $sizes = \Filter::getSizesByType($this->filterField);
            $image->resize($sizes['w'], $sizes['h'], false)->save($Filter->getAttribute($this->filterField.'_path'));

            Yii::app()->ajax->addOther([
                'ok' => true,
                'link' => Yii::app()->static->imageLink($link),
                'name' => $file->getName()
            ]);

            break;
        }

        Yii::app()->ajax->send();
    }
}