<?php
namespace backend\modules\filter\controllers\actions\filter;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class AssignAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        /** @var \Filter $filter */
        $filter = \Filter::model()->findByPk(\Yii::app()->request->getParam('filter_id'));
        if(!$filter)
            \MException::ShowError('Группа не найдена');

        $model = new \FilterFilter('createAction');
        $model->setFilterId($filter->getId());

        $post = \Yii::app()->request->getParam('FilterFilter');
        if($post) {
            $model->setAttributes($post);
            $exists = \Filter::model()->count('id = :id', [':id' => \Yii::app()->request->getParam('filter_id')]);
            if(!$exists)
                \Yii::app()->ajax->addErrors('Фильтр не найден')->send();

            $t = \Yii::app()->db->beginTransaction();
            try {
                //check on exists assign
                $criteria = new \CDbCriteria();
                $criteria->addCondition('filter_id = :filter_id');
                $criteria->addCondition('filter_assign_id = :filter_assign_id');
                $criteria->params = [':filter_id' => $filter->getId(), ':filter_assign_id' => $model->getFilterAssignId()];
                $exists = \FilterFilter::model()->count($criteria);
                if($exists)
                    \Yii::app()->ajax->addErrors('Этот фильтр уже привязан, выберите другой')->send();

                $criteria->params = [':filter_assign_id' => $filter->getId(), ':filter_id' => $model->getFilterAssignId()];
                $exists = \FilterFilter::model()->count($criteria);
                if(!$exists) {
                    $model2 = new \FilterFilter('createAction');
                    $model2->setFilterId($model->getFilterAssignId())
                        ->setFilterAssignId($filter->getId());
                    if(!$model2->save()) {
                        \Yii::app()->ajax->addErrors($model2)->send();
                        throw new \Exception();
                    }
                }

                if(!$model->save()) {
                    \Yii::app()->ajax->addErrors($model)->send();
                    throw new \Exception();
                }

                \Yii::app()->ajax
                    ->addMessage('Фильтр привязан')
                    ->runJS('closeCustom')
                    ->runJS('updateGrid', ['filter-list']);

                $t->commit();
            } catch (\Exception $ex) {
                $t->rollback();
            }
            \Yii::app()->ajax->send();
        } else {
            $criteria = new \CDbCriteria();
            $criteria->addCondition('id != :id');
            $criteria->scopes = ['enable'];
            $criteria->params = [':id' => $filter->getGroupId()];
            /** @var \FilterGroup[] $GroupList */
            $GroupList = \FilterGroup::model()->findAll($criteria);

            \Yii::app()->ajax
                ->addHtml('_assign', '#customModal #replacement', [
                    'model' => $model,
                    'groupList' => $GroupList
                ])
                ->runJS('openCustom')->send();
        }
    }
}