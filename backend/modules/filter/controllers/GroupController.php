<?php
namespace backend\modules\filter\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use backend\modules\filter\components\FilterBaseController;
class GroupController extends FilterBaseController
{
    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => [
                    'filter'
                ],
            ],
            [
                'allow',
                'actions' => [
                    'list'
                ],
                'roles' => ['filterView']
            ],
            [
                'allow',
                'actions' => [
                    'add', 'update', 'delete', 'page', 'pageDelete'
                ],
                'roles' => ['filterControl']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    /**
     * Actions attached to this controller
     *
     * @return array
     */
    public function actions()
    {
        return array(
            'list'          => ['class' => 'backend\modules\filter\controllers\actions\group\ListAction'],
            'add'           => ['class' => 'backend\modules\filter\controllers\actions\group\AddAction'],
            'update'        => ['class' => 'backend\modules\filter\controllers\actions\group\UpdateAction'],
            'filter'        => ['class' => 'common\modules\filter\controllers\actions\group\FilterAction'],
            'delete'        => ['class' => 'backend\modules\filter\controllers\actions\group\DeleteAction'],
            'page'          => ['class' => 'backend\modules\filter\controllers\actions\group\PageAction'],
            'pageDelete'    => ['class' => 'backend\modules\filter\controllers\actions\group\PageDeleteAction'],
        );
    }
}