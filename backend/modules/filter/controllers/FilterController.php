<?php
namespace backend\modules\filter\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use backend\modules\filter\components\FilterBaseController;
class FilterController extends FilterBaseController
{
    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => [
                    'list'
                ],
                'roles' => ['filterView']
            ],
            [
                'allow',
                'actions' => [
                    'add', 'update', 'copy', 'delete', 'assign', 'deleteAssign', 'horizont', 'vertical'
                ],
                'roles' => ['filterControl']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    /**
     * Actions attached to this controller
     *
     * @return array
     */
    public function actions()
    {
        return array(
            'add'           => ['class' => 'backend\modules\filter\controllers\actions\filter\AddAction'],
            'update'        => ['class' => 'backend\modules\filter\controllers\actions\filter\UpdateAction'],
            'list'          => ['class' => 'backend\modules\filter\controllers\actions\filter\ListAction'],
            'copy'          => ['class' => 'backend\modules\filter\controllers\actions\filter\CopyAction'],
            'delete'        => ['class' => 'backend\modules\filter\controllers\actions\filter\DeleteAction'],
            'assign'        => ['class' => 'backend\modules\filter\controllers\actions\filter\AssignAction'],
            'deleteAssign'  => ['class' => 'backend\modules\filter\controllers\actions\filter\DeleteAssignAction'],
            'horizont'      => [
                'class' => 'backend\modules\filter\controllers\actions\filter\UploadAction',
                'filterField' => 'h_image'
            ],
            'vertical'      => [
                'class' => 'backend\modules\filter\controllers\actions\filter\UploadAction',
                'filterField' => 'v_image'
            ],
        );
    }
}