<?php
namespace backend\modules\users\controllers\actions\user;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class ListAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $model = new \User('search');
        $post = \Yii::app()->request->getParam('User');
        if($post)
            $model->setAttributes($post);

        $this->controller->render('list', ['model' => $model]);
    }
}