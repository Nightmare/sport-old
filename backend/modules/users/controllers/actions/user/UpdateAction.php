<?php
namespace backend\modules\users\controllers\actions\user;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\components\VarDumper;

class UpdateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        /** @var \User $User */
        $User = \User::model()->with('userProfile')->findByPk(\Yii::app()->request->getParam('user_id'));
        if(!$User)
            \MException::ShowError('Пользователь не найден');

        $UserProfile = $User->userProfile;
        if(!empty($UserProfile->birthday))
            $UserProfile->birthday = date('d.m.Y', $UserProfile->birthday);

        $Permissions = new \Permissions($User);

        $post = \Yii::app()->request->getParam('User');
        $postProfile = \Yii::app()->request->getParam('UserProfile');
        $permissions = \Yii::app()->request->getParam('Permissions');
        if($post) {
            $t = \Yii::app()->db->beginTransaction();
            try {
                $User->setAttributes($post);
                if(!$User->save()) {
                    \Yii::app()->ajax->addErrors($User);
                    throw new \Exception();
                }

                if(isset($postProfile['birthday']))
                    $postProfile['birthday'] = strtotime($postProfile['birthday'].' 00:00:00');
                $UserProfile->setAttributes($postProfile);
                if(!$UserProfile->save()) {
                    \Yii::app()->ajax->addErrors($UserProfile);
                    throw new \Exception();
                }

                if(\Yii::app()->user->checkAccess('admin')) {
                    $Permissions->setAttributes($permissions, false);
                    if(!$Permissions->save()) {
                        \Yii::app()->ajax->addErrors('Не удалось обновить права доступа');
                        throw new \Exception();
                    }
                }

                $t->commit();
                \Yii::app()->ajax->addMessage('Пользователь обновлен')->send(\Yii::app()->createUrl('/users/user/list'));
            } catch (\Exception $ex) {
                $t->rollback();
            }
        }

        \Yii::app()->static->setLibrary('fileapi')
            ->registerScriptFile2('FileAPI/FileAPI.min.js')
            ->registerScriptFile2('FileAPI/FileAPI.exif.js')
            ->registerScriptFile2('jquery.fileapi.min.js');

        $this->controller->render('form', [
            'model' => $User,
            'profile' => $UserProfile,
            'permissions' => $Permissions
        ]);
    }
}