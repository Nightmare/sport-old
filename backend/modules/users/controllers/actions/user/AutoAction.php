<?php
namespace backend\modules\users\controllers\actions\user;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class AutoAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $user_list = [];

        $term = \Yii::app()->request->getParam('term');
        $criteria = new \CDbCriteria();
        $criteria->addSearchCondition('username', $term, true, 'OR');
        $criteria->addSearchCondition('email', $term, true, 'OR');
        /** @var \User[] $Users */
        $Users = \User::model()->findAll($criteria);
        foreach ($Users as $User)
            $user_list[] = [
                'label' => $User->getUsername(),
                'id' => $User->getId()
            ];

        \Yii::app()->ajax->addOther($user_list)->send();
    }
}