<?php
namespace backend\modules\users\controllers\actions\user;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\components\CImageHandler;
use common\components\VarDumper;
use Yii;
use CUploadedFile;
use UserProfile;
class LogoAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->addCondition('id = :id');
        $criteria->params = [':id' => Yii::app()->request->getParam('user_id')];
        /** @var \User $User */
        $User = \User::model()->with('userProfile')->find($criteria);
        if(!$User)
            Yii::app()->ajax->addErrors('Пользователь не существует')->send();
        $UserProfile = $User->userProfile;

        /** @var CUploadedFile[] $files */
        $files = CUploadedFile::getInstancesByName('Image');
        foreach ($files as $file) {
            $title = \UserProfile::generateName('logo-'.$User->getUsername().'-'.rand(0, 1000));

            /** @var CImageHandler $image */
            $image = Yii::app()->ih->load($file->getTempName());
            $origin = sprintf('%s/%s.%s', UserProfile::generateLogoPath(UserProfile::SIZE_TYPE_ORIGIN, true), $title, $image->getFormatType());

            $image->save($origin);
            foreach (UserProfile::getLogoSizes() as $type => $info) {
                $image
                    ->reload()
                    ->resize($info['w'], $info['h'], false)
                    ->save(sprintf('%s/%s.%s', UserProfile::generateLogoPath($type, true), $title, $image->getFormatType()));
            };

            $UserProfile->setLogoName(sprintf('%s.%s', $title, $image->getFormatType()));
            if(!$UserProfile->save())
                Yii::app()->ajax->addErrors($UserProfile)->send();

            Yii::app()->ajax->addOther([
                'ok' => true,
                'link' => Yii::app()->static->imageLink(sprintf(
                    '%s/%s.%s',
                    UserProfile::generateLogoPath(UserProfile::SIZE_TYPE_MAX),
                    $title,
                    $image->getFormatType())
                ),
                'name' => $file->getName()
            ]);

            break;
        }

        Yii::app()->ajax->send();
    }
}