<?php
namespace backend\modules\users\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use backend\modules\users\components\UserBaseController;
class UserController extends UserBaseController
{
    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => [
                    'list', 'auto', 'logo', 'avatar'
                ],
                'roles' => ['userView']
            ],
            [
                'allow',
                'actions' => [
                    'update'
                ],
                'roles' => ['userEdit']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'list'   => ['class' => 'backend\modules\users\controllers\actions\user\ListAction'],
            'update' => ['class' => 'backend\modules\users\controllers\actions\user\UpdateAction'],
            'auto'   => ['class' => 'backend\modules\users\controllers\actions\user\AutoAction'],
            'avatar' => ['class' => 'backend\modules\users\controllers\actions\user\AvatarAction'],
            'logo'   => ['class' => 'backend\modules\users\controllers\actions\user\LogoAction'],
		);
	}
}