<?php
namespace backend\modules\gallery\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use backend\modules\users\components\UserBaseController;
use Yii;
class VideoController extends UserBaseController
{
    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => [
                    'list', 'youtube'
                ],
                'roles' => ['galleryView']
            ],
            [
                'allow',
                'actions' => [
                    'step1', 'step2', 'upload', 'update', 'delete'
                ],
                'roles' => ['galleryControl']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'list'      => ['class' => 'backend\modules\gallery\controllers\actions\video\ListAction'],
            'step1'     => ['class' => 'backend\modules\gallery\controllers\actions\video\Step1Action'],
            'step2'     => ['class' => 'backend\modules\gallery\controllers\actions\video\Step2Action'],
            'update'    => ['class' => 'backend\modules\gallery\controllers\actions\video\UpdateAction'],
            'delete'    => ['class' => 'backend\modules\gallery\controllers\actions\video\DeleteAction'],
            'youtube'   => ['class' => 'backend\modules\gallery\controllers\actions\video\YoutubeAction'],
		);
	}
}