<?php
namespace backend\modules\gallery\controllers\actions\channel;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\components\CImageHandler;
use CUploadedFile;
use Yii;
use VideoChannel;
class Upload2Action extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->addCondition('id = :id');
        $criteria->params = [':id' => Yii::app()->request->getParam('channel_id')];

        /** @var VideoChannel $VideoChannel */
        $VideoChannel = VideoChannel::model()->find($criteria);
        if(!$VideoChannel)
            Yii::app()->ajax->addErrors('Канал не найден')->send();

        if(!isset($_FILES['Image']) || !isset($_FILES['Image']['tmp_name']) || count($_FILES['Image']['tmp_name']) != 2)
            Yii::app()->ajax->send();

        foreach ($_FILES['Image']['tmp_name'] as $key => $item) {
            foreach ($item as $type => $path) {
                switch ($type) {
                    case 'thumb':
                        $type = VideoChannel::SIZE_TYPE_MIN;
                        break;
                    case 'original':
                        $type = VideoChannel::SIZE_TYPE_ORIGIN;
                        break;
                }

                $VideoChannel->{$type} = $path;
            }
        }

        if(!$VideoChannel->updateAction())
            Yii::app()->ajax->addErrors($VideoChannel);
        else
            Yii::app()->ajax->addOther([
                'ok' => true,
                'min' => $VideoChannel->getMinImage(),
                'minLink' => Yii::app()->static->imageLink($VideoChannel->getMinImage()),
                'origin' => $VideoChannel->getOriginalImage(),
                'originLink' => Yii::app()->static->imageLink($VideoChannel->getOriginalImage()),
            ])->addMessage(Yii::t('messages', 'Изображение добавлено, заполните инфомрацию'));

        Yii::app()->ajax->send();
    }
}