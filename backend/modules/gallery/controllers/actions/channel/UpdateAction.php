<?php
namespace backend\modules\gallery\controllers\actions\channel;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use VideoChannel;
use Yii;
class UpdateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->addCondition('id = :id');
        $criteria->params = [':id' => Yii::app()->request->getParam('channel_id')];

        /** @var VideoChannel $VideoChannel */
        $VideoChannel = VideoChannel::model()->find($criteria);
        if(!$VideoChannel)
            Yii::app()->ajax->addErrors('Канал не найден')->send();
        $VideoChannel->scenario = 'updateAction';

        $post = \Yii::app()->request->getPost('VideoChannel');
        if($post) {

            $VideoChannel->setAttributes($post);
            if(!$VideoChannel->updateAction()) {
                Yii::app()->ajax->addErrors($VideoChannel);
            } else
                Yii::app()->ajax->addMessage(Yii::t('labels', 'Канал обновлен'))
                    ->runJS('closeCustom')
                    ->runJS('updateGrid', ['gallery-channel-list']);

            Yii::app()->ajax->send();
        } else
            \Yii::app()->ajax
                ->addHtml('_form2', '#customModal #replacement', [
                    'model' => $VideoChannel
                ])->runJS('openCustom')->send();
    }
}