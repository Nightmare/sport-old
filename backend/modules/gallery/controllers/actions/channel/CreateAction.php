<?php
namespace backend\modules\gallery\controllers\actions\channel;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use VideoChannel;
use Yii;
class CreateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $VideoChannel = new VideoChannel('createAction');

        $post = \Yii::app()->request->getPost('VideoChannel');
        if($post) {

            $VideoChannel->setAttributes($post);
            $VideoChannel->min = Yii::app()->static->staticPath().'/'.Yii::app()->request->getPost('min');
            $VideoChannel->origin = Yii::app()->static->staticPath().'/'.Yii::app()->request->getPost('origin');

            if(!$VideoChannel->createAction())
                Yii::app()->ajax->addErrors($VideoChannel)->send();
            else
                Yii::app()->ajax->addMessage(Yii::t('labels', 'Канал добавлен'))
                    ->runJS('closeCustom')
                    ->runJS('updateGrid', ['gallery-channel-list'])->send();

        } else
            \Yii::app()->ajax
                ->addHtml('_form', '#customModal #replacement', [
                    'model' => $VideoChannel
                ])->runJS('openCustom')->send();
    }
}