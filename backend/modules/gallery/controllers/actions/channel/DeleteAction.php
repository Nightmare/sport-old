<?php
namespace backend\modules\gallery\controllers\actions\channel;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use GalleryImage;
use Yii;
class DeleteAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->addCondition('id = :id');
        $criteria->params = [':id' => Yii::app()->request->getParam('channel_id')];

        /** @var \VideoChannel $VideoChannel */
        $VideoChannel = \VideoChannel::model()->find($criteria);
        if(!$VideoChannel)
            Yii::app()->ajax->addErrors('Канал не найден')->send();
        $VideoChannel->scenario = 'deleteAction';

        if(!$VideoChannel->deleteAction()) {
            Yii::app()->ajax->addErrors($VideoChannel);
        } else
            Yii::app()->ajax->addMessage(Yii::t('labels', 'Канал удален'))
                ->runJS('closeCustom')
                ->runJS('updateGrid', ['gallery-channel-list']);

        Yii::app()->ajax->send();
    }
}