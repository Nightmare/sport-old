<?php
namespace backend\modules\gallery\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\helpers\StreetHelper;
use GalleryImage;
use Yii;
class Step2Action extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $GalleryImage = new GalleryImage('createAction');

        $conference = \Yii::app()->request->getParam('conference');
        $filterPost = Yii::app()->request->getPost('filter', []);

        $conference_id = StreetHelper::getConferenceByName($conference);
        if($conference_id === null)
            Yii::app()->ajax->addErrors('Локация не найдена')->send();

        $post = \Yii::app()->request->getPost('GalleryImage');
        if($post) {
            $filter = [];
            foreach ($filterPost as $filter_id) {
                $Filter = Yii::app()->categories->getFilter($filter_id);
                if($Filter !== null)
                    $filter[] = $Filter->getId();
            }

            $GalleryImage->setAttributes($post);
            $GalleryImage->setFromAdmin(true)
                ->setConferenceId($conference_id)
                ->setFilterIds(serialize($filter));
            $GalleryImage->min = Yii::app()->static->staticPath().'/'.Yii::app()->request->getPost('min');
            $GalleryImage->original = Yii::app()->static->staticPath().'/'.Yii::app()->request->getPost('origin');

            if(!$GalleryImage->createAction())
                Yii::app()->ajax->addErrors($GalleryImage)->send();
            else
                Yii::app()->ajax->addMessage(Yii::t('labels', 'Изображение добавлено'))
                    ->runJS('closeCustom')
                    ->runJS('updateGrid', ['gallery-image-list'])->send();

        } else
            \Yii::app()->ajax
                ->addHtml('_step2', '#customModal #replacement', [
                    'model' => $GalleryImage,
                    'conference' => $conference,
                ])->send();
    }
}