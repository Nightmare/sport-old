<?php
namespace backend\modules\gallery\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\components\CImageHandler;
use CUploadedFile;
use Yii;
use GalleryImage;
class UploadAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        if(!isset($_FILES['Image']) || !isset($_FILES['Image']['tmp_name']) || count($_FILES['Image']['tmp_name']) != 2)
            Yii::app()->ajax->send();

        $imageNameWithoutExt = GalleryImage::generateImageName();
        foreach ($_FILES['Image']['tmp_name'] as $key => $item) {
            foreach ($item as $type => $path) {
                switch ($type) {
                    case 'thumb':
                        $type = GalleryImage::SIZE_TYPE_MIN;
                        break;
                    case 'original':
                        $type = GalleryImage::SIZE_TYPE_ORIGIN;
                        break;
                }
                /** @var CImageHandler $image */
                $image = new CImageHandler();
                $image->load($path);

                $imagePath = sprintf('%s/%s.%s', GalleryImage::getTempFilePath($type), $imageNameWithoutExt, $image->getFormatType());
                $path = sprintf('%s/%s', Yii::app()->static->staticPath(), $imagePath);
                $image->save($path);

                Yii::app()->ajax->addOther([
                    $type => $imagePath,
                    $type.'Link' => Yii::app()->static->imageLink($imagePath),
                    'path' => $path
                ]);
            }
        }

        Yii::app()->ajax->addMessage(Yii::t('messages', 'Изображение добавлено, заполните инфомрацию'))
            ->addOther(['ok' => true])->send();
    }
}