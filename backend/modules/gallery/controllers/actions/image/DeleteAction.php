<?php
namespace backend\modules\gallery\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use GalleryImage;
use Yii;
class DeleteAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->addCondition('id = :id');
        $criteria->params = [':id' => Yii::app()->request->getParam('image_id')];

        /** @var GalleryImage $GalleryImage */
        $GalleryImage = GalleryImage::model()->find($criteria);
        if(!$GalleryImage)
            Yii::app()->ajax->addErrors('Фотография не найдена')->send();
        $GalleryImage->scenario = 'deleteAction';

        if(!$GalleryImage->deleteAction()) {
            Yii::app()->ajax->addErrors($GalleryImage);
        } else
            Yii::app()->ajax->addMessage(Yii::t('labels', 'Изображение удалено'))
                ->runJS('closeCustom')
                ->runJS('updateGrid', ['gallery-image-list']);

        Yii::app()->ajax->send();
    }
}