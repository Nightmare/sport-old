<?php
namespace backend\modules\gallery\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class ListAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        \Yii::app()->static->setLibrary('fileapi')
            ->registerScriptFile2('FileAPI/FileAPI.min.js')
            ->registerScriptFile2('FileAPI/FileAPI.exif.js')
            ->registerScriptFile2('jquery.fileapi.min.js');

        \Yii::app()->static->setLibrary('fileapi')
            ->registerScriptFile2('jcrop/jquery.Jcrop.min.js')
            ->registerCssFile2('jcrop/jquery.Jcrop.min.css');

        $model = new \GalleryImage('search');
        $model->unsetAttributes();
        $post = \Yii::app()->request->getParam('GalleryImage');
        if($post)
            $model->setAttributes($post);

        $criteria = new \CDbCriteria();
        $criteria->scopes = ['enable'];
        $dataProvider = new \CActiveDataProvider($model, [
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        \Yii::app()->breadcrumbs->addBreadcrumb('Фотографии');

        $this->controller->render('list', ['dataProvider' => $dataProvider]);
    }
}