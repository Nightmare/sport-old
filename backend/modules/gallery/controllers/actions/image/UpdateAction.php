<?php
namespace backend\modules\gallery\controllers\actions\image;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use GalleryImage;
use Yii;
class UpdateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $filterPost = Yii::app()->request->getPost('filter', []);

        $criteria = new \CDbCriteria();
        $criteria->addCondition('id = :id');
        $criteria->params = [':id' => Yii::app()->request->getParam('image_id')];

        /** @var GalleryImage $GalleryImage */
        $GalleryImage = GalleryImage::model()->find($criteria);
        if(!$GalleryImage)
            Yii::app()->ajax->addErrors('Фотография не найдена')->send();
        $GalleryImage->scenario = 'updateAction';

        $post = \Yii::app()->request->getPost('GalleryImage');
        if($post) {
            $filter = [];
            foreach ($filterPost as $filter_id) {
                $Filter = Yii::app()->categories->getFilter($filter_id);
                if($Filter !== null)
                    $filter[] = $Filter->getId();
            }

            $GalleryImage->setAttributes($post);
            if(isset($post['conference_id']))
                $GalleryImage->setConferenceId($post['conference_id']);
            $GalleryImage->setFilterIds(serialize($filter));
            if(!$GalleryImage->updateAction()) {
                Yii::app()->ajax->addErrors($GalleryImage);
            } else
                Yii::app()->ajax->addMessage(Yii::t('labels', 'Изображение обновлено'))
                    ->runJS('closeCustom')
                    ->runJS('updateGrid', ['gallery-image-list']);

            Yii::app()->ajax->send();
        } else
            \Yii::app()->ajax
                ->addHtml('_form', '#customModal #replacement', [
                    'model' => $GalleryImage
                ])->runJS('openCustom')->send();
    }
}