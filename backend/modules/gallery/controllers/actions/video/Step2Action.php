<?php
namespace backend\modules\gallery\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use common\helpers\StreetHelper;
use GalleryVideo;
use Yii;
class Step2Action extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $GalleryVideo = new GalleryVideo('createAction');

        $conference = \Yii::app()->request->getParam('conference');
        $filterPost = Yii::app()->request->getPost('filter', []);

        $conference_id = StreetHelper::getConferenceByName($conference);
        if($conference_id === null)
            Yii::app()->ajax->addErrors('Локация не найдена')->send();

        $post = \Yii::app()->request->getPost('GalleryVideo');
        if($post) {
            $filter = [];
            foreach ($filterPost as $filter_id) {
                $Filter = Yii::app()->categories->getFilter($filter_id);
                if($Filter !== null)
                    $filter[] = $Filter->getId();
            }

            $GalleryVideo->setAttributes($post);
            $GalleryVideo->setFromAdmin(true)
                ->setConferenceId($conference_id)
                ->setFilterIds(serialize($filter));

            if(!$GalleryVideo->createAction())
                Yii::app()->ajax->addErrors($GalleryVideo)->send();
            else
                Yii::app()->ajax->addMessage(Yii::t('labels', 'Видеозапись добавлено'))
                    ->runJS('closeCustom')
                    ->runJS('updateGrid', ['gallery-video-list'])->send();

        } else
            \Yii::app()->ajax
                ->addHtml('_step2', '#customModal #replacement', [
                    'model' => $GalleryVideo,
                    'conference' => $conference,
                ])->send();
    }
}