<?php
namespace backend\modules\gallery\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use GalleryVideo;
use Yii;
class DeleteAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->addCondition('id = :id');
        $criteria->params = [':id' => Yii::app()->request->getParam('video_id')];

        /** @var GalleryVideo $GalleryVideo */
        $GalleryVideo = GalleryVideo::model()->find($criteria);
        if(!$GalleryVideo)
            Yii::app()->ajax->addErrors('Видеозапись не найдена')->send();
        $GalleryVideo->scenario = 'deleteAction';

        if(!$GalleryVideo->deleteAction()) {
            Yii::app()->ajax->addErrors($GalleryVideo);
        } else
            Yii::app()->ajax->addMessage(Yii::t('labels', 'Видеозапись удалена'))
                ->runJS('closeCustom')
                ->runJS('updateGrid', ['gallery-video-list']);

        Yii::app()->ajax->send();
    }
}