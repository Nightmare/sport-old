<?php
namespace backend\modules\gallery\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
class ListAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $model = new \GalleryVideo('search');
        $model->unsetAttributes();
        $post = \Yii::app()->request->getParam('GalleryVideo');
        if($post)
            $model->setAttributes($post);

        $criteria = new \CDbCriteria();
        $criteria->scopes = ['enable'];
        $dataProvider = new \CActiveDataProvider($model, [
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        \Yii::app()->breadcrumbs->addBreadcrumb('Видеозаписи');

        $this->controller->render('list', ['dataProvider' => $dataProvider]);
    }
}