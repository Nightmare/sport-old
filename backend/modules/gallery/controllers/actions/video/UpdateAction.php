<?php
namespace backend\modules\gallery\controllers\actions\video;
/**
 * Most basic landing page rendering action possible.
 *
 * @package YiiBoilerplate\Frontend\Actions
 */
use CAction;
use GalleryVideo;
use Yii;
class UpdateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $filterPost = Yii::app()->request->getPost('filter', []);

        $criteria = new \CDbCriteria();
        $criteria->addCondition('id = :id');
        $criteria->params = [':id' => Yii::app()->request->getParam('video_id')];

        /** @var GalleryVideo $GalleryVideo */
        $GalleryVideo = GalleryVideo::model()->find($criteria);
        if(!$GalleryVideo)
            Yii::app()->ajax->addErrors('Видеозапись не найдена')->send();
        $GalleryVideo->scenario = 'updateAction';

        $post = \Yii::app()->request->getPost('GalleryVideo');
        if($post) {
            $filter = [];
            foreach ($filterPost as $filter_id) {
                $Filter = Yii::app()->categories->getFilter($filter_id);
                if($Filter !== null)
                    $filter[] = $Filter->getId();
            }

            $GalleryVideo->setAttributes($post);
            if(isset($post['conference_id']))
                $GalleryVideo->setConferenceId($post['conference_id']);
            $GalleryVideo->setFilterIds(serialize($filter));
            if(!$GalleryVideo->updateAction()) {
                Yii::app()->ajax->addErrors($GalleryVideo);
            } else
                Yii::app()->ajax->addMessage(Yii::t('labels', 'Видеозапись обновлена'))
                    ->runJS('closeCustom')
                    ->runJS('updateGrid', ['gallery-video-list']);

            Yii::app()->ajax->send();
        } else
            \Yii::app()->ajax
                ->addHtml('_form', '#customModal #replacement', [
                    'model' => $GalleryVideo
                ])->runJS('openCustom')->send();
    }
}