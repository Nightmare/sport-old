<?php
namespace backend\modules\gallery\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use backend\modules\users\components\UserBaseController;
use Yii;
class ChannelController extends UserBaseController
{
    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => [
                    'list'
                ],
                'roles' => ['galleryView']
            ],
            [
                'allow',
                'actions' => [
                    'create', 'upload', 'upload2', 'update', 'delete'
                ],
                'roles' => ['galleryControl']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'list'      => ['class' => 'backend\modules\gallery\controllers\actions\channel\ListAction'],
            'create'    => ['class' => 'backend\modules\gallery\controllers\actions\channel\CreateAction'],
            'upload'    => ['class' => 'backend\modules\gallery\controllers\actions\channel\UploadAction'],
            'upload2'   => ['class' => 'backend\modules\gallery\controllers\actions\channel\Upload2Action'],
            'update'    => ['class' => 'backend\modules\gallery\controllers\actions\channel\UpdateAction'],
            'delete'    => ['class' => 'backend\modules\gallery\controllers\actions\channel\DeleteAction'],
		);
	}
}