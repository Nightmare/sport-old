<?php
namespace backend\modules\gallery\controllers;
/**
 * Basic "kitchen sink" controller for frontend.
 * It was configured to be accessible by `/site` route, not the `/frontendSite` one!
 *
 * @package YiiBoilerplate\Frontend
 */

use backend\modules\users\components\UserBaseController;
use Yii;
class ImageController extends UserBaseController
{
    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => [
                    'list'
                ],
                'roles' => ['galleryView']
            ],
            [
                'allow',
                'actions' => [
                    'step1', 'step2', 'upload', 'update', 'delete'
                ],
                'roles' => ['galleryControl']
            ],
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

	/**
     * Actions attached to this controller
     *
	 * @return array
	 */
	public function actions()
    {
		return array(
            'list'      => ['class' => 'backend\modules\gallery\controllers\actions\image\ListAction'],
            'step1'     => ['class' => 'backend\modules\gallery\controllers\actions\image\Step1Action'], //Выбор локации
            'step2'     => ['class' => 'backend\modules\gallery\controllers\actions\image\Step2Action'], //Настройка изображения
            'upload'    => ['class' => 'backend\modules\gallery\controllers\actions\image\UploadAction'],
            'update'    => ['class' => 'backend\modules\gallery\controllers\actions\image\UpdateAction'],
            'delete'    => ['class' => 'backend\modules\gallery\controllers\actions\image\DeleteAction'],
		);
	}
}