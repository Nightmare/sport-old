<?php
/**
 * Base class for the controllers in backend entry point of application.
 *
 * In here we have the behavior common to all backend routes, such as registering assets required for UI
 * and enforcing access control policy.
 *
 * @package YiiBoilerplate\Backend
 */
abstract class BackendController extends \common\components\Controller
{
    public $layout = 'main';

    /**
     * Rules for CAccessControlFilter.
     *
     * We allow all actions to logged in users and disable everything for others.
     *
     * @see http://www.yiiframework.com/doc/api/1.1/CController#accessRules-detail
     *
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'users' => ['@'] ],
			['deny'],
		];
    }

    private function registerAssets()
    {
        Yii::app()->static->setLibrary('jquery')
            ->registerScriptFile('jquery-ui.min.js');

        Yii::app()->static->setLibrary('scroll')
            ->registerScriptFile('scrollTo.js');

        Yii::app()->static->setOwn()
            ->registerCssFile('backend-main.css')
            ->registerCssFile('backend-style.css')
            ->registerCssFile('ajax.css')
            ->registerCssFile('form.css')
            ->registerScriptFile('ajax.js')
            ->registerScriptFile('script.js')
            ->registerScriptFile('autoNumeric.js')
            ->registerScriptFile('admin.js');

        Yii::app()->static->setLibrary('fileapi')
            ->registerScriptFile2('FileAPI/FileAPI.min.js')
            ->registerScriptFile2('FileAPI/FileAPI.exif.js')
            ->registerScriptFile2('jquery.fileapi.min.js');

        Yii::app()->static->setLibrary('fileapi')
            ->registerScriptFile2('jcrop/jquery.Jcrop.min.js')
            ->registerCssFile2('jcrop/jquery.Jcrop.min.css');
    }

    /**
     * Before rendering anything we register all of CSS and JS assets we require for backend UI.
     *
     * @see CController::beforeRender()
     *
     * @param string $view
     * @return bool
     */
    protected function beforeRender($view)
    {
        $result = parent::beforeRender($view);
        $this->registerAssets();
        return $result;
    }
}
