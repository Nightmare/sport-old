<?php
/**
 * Top menu definition.
 *
 * @var BackendController $this
 */

$this->widget(
    'booster.widgets.TbNavbar',
    array(
        'type' => 'inverse',
        'brand' => 'Вернуться на сайт',
        'brandUrl' => Yii::app()->createUrl('/site/index', [], 'frontend'),
        'collapse' => true,
        'items' => [
            [
                'class' => 'booster.widgets.TbMenu',
                'items' => [
                    [
                        'label' => 'Главная', 'url' => ['/admin/index']
                    ],
                    [
                        'label' => 'Выйти (' . Yii::app()->user->name . ')',
                        'url' => Yii::app()->createUrl('/users/user/logout', [], 'frontend'),
                        'visible' => !Yii::app()->user->isGuest
                    ],
                ],
            ],
        ],
    )
);