<?php
/**
 * Inner part of the layout which includes a sidebar with portlet widget containing menu for CRUD.
 *
 * @var BackendController $this
 * @var string $content
 */

$this->beginContent('//layouts/template');
?>
    <div class="container">
        <main class="content">
            <?= $content; ?>
        </main>
    </div>
    <aside class="left-sidebar">
        <?php $this->widget(
            'booster.widgets.TbMenu',
            [
                'type' => 'list',
                'items' => [
                    [
                        'label' => 'Пользователи',
                        'url' => ['/users/user/list'],
                        'visible' => Yii::app()->user->checkAccess('userView')
                    ],
                    [
                        'label' => 'Галерея',
                        'items' => [
                            [
                                'label' => 'Фото',
                                'url' => ['/gallery/image/list']
                            ],
                            [
                                'label' => 'Видео',
                                'url' => ['/gallery/video/list']
                            ],
                            [
                                'label' => 'Канал',
                                'url' => ['/gallery/channel/list']
                            ],
                        ],
                        'visible' => Yii::app()->user->checkAccess('galleryView')
                    ],
                    [
                        'label' => 'Фильтры',
                        'url' => ['/filter/group/list'],
                        'visible' => Yii::app()->user->checkAccess('filterView')
                    ],
                    [
                        'label' => 'Спорт-залы',
                        'url' => ['/gymnasium/index/list'],
                        'visible' => Yii::app()->user->checkAccess('gymnasiumView')
                    ],
                ]
            ]
        ); ?>
    </aside>
<?php $this->endContent(); ?>