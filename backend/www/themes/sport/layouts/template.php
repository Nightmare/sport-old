<?php
/**
 * Main layout file for the whole backend.
 * It is based on Twitter Bootstrap classes inside HTML5Boilerplate.
 *
 * @var BackendController $this
 * @var string $content
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <script type="text/javascript" src="<?= Yii::app()->static->setLibrary('jquery')->getLink('js/jquery-1.10.2.min.js'); ?>"></script>
    <title><?= CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<?php $this->renderPartial('//layouts/_navigation');?>
<div class="wrapper">
    <div class="middle">
        <?php $this->widget('\common\widgets\breadcrumb\BreadcrumbWidget', ['view' => 'backend']); ?>
        <?= $content; ?>
    </div>
</div>

<div id="page-dark">
    <div id="page-loader">

    </div>
</div>
<div class="modal fade" id="customModal" tabindex="-1" role="dialog" aria-labelledby="customModalLabel" aria-hidden="true">
    <div class="modal-dialog" id="replacement"></div>
</div>
<div id="crop-dark"></div>
<div id="crop-block">
    <div id="image-crop"></div>
    <hr>
    <div class="pull-right" id="crop-control">
        <a href="" id="crop-save" data-type="ajax" class="btn btn-primary btn-xs">Сохранить</a>
        <a href="javascript:void(0)" id="crop-cancel" class="btn btn-default btn-xs">Отмена</a>
    </div>
</div>
</body>
</html>
