<?php
/**
 * @var BackendAdminController $this
 */
$this->pageTitle=Yii::app()->name; ?>

<div class="">
    <div class="line">
        <table class="table" style="width: 300px;display: inline-block;">
            <tr>
                <th colspan="2">Медиа и пользователи</th>
            </tr>
            <tr>
                <td>Пользователей:</td>
                <td><?= $user_count; ?></td>
            </tr>
            <tr>
                <td>Фотографий:</td>
                <td><?= $image_count; ?></td>
            </tr>
            <tr>
                <td>Видеозаписей:</td>
                <td><?= $video_count; ?></td>
            </tr>
        </table>
        <table class="table" style="width: 300px;display: inline-block;">
            <tr>
                <th colspan="2">Комментарии</th>
            </tr>
            <tr>
                <td>К видео:</td>
                <td><?= $video_comment_count; ?></td>
            </tr>
            <tr>
                <td>К фото:</td>
                <td><?= $image_comment_count; ?></td>
            </tr>
            <tr>
                <td>За сегодня:</td>
                <td><?= $today_comment_count; ?></td>
            </tr>
        </table>
    </div>
</div>




