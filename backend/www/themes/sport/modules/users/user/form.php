<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 26.02.2015
 * Time: 22:49
 *
 * @var TbActiveForm $form
 * @var \backend\modules\users\components\UserBaseController $this
 * @var User $model
 * @var UserProfile $profile
 * @var Permissions $permissions
 */ ?>

<?php $form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    [
        'id' => 'horizontalForm',
        'type' => 'horizontal',
    ]
); ?>
    <fieldset>
        <legend>Редактирование <?= $model->getUsername(); ?></legend>
        <div class="user-edit-block">
            <table style="width:100%;">
                <tr>
                    <td>
                        <?= $form->textFieldGroup(
                            $model,
                            'username',
                            [
                                'wrapperHtmlOptions' => ['class' => 'col-sm-5'],
                            ]
                        ); ?>
                    </td>
                    <td rowspan="4">
                        <div class="avatar-block" id="avatar-file-block" style="background:url('<?= $model->userProfile->getAvatar(UserProfile::SIZE_TYPE_MAX) ?>')">
                            <a data-type="file2" data-for-name="Image[avatar]" class="btn btn-primary sq" data-file-block="avatar-file-block" href="<?= Yii::app()->createUrl('/users/user/avatar', ['user_id' => $model->getId()]) ?>">Аватар</a>
                            <input class="hidden-file" type="file" name="Image[avatar]">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= $form->textFieldGroup(
                            $model,
                            'email',
                            [
                                'wrapperHtmlOptions' => ['class' => 'col-sm-5'],
                            ]
                        ); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= $form->textFieldGroup(
                            $profile,
                            'first_name',
                            [
                                'wrapperHtmlOptions' => ['class' => 'col-sm-5'],
                            ]
                        ); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= $form->textFieldGroup(
                            $profile,
                            'middle_name',
                            [
                                'wrapperHtmlOptions' => ['class' => 'col-sm-5'],
                            ]
                        ); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= $form->textFieldGroup(
                            $profile,
                            'last_name',
                            [
                                'wrapperHtmlOptions' => ['class' => 'col-sm-5'],
                            ]
                        ); ?>
                    </td>
                    <td rowspan="4">
                        <div class="logo-block" id="logo-file-block" style="background:url('<?= $model->userProfile->getLogo(UserProfile::SIZE_TYPE_MAX) ?>')">
                            <a data-type="file2" data-for-name="Image[logo]" class="btn btn-primary sq" data-file-block="logo-file-block" href="<?= Yii::app()->createUrl('/users/user/logo', ['user_id' => $model->getId()]) ?>">Лого</a>
                            <input class="hidden-file" type="file" name="Image[logo]">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= $form->radioButtonListGroup(
                            $profile,
                            'sex',
                            [
                                'wrapperHtmlOptions' => ['class' => 'col-sm-5'],
                                'widgetOptions' => [
                                    'data' => UserProfile::getSexList(),
                                ],
                            ]
                        ); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="form-group">
                            <?= $form->label($profile, 'birthday', ['class' => 'col-sm-3 control-label']); ?>
                            <div class="col-sm-5">
                                <?php
                                $this->widget('zii.widgets.jui.CJuiDatePicker', [
                                    'model' => $profile,
                                    'attribute' => 'birthday',
                                    'options' => [
                                        'changeYear' => true,
                                        'changeMonth' => true,
                                        'yearRange' => '-70:-15',
                                        'dateFormat' => 'dd.mm.yy',
                                        'orientation' => 'auto',
                                    ],
                                    'htmlOptions' => ['class' => 'form-control']
                                ]);
                                ?>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <?php if(Yii::app()->user->checkAccess('admin')): ?>
            <div class="">
                <h3>Права доступа</h3>
                <table class="table">
                    <tr>
                        <th></th>
                        <th class="center">Пользователями</th>
                        <th class="center">Галереей</th>
                        <th class="center">Фильтрами</th>
                        <th class="center">Спорт-залы</th>
                    </tr>
                    <tr>
                        <td class="">Просмотр</td>
                        <td class="center">
                            <?= CHtml::activeCheckBox($permissions, 'userView') ?>
                        </td>
                        <td class="center">
                            <?= CHtml::activeCheckBox($permissions, 'galleryView') ?>
                        </td>
                        <td class="center">
                            <?= CHtml::activeCheckBox($permissions, 'filterView') ?>
                        </td>
                        <td class="center">
                            <?= CHtml::activeCheckBox($permissions, 'gymnasiumView') ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="">Управление</td>
                        <td class="center">
                            <?= CHtml::activeCheckBox($permissions, 'userControl') ?>
                        </td>
                        <td class="center">
                            <?= CHtml::activeCheckBox($permissions, 'galleryControl') ?>
                        </td>
                        <td class="center">
                            <?= CHtml::activeCheckBox($permissions, 'filterControl') ?>
                        </td>
                        <td class="center">
                            <?= CHtml::activeCheckBox($permissions, 'gymnasiumControl') ?>
                        </td>
                    </tr>
                </table>
            </div>
        <?php endif; ?>
    </fieldset>
    <div class="form-actions">
        <?php $this->widget(
            'booster.widgets.TbButton',
            [
                'buttonType' => 'submit',
                'context' => 'primary',
                'label' => 'Сохранить'
            ]
        ); ?>
        <?php $this->widget(
            'booster.widgets.TbButton',
            ['buttonType' => 'reset', 'label' => 'Reset']
        ); ?>
    </div>
<?php $this->endWidget(); ?>