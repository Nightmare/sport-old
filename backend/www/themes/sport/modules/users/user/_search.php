<?php
/**
 * Created by PhpStorm.
 * User: Nick Nikitchenko (skype: quietasice)
 * Date: 28.02.2015
 * Time: 22:52
 *
 * @var User $model
 * @var BackendAdminController $this
 */ ?>

<?php
$this->widget('zii.widgets.jui.CJuiAutoComplete', [
    'attribute' => 'username',
    'model' => $model,
    'sourceUrl' => ['/users/user/auto'],
    'options' => [
        'minLength' => 3,
        'select' => "js:function(event, ui) {
            updateGrid('user-list', {'User[username]':ui.item['label']});
        }"
    ],
    'htmlOptions' => [
        'size' => 45,
        'maxlength' => 45,
        'placeholder' => 'Начните вводить логин или email',
        'class' => 'form-control'
    ],
]); ?>