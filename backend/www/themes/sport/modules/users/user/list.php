<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 23.02.2015
 * Time: 2:22
 *
 * @var \backend\modules\users\components\UserBaseController $this
 * @var User $model
 */?>

<div class="search-filter">
    <?php $this->renderPartial('_search', ['model' => $model]) ?>
</div>
<div class="grid-block">
    <?php $this->widget(
        'booster.widgets.TbGridView',
        [
            'id' => 'user-list',
            'dataProvider' => $model->search(),
            'template' => "{pager}{items}{pager}",
            'columns' => [
                ['name' => 'id', 'header' => '#', 'htmlOptions' => ['style'=>'width: 60px']],
                'username',
                'email',
                [
                    'name' => 'create_at',
                    'value' => 'date("d.m.Y", $data->getCreateAt())'
                ],
                [
                    'class' => 'booster.widgets.TbButtonColumn',
                    'htmlOptions' => ['nowrap' => 'nowrap'],
                    'viewButtonUrl' => null,
                    'updateButtonUrl' => 'Yii::app()->createUrl("/users/user/update", ["user_id" => $data->getId()])',
                    'deleteButtonUrl' => null,
                    'template' => '{update}'
                ]
            ],
        ]
    ); ?>
</div>
<script>
    $(function(){
        $(document.body).on('blur', '#User_username', function(){
            var $self = $(this);
            if($.trim($self.val()) == "")
                updateGrid('user-list', {'User[username]':''});
        });
    });
</script>