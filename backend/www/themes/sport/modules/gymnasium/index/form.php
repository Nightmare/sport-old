<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 26.02.2015
 * Time: 22:49
 *
 * @var TbActiveForm $form
 * @var \backend\modules\users\components\UserBaseController $this
 * @var Gymnasium $model
 * @var EGMap $gMap
 */ ?>
<?php $form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    [
        'id' => 'gymnasium',
        'type' => 'horizontal',
        'htmlOptions' => [
            'class' => 'ajax'
        ]
    ]
); ?>
<fieldset>
    <legend>Спорт-зал</legend>
    <div class="gymnasium-edit-block">
        <table style="width:100%;">
            <tr>
                <td>
                    <div class="form-group">
                        <div class="">
                            <?= $form->labelEx($model, 'country_id', ['class' => 'control-label', 'style' => 'padding-left: 15px;']) ?>
                        </div>
                        <div class="col-sm-5">
                            <?php $data = $model->getIsNewRecord() ? [] : [$model->country->getId() => $model->country->getNameRu()]; ?>
                            <?= Select2::activeDropDownList(
                                $model,
                                'country_id',
                                $data,
                                [
                                    'class' => 'field',
                                    'empty' => '',
                                    'select2Options' => [
                                        'minimumInputLength' => 3,
                                        'placeholder' => 'Выбирите страну',
                                        'ajax' => [
                                            'delay' => 250,
                                            'cache' => true,
                                            'url' => Yii::app()->createUrl('/location/country', [], 'frontend'),
                                            'type' => 'post',
                                            'dataType' => 'json',
                                            'data' => new CJavaScriptExpression('function (params) { return {q: params.term}}'),
                                            'processResults' => new CJavaScriptExpression('function (data, page) {return {results:  data.locations}}'),
                                        ],
                                        'onTrigger' => [
                                            'select2:select' => new CJavaScriptExpression('function(e) { changeCountry(e);}'),
                                        ]
                                    ]
                                ]
                            ) ?>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <div class="">
                            <?= $form->labelEx($model, 'region_id', ['class' => 'control-label', 'style' => 'padding-left: 15px;']) ?>
                        </div>
                        <div class="col-sm-5">
                            <?php $data = $model->getIsNewRecord() ? [] : [$model->region->getId() => $model->region->getNameRu()]; ?>
                            <?= Select2::activeDropDownList(
                                $model,
                                'region_id',
                                $data,
                                [
                                    'class' => 'field',
                                    'empty' => '',
                                    'select2Options' => [
                                        'minimumInputLength' => 3,
                                        'placeholder' => 'Выбирите регион',
                                        'ajax' => [
                                            'delay' => 250,
                                            'url' => Yii::app()->createUrl('/location/region', [], 'frontend'),
                                            'type' => 'post',
                                            'dataType' => 'json',
                                            'data' => new CJavaScriptExpression('function (params) { return {q: params.term, country_id: $("#Gymnasium_country_id").val()}}'),
                                            'processResults' => new CJavaScriptExpression('function (data, page) {return {results:  data.locations}}'),
                                        ],
                                        'onTrigger' => [
                                            'select2:select' => new CJavaScriptExpression('function(e) { changeRegion();}'),
                                        ]
                                    ]
                                ]
                            ) ?>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <div class="">
                            <?= $form->labelEx($model, 'city_id', ['class' => 'control-label', 'style' => 'padding-left: 15px;']) ?>
                        </div>
                        <div class="col-sm-5">
                            <?php $data = $model->getIsNewRecord() ? [] : [$model->city->getId() => $model->city->getNameRu()]; ?>
                            <?= Select2::activeDropDownList(
                                $model,
                                'city_id',
                                $data,
                                [
                                    'class' => 'field',
                                    'empty' => '',
                                    'select2Options' => [
                                        'minimumInputLength' => 3,
                                        'placeholder' => 'Выбирите город',
                                        'ajax' => [
                                            'delay' => 250,
                                            'url' => Yii::app()->createUrl('/location/city', [], 'frontend'),
                                            'type' => 'post',
                                            'dataType' => 'json',
                                            'data' => new CJavaScriptExpression('function (params) { return {q: params.term, region_id: $("#Gymnasium_region_id").val()}}'),
                                            'processResults' => new CJavaScriptExpression('function (data, page) {return {results:  data.locations}}'),
                                        ],
                                        'onTrigger' => [
                                            'select2:select' => new CJavaScriptExpression('function(e) { changeCity(e);}'),
                                        ]
                                    ]
                                ]
                            ) ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <a href="javascript:void(0)" onclick="$('#map_block').toggle();google.maps.event.trigger(<?= $gMap->getJsName() ?>, 'resize');">Карта. Детализировать адрес</a>
                    <div id="map_block" style="display: none">
                        <input id="searchTextField" class="form-control field" name="Gymnasium[street]" style="" type="text" value="<?= $model->getStreet() ?>">
                        <?php $gMap->renderMap(['setMarker(47.25, 28.58);'], Yii::app()->language); ?>
                        <div id="map_render"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="image-block" id="image-file-block" style="background:url('<?= $model->getImage('main_image') ?>')">
                        <a
                            data-type="file-crop"
                            data-preview-width="300"
                            data-preview-height="150"
                            data-for-name="Image[main]"
                            class="btn btn-primary sq"
                            data-file-block="image-file-block"
                            data-for-name-origin="Gymnasium[main_image]"
                            href="<?= Yii::app()->createUrl('/gymnasium/index/image') ?>">Главная картинка</a>
                        <input class="hidden-file" type="file" name="Image[main]">
                        <input type="hidden" class="field" name="Gymnasium[main_image]" value="<?= $model->getMainImage() ?>">
                    </div>
                </td>
                <td>
                    <div class="logo-block" id="logo-file-block" style="background:url('<?= $model->getImage() ?>')">
                        <a
                            data-type="file-crop"
                            data-preview-width="96"
                            data-preview-height="96"
                            data-for-name="Image[logo]"
                            class="btn btn-primary sq"
                            data-file-block="logo-file-block"
                            data-for-name-origin="Gymnasium[logo]"
                            href="<?= Yii::app()->createUrl('/gymnasium/index/image') ?>">Лого</a>
                        <input class="hidden-file" type="file" name="Image[logo]">
                        <input type="hidden" class="field" name="Gymnasium[logo]"  value="<?= $model->getLogo() ?>">
                    </div>
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3">
                    <?= $form->textFieldGroup(
                        $model,
                        'title',
                        [
                            'wrapperHtmlOptions' => ['class' => 'col-sm-4'],
                            'widgetOptions' => ['htmlOptions' => [
                                'placeholder' => 'Введите название',
                                'class' => 'field'
                            ]],
                            'labelOptions' => ['label' => false]
                        ]); ?>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <?php $this->widget('\common\extensions\imperavi\ImperaviRedactorWidget', [
                        // You can either use it for model attribute
                        'model' => $model,
                        'attribute' => 'description',
                        'options' => [
                            'lang' => 'ru',
                            //'toolbar' => false,
                            'iframe' => true,
                            'convertImageLinks' => true
                        ],
                        'htmlOptions' => [
                            'class' => 'field'
                        ],
                        'plugins' => [
                            'fontcolor' => [
                                'js' => ['fontcolor.js']
                            ],
                            'fontfamily' => [
                                'js' => ['fontfamily.js']
                            ],
                            'fontsize' => [
                                'js' => ['fontsize.js']
                            ],
                            'imagemanager' => [
                                'js' => ['imagemanager.js']
                            ],
                            'table' => [
                                'js' => ['table.js']
                            ],
                            'textdirection' => [
                                'js' => ['textdirection.js']
                            ],
                            'textexpander' => [
                                'js' => ['textexpander.js']
                            ]
                        ]
                    ]); ?>
                </td>
            </tr>
        </table>
    </div>
</fieldset>
<?= $form->hiddenField($model, 'lat', ['class' => 'field']) ?>
<?= $form->hiddenField($model, 'lon', ['class' => 'field']) ?>
<div class="form-actions">
    <?php $this->widget(
        'booster.widgets.TbButton',
        [
            'buttonType' => 'submit',
            'context' => 'primary',
            'label' => 'Сохранить',
            'htmlOptions' => [
                'data-submit' => 'ajax',
                'data-for' => 'gymnasium'
            ]
        ]
    ); ?>
    <?php $this->widget(
        'booster.widgets.TbButton',
        ['buttonType' => 'reset', 'label' => 'Reset']
    ); ?>
</div>
<?php $this->endWidget(); ?>
<script>
    var $country = $('#Gymnasium_country_id');
    var $region = $('#Gymnasium_region_id');
    var $city = $('#Gymnasium_city_id');
    var $search = $('#searchTextField');
    $(function(){
        <?php if(!$model->region): ?>
            $region.prop("disabled", true);
        <?php endif; ?>
        <?php if(!$model->city): ?>
            $city.prop("disabled", true);
        <?php endif; ?>
        <?php if(!$model->getStreet()): ?>
            $search.prop("disabled", true);
        <?php endif; ?>
    });

    function changeCountry(e) {
        $region.val(null).trigger("change").prop("disabled", false);
        $city.val(null).trigger("change").prop("disabled", true);
        $search.prop("disabled", true);

        var lat = parseFloat(e.params.data.lat);
        var lng = parseFloat(e.params.data.lon);

        moveMap(lat, lng);
        <?= $gMap->getJsName() ?>.setZoom(6);
    }

    function changeRegion() {
        $city.val(null).trigger("change").prop("disabled", false);
    }

    var autocomplete = null;
    var places = null;
    function changeCity(e)
    {
        $search.prop("disabled", false);
        moveMap(e.params.data.lat, e.params.data.lon);
        <?= $gMap->getJsName() ?>.setZoom(9);
        if(autocomplete === null) {
            autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('searchTextField')));

            <?php $event = new \EGMapEvent('place_changed', "function() {
                                                                  var place = autocomplete.getPlace();
                                                                  if (place.geometry.viewport) {
                                                                    ".$gMap->getJsName().".fitBounds(place.geometry.viewport);
                                                                  } else {
                                                                    ".$gMap->getJsName().".setCenter(place.geometry.location);
                                                                    ".$gMap->getJsName().".setZoom(17);
                                                                  }
                                                                  moveMap(place.geometry.location.A, place.geometry.location.F);
                                                              }",false); ?>
            <?= $event->getEventJs('autocomplete') ?>
        }

        autocomplete.setTypes(['address']);
        autocomplete.setComponentRestrictions({ 'country': e.params.data.country });
    }

    <?php
    $dragevent = new \EGMapEvent('dragend', "function (event) { moveMarker(event.latLng.lat(), event.latLng.lng()); }", false, \EGMapEvent::TYPE_EVENT_DEFAULT);
    $icon = new \EGMapMarkerImage(\Yii::app()->static->imageLink('images/maps/weights.png'));
    $icon->setSize(32, 37);
    $icon->setAnchor(16, 16.5);
    $icon->setOrigin(0, 0);
    ?>

    var _marker = null;
    function setMarker(lat, lng)
    {
        _marker = new google.maps.Marker({
            map: <?= $gMap->getJsName(); ?>,
            draggable: true,
            icon: <?= $icon->toJs() ?>
        });
        <?= $dragevent->toJs('_marker') ?>;
        moveMarker(lat, lng);
    }

    function moveMap(lat, lng)
    {
        lat = parseFloat(lat);
        lng = parseFloat(lng);
        <?= $gMap->getJsName(); ?>.setCenter({lat: lat, lng:lng});
        moveMarker(lat, lng);
    }

    function moveMarker(lat, lng)
    {
        lat = parseFloat(lat);
        lng = parseFloat(lng);
        _marker.setPosition({lat: lat, lng:lng});

        $('#Gymnasium_lat').val(lat);
        $('#Gymnasium_lon').val(lng);
    }
</script>