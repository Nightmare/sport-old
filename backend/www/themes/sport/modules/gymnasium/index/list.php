<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 23.02.2015
 * Time: 2:22
 *
 * @var \backend\modules\users\components\UserBaseController $this
 * @var User $model
 */?>

<div class="search-filter">
    <?php $this->renderPartial('_search', ['model' => $model]) ?>
</div>
<a href="<?= Yii::app()->createUrl('/gymnasium/index/create') ?>" class="btn btn-primary btn-xs">Добавить спорт-зал</a>
<div class="grid-block">
    <?php $this->widget(
        'booster.widgets.TbGridView',
        [
            'id' => 'gymnasium-list',
            'dataProvider' => $model->search(),
            'template' => "{pager}{items}{pager}",
            'columns' => [
                ['name' => 'id', 'header' => '#', 'htmlOptions' => ['style'=>'width: 60px']],
                [
                    'header' => 'Расположение',
                    'value' => '$data->buildPlacement()'
                ],
                'title',
                'user_enable_count',
                [
                    'name' => 'create_at',
                    'value' => 'date("d.m.Y", $data->getCreateAt())'
                ],
                [
                    'class' => 'booster.widgets.TbButtonColumn',
                    'htmlOptions' => ['nowrap' => 'nowrap'],
                    'viewButtonUrl' => null,
                    'updateButtonUrl' => 'Yii::app()->createUrl("/gymnasium/index/update", ["gymnasium_id" => $data->getId()])',
                    'deleteButtonUrl' => null,
                    'template' => '{update}'
                ]
            ],
        ]
    ); ?>
</div>