<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 23.02.2015
 * Time: 2:29
 *
 * @var BackendAdminController $this
 * @var CActiveDataProvider $dataProvider
 *
 */ ?>

<a href="<?= Yii::app()->createUrl('/filter/group/add') ?>" data-type="ajax" class="btn btn-primary btn-xs">Создать группу</a>
<div class="grid-block">
    <?php $this->widget(
        'booster.widgets.TbGridView',
        [
            'id' => 'filter-group-list',
            'dataProvider' => $dataProvider,
            'template' => "{pager}\n{items}\n{pager}",
            'columns' => [
                ['name' => 'id', 'header' => '#', 'htmlOptions' => ['style'=>'width: 60px']],
                'title',
                [
                    'header' => 'Фильтры',
                    'type' => 'raw',
                    'value' => '$data->getFilterListString()'
                ],
                [
                    'header' => 'Привязка к страницам',
                    'type' => 'raw',
                    'value' => '$data->getPageListString()'
                ],
                [
                    'name' => 'create_at',
                    'value' => 'date("d.m.Y", $data->getCreateAt())'
                ],
                [
                    'class' => 'booster.widgets.TbButtonColumn',
                    'htmlOptions' => ['nowrap' => 'nowrap'],
                    'viewButtonUrl' => 'Yii::app()->createUrl("/filter/filter/list", ["group_id" => $data->getId()])',
                    'updateButtonUrl' => 'Yii::app()->createUrl("/filter/group/update", ["group_id" => $data->getId()])',
                    'updateButtonOptions' => ['data-type' => 'ajax'],
                    'deleteButtonUrl' => 'Yii::app()->createUrl("/filter/group/delete", ["group_id" => $data->getId()])',
                    'template' => '{view}{update}{delete}'
                ]
            ],
        ]
    ); ?>
</div>