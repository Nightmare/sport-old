<?php
/**
 * Created by PhpStorm.
 * User: Nick Nikitchenko (skype: quietasice)
 * Date: 03.03.2015
 * Time: 0:01
 *
 * @var BackendAdminController $this
 * @var FilterGroupPage $model
 */ ?>

<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Привязать страницу</h4>
    </div>
    <?php /** @var TbActiveForm $form */
    $form = $this->beginWidget(
        '\common\widgets\booster\ActiveForm',
        [
            'id' => 'group-page-form',
            'type' => 'horizontal',
            'htmlOptions' => [
                'class' => 'ajax'
            ],
        ]
    ); ?>
    <div class="modal-body">
        <?= $form->dropDownListGroup(
            $model,
            'page_id',
            [
                'wrapperHtmlOptions' => ['class' => 'col-sm-5'],
                'widgetOptions' => [
                    'data' => CMap::mergeArray(['Выберите страницу'], \common\helpers\StreetHelper::pages()),
                    'htmlOptions' => ['class' => 'field']
                ],
            ]
        ); ?>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-sm" data-submit="ajax" data-for="group-page-form">Сохранить</button>
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Отмена</button>
    </div>
    <?php $this->endWidget(); ?>
</div>