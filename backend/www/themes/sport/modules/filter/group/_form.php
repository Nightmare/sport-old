<?php
/**
 * Created by PhpStorm.
 * User: Nick Nikitchenko (skype: quietasice)
 * Date: 03.03.2015
 * Time: 0:01
 *
 * @var BackendAdminController $this
 * @var FilterGroup $model
 */ ?>

<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Создать группу</h4>
    </div>
    <?php /** @var TbActiveForm $form */
    $form = $this->beginWidget(
        '\common\widgets\booster\ActiveForm',
        [
            'id' => 'filter-group-form',
            'type' => 'horizontal',
            'htmlOptions' => [
                'class' => 'ajax'
            ],
        ]
    ); ?>
    <div class="modal-body">
        <?= $form->textFieldGroup(
            $model,
            'title',
            [
                'wrapperHtmlOptions' => ['class' => 'col-sm-5'],
                'widgetOptions' => ['htmlOptions' => ['class' => 'field']],
            ]
        ); ?>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-sm" data-submit="ajax" data-for="filter-group-form">Сохранить</button>
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Отмена</button>
    </div>
    <?php $this->endWidget(); ?>
</div>