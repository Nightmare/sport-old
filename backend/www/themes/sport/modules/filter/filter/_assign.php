<?php
/**
 * Created by PhpStorm.
 * User: Nick Nikitchenko (skype: quietasice)
 * Date: 03.03.2015
 * Time: 0:01
 *
 * @var BackendAdminController $this
 * @var FilterGroup $model
 * @var FilterGroup[] $groupList
 */ ?>

<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Привязать фильтр</h4>
    </div>
    <?php /** @var TbActiveForm $form */
    $form = $this->beginWidget(
        '\common\widgets\booster\ActiveForm',
        [
            'id' => 'filter-form',
            'type' => 'horizontal',
            'htmlOptions' => [
                'class' => 'ajax'
            ],
        ]
    ); ?>
    <div class="modal-body">
        <?= $form->dropDownListGroup(
            $model,
            'group_id',
            [
                'wrapperHtmlOptions' => ['class' => 'col-sm-5'],
                'widgetOptions' => [
                    'data' => CMap::mergeArray(['Выберите группу'], CHtml::listData($groupList, 'id', 'title')),
                    'htmlOptions' => [
                        'data-group-filter-link' => Yii::app()->createUrl('/filter/group/filter'),
                        'class' => 'groupSelect'
                    ]
                ],
            ]
        ); ?>
        <?= $form->dropDownListGroup(
            $model,
            'filter_assign_id',
            [
                'wrapperHtmlOptions' => ['class' => 'col-sm-5'],
                'widgetOptions' => [
                    'data' => [],
                    'htmlOptions' => ['class' => 'field filterSelect']
                ],
            ]
        ); ?>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-sm" data-submit="ajax" data-for="filter-form">Сохранить</button>
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Отмена</button>
    </div>
    <?php $this->endWidget(); ?>
</div>