<?php
/**
 * Created by PhpStorm.
 * User: Nick Nikitchenko (skype: quietasice)
 * Date: 03.03.2015
 * Time: 0:35
 *
 * @var BackendAdminController $this
 * @var Filter $model
 * @var FilterGroup $group
 * @var CActiveDataProvider $dataProvider
 */ ?>

<a href="<?= Yii::app()->createUrl('/filter/filter/add', ['group_id' => $group->getId()]) ?>" data-type="ajax" class="btn btn-primary btn-xs">Создать фильтр</a>
<a href="<?= Yii::app()->createUrl('/filter/filter/copy', ['group_id' => $group->getId()]) ?>" data-type="ajax" class="btn btn-primary btn-xs">Копировать фильтр</a>
<div class="grid-block">
    <?php $this->widget(
        'booster.widgets.TbGridView',
        [
            'id' => 'filter-list',
            'dataProvider' => $dataProvider,
            'template' => "{pager}\n{items}\n{pager}",
            'columns' => [
                ['name' => 'id', 'header' => '#', 'htmlOptions' => ['style'=>'width: 60px']],
                'title',
                [
                    'header' => 'Вертикальное изображение',
                    'class' => 'common\extensions\grid\UploadColumn',
                    'name' => 'v_image_link',
                    'uploadLink' => 'Yii::app()->createUrl("filter/filter/vertical", ["filter_id" => $data->id])'
                ],
                [
                    'header' => 'Горизонтальное изображение',
                    'class' => 'common\extensions\grid\UploadColumn',
                    'name' => 'h_image_link',
                    'uploadLink' => 'Yii::app()->createUrl("filter/filter/horizont", ["filter_id" => $data->id])'
                ],
                [
                    'header' => 'Связанные фильтры',
                    'type' => 'raw',
                    'value' => '$data->getAssignFilterListString()'
                ],
                [
                    'name' => 'create_at',
                    'value' => 'date("d.m.Y", $data->getCreateAt())'
                ],
                [
                    'class' => 'booster.widgets.TbButtonColumn',
                    'htmlOptions' => ['nowrap' => 'nowrap'],
                    'updateButtonUrl' => 'Yii::app()->createUrl("/filter/filter/update", ["filter_id" => $data->getId()])',
                    'updateButtonOptions' => ['data-type' => 'ajax'],
                    'buttons' => [
                        'copy' => [
                            'label' => 'Копировать в...',
                            'icon' => 'glyphicon glyphicon-file'
                        ]
                    ],
                    'deleteButtonUrl' => 'Yii::app()->createUrl("/filter/filter/delete", ["filter_id" => $data->getId()])',
                    'template' => '{update}{delete}'
                ]
            ],
        ]
    ); ?>
</div>
<a href="<?= Yii::app()->createUrl('/filter/filter/add', ['group_id' => $group->getId()]) ?>" data-type="ajax" class="btn btn-primary btn-xs">Создать фильтр</a>
<a href="<?= Yii::app()->createUrl('/filter/filter/copy', ['group_id' => $group->getId()]) ?>" data-type="ajax" class="btn btn-primary btn-xs">Копировать фильтр</a>