<?php
/**
 * Created by PhpStorm.
 *
 * @var BackendAdminController $this
 * @var VideoChannel $model
 */ ?>

<div class="modal-dialog form" id="replacement">
    <?php /** @var TbActiveForm $form */
    $form = $this->beginWidget(
        '\common\widgets\booster\ActiveForm',
        [
            'id' => 'channel_form',
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            'htmlOptions' => [
                'class' => 'ajax'
            ],
            'clientOptions' => [
                'validateOnChange' => false
            ]
        ]
    ); ?>
    <input type="hidden" class="field" name="min" value="">
    <input type="hidden" class="field" name="origin" value="">
    <div class="modal-content" style="width: 700px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel"><?= Yii::t('labels', 'Информация'); ?></h4>
        </div>
        <div class="modal-body">
            <table>
                <tr>
                    <td>
                        <ul>
                            <li>
                                <?= $form->textFieldGroup(
                                    $model,
                                    'title',
                                    [
                                        'wrapperHtmlOptions' => ['class' => 'col-sm-8'],
                                        'widgetOptions' => [
                                            'htmlOptions' => [
                                                'class' => 'field',
                                                'placeholder' => '',
                                            ],
                                        ],
                                        'labelClass' => 'col-sm-3',
                                    ]
                                ); ?>
                            </li>
                            <li>
                                <?= $form->textFieldGroup(
                                    $model,
                                    'link',
                                    [
                                        'wrapperHtmlOptions' => ['class' => 'col-sm-8'],
                                        'widgetOptions' => [
                                            'htmlOptions' => [
                                                'class' => 'field',
                                                'placeholder' => '',
                                            ],
                                        ],
                                        'labelClass' => 'col-sm-3',
                                    ]
                                ); ?>
                            </li>
                            <li>
                                <?= $form->textAreaGroup(
                                    $model,
                                    'description',
                                    [
                                        'wrapperHtmlOptions' => ['class' => 'col-sm-8'],
                                        'widgetOptions' => [
                                            'htmlOptions' => [
                                                'class' => 'field',
                                                'placeholder' => '',
                                            ],
                                        ],
                                        'labelClass' => 'col-sm-3',
                                    ]
                                ); ?>
                            </li>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <li>
                                <div class="logo-file-block" id="logo-file-block">
                                    <a data-type="file-crop" data-preview-width="50" data-preview-height="50" data-for-name="Image[]" class="btn btn-primary sq" data-file-block="logo-file-block" href="<?= \Yii::app()->createUrl('/gallery/channel/upload'); ?>">Изображение</a>
                                    <input class="hidden-file" type="file" name="Image[]">
                                </div>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
        <div class="modal-footer buttons">
            <button type="button" class="btn btn-default sq" data-dismiss="modal"><?= Yii::t('labels', 'Отмена') ?></button>
            <button type="button" data-modal-selector="#customModal" class="btn btn-primary sq" data-submit="ajax" data-for="channel_form"><?= Yii::t('labels', 'Сохранить') ?></button>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>