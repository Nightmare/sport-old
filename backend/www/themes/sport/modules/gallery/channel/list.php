<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 23.02.2015
 * Time: 2:22
 *
 * @var BackendAdminController $this
 * @var CActiveDataProvider $dataProvider
 */?>
<a href="<?= Yii::app()->createUrl('/gallery/channel/create') ?>" data-type="ajax" class="btn btn-primary btn-xs">Добавить канал</a>
<div class="grid-block">
    <?php $this->widget(
        'booster.widgets.TbGridView',
        [
            'id' => 'gallery-channel-list',
            'dataProvider' => $dataProvider,
            'template' => "{pager}{items}{pager}",
            'columns' => [
                ['name' => 'id', 'header' => '#', 'htmlOptions' => ['style'=>'width: 60px']],
                [
                    'name' => 'title',
                ],
                [
                    'name' => 'link',
                    'type' => 'raw',
                    'value' => 'CHtml::link($data->getLink(), $data->getLink(), ["target" => "_blank"])'
                ],
                [
                    'header' => 'Лого',
                    'type' => 'raw',
                    'value' => 'CHtml::link(CHtml::image(Yii::app()->static->imageLink($data->getMinImage()."?".rand())), Yii::app()->static->imageLink($data->getOriginalImage()), ["class" => "image_fancy"])'
                ],
                [
                    'header' => 'Обновили/Добавили',
                    'type' => 'raw',
                    'value' => 'date("d.m.Y H:i", $data->getUpdateAt())."/".date("d.m.Y H:i", $data->getCreateAt())'
                ],
                [
                    'class' => 'booster.widgets.TbButtonColumn',
                    'htmlOptions' => ['nowrap' => 'nowrap'],
                    'updateButtonUrl' => 'Yii::app()->createUrl("/gallery/channel/update", ["channel_id" => $data->getId()])',
                    'updateButtonOptions' => ['data-type' => 'ajax'],
                    'deleteButtonUrl' => 'Yii::app()->createUrl("/gallery/channel/delete", ["channel_id" => $data->getId()])',
                    'template' => '{update}{delete}'
                ]
            ],
        ]
    ); ?>
</div>