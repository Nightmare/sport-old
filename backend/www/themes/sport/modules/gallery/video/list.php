<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 23.02.2015
 * Time: 2:22
 *
 * @var BackendAdminController $this
 * @var CActiveDataProvider $dataProvider
 */?>
<a href="<?= Yii::app()->createUrl('/gallery/video/step1') ?>" data-type="ajax" class="btn btn-primary btn-xs">Добавить видеозапись</a>
<div class="grid-block">
    <?php $this->widget(
        'booster.widgets.TbGridView',
        [
            'id' => 'gallery-video-list',
            'dataProvider' => $dataProvider,
            'template' => "{pager}{items}{pager}",
            'columns' => [
                ['name' => 'id', 'header' => '#', 'htmlOptions' => ['style'=>'width: 60px']],
                [
                    'header' => 'Превью',
                    'type' => 'raw',
                    'value' => 'CHtml::link(CHtml::image(Yii::app()->static->imageLink($data->getMinImage())), Yii::app()->static->imageLink($data->getOriginalImage()), ["class" => "image_fancy"])'
                ],
                [
                    'name' => 'conference_id',
                    'header' => 'Локация',
                    'value' => '\common\helpers\StreetHelper::getConferenceTitleById($data->getConferenceId())'
                ],
                [
                    'header' => 'Фильтры',
                    'type' => 'raw',
                    'value' => '$data->getFilterString()'
                ],
                [
                    'header' => 'Информация',
                    'type' => 'raw',
                    'value' => '$data->getGetYoutubeInfo()'
                ],
                [
                    'class' => 'booster.widgets.TbButtonColumn',
                    'htmlOptions' => ['nowrap' => 'nowrap'],
                    'updateButtonUrl' => 'Yii::app()->createUrl("/gallery/video/update", ["video_id" => $data->getId()])',
                    'updateButtonOptions' => ['data-type' => 'ajax'],
                    'deleteButtonUrl' => 'Yii::app()->createUrl("/gallery/video/delete", ["video_id" => $data->getId()])',
                    'buttons' => [
                        'youtube' => [
                            'label' => 'Обновить данные с Youtube',
                            'icon' => 'glyphicon glyphicon-facetime-video',
                            'options' => ['data-type' => 'ajax'],
                            'url' => 'Yii::app()->createUrl("/gallery/video/youtube", ["video_id" => $data->getId()])'
                        ]
                    ],
                    'template' => '{update}{youtube}{delete}'
                ]
            ],
        ]
    ); ?>
</div>