<?php
/**
 * Created by PhpStorm.
 *
 * @var BackendAdminController $this
 * @var GalleryImage $model
 * @var string $conference
 */ ?>

<div class="modal-dialog form" id="replacement">
    <?php /** @var TbActiveForm $form */
    $form = $this->beginWidget(
        '\common\widgets\booster\ActiveForm',
        [
            'id' => 'image_form',
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            'htmlOptions' => [
                'class' => 'ajax'
            ],
            'clientOptions' => [
                'validateOnChange' => false
            ]
        ]
    ); ?>
    <input type="hidden" class="field" name="min" value="">
    <input type="hidden" class="field" name="origin" value="">
    <div class="modal-content" style="width: 700px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel"><?= Yii::t('labels', 'Добавить в'); ?> <?= \common\helpers\StreetHelper::getConferenceTitle($conference) ?></h4>
        </div>
        <div class="modal-body">
            <table>
                <tr>
                    <td>
                        <ul>
                            <li>
                                <?= $form->textFieldGroup(
                                    $model,
                                    'title',
                                    [
                                        'wrapperHtmlOptions' => ['class' => 'col-sm-8'],
                                        'widgetOptions' => [
                                            'htmlOptions' => [
                                                'class' => 'field',
                                                'placeholder' => '',
                                            ],
                                        ],
                                        'labelClass' => 'col-sm-3',
                                    ]
                                ); ?>
                            </li>
                            <li>
                                <?= $form->textAreaGroup(
                                    $model,
                                    'description',
                                    [
                                        'wrapperHtmlOptions' => ['class' => 'col-sm-8'],
                                        'widgetOptions' => [
                                            'htmlOptions' => [
                                                'class' => 'field',
                                                'placeholder' => '',
                                            ],
                                        ],
                                        'labelClass' => 'col-sm-3',
                                    ]
                                ); ?>
                            </li>
                            <li>
                                <?= $form->dropDownListGroup(
                                    $model,
                                    'image_filter_group_id',
                                    [
                                        'wrapperHtmlOptions' => ['class' => 'col-sm-8'],
                                        'widgetOptions' => [
                                            'data' => CMap::mergeArray(['Выберите группу'], CHtml::listData(FilterGroup::model()->enable()->findAll(), 'id', 'title')),
                                            'htmlOptions' => [
                                                'data-group-filter-link' => Yii::app()->createUrl('/filter/group/filter'),
                                                'class' => 'groupSelect'
                                            ]
                                        ],
                                        'labelOptions' => ['label' => '']
                                    ]
                                ); ?>
                            </li>
                            <li>
                                <?= $form->dropDownListGroup(
                                    $model,
                                    'image_filter_id',
                                    [
                                        'wrapperHtmlOptions' => ['class' => 'col-sm-8'],
                                        'widgetOptions' => [
                                            'data' => [],
                                            'htmlOptions' => ['class' => 'field filterSelect']
                                        ],
                                        'labelOptions' => ['label' => '']
                                    ]
                                ); ?>
                                <div class="buttons" style="display: inline-block;">
                                    <?= CHtml::link(
                                        '+',
                                        'javascript:void(0);',
                                        [
                                            'class' => 'btn btn-success btn-sm sq',
                                            'id' => 'media-filter-add',
                                            'data-block-out' => 'media-assign-items'
                                        ]
                                    ); ?>
                                </div>
                            </li>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <li>
                                <div class="image-file-block" id="image-file-block">
                                    <a data-type="file-crop" data-preview-width="230" data-preview-height="94" data-for-name="Image[]" class="btn btn-primary sq" data-file-block="image-file-block" href="<?= \Yii::app()->createUrl('/gallery/image/upload'); ?>">Изображение</a>
                                    <input class="hidden-file" type="file" name="Image[]">
                                </div>
                            </li>
                            <li>
                                <ul id="media-assign-items"></ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
        <div class="modal-footer buttons">
            <button type="button" class="btn btn-default sq" data-dismiss="modal"><?= Yii::t('labels', 'Отмена') ?></button>
            <button type="button" data-modal-selector="#customModal" class="btn btn-primary sq" data-submit="ajax" data-for="image_form"><?= Yii::t('labels', 'Добавить') ?></button>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>