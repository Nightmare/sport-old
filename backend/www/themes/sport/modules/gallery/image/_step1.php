<?php
/**
 * Created by PhpStorm.

 * @var BackendAdminController $this
 */ ?>

<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Выберите локацию</h4>
    </div>
    <div class="modal-body">
        <ul class="conf-choose-list">
            <?php foreach (\common\helpers\StreetHelper::getConferenceList() as $id => $title): ?>
                <?php $subList = \common\helpers\StreetHelper::getConferenceSubList($id); ?>
                <?php if($subList): ?>
                    <li class="location" data-slider-for="<?= $id; ?>"><?= $title; ?></li>
                    <?php foreach($subList as $sub_id => $sub_title): ?>
                        <li
                            data-type="ajax"
                            data-link="<?= Yii::app()->createUrl('/gallery/image/step2', ['conference' => $sub_id]) ?>"
                            class="location sub-location"
                            data-slider-in="<?= $id ?>"><?= $sub_title ?></li>
                    <?php endforeach ?>
                <?php else: ?>
                    <li
                        data-type="ajax"
                        data-link="<?= Yii::app()->createUrl('/gallery/image/step2', ['conference' => $id]) ?>"
                        class="location"><?= $title; ?></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>