<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 23.02.2015
 * Time: 2:22
 *
 * @var BackendAdminController $this
 * @var CActiveDataProvider $dataProvider
 */?>
<a href="<?= Yii::app()->createUrl('/gallery/image/step1') ?>" data-type="ajax" class="btn btn-primary btn-xs">Добавить фотографию</a>
<div class="grid-block">
    <?php $this->widget(
        'booster.widgets.TbGridView',
        [
            'id' => 'gallery-image-list',
            'dataProvider' => $dataProvider,
            'template' => "{pager}{items}{pager}",
            'columns' => [
                ['name' => 'id', 'header' => '#', 'htmlOptions' => ['style'=>'width: 60px']],
                [
                    'header' => 'Превью',
                    'type' => 'raw',
                    'value' => 'CHtml::link(CHtml::image(Yii::app()->static->imageLink($data->getMinImage())), Yii::app()->static->imageLink($data->getOriginalImage()), ["class" => "image_fancy"])'
                ],
                [
                    'name' => 'conference_id',
                    'header' => 'Локация',
                    'value' => '\common\helpers\StreetHelper::getConferenceTitleById($data->getConferenceId())'
                ],
                [
                    'header' => 'Фильтры',
                    'type' => 'raw',
                    'value' => '$data->getFilterString()'
                ],
                [
                    'name' => 'update_at',
                    'value' => 'date("d.m.Y", $data->getUpdateAt())'
                ],
                [
                    'name' => 'create_at',
                    'value' => 'date("d.m.Y", $data->getCreateAt())'
                ],
                [
                    'class' => 'booster.widgets.TbButtonColumn',
                    'htmlOptions' => ['nowrap' => 'nowrap'],
                    'updateButtonUrl' => 'Yii::app()->createUrl("/gallery/image/update", ["image_id" => $data->getId()])',
                    'updateButtonOptions' => ['data-type' => 'ajax'],
                    'deleteButtonUrl' => 'Yii::app()->createUrl("/gallery/image/delete", ["image_id" => $data->getId()])',
                    'template' => '{update}{delete}'
                ]
            ],
        ]
    ); ?>
</div>