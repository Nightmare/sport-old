<?php
/**
 * Specific config overrides for backend entry point at production server.
 */
return [
    'behaviors' => [
        [
            'class' => '\common\extensions\behaviors\cors\CorsBehavior',
            'route' => '*',
            'allowOrigin' => '*.sportime.ru'
        ]
    ],
];