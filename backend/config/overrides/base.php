<?php
/**
 * Base config overrides for backend application
 */
return [
    // So our relative path aliases will resolve against the `/backend` subdirectory and not nonexistent `/protected`
    'basePath' => 'backend',
    'import' => [
        'application.controllers.*',
        'application.controllers.actions.*',
        'common.actions.*',
        'common.extensions.select2.Select2',
    ],
    'controllerMap' => [
        // Overriding the controller ID so we have prettier URLs without meddling with URL rules
        'admin' => 'BackendAdminController'
    ],
    'modules' => [
        'users' => [
            'class' => 'backend\modules\users\UsersModule'
        ],
        'gallery' => [
            'class' => 'backend\modules\gallery\GalleryModule'
        ],
        'filter' => [
            'class' => 'backend\modules\filter\FilterModule'
        ],
        'gymnasium' => [
            'class' => 'backend\modules\gymnasium\GymnasiumModule'
        ],
    ],
    'theme' => 'sport',
    'components' => [
        /*'viewRenderer' => [
            'class' => 'vendor.yiiext.twig-renderer.ETwigViewRenderer',
            'twigPathAlias' => 'vendor.twig.twig.lib.Twig',

            'fileExtension' => '.twig',
            'options' => [
                'autoescape' => true,
                'cache' => false
            ],
            'extensions' => [
                //'My_Twig_Extension'
            ],
            'globals' => [
                'html' => 'CHtml'
            ],
            'functions' => [
                'rot13' => 'str_rot13'
            ],
            'filters' => [
                'jencode' => 'CJSON::encode'
            ],
        ],*/
        'errorHandler' => [
            'errorAction' => 'admin/error'
        ],
        'urlManager' => [
            'rules' => [

            ]
        ],
    ],
    'params' => [
        'sitePart' => 'backend'
    ]
];