<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 27.04.14
 * Time: 16:11
 */

namespace common\components\video\youtube\types;

/**
 * Class Content
 * @package application\components\Video\Youtube\types
 *
 * @property string uploadStatus
 * @property string privacyStatus
 * @property string license
 * @property boolean embeddable
 * @property boolean publicStatsViewable
 */
class Status extends BaseTypes
{

} 