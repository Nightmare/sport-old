<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 27.04.14
 * Time: 16:06
 */

namespace common\components\video\youtube\types;


class BaseTypes
{
    /**
     * @var array
     */
    protected $params = array();

    public function __construct($array = array())
    {
        foreach ($array as $key => $values) {
            $this->{$key} = $values;
        }
    }

    public function __get($name)
    {
        if(!isset($this->params[$name]) || empty($this->params[$name]))
            return null;
        if(is_array($this->params[$name]))
            return new \ActiveArray($this->params[$name]);
        else
            return $this->params[$name];
    }

    public function __set($name, $value)
    {
        $this->params[$name] = $value;
    }
} 