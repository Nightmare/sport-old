<?php
/**
 * Created by PhpStorm.
 */

namespace common\components\video\youtube\types\snippet;


use common\components\video\youtube\types\BaseTypes;

/**
 * Class Image
 * @package common\components\video\youtube\types\snippet
 *
 * @property string url
 * @property int width
 * @property int height
 */
class Image extends BaseTypes
{

}