<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 27.04.14
 * Time: 16:09
 */

namespace common\components\video\youtube\types\snippet;
use common\components\video\youtube\types\BaseTypes;

/**
 * Class Category
 * @package application\components\Video\Youtube\types
 *
 * @property string title
 * @property string description
 */
class Localized extends BaseTypes
{

} 