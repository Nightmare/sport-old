<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 27.04.14
 * Time: 16:11
 */

namespace common\components\video\youtube\types;

/**
 * Class Content
 * @package application\components\Video\Youtube\types
 *
 * @property int viewCount
 * @property int likeCount
 * @property int dislikeCount
 * @property int favoriteCount
 * @property int commentCount
 */
class Statistics extends BaseTypes
{

} 