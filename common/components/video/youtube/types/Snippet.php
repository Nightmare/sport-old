<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 27.04.14
 * Time: 16:11
 */

namespace common\components\video\youtube\types;
use common\components\video\youtube\types\snippet\Localized;
use common\components\video\youtube\types\snippet\Thumbnails;

/**
 * Class Content
 * @package application\components\Video\Youtube\types
 *
 * @property string publishedAt
 * @property string channelId
 * @property string title
 * @property string description
 * @property string channelTitle
 * @property string categoryId
 * @property string liveBroadcastContent
 * @property Thumbnails thumbnails
 * @property Localized localized
 */
class Snippet extends BaseTypes
{

} 