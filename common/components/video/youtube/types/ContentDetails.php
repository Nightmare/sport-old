<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 27.04.14
 * Time: 16:11
 */

namespace common\components\video\youtube\types;

/**
 * Class Content
 * @package application\components\Video\Youtube\types
 *
 * @property string duration
 * @property string dimension
 * @property string definition
 * @property string caption
 * @property boolean licensedContent
 */
class ContentDetails extends BaseTypes
{

} 