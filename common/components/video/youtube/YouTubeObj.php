<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 27.04.14
 * Time: 16:01
 */

namespace common\components\video\youtube;
use common\components\VarDumper;
use common\components\video\iVideo;
use common\components\video\youtube\types\BaseTypes;
use common\components\video\youtube\types\ContentDetails;
use common\components\video\youtube\types\Snippet;
use common\components\video\youtube\types\snippet\Image;
use common\components\video\youtube\types\Statistics;
use common\components\video\youtube\types\Status;

/**
 * Class YouTubeObj
 * @package application\components\Video\Youtube\types
 *
 * @property string kind
 * @property string etag
 * @property string id
 * @property Snippet snippet
 * @property ContentDetails contentDetails
 * @property Status status
 * @property Statistics statistics
 */
class YouTubeObj extends BaseTypes implements iVideo
{
    /**
     * @var array
     */
    protected $params = array();
    public $host_video_id;
    public $video_host = 'youtube';

    public function __construct($array = array())
    {
        if(empty($array))
            $array = $this->getDefault();

        parent::__construct($array);

        $this->host_video_id = $this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    private function getDefault()
    {
        return [
            'kind'              => null,
            'etag'              => null,
            'id'                => null,
            'snippet'           => [],
            'contentDetails'    => [],
            'status'            => [],
            'statistics'        => [],
        ];
    }

    public function getViewCount()
    {
        return $this->statistics->viewCount;
    }

    /**
     * @param bool $timestamp
     * @return int|string
     */
    public function getPostedDate($timestamp = true)
    {
        $time = $this->snippet->publishedAt;
        if($timestamp)
            $time = strtotime($time);

        return $time;
    }

    public function getAuthor()
    {
        return $this->snippet->channelTitle;
    }

    public function getDuration()
    {
        $reptms = "/^PT(?:(\d+)H)?(?:(\d+)M)?(?:(\d+)S)?$/ui";
        $totalseconds = 0;
        if(preg_match($reptms, $this->contentDetails->duration, $out)) {
            $hours = $out[1];
            $minutes = $out[2];
            $seconds = $out[3];

            $totalseconds = $hours * 3600 + $minutes * 60 + $seconds;
        }

        return $totalseconds;
    }

    public function getImage()
    {
        $thumb = $this->getMaxImage();
        if($thumb === null)
            return null;

        $img = '/tmp/'.uniqid();
        file_put_contents($img, file_get_contents($thumb->url));

        return $img;
    }

    public function getTitle()
    {
        return $this->snippet->title;
    }

    /**
     * @return mixed
     */
    public function getHostVideoId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getVideoHost()
    {
        return $this->video_host;
    }

    protected $image_types = ['maxres', 'standard', 'high', 'medium', 'default'];

    /**
     * @return Image|null
     */
    public function getMaxImage()
    {
        foreach ($this->image_types as $image) {
            if($this->snippet->thumbnails->{$image} !== null)
                return $this->snippet->thumbnails->{$image};
        }

        return null;
    }
}