<?php
namespace common\components\video;
/**
 * Created by PhpStorm.
 * User: me
 * Date: 09.10.2014
 * Time: 22:16
 */

interface iVideo
{
    public function getViewCount();

    /**
     * @param bool $timestamp
     * @return int|string
     */
    public function getPostedDate($timestamp = true);

    public function getAuthor();

    public function getDuration();

    public function getImage();

    public function getTitle();

    public function getHostVideoId();

    public function getVideoHost();
} 