<?php
namespace common\components\video;
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 21.11.12
 * Time: 15:19
 * To change this template use File | Settings | File Templates.
 */
use CApplicationComponent;
use Yii;
use CJSON;
class Youtube extends CApplicationComponent
{
    /** @var \common\components\video\youtube\YouTubeObj[]  */
    private $items = [];
    private $host_video_id;

    public $api_link;
    public $api_key;


    public function init()
    {
        parent::init();
    }

    /**
     * @param $id
     * @return \common\components\video\youtube\YouTubeObj
     */
    public function getVideoInfo($id)
    {
        if(isset($this->items[$id]))
            return $this->items[$id];

        $items = [];
        $link = sprintf('%s?id=%s&key=%s&part=snippet,contentDetails,statistics,status', $this->api_link, $id, $this->api_key);
        $_info = Yii::app()->curl->get($link);
        if($_info !== false) {
            $_info = CJSON::decode($_info);
            $items = isset($_info['items']) ? $_info['items'] :[];
        }
        foreach ($items as $item) {
            $obj = new \common\components\video\youtube\YouTubeObj($item);
            $this->items[$id] = $obj;
        }


        return $this->items[$id];
    }
}
