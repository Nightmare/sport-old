<?php
/**
 * Created by PhpStorm.
 * User: Ice
 * Date: 13.07.14
 * Time: 1:53
 */
namespace common\components;

use Yii;
use CClientScript;
class StaticContent extends \CApplicationComponent
{
    private $assets;
    private $static_www;
    private $static_own;
    private $libraries;
    private $widgets;
    private $images;

    private $currPath = null;
    private $_loadJsList = [];
    private $_loadCssList = [];

    public function init()
    {
        parent::init();
        $domain = rtrim(Yii::app()->params['static_domain'], '/');

        $this->static_www = $domain.'/www';
        $this->static_own = $domain.'/own/'.Yii::app()->theme->name;
        $this->images =     $domain.'/images';
        $this->libraries =  $domain.'/packages';
        $this->widgets =    $domain.'/widgets/'.Yii::app()->theme->name;
        $this->assets =     $domain.'/js';
    }

    public function setAssets()
    {
        $this->currPath = rtrim($this->assets, '/');

        return $this;
    }

    public function setLibrary($libraryName)
    {
        $this->currPath = rtrim($this->libraries).'/'.$libraryName;

        return $this;
    }

    public function setWww()
    {
        $this->currPath = rtrim($this->static_www, '/');

        return $this;
    }

    public function setOwn()
    {
        $this->currPath = rtrim($this->static_own, '/');

        return $this;
    }

    public function setWidget($widgetName)
    {
        $this->currPath = rtrim($this->widgets, '/').'/'.$widgetName;

        return $this;
    }

    public function setImages()
    {
        $this->currPath = $this->images;

        return $this;
    }

    public function getLink($filePath)
    {
        $filePath = ltrim($filePath, '/');
        return "{$this->currPath}/{$filePath}";
    }

    public function registerScriptFile($fileName, $position = CClientScript::POS_HEAD)
    {
        $fileName = ltrim($fileName, '/');
        $link = "{$this->currPath}/js/{$fileName}";
        if(!in_array($link, $this->_loadJsList)) {
            $this->_loadJsList[] = $link;
            Yii::app()->clientScript->registerScriptFile($link, $position);
        }

        return $this;
    }

    public function registerScriptFile2($fileName, $position = CClientScript::POS_HEAD)
    {
        $fileName = ltrim($fileName, '/');
        $link = "{$this->currPath}/{$fileName}";
        if(!in_array($link, $this->_loadJsList)) {
            $this->_loadJsList[] = $link;
            Yii::app()->clientScript->registerScriptFile($link, $position);
        }

        return $this;
    }

    public function registerLangFile($fileName, $position = CClientScript::POS_HEAD)
    {
        $fileName = ltrim($fileName, '/');
        Yii::app()->clientScript->registerScriptFile("{$this->currPath}/lang/{$fileName}", $position);

        return $this;
    }

    public function registerCssFile($fileName)
    {
        $fileName = ltrim($fileName, '/');
        $link = "{$this->currPath}/css/{$fileName}";
        if(!in_array($link, $this->_loadCssList)) {
            $this->_loadCssList[] = $link;
            Yii::app()->clientScript->registerCssFile($link);
        }

        return $this;
    }

    public function registerCssFile2($fileName)
    {
        $fileName = ltrim($fileName, '/');
        $link = "{$this->currPath}/{$fileName}";
        if(!in_array($link, $this->_loadCssList)) {
            $this->_loadCssList[] = $link;
            Yii::app()->clientScript->registerCssFile($link);
        }

        return $this;
    }

    public function staticPath()
    {
        return realpath(Yii::app()->basePath.'/../static');
    }

    public function imageLink($filePath, $withSelectPath = false)
    {
        $filePath = ltrim($filePath, '/');

        if($withSelectPath === false)
            return rtrim(Yii::app()->params['static_domain'], '/')."/{$filePath}";
        else
            return $this->currPath.'/'.$filePath;
    }
} 