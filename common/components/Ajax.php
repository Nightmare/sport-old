<?php
/**
 * Created by PhpStorm.
 * User: Ice
 * Date: 13.07.14
 * Time: 1:53
 */
namespace common\components;

class Ajax extends \CApplicationComponent
{
    private $_messages = array();
    private $_other = array();
    private $_errors = array();
    private $_url = null;
    private $_js = [];
    private $_replace = [];
    private $_html = [];

    private $_typeReplace = [
        'danger' => 'error',
        'info' => 'information'
    ];

    /**
     * @param $name
     * @param $params
     * @return $this
     */
    public function runJS($name, $params = [])
    {
        if(!is_array($params))
            $params = [$params];

        $this->_js[$name] = $params;

        return $this;
    }

    private $_loadJS = [];

    /**
     * @param $url
     * @return $this
     */
    public function loadJS($url)
    {
        $this->_loadJS[] = $url;

        return $this;
    }

    /**
     * @param $url
     * @return $this
     */
    public function setUrl($url)
    {
        if(is_array($url) && isset($url['route']) && isset($url['params']))
            $this->_url = \Yii::app()->createUrl($url['route'], $url['params']);
        else
            $this->_url = $url;

        return $this;
    }

    /**
     * @param $view
     * @param $selector
     * @param $params
     * @param $render
     * @return $this
     */
    public function addReplace($view, $selector, $params = [], $render = true)
    {
        if($render)
            $view = \Yii::app()->getController()->renderPartial($view, $params, true);
        $this->_replace[] = [
            'view' => $view,
            'selector' => $selector,
        ];

        return $this;
    }

    /**
     * @param $view
     * @param $selector
     * @param $params
     * @param $render
     * @return $this
     */
    public function addHtml($view, $selector, $params = [], $render = true)
    {
        if($render)
            $view = \Yii::app()->getController()->renderPartial($view, $params, true);
        $this->_html[] = [
            'view' => $view,
            'selector' => $selector,
        ];

        return $this;
    }

    /**
     * @param $messages
     * @param string $type
     * @return $this|void
     */
    public function addMessage($messages, $type = 'success')
    {
        $type = str_replace(array_keys($this->_typeReplace), array_values($this->_typeReplace), $type);
        if(!is_array($messages))
            $messages = [$messages];

        foreach ($messages as $message) {
            if(isset($this->_messages[$type]) && in_array($message, $this->_messages[$type]))
                continue;

            $this->_messages[$type][] = $message;
        }

        return $this;
    }

    public function hasErrors()
    {
        return !empty($this->_errors);
    }

    /**
     * @param $type
     * @param array|string|\CModel $model
     * @param null $number
     * @return $this
     */
    public function addErrors($model, $type = 'danger', $number = null)
    {
        $type = str_replace(array_keys($this->_typeReplace), array_values($this->_typeReplace), $type);
        if(is_object($model) !== false) {
            foreach($model->getErrors() as $attribute=>$errors) {
                if($number === null)
                    $this->_errors[$type][\CHtml::activeId($model,$attribute)] = $errors[0];
                else
                    $this->_errors[$type][get_class($model).'_'.$number.'_'.$attribute] = $errors;
            }
        } else {
            if(!is_array($model))
                $this->_errors[$type][] = $model;
            else {
                foreach($model as $field => $error)
                    $this->_errors[$type][$field] = $error[0];
            }
        }

        return $this;
    }

    /**
     * @param array $other
     * @return $this
     */
    public function addOther(array $other)
    {
        $this->_other = \CMap::mergeArray($this->_other, $other);

        return $this;
    }


    public function send($url = null, $force = false)
    {
        $params = [];
        if($url !== null)
            $this->_url = $url;

        if(\Yii::app()->request->isAjaxRequest && !$force) {
            $errorsArray = $this->_errors;
            if(!empty($errorsArray))
                $params = \CMap::mergeArray($params, ['errors' => $errorsArray]);

            $textArray = $this->_messages;
            if(!empty($textArray))
                $params = \CMap::mergeArray($params, ['messages' => $textArray]);
        } else {
            foreach($this->_errors as $type => $errors) {
                foreach($errors as $error)
                    \Yii::app()->user->setFlash($type, $error);
            }
            foreach($this->_messages as $type => $textList) {
                foreach($textList as $text)
                    \Yii::app()->user->setFlash($type, $text);
            }
        }

        if(\Yii::app()->request->isAjaxRequest) {
            if($this->_url !== false && $this->_url !== null)
                $params = \CMap::mergeArray($params, array('redirectLink' => $this->_url));

            $params = \CMap::mergeArray($this->_other, $params);
            $params['runJS'] = [];
            if(!empty($this->_loadJS))
                $params['runJS'][] = ['name' => '$ajax.loadScript', 'params' => [$this->_loadJS]];
            foreach ($this->_js as $name => $jsArgs)
                $params['runJS'][] = ['name' => $name, 'params' => $jsArgs];

            $params['replaceList'] = [];
            foreach ($this->_replace as $replace)
                $params['replaceList'][] = ['selector' => $replace['selector'], 'view' => $replace['view']];
            $params['htmlList'] = [];
            foreach ($this->_html as $replace)
                $params['htmlList'][] = ['selector' => $replace['selector'], 'view' => $replace['view']];

            echo \CJSON::encode($params);
            \Yii::app()->end();
        } else {
            $referrer = \Yii::app()->request->getUrlReferrer();
            if($this->_url !== false && $this->_url !== null)
                \Yii::app()->controller->redirect($this->_url);
            elseif($referrer !== null)
                \Yii::app()->controller->redirect($referrer);
        }
    }
} 