<?php
namespace common\components;

/**
 * Class WebUser контейнер пользователя, который зашел на портал
 *
 * @property integer $level
 * @property integer $id
 * @property integer $game_id
 * @property integer $align
 * @property string $clan
 * @property string $login
 *
 * @package application.components
 */
use CWebUser;
use Yii;
use User;
use CDbCacheDependency;

class WebUser extends CWebUser
{
    // Store model to not repeat query.
    private $_model;
    private $params = array();

    private function getSessionId()
    {
        if (!isset($_SERVER['HTTP_COOKIE']))
            return null;

        /** @var User $model */
        $model = $this->loadUser($this->id);
        if (!$this->isGuest && $model !== null)
            return $model->getId();

        if (preg_match('/sportime_ru=(.*?)(?:;|$)/ui', $_SERVER['HTTP_COOKIE'], $out))
            return $out[1];
        else
            return null;
    }

    private function clearFromOnline()
    {
        $Online = Yii::app()->cache->get('online');
        if ($Online === false) $Online = [];
        if (($sessionId = $this->getSessionId()) !== null && isset($Online[$sessionId])) {
            $Online[$sessionId]['update_at'] = 0;
            Yii::app()->cache->set('online', $Online);
        }
    }

    public function init()
    {
        $conf = Yii::app()->session->cookieParams;
        $this->identityCookie = array(
            'path' => $conf['path'],
            'domain' => $conf['domain'],
        );

        parent::init();

        /** @var User $model */
        $model = $this->loadUser($this->id);
        if ($this->isGuest || null === $model)
            return;

        if ($model->status == User::STATUS_BLOCKED) {
            $this->logout();
            $this->setFlash('error', 'Пользователь заблокирован!');
            return;
        }

        foreach ($model->attributes as $name => $value)
            $this->params[$name] = $value;

        $Online = Yii::app()->cache->get('online');
        if ($Online === false) $Online = [];

        if (($sessionId = $this->getSessionId()) !== null) {
            if ($this->isGuest)
                $Online[$sessionId] = [
                    'last_activity' => time(),
                    'update_at' => time(),
                    'login' => 'guest',
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                ];
            else {
                $Online[$sessionId] = [
                    'last_activity' => time(),
                    'update_at' => time(),
                    'login' => $this->getName(),
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                ];
            }

            Yii::app()->cache->set('online', $Online);
        }
    }

    public function beforeLogin($id, $states, $fromCookie)
    {
        $r = parent::beforeLogin($id, $states, $fromCookie);

        $this->clearFromOnline();

        return $r;
    }

    public function beforeLogout()
    {
        $r = parent::beforeLogout();

        $this->clearFromOnline();

        return $r;
    }

    // Return first name.
    // access it by Yii::app()->user->first_name
    public function getName()
    {
        $user = $this->loadUser($this->id);
        if (null !== $user)
            return $user->username != null && $user->username != '' ? $user->username : $user->email;
        else
            return false;
    }

    public function getGender()
    {
        $user = $this->loadUser($this->id);
        if (null !== $user && $user->userProfile->getSex() !== null)
            return $user->userProfile->getSex() ? 'man' : 'woman';
        else
            return null;
    }

    public function getOnline()
    {
        $user = $this->loadUser($this->id);
        if (null !== $user)
            return $user->getOnline();
        else
            return 'offline';
    }

    public function getAvatar($type = \UserProfile::SIZE_TYPE_MAX)
    {
        $user = $this->loadUser($this->id);
        return $user->userProfile->getAvatar($type);
    }

    public function isModer()
    {
        return false;
    }

    public function getPanelStatus()
    {
        $status = $this->getState('panelStatus');
        if ($status !== null)
            return $status;
        $user = $this->loadUser($this->id);
        if ($user !== null) {
            $this->setState('panelStatus', $user->userProfile->panel_status);
            return $user->userProfile->panel_status;
        } else
            return 1;
    }

    public function getModel()
    {
        return $this->loadUser($this->id);
    }

    /**
     * @param null $id
     * @return User|null
     */
    protected function loadUser($id = null)
    {
        if ($this->_model === null) {
            if ($id !== null) {
                $dependency = new CDbCacheDependency('SELECT MAX(update_at) FROM {{user}} where id = :user_id');
                $dependency->params = [':user_id' => $id];
                $dependency->reuseDependentData = true;

                $this->_model = User::model()->cache(10000, $dependency)->with('userProfile')->findByPk($id);
            }
        }
        return $this->_model;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->params))
            return $this->params[$name];
        else
            return parent::__get($name);
    }
}