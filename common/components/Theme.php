<?php
namespace common\components;
/**
 * Created by PhpStorm.
 * User: me
 * Date: 22.02.2015
 * Time: 20:54
 */

use CTheme;
use Yii;
class Theme extends CTheme
{
    private $_name;
    private $_basePath;
    private $_baseUrl;

    /**
     * Constructor.
     * @param string $name name of the theme
     * @param string $basePath base theme path
     * @param string $baseUrl base theme URL
     */
    public function __construct($name,$basePath,$baseUrl)
    {
        $this->_name=$name;
        $this->_baseUrl=$baseUrl;
        $this->_basePath=$basePath;
    }
    /**
     * @return string theme name
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @return string the relative URL to the theme folder (without ending slash)
     */
    public function getBaseUrl()
    {
        return $this->_baseUrl;
    }

    /**
     * @return string the file path to the theme folder
     */
    public function getBasePath()
    {
        return $this->_basePath;
    }

    public function getViewPath()
    {
        return $this->_basePath;
    }

    public function getViewFile($controller,$viewName)
    {
        $moduleViewPath=$this->getViewPath();
        if(($module=$controller->getModule())!==null) {
            $moduleViewPath .= '/modules/' . $controller->getUniqueId();
            $viewName = '/'.$viewName;
        }

        return $controller->resolveViewFile($viewName,$this->getViewPath().'/www/'.$controller->getUniqueId(),$this->getViewPath(),$moduleViewPath);
    }

    public function getLayoutFile($controller,$layoutName)
    {
        $r = parent::getLayoutFile($controller, $layoutName);
        if($r !== false)
            return $r;

        $basePath=$this->getViewPath();
        return $controller->resolveViewFile($layoutName,$basePath.'/layouts',$basePath,$basePath);
    }
}