<?php
/**
 * Created by PhpStorm.
 * User: Ice
 * Date: 13.07.14
 * Time: 1:53
 */
namespace common\components;

use Yii;
use Muscle;
use Simulator;
use Filter;
class Categories extends \CApplicationComponent
{
    private $muscle = [];
    private $simulators = [];
    private $filter = [];

    public function init()
    {
        $this->filter = Yii::app()->cache->get('filterList');
        if($this->filter === false) {
            $this->filter = [];
            /** @var Filter[] $Models */
            $Models = Filter::model()->findAll(['order' => 'title asc']);
            foreach ($Models as $Model)
                $this->filter[$Model->getId()] = $Model;

            Yii::app()->cache->add('filterList', $this->filter);
        }
    }

    /**
     * @param null $id
     * @return Simulator|Simulator[]
     */
    public function getSimulators($id = null)
    {
        if($id === null)
            return $this->simulators;

        return isset($this->simulators[$id]) ? $this->simulators[$id] : null;
    }

    /**
     * @param null $id
     * @return Muscle|Muscle[]
     */
    public function getMuscle($id = null)
    {
        if($id === null)
            return $this->muscle;

        return isset($this->muscle[$id]) ? $this->muscle[$id] : null;
    }

    /**
     * @param $page
     * @return array|\Filter[]
     */
    public function getFilterByPage($page)
    {
        $criteria = new \CDbCriteria();
        $criteria->addCondition('`t`.page_id = :page_id');
        $criteria->with = ['filterGroup' => ['scopes' => ['enable']]];
        $criteria->params = [':page_id' => $page];
        /** @var \FilterGroupPage $Page */
        if($Page = \FilterGroupPage::model()->find($criteria))
        {
            $criteria = new \CDbCriteria();
            $criteria->addCondition('group_id = :group_id');
            $criteria->scopes = ['enable'];
            $criteria->params = [':group_id' => $Page->getFilterGroupId()];
            return Filter::model()->findAll($criteria);
        }

        return [];
    }

    /**
     * @param null $id
     * @return \Filter|\Filter[]
     */
    public function getFilter($id = null)
    {
        if($id === null)
            return $this->filter;

        return isset($this->filter[$id]) ? $this->filter[$id] : null;
    }
} 