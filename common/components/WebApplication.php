<?php
/**
 * Base class for our web application.
 *
 * Here will be the tweaks affecting the behavior of all entry points.
 *
 * @package YiiBoilerplate
 *
 * @property ARedisConnection redis
 * @property ARedisCache cache
 * @property \common\components\Ajax ajax
 * @property \common\components\StaticContent static
 * @property \common\components\WebUser user
 * @property EasyImage easyImage
 * @property EMongoClient mongodb
 * @property \common\components\video\Youtube youtube
 * @property Curl curl
 * @property \common\components\ImageUpload imageUpload
 * @property \common\components\CImageHandler ih
 * @property \common\components\Categories categories
 * @property \common\components\Breadcrumbs breadcrumbs
 */
class WebApplication extends CWebApplication
{
	/**
     * Workaround to fallback to `en` locale even if something unknown to us was requested.
     *
     * @param string $localeID
     * @return CLocale
	 */
	public function getLocale($localeID = null)
    {
		try
        {
			return parent::getLocale($localeID);
		}
        catch (Exception $e)
        {
			return CLocale::getInstance('en');
		}
	}

	protected function init()
	{
		register_shutdown_function(array($this, 'onShutdownHandler'));
		parent::init();
	}

	public function onShutdownHandler()
	{
		// 1. error_get_last() returns NULL if error handled via set_error_handler
		// 2. error_get_last() returns error even if error_reporting level less then error
		$e = error_get_last();

		$errorsToHandle = E_ERROR | E_PARSE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_COMPILE_WARNING;

		if(!is_null($e) && ($e['type'] & $errorsToHandle)) {
			$msg = 'Fatal error: '.$e['message'];
			// it's better to set errorAction = null to use system view "error.php" instead of run another controller/action (less possibility of additional errors)
			yii::app()->errorHandler->errorAction = null;
			// handling error
			yii::app()->handleError($e['type'], $msg, $e['file'], $e['line']);
		}
	}

	public function createUrl($route, $params = array(), $sitePart = null, $ampersand='&')
	{
		return $this->getUrlManager()->createUrl($route, $params, $ampersand, $sitePart);
	}
}
