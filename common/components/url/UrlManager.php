<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 08.10.2014
 * Time: 1:17
 */

class UrlManager extends CUrlManager
{
    private $_rules = [];
    private $_baseUrl = null;
    private $mapping = array(
        //'locale' => null,
        //'branch' => null,
        //'env' => null,
        'sitePart' => null,
        'host' => null,
        'domain' => null,
    );
    private $_siteParts = [
        'profile',
        'frontend'
    ];

    public $urlRuleClass = 'UrlRule';

    public function init()
    {
        parent::init();

        $info = explode('.', Yii::app()->request->getServerName());
        krsort($info);
        $info = array_values($info);

        foreach ($info as $index => $value) {
            if($index == 0)
                $this->mapping['domain'] = $value;
            elseif($index == 1)
                $this->mapping['host'] = $value;
            elseif(in_array($value, $this->_siteParts))
                $this->mapping['sitePart'] = $value;
        }
    }

    public function createUrl($route, $params=array(), $ampersand='&', $sitePart = null)
    {
        $link = parent::createUrl($route, $params, $ampersand);
        foreach ($this->rules as $rule) {
            if(!is_array($rule) || !isset($rule[0]) || !isset($rule['sitePart']) || trim($route, '/') != $rule[0])
                continue;

            if(!$sitePart) $sitePart = $rule['sitePart'];

            if($sitePart != $this->mapping['sitePart']) {
                switch ($sitePart) {
                    case 'profile':
                        $link = 'http://profile.' . $this->mapping['host'] . '.' . $this->mapping['domain'] . $link;
                        break;
                    case 'backend':
                        $link = 'http://backend.' . $this->mapping['host'] . '.' . $this->mapping['domain'] . $link;
                        break;
                    case 'frontend':
                        $link = 'http://' . $this->mapping['host'] . '.' . $this->mapping['domain'] . $link;
                        break;
                }
            }
        }

        return $link;
    }
} 