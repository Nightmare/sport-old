<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 02.10.2014
 * Time: 1:01
 */

class MException
{
    public static function ShowError($message, $code = 404)
    {
        if(Yii::app()->request->getIsAjaxRequest()) {
            Yii::app()->controller->renderPartial('ajax.error', array(
                'code' => $code,
                'message' => $message
            ));
        } else {
            \common\components\VarDumper::dump($message);die;
            Yii::app()->controller->render('//common.views.www.site.error', array(
                'code' => $code,
                'message' => $message
            ));
        }
        Yii::app()->end();
    }
} 