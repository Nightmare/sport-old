<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 16.09.2014
 * Time: 1:38
 */

class UserIdentity extends CUserIdentity
{
    /**
     * @var string email
     */
    public $email;
    /**
     * @var int id
     */
    private $_id;

    const ERROR_EMAIL_INVALID = 1;
    const ERROR_USERNAME_BLOCKED = 2;

    public function __construct($username, $password, $email)
    {
        parent::__construct($username, $password);
        $this->email = $email;
    }


    /**
     * Authenticates a user.
     * The example implementation makes sure if the email and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('UPPER(username) = :username or UPPER(email) = :email');
        $criteria->params = [
            ':username' => strtoupper($this->username),
            ':email' => strtoupper($this->username),
        ];

        /** @var User $model */
        $model = User::model()->find($criteria);
        if($model === null)
            $this->errorCode = self::ERROR_EMAIL_INVALID;
        elseif($model->status == User::STATUS_BLOCKED)
            $this->errorCode = self::ERROR_USERNAME_BLOCKED;
        elseif($model->verifyPassword($this->password) === false)
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id = $model->id;
            $this->username = $model->username;
            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;
    }

    /**
     * @return integer the ID of the user record
     */
    public function getId()
    {
        return $this->_id;
    }
} 