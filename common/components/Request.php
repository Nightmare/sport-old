<?php
namespace common\components;
/**
 * Created by PhpStorm.
 * User: me
 * Date: 08.10.2014
 * Time: 6:46
 */

use CHttpRequest;
class Request extends CHttpRequest
{
    public function getIsAjaxRequest()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']==='XMLHttpRequest') || isset($_REQUEST['request_width']);
    }
} 