<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 30.09.2014
 * Time: 22:12
 */

namespace common\components;

use CController;
use Yii;
class Controller extends CController
{
    public function beforeAction($action)
    {
        if(Yii::app()->user->isGuest && !in_array($action->id, ['login', 'error', 'registration', 'restore', 'activation'])) {
            Yii::app()->ajax->setUrl(['route' => '/users/user/login', 'params' => []])->send();
        }

        if(defined('YII_DEBUG') && YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
            Yii::app()->assetManager->forceCopy = true;
        }

        $result = parent::beforeAction($action);
        $this->registerAssets();

        return $result;
    }

    private function registerAssets()
    {
        Yii::app()->static->setOwn()
            ->registerScriptFile('script.js');

        Yii::app()->static->setLibrary('needim-noty')
            ->registerScriptFile('jquery.noty.packaged.min.js');

        Yii::app()->static->setLibrary('fancybox')
            ->registerCssFile('jquery.fancybox.css')
            ->registerScriptFile('jquery.fancybox.pack.js')
            ->registerScriptFile('jquery.fancybox-media.js');

        Yii::app()->static->setLibrary('history')
            ->registerScriptFile('jquery.history.js');

        Yii::app()->static->setLibrary('infinite-scroll')
            ->registerScriptFile('jquery.infinitescroll.js');
    }
} 