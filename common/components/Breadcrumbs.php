<?php
namespace common\components;
/**
 * Created by PhpStorm.
 * User: me
 * Date: 24.10.2014
 * Time: 17:03
 */

class Breadcrumbs extends \CApplicationComponent
{
    private $_breadcrumbs = [];

    public function init()
    {
        parent::init();
    }

    /**
     * @param $label
     * @param bool $link
     * @return $this
     */
    public function addBreadcrumb($label, $link = false)
    {
        $this->_breadcrumbs[] = [
            'label' => $label,
            'link' => $link
        ];

        return $this;
    }

    public function getBreadcrumbs()
    {
        return $this->_breadcrumbs;
    }
} 