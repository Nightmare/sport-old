<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 11.10.2014
 * Time: 5:16
 *
 * @var string $link
 */ ?>

<nav class="filter-list" id="simulator-filter">
    <h3 class="title">
        выбранный тренажер
    </h3>
    <div class="preview-box">
        <a href="#"><img src="<?= Yii::app()->static->setOwn()->imageLink('img/sm_zgl3.jpg', true) ?>" alt=""></a>
    </div>
    <h2>выберите тренажер</h2>
    <div class="tab-content">
        <ul>
            <?php foreach(Yii::app()->categories->getSimulators() as $simulator): ?>
                <li><a data-image-small="<?= Yii::app()->static->imageLink($simulator->getHImageSmallLink()); ?>" data-image="<?= Yii::app()->static->imageLink($simulator->getHImageLink()); ?>" data-simulator="<?= $simulator->getDir(); ?>" href="<?= $link ?>"><?= $simulator->getTitle() ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</nav>