<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 11.10.2014
 * Time: 5:16
 *
 * @var string $link
 * @var string $img
 * @var int $active_id
 */ ?>

<nav class="filter-list" id="muscle-filter">
    <h3 class="title">
        выбранная мышца
    </h3>
    <div class="preview-box">
        <a href="#"><img src="<?= Yii::app()->static->setOwn()->imageLink('img/sm_zgl3.jpg', true) ?>" alt=""></a>
    </div>
    <h2>выберите мышцы</h2>
    <div class="tab-content">
        <ul class="image-list">
            <?php foreach (Yii::app()->categories->getMuscle() as $muscle): ?>
                <li>
                    <a data-image="<?= Yii::app()->static->imageLink($muscle->getHImageLink()); ?>" data-muscle="<?= $muscle->getDir(); ?>" href="<?= $link; ?>">
                        <img src="<?= Yii::app()->static->imageLink($muscle->getHImageLink()) ?>" alt=""></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</nav>