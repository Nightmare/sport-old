<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 11.10.2014
 * Time: 5:16
 *
 * @var string $link
 * @var Filter[] $filters
 */ ?>

<nav class="filter-list" id="filter-filter">
    <div class="hidden" id="data-filter-link" data-filter-link="<?= $link; ?>"></div>
    <h3 class="title">
        выбранный фильтр
    </h3>
    <div class="preview-box" data-img="<?= Yii::app()->static->setOwn()->imageLink('img/sm_zgl3.jpg', true) ?>">
        <a href="javascript:void(0);"><img src="<?= Yii::app()->static->setOwn()->imageLink('img/sm_zgl3.jpg', true) ?>" alt=""></a>
    </div>
    <h2>выберите фильтр</h2>
    <div class="tab-content">
        <ul class="image-list">
            <?php foreach ($filters as $Filter): ?>
                <li>
                    <a data-image="<?= Yii::app()->static->imageLink($Filter->getHImageLink()); ?>" data-filter="<?= $Filter->getDir(); ?>" href="<?= $link; ?>">
                        <img src="<?= Yii::app()->static->imageLink($Filter->getHImageLink()) ?>" alt=""></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</nav>