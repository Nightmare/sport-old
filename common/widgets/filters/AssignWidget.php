<?php
namespace common\widgets\filters;

use CWidget;
use Yii;
/**
 * Class MessageListWidget
 *
 * @package application.widgets.messageList
 */
class AssignWidget extends CWidget
{
    public $link = null;
    /** @var null */
    public $parent_id = null;

    public function init()
    {
        parent::init();

        Yii::app()->static->setWidget('filter')
            ->registerScriptFile('script.js')
            ->registerCssFile('filter.css');
    }

    public function run()
    {
        $filters = [];

        $criteria = new \CDbCriteria();
        $criteria->addCondition('`t`.filter_id = :filter_id');
        $criteria->with = ['filterAssign' => ['scopes' => ['enable']]];
        $criteria->params = [':filter_id' => $this->parent_id];
        /** @var \FilterFilter $FilterFilter */
        $FilterFilter = \FilterFilter::model()->findAll($criteria);
        foreach ($FilterFilter as $F)
            $filters[] = $F->filterAssign;

        $this->render('assign', ['link' => $this->link, 'filters' => $filters]);
    }
}