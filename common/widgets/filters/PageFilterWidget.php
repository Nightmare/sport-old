<?php
namespace common\widgets\filters;

use CWidget;
use Yii;
/**
 * Class MessageListWidget
 *
 * @package application.widgets.messageList
 */
class PageFilterWidget extends CWidget
{
    public $link = null;
    /** @var null */
    public $page_id = null;

    public function init()
    {
        parent::init();

        Yii::app()->static->setWidget('filter')
            ->registerScriptFile('script.js')
            ->registerCssFile('filter.css');
    }

    public function run()
    {
        $criteria = new \CDbCriteria();
        $criteria->with = [
            'filterGroup' => [
                'with' => [
                    'pages' => [
                        'joinType' => 'inner join'
                    ]
                ]
            ]
        ];
        $criteria->addCondition('pages.page_id = :page_id');
        $criteria->params = [':page_id' => $this->page_id];
        /** @var \Filter $Filters */
        $Filters = \Filter::model()->findAll($criteria);

        $this->render('assign', ['link' => $this->link, 'filters' => $Filters]);
    }
}