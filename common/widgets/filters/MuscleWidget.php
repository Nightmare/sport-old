<?php
namespace common\widgets\filters;

use CWidget;
use Yii;
use EMongoCriteria;
use HistoryMessage;
/**
 * Class MessageListWidget
 *
 * @package application.widgets.messageList
 */
class MuscleWidget extends CWidget
{
    public $link = null;
    /** @var null|\Simulator */
    public $simulator = null;

    public function init()
    {
        parent::init();

        Yii::app()->static->setWidget('filter')
            ->registerScriptFile('script.js');
    }

    public function run()
    {
        $this->render('muscle', ['link' => $this->link]);
    }
}