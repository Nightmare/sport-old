<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 16.09.2014
 * Time: 3:13
 */
namespace common\widgets\panel;

use CWidget;
class Panel extends CWidget
{
    public function run()
    {
        $this->render('panel');
    }
} 