<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 11.12.13
 * Time: 22:28
 *
 * @var User $model
 * @var integer $msgCount
 */
$msgCountString = '';
if($msgCount)
    $msgCountString = ' ('.$msgCount.')';
?>

<a href="#" class="v-btn">sportime</a>
<div class="inner">
    <div class="inner">
        <a href="#" class="username gender-<?= Yii::app()->user->getGender(); ?>"><?= Yii::app()->user->getName(); ?></a>
        <figure>
            <img width="100" height="100" src="<?= Yii::app()->user->getAvatar(UserProfile::SIZE_TYPE_MIDDLE) ?>" alt="">
        </figure>
        <ul>
            <li><?= CHtml::link(Yii::t('labels', 'Главная'), Yii::app()->createUrl('/site/index')); ?></li>
            <li><?= CHtml::link(Yii::t('labels', 'Профиль'), Yii::app()->createUrl('/users/profile/index')); ?></li>
            <li><?= CHtml::link(Yii::t('labels', 'Сообщения'.$msgCountString), Yii::app()->createUrl('/message/profile/index'), ['id' => 'message_link']) ?></li>
            <li><?= CHtml::link(Yii::t('labels', 'Админка'), Yii::app()->createUrl('/site/index', [], 'backend')) ?></li>
            <li class="empty last"><?= CHtml::link(Yii::t('labels', 'Выход'), Yii::app()->createUrl('/users/user/logout')); ?></li>
        </ul>
        <!--<a href="#" class="btn-nav">Развернуть</a>-->
    </div>
</div>