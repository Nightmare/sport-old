<?php
/**
 * @var \common\components\Controller $this
 */
?>
<div class="panel" id="panel_bottom">
    <div class="top-panel">
        <a href="#" class="btn-nav">Навигация</a>
    </div>
    <div class="bottom-panel">
        <section class="center">
            <div id="panel-page-dark">
                <div id="panel-page-loader"></div>
            </div>
            <div class="center-content">
                <div class="center-content-wrapper">
                    <div class="tab_content comment-form" data-tab-in="form">
                        <form action="" id="bottom_form" method="post" enctype="multipart/form-data">
                            <table>
                                <tr>
                                    <td class="text_body">
                                        <div class="form-item">
                                            <textarea id="bottom_form_body" name="comment" cols="30" rows="7" placeholder="Напишите тут свой комментарий..."></textarea>
                                        </div>
                                        <div class="form-item">
                                            <input class="btn" type="submit" id="bottom_form_submit" value="опубликовать комментарий">
                                        </div>
                                    </td>
                                    <td class="form-item-group">
                                        <div class="title-grey"><?= Yii::t('labels', 'Прикрепленные файлы')?>:</div>
                                        <ul class="form-items">

                                        </ul>
                                        <div class="form-item">
                                            <div id="ajax_upload_image">
                                                <input type="text" placeholder="Выбирите изображения" readonly="true">
                                                <input type="file" class="hidden-file">
                                                <a href="#" class="btn btn-file add_image">обзор...</a>
                                                <a href="#" class="btn start_upload_image">вставить фото</a>
                                            </div>
                                        </div>
                                        <div class="form-item">
                                            <div id="ajax_upload_video">
                                                <input type="text" placeholder="Вставьте ссылку на видео">
                                                <a href="#" class="btn start_upload_video">вставить видео</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <div class="tab_content site-rules" data-tab-in="news">
                        <h3>Правила сайта</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad animi consectetur consequuntur, culpa distinctio dolorem eligendi et excepturi fuga ipsum nisi non, obcaecati perferendis perspiciatis possimus qui voluptas! Ducimus facilis ipsum iste laborum, molestias placeat sed totam voluptate?</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <ul class="video-list">
                            <li class="video-item">
                                <div class="video-item-inner">
                                    <img src="<?= Yii::app()->static->imageLink('www/img/panel/avatar.png'); ?>" alt="">
                                </div>
                                <a href="#" class="video-item-title">Удалить фото</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab_content" data-tab-in="saver"></div>
                    <div class="tab_content" data-tab-in="pocket"></div>
                </div>
            </div>
        </section>
        <aside class="left">
            <a href="#" class="v-btn">sportime</a>
            <div class="inner">
                <div class="inner">
                    <figure>
                        <img src="<?= Yii::app()->static->imageLink('www/img/panel/sport-men.png') ?>" alt="">
                    </figure>
                    <ul>
                        <li><a href="#" class="tab_buttons" data-tab-for="form">Текcтовая форма</a></li>
                        <li><a href="#" class="tab_buttons" data-tab-for="news">Новости</a></li>
                        <li><a href="#" class="tab_buttons" data-tab-for="saver">Сейвер</a></li>
                        <li><a href="#" class="tab_buttons" data-tab-for="pocket">Карман</a></li>
                        <li class="empty"><a href="#">&nbsp;</a></li>
                        <li class="empty last"><a href="#">&nbsp;</a></li>
                    </ul>
                    <!--<a href="#" class="btn-nav">Развернуть</a>-->
                </div>
            </div>
        </aside>
        <aside class="right">
            <?php $this->widget('\common\widgets\panel\ProfileWidget'); ?>
        </aside>
    </div>
</div>
<div class="center">
    <ul>
        <li class="pull-left"><a href="<?= Yii::app()->createUrl('/site/index') ?>"><?= Yii::t('labels', 'Главная'); ?></a></li>
        <?php if(($conference = Yii::app()->request->getParam('conference')) && \common\helpers\StreetHelper::getConferenceByName($conference)): ?>
            <li class="pull-left"><a href="<?= Yii::app()->createUrl('/conference/conference/index', ['conference' => $conference]) ?>"><?= Yii::t('labels', 'Конференция'); ?></a></li>
            <li class="pull-right"><a href="<?= Yii::app()->createUrl('/gallery/video/list', ['conference' => $conference]) ?>"><?= Yii::t('labels', 'Видео'); ?></a></li>
            <li class="pull-right"><a href="<?= Yii::app()->createUrl('/gallery/image/list', ['conference' => $conference]) ?>"><?= Yii::t('labels', 'Фото'); ?></a></li>
        <?php endif; ?>

        <li class="pull-right"><a href="<?= Yii::app()->createUrl('/pocket/image/index') ?>"><?= Yii::t('labels', 'Карман'); ?></a></li>
    </ul>
</div>
<script>
    $(function(){
        <?php if(Yii::app()->user->getPanelStatus()): ?>
            $panel.showStartup();
        <?php endif; ?>

        $(document.body).on('click', '.panel .top-panel .btn-nav', function(){
            var $el = $('#footer div.panel');
            var status = 1;
            if($el.hasClass('open')) {
                status = 0;
            }

            $ajax.send('<?= Yii::app()->createUrl('/users/profile/panel') ?>', {'status':status}, null,'post', false);
        });
    });
</script>