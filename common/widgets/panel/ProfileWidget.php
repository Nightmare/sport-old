<?php
namespace common\widgets\panel;

use CWidget;
use Yii;
use EMongoCriteria;
use HistoryMessage;
/**
 * Class MessageListWidget
 *
 * @package application.widgets.messageList
 */
class ProfileWidget extends CWidget
{
    public function init()
    {
        parent::init();

        Yii::app()->static->setWidget('panel')
            ->registerScriptFile('script.js')
            ->registerCssFile('style.css');

        $registry = Yii::app()->clientScript;
        $registry
            ->registerScript(uniqid(), "var infoUpdateLink = '".Yii::app()->createUrl('/message/profile/message')."';", \CClientScript::POS_END)
            ->registerScript(uniqid(), "var saverLink = '".Yii::app()->createUrl('/saver/all/get')."';", \CClientScript::POS_END)
            ->registerScript(uniqid(), "var pocketLink = '".Yii::app()->createUrl('/pocket/all/get')."';", \CClientScript::POS_END);
    }

    public function run()
    {
        $criteria = new EMongoCriteria();
        $criteria->addCondition('user_receiver_id', Yii::app()->user->getId());
        $criteria->addCondition('receiver_view', false);
        $count = HistoryMessage::model()->count($criteria);

        $this->render('profile_menu', ['msgCount' => $count]);
    }
}