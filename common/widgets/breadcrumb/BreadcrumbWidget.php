<?php
namespace common\widgets\breadcrumb;

use CWidget;
use Yii;
use EMongoCriteria;
use HistoryMessage;
/**
 * Class MessageListWidget
 *
 * @package application.widgets.messageList
 */
class BreadcrumbWidget extends CWidget
{
    public $view = 'index';

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $this->render($this->view, ['breadcrumbs' => Yii::app()->breadcrumbs->getBreadcrumbs()]);
    }
}