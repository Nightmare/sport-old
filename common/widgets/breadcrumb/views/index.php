<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 24.10.2014
 * Time: 16:53
 *
 * @var array $breadcrumbs
 */ ?>

<nav class="main-menu">
    <ul>
        <li><a href="<?= Yii::app()->createUrl('/site/index'); ?>">&nbsp;</a></li>
        <?php foreach ($breadcrumbs as $breadcrumb): ?>
            <?php
            if($breadcrumb['link'] !== false)
                echo '<li>'.CHtml::link($breadcrumb['label'], $breadcrumb['link']).'</li>';
            else
                echo '<li class="active">'.CHtml::link($breadcrumb['label'], 'javascript:void(0)').'</li>';
            ?>
        <?php endforeach; ?>
    </ul>
</nav>