<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 24.10.2014
 * Time: 16:53
 *
 * @var array $breadcrumbs
 */ ?>

<ol class="breadcrumb">
    <li><a href="<?= Yii::app()->createUrl('/admin/index'); ?>">Главная</a></li>
    <?php foreach ($breadcrumbs as $breadcrumb): ?>
        <?php
        if($breadcrumb['link'] !== false)
            echo '<li>'.CHtml::link($breadcrumb['label'], $breadcrumb['link']).'</li>';
        else
            echo '<li class="active">'.$breadcrumb['label'].'</li>';
        ?>
    <?php endforeach; ?>
</ol>
