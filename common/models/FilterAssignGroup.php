<?php

Yii::import('common.models._base.BaseFilterAssignGroup');

class FilterAssignGroup extends BaseFilterAssignGroup
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    /**
     * @return int
     */
    public function getFilterGroupId()
    {
        return $this->filter_group_id;
    }

    /**
     * @param int $filter_group_id
     * @return $this
     */
    public function setFilterGroupId($filter_group_id)
    {
        $this->filter_group_id = $filter_group_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getFilterId()
    {
        return $this->filter_id;
    }

    /**
     * @param int $filter_id
     * @return $this
     */
    public function setFilterId($filter_id)
    {
        $this->filter_id = $filter_id;
        return $this;
    }
}