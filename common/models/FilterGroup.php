<?php

Yii::import('common.models._base.BaseFilterGroup');
/**
 * Class FilterGroup
 *
 * @property string filter_ids
 * @property boolean is_deleted
 *
 *
 * @property FilterGroupPage[] $pages
 *
 * @method FilterGroup enable()
 */
class FilterGroup extends BaseFilterGroup
{
    public $filter_id;

    /**
     * @param string $className
     * @return FilterGroup
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function behaviors()
    {
        return [
            // Password behavior strategy
            'CTimestampbehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    public function relations() {
        return [
            'filters' => [self::HAS_MANY, 'Filter', 'group_id'],
            'pages' => [self::HAS_MANY, 'FilterGroupPage', 'filter_group_id'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Название'),
            'update_at' => Yii::t('app', 'Дата обновления'),
            'create_at' => Yii::t('app', 'Дата создания'),
            'filters' => null,
        ];
    }

    public function scopes()
    {
        $t = $this->getTableAlias(false, false);
        return [
            'enable' => [
                'condition' => "`{$t}`.is_deleted = :{$t}_is_deleted",
                'params' => [":{$t}_is_deleted" => 0]
            ]
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param int $update_at
     * @return $this
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param int $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @param boolean $unserialize
     * @return string|array
     */
    public function getFilterIds($unserialize = false)
    {
        if(!$unserialize)
            return $this->filter_ids;

        return unserialize($this->filter_ids);
    }

    /**
     * @param string $filter_ids
     * @return $this
     */
    public function setFilterIds($filter_ids)
    {
        $this->filter_ids = $filter_ids;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $is_deleted
     * @return $this
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }

    public function getFilterListString()
    {
        if(!$this->hasRelated('filters') || !$this->filters)
            return 'Фильтры отсутствуют';

        $string = '';
        foreach ($this->filters as $filter)
            $string .= $filter->getTitle().'<br>';

        return trim($string, '<br>');
    }

    public function getPageListString()
    {
        $string = '';
        $string .= CHtml::tag(
            'div', ['class' => 'center'],
            CHtml::link(
                '+',
                Yii::app()->createUrl('/filter/group/page', ['group_id' => $this->getId()]),
                ['class' => 'btn btn-primary sq', 'data-type' => 'ajax']
            )
        );
        if(!$this->hasRelated('pages') || !$this->pages)
            return $string.'Привязка к страницам отсутствует';

        $string .= '<ul class="group-page-list">';
        foreach ($this->pages as $page) {

            $icon = CHtml::tag('i', [
                'class' => 'glyphicon glyphicon-trash pointer',
                'title' => 'Удалить связь',
                'data-toggle' => 'tooltip',
                'data-type' => 'ajax',
                'data-link' => Yii::app()->createUrl('/filter/group/pageDelete', [
                    'filter_group_id' => $page->getFilterGroupId(),
                    'page_id' => $page->getPageId()
                ])
            ]);
            $string .= CHtml::tag('li', [], \common\helpers\StreetHelper::getPageTitle($page->getPageId()).' '.$icon);
        }
        $string .= '</ul>';

        return $string;
    }
}