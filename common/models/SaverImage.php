<?php

Yii::import('common.models._base.BaseSaverImage');

class SaverImage extends BaseSaverImage
{
	/**
	 * @param string $className
	 * @return SaverImage
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'CTimestampbehavior' => [
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_at',
				'updateAttribute' => 'view_at',
				'setUpdateOnCreate' => true,
			]
		];
	}

	public function relations() {
		return [
			'user' => [self::BELONGS_TO, 'User', 'user_id'],
			'image' => [self::BELONGS_TO, 'GalleryImage', 'image_id', 'joinType' => 'inner join'],
		];
	}

	public function scopes()
	{
		$t = $this->getTableAlias(false, false);
		return [
			'own' => [
				'condition' => "`{$t}`.user_id = :{$t}_user_id",
				'params' => [":{$t}_user_id" => Yii::app()->user->id]
			],
			'enable' => [
				'condition' => "`{$t}`.is_deleted = :{$t}_is_deleted",
				'params' => [":{$t}_is_deleted" => 0]
			]
		];
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getImageId()
	{
		return $this->image_id;
	}

	/**
	 * @param int $image_id
	 * @return $this
	 */
	public function setImageId($image_id)
	{
		$this->image_id = $image_id;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getUserId()
	{
		return $this->user_id;
	}

	/**
	 * @param int $user_id
	 * @return $this
	 */
	public function setUserId($user_id)
	{
		$this->user_id = $user_id;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getViewAt()
	{
		return $this->view_at;
	}

	/**
	 * @param int $view_at
	 * @return $this
	 */
	public function setViewAt($view_at)
	{
		$this->view_at = $view_at;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getCreateAt()
	{
		return $this->create_at;
	}

	/**
	 * @param int $create_at
	 * @return $this
	 */
	public function setCreateAt($create_at)
	{
		$this->create_at = $create_at;
		return $this;
	}
}