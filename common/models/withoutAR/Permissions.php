<?php
/**
 * Created by PhpStorm.
 */

class Permissions extends CFormModel
{
    /** @var User */
    protected $_user;

    public $filterControl;
    public $filterView;

    public $galleryControl;
    public $galleryView;

    public $userControl;
    public $userView;

    public $gymnasiumControl;
    public $gymnasiumView;

    /**
     * @param User $user
     * @param string $scenario
     */
    public function __construct(User $user, $scenario = '')
    {
        $this->_user = $user;

        $this->setScenario($scenario);
        $this->init();
        $this->attachBehaviors($this->behaviors());
        $this->afterConstruct();
    }

    public function afterConstruct()
    {
        $auth = Yii::app()->authManager;

        foreach ($this->getAttributes() as $name => $value) {
            if($auth->checkAccess($name, $this->_user->getId()))
                $this->{$name} = true;
            else
                $this->{$name} = false;
        }
    }

    public function save()
    {
        $auth = Yii::app()->authManager;

        try {
            foreach ($this->getAttributes() as $name => $value) {
                $isAssign = $auth->isAssigned($name, $this->_user->getId());

                if($this->{$name}) {
                    if(!$isAssign)
                        $auth->assign($name, $this->_user->getId());
                } elseif($isAssign)
                    $auth->revoke($name, $this->_user->getId());
            }
        } catch (Exception $ex) {
            return false;
        }

        return true;
    }
}