<?php

Yii::import('common.models._base.BaseGalleryVideo');

/**
 * Class GalleryVideo
 *
 * @property User $user
 * @property UserProfile $userProfile
 * @property VideoTemplate $template
 * @property int conference_id
 * @property string filter_ids
 * @property boolean from_admin
 * @property boolean is_deleted
 * @property int channel_id
 * @property VideoChannel channel
 */
class GalleryVideo extends BaseGalleryVideo implements \common\interfaces\iSizesGalleryVideo, \common\interfaces\iComment
{
    public $link;
    public $template_id;

    const STATUS_NEW = 0;

    public $video_filter_group_id;
    public $video_filter_id;
    protected $video_filter_info = [];

    const IMAGE_EXT = 'jpeg';

    /**
     * @param string $className
     * @return GalleryVideo
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function init()
    {
        if($this->isNewRecord || $this->unique_id === null)
            $this->unique_id = md5(uniqid(rand(), true));
    }

    public function scopes()
    {
        $t = $this->getTableAlias(false, false);
        return [
            'own' => [
                'condition' => "`{$t}`.user_id = :{$t}_user_id",
                'params' => [":{$t}_user_id" => Yii::app()->user->id]
            ],
            'enable' => [
                'condition' => "`{$t}`.is_deleted = :{$t}_is_deleted",
                'params' => [":{$t}_is_deleted" => 0]
            ],
            'admin' => [
                'condition' => "`{$t}`.from_admin = :{$t}_from_admin",
                'params' => [":{$t}_from_admin" => 1]
            ]
        ];
    }

    public function relations() {
        return [
            'user' => [self::BELONGS_TO, 'User', 'user_id', 'joinType' => 'inner join'],
            'userProfile' => [self::BELONGS_TO, 'UserProfile', 'user_id', 'joinType' => 'inner join'],
            'template' => [self::BELONGS_TO, 'VideoTemplate', 'template_id', 'joinType' => 'inner join'],
            'channel' => [
                self::HAS_ONE,
                'VideoChannel',
                ['id' => 'channel_id'],
                'joinType' => 'left join'
            ],
        ];
    }

    public function behaviors()
    {
        Yii::import('common.extensions.behaviors.password.*');
        return [
            // Password behavior strategy
            'CTimestampbehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    public function rules() {
        return [
            ['user_id, title, link, template_id', 'required'],
            ['album_id, user_id, template_id, view_role, status, update_at, create_at', 'numerical', 'integerOnly'=>true],
            ['title', 'length', 'max'=>255],
            ['description', 'safe'],
            ['user_id', 'unsafe'],
            ['album_id, title, description, view_role, status, update_at, create_at, channel_id', 'default', 'setOnEmpty' => true, 'value' => null],
            ['id, album_id, user_id, template_id, title, description, view_role, status, update_at, create_at, channel_id', 'safe', 'on'=>'search'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'channel_id' => Yii::t('app', 'Канал'),
            'album_id' => Yii::t('app', 'Album'),
            'user_id' => null,
            'template_id' => Yii::t('app', 'Видео хостинг'),
            'title' => Yii::t('app', 'Название'),
            'description' => Yii::t('app', 'Описание'),
            'view_role' => Yii::t('app', 'View Role'),
            'status' => Yii::t('app', 'Status'),
            'update_at' => Yii::t('app', 'Дата обновления'),
            'create_at' => Yii::t('app', 'Дата создания'),
            'link' => Yii::t('app', 'Ссылка'),
            'template' => null,
            'user' => null,
        ];
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->link = $this->getSourceLink();
    }

    public function getCommentField()
    {
        return 'video_id';
    }

    public function getCommentSendLink()
    {
        return Yii::app()->createUrl("/comment/video/add", ["video_dir" => $this->getDir(), 'conference' => $this->getConferenceString()]);
    }

    public function getCommentListLink()
    {
        return Yii::app()->createUrl("/comment/video/get", ["video_dir" => $this->getDir(), 'conference' => $this->getConferenceString()]);
    }

    public function getCommentUploadImageLink()
    {
        return Yii::app()->createUrl("/comment/video/upload", ["video_dir" => $this->getDir(), 'conference' => $this->getConferenceString()]);
    }

    public function getCommentUploadVideoLink()
    {
        return Yii::app()->createUrl("/comment/video/video", ["video_dir" => $this->getDir(), 'conference' => $this->getConferenceString()]);
    }

    public function getCommentPageRoute()
    {
        return "/comment/video/get";
    }

    /**
     * @return mixed
     */
    public function getAlbumId()
    {
        return $this->album_id;
    }

    /**
     * @param mixed $album_id
     * @return $this
     */
    public function setAlbumId($album_id)
    {
        $this->album_id = $album_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param mixed $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (int)$id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     * @return $this
     */
    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemplateId()
    {
        return $this->template_id;
    }

    /**
     * @param mixed $template_id
     * @return $this
     */
    public function setTemplateId($template_id)
    {
        $this->template_id = $template_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param mixed $update_at
     * @return $this
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getViewRole()
    {
        return $this->view_role;
    }

    /**
     * @param mixed $view_role
     * @return $this
     */
    public function setViewRole($view_role)
    {
        $this->view_role = $view_role;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHostVideoId()
    {
        return $this->host_video_id;
    }

    /**
     * @param mixed $host_video_id
     * @return $this
     */
    public function setHostVideoId($host_video_id)
    {
        $this->host_video_id = $host_video_id;
        return $this;
    }

    /**
     * @param mixed $format
     * @return mixed
     */
    public function getHostCreateAt($format = null)
    {
        if($format === null)
            return $this->host_create_at;

        return date($format, $this->host_create_at);
    }

    /**
     * @param mixed $host_create_at
     * @return $this
     */
    public function setHostCreateAt($host_create_at)
    {
        $this->host_create_at = $host_create_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHostView()
    {
        return $this->host_view;
    }

    /**
     * @param mixed $host_view
     * @return $this
     */
    public function setHostView($host_view)
    {
        $this->host_view = $host_view;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHostAuthor()
    {
        return $this->host_author;
    }

    /**
     * @param string $host_author
     * @return $this
     */
    public function setHostAuthor($host_author)
    {
        $this->host_author = $host_author;
        return $this;
    }

    /**
     * @param mixed $format
     * @return mixed
     */
    public function getDuration($format = false)
    {
        if($format === false)
            return $this->duration;
        else {
            $_h = 60 * 60;
            $_m = 60;
            $h = floor($this->duration / ($_h));
            $m = floor(($this->duration - ($h * $_h)) / $_m);
            $s = $this->duration -  ($h * $_h) - ($m * $_m);
            if($h < 10) $h = '0'.$h;
            if($m < 10) $m = '0'.$m;
            if($s < 10) $s = '0'.$s;

            return $h.':'.$m.':'.$s;
        }
    }

    /**
     * @param mixed $duration
     * @return $this
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getViewCount()
    {
        return $this->view_count;
    }

    /**
     * @param mixed $view_count
     * @return $this
     */
    public function setViewCount($view_count)
    {
        $this->view_count = $view_count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUniqueId()
    {
        return $this->unique_id;
    }

    /**
     * @param mixed $unique_id
     * @return $this
     */
    public function setUniqueId($unique_id)
    {
        $this->unique_id = $unique_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->image_name;
    }

    /**
     * @param $image_name
     * @return $this
     */
    public function setImageName($image_name)
    {
        $this->image_name = $image_name;
        return $this;
    }

    /**
     * @return int
     */
    public function getConferenceId()
    {
        return $this->conference_id;
    }

    /**
     * @param int $conference_id
     * @return $this
     */
    public function setConferenceId($conference_id)
    {
        $this->conference_id = $conference_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * @param string $dir
     * @return $this
     */
    public function setDir($dir)
    {
        $this->dir = $dir;
        return $this;
    }

    /**
     * @return string
     */
    public function getDirMain()
    {
        return $this->dir_main;
    }

    /**
     * @param string $dir_main
     * @return $this
     */
    public function setDirMain($dir_main)
    {
        $this->dir_main = $dir_main;
        return $this;
    }

    /**
     * @return string|array
     */
    public function getFilterIds($unserialize = false)
    {
        if($unserialize === false)
            return $this->filter_ids;

        $r = unserialize($this->filter_ids);
        return $r === false ? [] : array_map(function($v){ return (int)$v; }, $r);
    }

    /**
     * @param string $filter_ids
     * @return $this
     */
    public function setFilterIds($filter_ids)
    {
        $this->filter_ids = $filter_ids;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVideoFilterGroupId()
    {
        return $this->video_filter_group_id;
    }

    /**
     * @param mixed $video_filter_group_id
     * @return $this
     */
    public function setVideoFilterGroupId($video_filter_group_id)
    {
        $this->video_filter_group_id = $video_filter_group_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVideoFilterId()
    {
        return $this->video_filter_id;
    }

    /**
     * @param mixed $video_filter_id
     * @return $this
     */
    public function setVideoFilterId($video_filter_id)
    {
        $this->video_filter_id = $video_filter_id;
        return $this;
    }

    /**
     * @return array
     */
    public function getVideoFilterInfo()
    {
        return $this->video_filter_info;
    }

    /**
     * @param array $video_filter_info
     * @return $this
     */
    public function setVideoFilterInfo($video_filter_info)
    {
        $this->video_filter_info = $video_filter_info;
        return $this;
    }

    /**
     * @param $id
     * @param $title
     * @return $this
     */
    public function addVideoFilterInfo($id, $title)
    {
        $this->video_filter_info[$id] = $title;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isFromAdmin()
    {
        return $this->from_admin;
    }

    /**
     * @param boolean $from_admin
     * @return $this
     */
    public function setFromAdmin($from_admin)
    {
        $this->from_admin = $from_admin;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $is_deleted
     * @return $this
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }

    /**
     * @return null
     */
    public function getConferenceString()
    {
        return \common\helpers\StreetHelper::getConferenceById($this->conference_id);
    }

    public function getMinImage()
    {
        return self::generateFilePath($this->getId(), self::SIZE_TYPE_MIN, false, false).'/'.$this->getImageName();
    }

    public function getOriginalImage()
    {
        return self::generateFilePath($this->getId(), self::SIZE_TYPE_ORIGIN, false, false).'/'.$this->getImageName();
    }

    public static function getSizes()
    {
        return [self::SIZE_TYPE_MIN => self::getSizesMin()];
    }

    public static function getSizesMin()
    {
        return ['w' => self::SIZE_MIN_WIDTH, 'h' => self::SIZE_MIN_HEIGHT];
    }

    /**
     * @param int $video_id
     * @param string $type
     * @param $absolute
     * @param $checked
     * @return string
     */
    public static function generateFilePath($video_id, $type = self::SIZE_TYPE_ORIGIN, $absolute = false, $checked = true)
    {
        $path = sprintf('images/uploads/gallery/video/%d/%s', $video_id, $type);
        if($checked && !is_dir(Yii::app()->static->staticPath().'/'.$path))
            mkdir(Yii::app()->static->staticPath().'/'.$path, 0777, true);

        return $absolute ? Yii::app()->static->staticPath().'/'.$path : $path;
    }

    public static function generateImageName($name = null)
    {
        return $name === null ? uniqid() : \common\helpers\StringHelper::doDir($name);
    }

    private $imagePath = null;

    public function createAction()
    {
        $imageNameWithExt = null;
        /** @var \common\components\CImageHandler $image */
        $image = null;
        $t = Yii::app()->db->beginTransaction();
        try {
            $this
                ->setDir(\common\helpers\StringHelper::doDir($this->title))
                ->setDirMain(\common\helpers\StringHelper::doDir($this->title));

            $criteria = new CDbCriteria();
            $criteria->addCondition('dir_main = :dir_main');
            $criteria->params = [':dir_main' => $this->dir_main];
            $count = self::model()->count($criteria);
            if($count > 0)
                $this->dir .= '-'.$count;

            $this->setUserId(Yii::app()->user->id)->setStatus(self::STATUS_NEW);

            /** @var VideoTemplate $Template */
            $Template = VideoTemplate::model()->findByPk($this->getTemplateId());
            if(!$Template)
                $this->addError('template_id', Yii::t('errors', 'Видео-хостинг не определен'));
            else {
                /** @var \common\components\video\youtube\YouTubeObj $VideoObj */
                if(($VideoObj = $Template->checkLink($this->link)) !== false) {
                    $this->imagePath = $VideoObj->getImage();

                    $image = Yii::app()->ih->load($this->imagePath);
                    $imageNameWithExt = sprintf('%s.%s', self::generateImageName($this->title), $image->getFormatType());

                    $this->setHostVideoId($VideoObj->getHostVideoId())
                        ->setHostView($VideoObj->getViewCount())
                        ->setHostCreateAt($VideoObj->getPostedDate())
                        ->setHostAuthor($VideoObj->getAuthor())
                        ->setDuration($VideoObj->getDuration())
                        ->setImageName($imageNameWithExt);
                } else
                    $this->addError('link', Yii::t('errors', 'Некорректная ссылка для этого видео-хостинга'));
            }

            if(!$this->validate(null, false) || !$this->save(false)) {
                Yii::app()->ajax->addErrors($this);
                throw new \Exception();
            }

            /** @var Items $Item */
            $Item = Items::model()->findByPk($this->unique_id);
            if(!$Item) $Item = new Items();

            $Item
                ->setPrimaryKey($this->unique_id, false)
                ->setVideoId($this->getId())
                ->setFilter($this->getFilterIds(true))
                ->setAttributes($this->getAttributes(), false);

            if(!$Item->validate()) {
                Yii::app()->ajax->addErrors($Item);
                throw new \Exception(Yii::t('errors', 'Возникла ошибка при добавлении изображения. Повторите позже'));
            }

            $image
                ->save(self::generateFilePath($this->getId(), self::SIZE_TYPE_ORIGIN, true).'/'.$imageNameWithExt)
                ->reload()
                ->resize(self::SIZE_MIN_WIDTH, self::SIZE_MIN_HEIGHT, false)
                ->save(self::generateFilePath($this->getId(), self::SIZE_TYPE_MIN, true).'/'.$imageNameWithExt);

            $Item->save(false);
            $t->commit();
            return true;
        } catch (\Exception $ex) {
            $t->rollback();
            if($ex->getCode() === 0)
                Yii::app()->ajax->addErrors($ex->getMessage());

            return false;
        }
    }

    public function updateAction()
    {
        $t = Yii::app()->db->beginTransaction();
        try {
            /** @var VideoTemplate $Template */
            $Template = VideoTemplate::model()->findByPk($this->getTemplateId());
            if(!$Template)
                $this->addError('template_id', Yii::t('errors', 'Видео-хостинг не определен'));
            else {
                /** @var \common\components\video\youtube\YouTubeObj $VideoObj */
                if(($VideoObj = $Template->checkLink($this->link)) !== false)
                    $this->setHostView($VideoObj->getViewCount());
                else
                    $this->addError('link', Yii::t('errors', 'Некорректная ссылка для этого видео-хостинга'));
            }

            if(!$this->validate(null, false) || !$this->save(false)) {
                Yii::app()->ajax->addErrors($this);
                throw new \Exception();
            }

            /** @var Items $Item */
            $Item = Items::model()->findByPk($this->unique_id);
            if(!$Item) $Item = new Items();

            $Item
                ->setPrimaryKey($this->unique_id, false)
                ->setVideoId($this->getId())
                ->setFilter($this->getFilterIds(true))
                ->setAttributes($this->getAttributes(), false);

            if(!$Item->validate()) {
                Yii::app()->ajax->addErrors($Item);
                throw new \Exception(Yii::t('errors', 'Возникла ошибка при добавлении изображения. Повторите позже'));
            }

            $Item->save(false);
            $t->commit();
            return true;
        } catch (\Exception $ex) {
            $t->rollback();
            if($ex->getCode() === 0)
                Yii::app()->ajax->addErrors($ex->getMessage());

            return false;
        }
    }

    /**
     * @return bool
     */
    public function deleteAction()
    {
        $t = Yii::app()->db->beginTransaction();
        try {
            $this->setIsDeleted(true);
            if(!$this->validate(null, false) || !$this->save(false)) {
                Yii::app()->ajax->addErrors($this);
                throw new \Exception();
            }

            /** @var Items $Item */
            $Item = Items::model()->findByPk($this->unique_id);
            if(!$Item)
                $Item = new Items();
            $Item
                ->setPrimaryKey($this->unique_id, false)
                ->setVideoId($this->getId())
                ->setFilter($this->getFilterIds(true))
                ->setAttributes($this->getAttributes(), false);

            if(!$Item->validate()) {
                Yii::app()->ajax->addErrors($Item);
                throw new \Exception();
            }

            $Item->save(false);
            $t->commit();
            return true;
        } catch (\Exception $ex) {
            $t->rollback();
            if($ex->getCode() === 0)
                Yii::app()->ajax->addErrors($ex->getMessage());

            return false;
        }
    }

    public function getSourceLink()
    {
        /** @var VideoTemplate $template */
        $template = $this->getTemplate();

        return str_replace('{id}', $this->getHostVideoId(), $template->getLink());
    }

    public function getFilterString()
    {
        $string = '';
        $string .= '<ul class="video-filter-list ">';
        foreach ($this->getFilterIds(true) as $filter_id) {
            $Filter = Yii::app()->categories->getFilter($filter_id);
            if($Filter === null) continue;
            $string .= CHtml::tag('li', [], $Filter->getTitle());
        }

        $string .= '</ul>';

        return $string;
    }

    public function getGetYoutubeInfo()
    {
        $string = '';
        $string .= '<ul class="video-info-list">';
        $string .= CHtml::tag('li', [], sprintf('Продолжительность: %d сек.', $this->getDuration()));
        $string .= CHtml::tag('li', [], sprintf('Просмотров: %d', $this->getHostView()));
        $string .= CHtml::tag('li', [], sprintf('Автор: %s', $this->getHostAuthor()));
        $string .= CHtml::tag('li', [], sprintf('Дата публикации: %s', $this->getHostCreateAt('d.m.Y H:i')));

        $string .= '</ul>';

        return $string;
    }

    /**
     * @return bool
     */
    public function updateInfoAction()
    {
        $imageNameWithExt = null;
        /** @var \common\components\CImageHandler $image */
        $image = null;

        $t = Yii::app()->db->beginTransaction();
        try {
            /** @var VideoTemplate $Template */
            $Template = VideoTemplate::model()->findByPk($this->getTemplateId());
            if(!$Template)
                $this->addError('template_id', Yii::t('errors', 'Видео-хостинг не определен'));
            else {
                /** @var \common\components\video\youtube\YouTubeObj $VideoObj */
                if(($VideoObj = $Template->checkLink($this->link)) !== false) {
                    $this->imagePath = $VideoObj->getImage();

                    $image = Yii::app()->ih->load($this->imagePath);
                    $imageNameWithExt = sprintf('%s.%s', self::generateImageName($this->title), $image->getFormatType());

                    $this->setHostVideoId($VideoObj->getHostVideoId())
                        ->setHostView($VideoObj->getViewCount())
                        ->setHostCreateAt($VideoObj->getPostedDate())
                        ->setHostAuthor($VideoObj->getAuthor())
                        ->setDuration($VideoObj->getDuration())
                        ->setImageName($imageNameWithExt);
                } else
                    $this->addError('link', Yii::t('errors', 'Некорректная ссылка для этого видео-хостинга'));
            }

            if(!$this->validate(null, false) || !$this->save(false)) {
                Yii::app()->ajax->addErrors($this);
                return false;
            }

            $original = self::generateFilePath($this->getId(), self::SIZE_TYPE_ORIGIN, true).'/'.$imageNameWithExt;
            $thumb = self::generateFilePath($this->getId(), self::SIZE_TYPE_MIN, true).'/'.$imageNameWithExt;
            $image
                ->save($original)
                ->reload()
                ->resize(self::SIZE_MIN_WIDTH, static::SIZE_MIN_HEIGHT, false)
                ->save($thumb);

            $t->commit();
            return true;
        } catch (Exception $ex) {
            $t->rollback();
            if($ex->getCode() === 0)
                Yii::app()->ajax->addErrors($ex->getMessage());
        }

        return false;
    }

    public function getChannelImage($full = false)
    {
        if($this->channel_id && $this->channel) {
            $link = Yii::app()->static->imageLink($this->channel->getMinImage());
            if($full) return CHtml::image($link, $this->channel->getTitle(),
                [
                    'data-channel-link' => $this->channel->getLink(),
                    'class' => 'channel_img',
                    'data-toggle' => 'tooltip',
                    'title' => $this->channel->getTitle()
                ]
            );
            else return $link;
        } else {
            $link = Yii::app()->static->imageLink('www/img/video/adidas.jpg');
            if($full) return CHtml::image($link, '', ['class' => 'channel_img']);
            else return $link;
        }
    }
}