<?php

Yii::import('common.models._base.BaseConference');

class Conference extends BaseConference
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function behaviors()
	{
		return [
			// Password behavior strategy
			'CTimestampbehavior' => [
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_at',
				'updateAttribute' => 'update_at',
				'setUpdateOnCreate' => true
			]
		];
	}
}