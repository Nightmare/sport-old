<?php

Yii::import('common.models._base.BaseConferencePost');

class ConferencePost extends BaseConferencePost implements \common\interfaces\iComment
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function behaviors()
	{
		Yii::import('common.extensions.behaviors.password.*');
		return [
			// Password behavior strategy
			'CTimestampbehavior' => [
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_at',
				'updateAttribute' => 'update_at',
				'setUpdateOnCreate' => true
			]
		];
	}

	public function scopes()
	{
		$t = $this->getTableAlias(false, false);
		return [
			'enable' => [
				'condition' => "`{$t}`.is_deleted = 0"
			],
		];
	}

	public function getCommentField()
	{
		return 'post_id';
	}

	public function getCommentSendLink()
	{
		return Yii::app()->createUrl("/comment/post/add", [
			"post_dir" => $this->getDir(),
			'conference' => $this->getConferenceString()
		]);
	}

	public function getCommentListLink()
	{
		return Yii::app()->createUrl("/comment/post/get", [
			"post_dir" => $this->getDir(),
			'conference' => $this->getConferenceString()
		]);
	}

	public function getCommentUploadImageLink()
	{
		return Yii::app()->createUrl("/comment/post/upload", [
			"post_dir" => $this->getDir(),
			'conference' => $this->getConferenceString()
		]);
	}

	public function getCommentUploadVideoLink()
	{
		return Yii::app()->createUrl("/comment/post/video", [
			"post_dir" => $this->getDir(),
			'conference' => $this->getConferenceString()
		]);
	}

	public function getCommentPageRoute()
	{
		return "/comment/image/get";
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getConferenceId()
	{
		return $this->conference_id;
	}

	/**
	 * @param int $conference_id
	 * @return $this
	 */
	public function setConferenceId($conference_id)
	{
		$this->conference_id = $conference_id;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getCategoryId()
	{
		return $this->category_id;
	}

	/**
	 * @param int $category_id
	 * @return $this
	 */
	public function setCategoryId($category_id)
	{
		$this->category_id = $category_id;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getUserId()
	{
		return $this->user_id;
	}

	/**
	 * @param int $user_id
	 * @return $this
	 */
	public function setUserId($user_id)
	{
		$this->user_id = $user_id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDir()
	{
		return $this->dir;
	}

	/**
	 * @param string $dir
	 * @return $this
	 */
	public function setDir($dir)
	{
		$this->dir = $dir;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDirMain()
	{
		return $this->dir_main;
	}

	/**
	 * @param string $dir_main
	 * @return $this
	 */
	public function setDirMain($dir_main)
	{
		$this->dir_main = $dir_main;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return $this
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return $this
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getUpdateAt()
	{
		return $this->update_at;
	}

	/**
	 * @param int $update_at
	 * @return $this
	 */
	public function setUpdateAt($update_at)
	{
		$this->update_at = $update_at;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getCreateAt()
	{
		return $this->create_at;
	}

	/**
	 * @param int $create_at
	 * @return $this
	 */
	public function setCreateAt($create_at)
	{
		$this->create_at = $create_at;
		return $this;
	}

	/**
	 * @return ConferenceCategory
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * @param ConferenceCategory $category
	 * @return $this
	 */
	public function setCategory($category)
	{
		$this->category = $category;
		return $this;
	}

	/**
	 * @return Conference
	 */
	public function getConference()
	{
		return $this->conference;
	}

	/**
	 * @param Conference $conference
	 * @return $this
	 */
	public function setConference($conference)
	{
		$this->conference = $conference;
		return $this;
	}

	/**
	 * @return User
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @param User $user
	 * @return $this
	 */
	public function setUser($user)
	{
		$this->user = $user;
		return $this;
	}
	/**
	 * @return null
	 */
	public function getConferenceString()
	{
		return \common\helpers\StreetHelper::getConferenceById($this->conference_id);
	}

}