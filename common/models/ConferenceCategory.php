<?php

Yii::import('common.models._base.BaseConferenceCategory');

class ConferenceCategory extends BaseConferenceCategory
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function behaviors()
	{
		Yii::import('common.extensions.behaviors.password.*');
		return [
			// Password behavior strategy
			'CTimestampbehavior' => [
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_at',
				'updateAttribute' => 'update_at',
				'setUpdateOnCreate' => true
			]
		];
	}

	public function scopes()
	{
		$t = $this->getTableAlias(false, false);
		return [
			'enable' => [
				'condition' => "`{$t}`.is_deleted = 0"
			],
		];
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getConferenceId()
	{
		return $this->conference_id;
	}

	/**
	 * @param int $conference_id
	 * @return $this
	 */
	public function setConferenceId($conference_id)
	{
		$this->conference_id = $conference_id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDir()
	{
		return $this->dir;
	}

	/**
	 * @param string $dir
	 * @return $this
	 */
	public function setDir($dir)
	{
		$this->dir = $dir;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDirMain()
	{
		return $this->dir_main;
	}

	/**
	 * @param string $dir_main
	 * @return $this
	 */
	public function setDirMain($dir_main)
	{
		$this->dir_main = $dir_main;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return $this
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return $this
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getUpdateAt()
	{
		return $this->update_at;
	}

	/**
	 * @param string $update_at
	 * @return $this
	 */
	public function setUpdateAt($update_at)
	{
		$this->update_at = $update_at;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCreateAt()
	{
		return $this->create_at;
	}

	/**
	 * @param string $create_at
	 * @return $this
	 */
	public function setCreateAt($create_at)
	{
		$this->create_at = $create_at;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getIsDeleted()
	{
		return $this->is_deleted;
	}

	/**
	 * @param int $is_deleted
	 * @return $this
	 */
	public function setIsDeleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
		return $this;
	}

	/**
	 * @return Conference
	 */
	public function getConference()
	{
		return $this->conference;
	}

	/**
	 * @param Conference $conference
	 * @return $this
	 */
	public function setConference($conference)
	{
		$this->conference = $conference;
		return $this;
	}

	/**
	 * @return ConferencePost[]
	 */
	public function getConferencePosts()
	{
		return $this->conferencePosts;
	}

	/**
	 * @param ConferencePost[] $conferencePosts
	 * @return $this
	 */
	public function setConferencePosts($conferencePosts)
	{
		$this->conferencePosts = $conferencePosts;
		return $this;
	}

	/**
	 * @return null
	 */
	public function getConferenceString()
	{
		return \common\helpers\StreetHelper::getConferenceById($this->conference_id);
	}
}