<?php

Yii::import('common.models._base.BaseGymnasium');

/**
 * Class Gymnasium
 *
 * Relations
 * @property User $user
 * @property SxgeoCountry $country
 * @property SxgeoRegions $region
 * @property SxgeoCities $city
 * @property string $street
 */
class Gymnasium extends BaseGymnasium
{
    const IMAGE_ORIGIN = 'original';
    const IMAGE_THUMB = 'thumb';

    public static $sizes = [
        'main' => ['w' => 300, 'h' => 150],
        'logo' => ['w' => 96, 'h' => 96]
    ];
    /**
     * @param string $className
     * @return Gymnasium
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    private $_oldParams = [];
    public function afterFind()
    {
        $this->_oldParams = $this->getAttributes();
        parent::afterFind();
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_id' => Yii::t('app', 'Страна'),
            'region_id' => Yii::t('app', 'Регион'),
            'city_id' => Yii::t('app', 'Город'),
            'title' => Yii::t('app', 'Название'),
            'description' => Yii::t('app', 'Description'),
            'logo' => Yii::t('app', 'Logo'),
            'main_image' => Yii::t('app', 'Main Image'),
            'lat' => Yii::t('app', 'Lat'),
            'lon' => Yii::t('app', 'Lon'),
            'user_all_count' => Yii::t('app', 'User All Count'),
            'user_enable_count' => Yii::t('app', 'Участников'),
            'update_at' => Yii::t('app', 'Update At'),
            'create_at' => Yii::t('app', 'Добавлен'),
            'users' => null,
        ];
    }

    public function behaviors()
    {
        return [
            // Password behavior strategy
            'CTimestampbehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    public function relations()
    {
        return [
            'user' => [
                self::BELONGS_TO,
                'User',
                'user_id',
                'joinType' => 'inner join'
            ],
            'country' => [
                self::BELONGS_TO,
                'SxgeoCountry',
                'country_id',
                'joinType' => 'inner join'
            ],
            'region' => [
                self::BELONGS_TO,
                'SxgeoRegions',
                'region_id',
                'joinType' => 'inner join'
            ],
            'city' => [
                self::BELONGS_TO,
                'SxgeoCities',
                'city_id',
                'joinType' => 'inner join'
            ],
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * @param int $country_id
     * @return $this
     */
    public function setCountryId($country_id)
    {
        $this->country_id = $country_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getRegionId()
    {
        return $this->region_id;
    }

    /**
     * @param int $region_id
     * @return $this
     */
    public function setRegionId($region_id)
    {
        $this->region_id = $region_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCityId()
    {
        return $this->city_id;
    }

    /**
     * @param int $city_id
     * @return $this
     */
    public function setCityId($city_id)
    {
        $this->city_id = $city_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     * @return $this
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainImage()
    {
        return $this->main_image;
    }

    /**
     * @param string $main_image
     * @return $this
     */
    public function setMainImage($main_image)
    {
        $this->main_image = $main_image;
        return $this;
    }

    /**
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param string $lat
     * @return $this
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * @param string $lon
     * @return $this
     */
    public function setLon($lon)
    {
        $this->lon = $lon;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserAllCount()
    {
        return $this->user_all_count;
    }

    /**
     * @param int $user_all_count
     * @return $this
     */
    public function setUserAllCount($user_all_count)
    {
        $this->user_all_count = $user_all_count;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserEnableCount()
    {
        return $this->user_enable_count;
    }

    /**
     * @param int $user_enable_count
     * @return $this
     */
    public function setUserEnableCount($user_enable_count)
    {
        $this->user_enable_count = $user_enable_count;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param int $update_at
     * @return $this
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param int $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return User[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param User[] $users
     * @return $this
     */
    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return SxgeoCountry
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param SxgeoCountry $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return SxgeoRegions
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param SxgeoRegions $region
     * @return $this
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return SxgeoCities
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param SxgeoCities $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    public function buildPlacement()
    {
        return sprintf('%s, %s (%s)',
            $this->country->getNameRu(), $this->city->getNameRu(), $this->region->getNameRu());
    }

    public static function getTempImagePath($type, $absolute = false, $checked = true)
    {
        $path = sprintf('images/uploads/gymnasium/temp/%s', $type);
        if($checked && !is_dir(Yii::app()->static->staticPath().'/'.$path))
            mkdir(Yii::app()->static->staticPath().'/'.$path, 0777, true);

        return $absolute ? Yii::app()->static->staticPath().'/'.$path : $path;
    }

    public static function getImagePath($type, $absolute = false, $checked = true)
    {
        $path = sprintf('images/uploads/gymnasium/%s', $type);
        if($checked && !is_dir(Yii::app()->static->staticPath().'/'.$path))
            mkdir(Yii::app()->static->staticPath().'/'.$path, 0777, true);

        return $absolute ? Yii::app()->static->staticPath().'/'.$path : $path;
    }

    public static function generateImageName($name = null)
    {
        return $name === null ? uniqid() : \common\helpers\StringHelper::doDir($name);
    }

    public function getImage($image_type = 'logo', $thumb = true)
    {
        if(!$this->{$image_type})
            return null;

        $path = $thumb ? $this->getImagePath('thumb', true) : $this->getImagePath('origin', true);
        $link = $thumb ? $this->getImagePath('thumb') : $this->getImagePath('origin');
        if(!file_exists(sprintf('%s/%s', $path, $this->{$image_type})))
            return null;

        return Yii::app()->static->imageLink(sprintf('%s/%s', $link, $this->{$image_type}));
    }

    public function createAction()
    {
        $t = Yii::app()->db->beginTransaction();
        try {
            if(!$this->save()) {
                Yii::app()->ajax->addErrors($this);
                throw new Exception();
            }

            $main_origin = $this->preparePath($this->getMainImage(), self::IMAGE_ORIGIN);
            $main_thumb = $this->preparePath($this->getMainImage(), self::IMAGE_THUMB);
            $this->uploadImage($this->getMainImage(), $main_origin, $main_thumb);

            $logo_origin = $this->preparePath($this->getLogo(), self::IMAGE_ORIGIN);
            $logo_thumb = $this->preparePath($this->getLogo(), self::IMAGE_THUMB);
            $this->uploadImage($this->getLogo(), $logo_origin, $logo_thumb);

            //@unlink($main_origin);
            //@unlink($main_thumb);
            //@unlink($logo_origin);
            //@unlink($logo_thumb);

            $t->commit();
            return true;
        } catch (Exception $ex) {
            $t->rollback();
        }

        return false;
    }

    public function updateAction()
    {
        $t = Yii::app()->db->beginTransaction();
        try {
            if(!$this->save()) {
                Yii::app()->ajax->addErrors($this);
                throw new Exception();
            }

            $logo_thumb = $logo_origin = $main_thumb = $main_origin = null;
            if($this->_oldParams['main_image'] != $this->getMainImage()) {
                $main_origin = $this->preparePath($this->getMainImage(), self::IMAGE_ORIGIN);
                $main_thumb = $this->preparePath($this->getMainImage(), self::IMAGE_THUMB);
                $this->uploadImage($this->getMainImage(), $main_origin, $main_thumb);
            }

            if($this->_oldParams['logo'] != $this->getLogo()) {
                $logo_origin = $this->preparePath($this->getLogo(), self::IMAGE_ORIGIN);
                $logo_thumb = $this->preparePath($this->getLogo(), self::IMAGE_THUMB);
                $this->uploadImage($this->getLogo(), $logo_origin, $logo_thumb);
            }

            //if($main_origin) @unlink($main_origin);
            //if($main_thumb) @unlink($main_thumb);
            //if($logo_origin) @unlink($logo_origin);
            //if($logo_thumb) @unlink($logo_thumb);

            $t->commit();
            return true;
        } catch (Exception $ex) {
            $t->rollback();
        }

        return false;
    }

    private function preparePath($image_name, $image_size = self::IMAGE_ORIGIN)
    {
        return sprintf('%s/%s', \Gymnasium::getTempImagePath($image_size, true), $image_name);
    }

    private function uploadImage($image_name, $path_origin, $path_thumb)
    {
        if(!file_exists($path_origin) || !file_exists($path_thumb)) {
            Yii::app()->ajax->addErrors('Картинка не задана. '.$image_name);
            throw new Exception();
        }
        $path_origin_new = sprintf('%s/%s', \Gymnasium::getImagePath(self::IMAGE_ORIGIN, true), $image_name);
        $path_thumb_new = sprintf('%s/%s', \Gymnasium::getImagePath(self::IMAGE_THUMB, true), $image_name);

        $image = Yii::app()->ih->load($path_origin);
        if(!$image->save($path_origin_new)) {
            Yii::app()->ajax->addErrors('Не удалось сохранить изображение');
            throw new Exception();
        }

        $image = Yii::app()->ih->load($path_thumb);
        if(!$image->save($path_thumb_new)) {
            Yii::app()->ajax->addErrors('Не удалось сохранить изображение');
            throw new Exception();
        }

        return true;
    }
}