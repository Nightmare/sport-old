<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 30.09.2014
 * Time: 21:20
 */

class HistoryMessage extends BaseMongo
{
    /** @var null */
    public $user_sender_id = null;
    /** @var null */
    public $user_receiver_id = null;

    /** @var bool */
    public $sender_delete = false;
    /** @var bool */
    public $receiver_delete = false;

    /** @var bool */
    public $sender_view = true;
    /** @var bool */
    public $receiver_view = false;

    /** @var null */
    public $body = null;

    /** @var Media[]  */
    public $media = [];

    /** @var int */
    public $update_at;
    /** @var int */
    public $create_at;

    public function collectionName()
    {
        return 'history_message';
    }

    public function behaviors()
    {
        return [
            'CTimestampbehavior' => [
                'class' => 'EMongoTimestampBehaviour',
                'timestampExpression' => 'time()',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    /**
     * @param string $className
     * @return HistoryMessage
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        return parent::beforeSave();
    }

    public function init()
    {
        parent::init();
    }

    /**
     * @return null
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param null $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param int $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return MessageMedia[]
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param MessageMedia[] $media
     * @return $this
     */
    public function setMedia($media)
    {
        $this->media = $media;
        return $this;
    }

    /**
     * @param $media
     * @return $this
     */
    public function addMedia($media)
    {
        $this->media[] = $media;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isReceiverDelete()
    {
        return $this->receiver_delete;
    }

    /**
     * @param boolean $receiver_delete
     * @return $this
     */
    public function setReceiverDelete($receiver_delete)
    {
        $this->receiver_delete = $receiver_delete;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isReceiverView()
    {
        return $this->receiver_view;
    }

    /**
     * @param boolean $receiver_view
     * @return $this
     */
    public function setReceiverView($receiver_view)
    {
        $this->receiver_view = $receiver_view;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSenderDelete()
    {
        return $this->sender_delete;
    }

    /**
     * @param boolean $sender_delete
     * @return $this
     */
    public function setSenderDelete($sender_delete)
    {
        $this->sender_delete = $sender_delete;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSenderView()
    {
        return $this->sender_view;
    }

    /**
     * @param boolean $sender_view
     * @return $this
     */
    public function setSenderView($sender_view)
    {
        $this->sender_view = $sender_view;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param int $update_at
     * @return $this
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return null
     */
    public function getUserReceiverId()
    {
        return $this->user_receiver_id;
    }

    /**
     * @param null $user_receiver_id
     * @return $this
     */
    public function setUserReceiverId($user_receiver_id)
    {
        $this->user_receiver_id = $user_receiver_id;
        return $this;
    }

    /**
     * @return null
     */
    public function getUserSenderId()
    {
        return $this->user_sender_id;
    }

    /**
     * @param null $user_sender_id
     * @return $this
     */
    public function setUserSenderId($user_sender_id)
    {
        $this->user_sender_id = $user_sender_id;
        return $this;
    }

    public static function readMessages($ids)
    {
        $criteriaUpdate = new EMongoCriteria();
        $criteriaUpdate->compare('_id', $ids);
        self::model()->updateAll($criteriaUpdate, ['$set' => ['receiver_view' => true]]);
    }
} 