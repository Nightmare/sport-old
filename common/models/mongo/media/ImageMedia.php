<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 06.10.2014
 * Time: 23:56
 */

class ImageMedia extends Media
{
    /** @var string  */
    public $type = 'image';
    /** @var null  */
    public $image_name = null;

    public function rules()
    {
        return [
            ['type, image_name, unique, parent_id', 'safe'],
        ];
    }

    /**
     * @return null
     */
    public function getImageName()
    {
        return $this->image_name;
    }

    /**
     * @param null $origin
     * @return $this
     */
    public function setImageName($origin)
    {
        $this->image_name = $origin;
        return $this;
    }

    public function getOrigin()
    {
        return CommentItems::getImagePath($this->getParentId(), CommentItems::SIZE_TYPE_ORIGIN, false, false).'/'.$this->getImageName();
    }

    public function getThumb()
    {
        return CommentItems::getImagePath($this->getParentId(), CommentItems::SIZE_TYPE_MIN, false, false).'/'.$this->getImageName();
    }

    public function createAction()
    {
        try {
            /** @var \common\components\CImageHandler $image */
            $image = Yii::app()->ih->load($this->path);
            if(!$image)
                throw new Exception(Yii::t('error', 'Невозможно загрузить изображение'));

            $imageNameWithExt = sprintf('%s.%s', CommentItems::generateImageName(), $image->getFormatType());

            $image->save(CommentItems::getImagePath($this->getParentId(), CommentItems::SIZE_TYPE_ORIGIN, true).'/'.$imageNameWithExt);
            foreach (CommentItems::getSizes() as $type => $info) {
                $image
                    ->reload()
                    ->resize($info['w'], $info['h'])
                    ->save(CommentItems::getImagePath($this->getParentId(), $type, true).'/'.$imageNameWithExt);
            }
            $this->setImageName($imageNameWithExt);

        } catch (Exception $ex) {
            Yii::app()->ajax->addErrors($ex->getMessage());
            return false;
        }

        return true;
    }
} 