<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 06.10.2014
 * Time: 23:56
 */

class VideoMedia extends Media
{
    /** @var string  */
    public $type = 'video';
    /** @var string */
    public $video_link;
    /** @var string */
    public $video_id;
    /** @var string */
    public $template_id;
    /** @var string */
    public $image_name;

    public function rules()
    {
        return [
            ['type, video_link, video_id, $template_id, unique, parent_id', 'safe'],
            ['image_name', 'safe'],
        ];
    }

    /**
     * @return string
     */
    public function getVideoId()
    {
        return $this->video_id;
    }

    /**
     * @param string $video_id
     * @return $this
     */
    public function setVideoId($video_id)
    {
        $this->video_id = $video_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getVideoLink()
    {
        return $this->video_link;
    }

    /**
     * @param string $video_link
     * @return $this
     */
    public function setVideoLink($video_link)
    {
        $this->video_link = $video_link;
        return $this;
    }

    /**
     * @return string
     */
    public function getTemplateId()
    {
        return $this->template_id;
    }

    /**
     * @param string $template_id
     * @return $this
     */
    public function setTemplateId($template_id)
    {
        $this->template_id = $template_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->image_name;
    }

    /**
     * @param string $image_name
     * @return $this
     */
    public function setImageName($image_name)
    {
        $this->image_name = $image_name;
        return $this;
    }

    public function getOrigin()
    {
        return CommentItems::getImagePath($this->getParentId(), CommentItems::SIZE_TYPE_ORIGIN, false, false).'/'.$this->getImageName();
    }

    public function getThumb()
    {
        return CommentItems::getImagePath($this->getParentId(), CommentItems::SIZE_TYPE_MIN, false, false).'/'.$this->getImageName();
    }

    public function createAction()
    {
        /** @var \common\components\video\iVideo $videoInfo */
        $videoInfo = VideoTemplate::checkLink($this->getVideoLink());
        if($videoInfo === false) {
            Yii::app()->ajax->addErrors($this->getVideoLink() . ' ' . Yii::t('errors', 'Видео хостинг не поддерживается'));
            return false;
        }

        $criteria = new CDbCriteria();
        $criteria->addCondition('validate_id = :validate_id');
        $criteria->params = [':validate_id' => $videoInfo->getVideoHost()];
        /** @var VideoTemplate $Template */
        $Template = VideoTemplate::model()->find($criteria);
        if(!$Template) {
            Yii::app()->ajax->addErrors($this->getVideoLink() . ' ' . Yii::t('errors', 'Видео хостинг не поддерживается'));
            return false;
        }

        $imageOriginal = $videoInfo->getImage();
        /** @var \common\components\CImageHandler $image */
        $image = Yii::app()->ih->load($imageOriginal);
        $imageNameWithExt = sprintf('%s.%s', CommentItems::generateImageName(), $image->getFormatType());

        $image->save(CommentItems::getImagePath($this->getParentId(), CommentItems::SIZE_TYPE_ORIGIN, true).'/'.$imageNameWithExt);
        foreach (CommentItems::getSizes() as $type => $info) {
            $image
                ->reload()
                ->resize($info['w'], $info['h'])
                ->save(CommentItems::getImagePath($this->getParentId(), $type, true).'/'.$imageNameWithExt);
        }
        $this->setImageName($imageNameWithExt);


        return true;
    }
} 