<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 06.10.2014
 * Time: 23:56
 */

class Media extends EMongoModel
{
    public $path;
    /** @var null  */
    public $type = null;
    /** @var null  */
    public $unique = null;
    /** @var null  */
    public $parent_id = null;

    public function __construct($scenario = 'insert')
    {
        parent::__construct($scenario);
        $this->unique = uniqid();
    }

    /**
     * @return null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param null $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return null
     */
    public function getUnique()
    {
        return $this->unique;
    }

    /**
     * @param null $unique
     * @return $this
     */
    public function setUnique($unique)
    {
        $this->unique = $unique;
        return $this;
    }

    /**
     * @return null
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @param null $parent_id
     * @return $this
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }
} 