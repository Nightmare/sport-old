<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 30.09.2014
 * Time: 21:20
 */

class CommentItems extends BaseMongo implements \common\interfaces\iSizesComment
{
    const IMAGE_FIELD = 'image_id';
    const VIDEO_FIELD = 'video_id';

    const COMMENT_IMAGE = 'image';
    const COMMENT_VIDEO = 'video';

    const IMAGE_EXT = 'jpg';

    /** @var string */
    public $parent;

    /** @var string */
    public $main_parent;

    /** @var array */
    public $parents;

    /** @var int */
    public $video_id;

    /** @var int */
    public $image_id;

    /** @var int  */
    public $user_sender_id;

    /** @var string */
    public $body;

    /** @var Media[]  */
    public $media = [];

    /** @var string */
    public $unique;

    /** @var int */
    public $update_at;
    /** @var int */
    public $create_at;

    public function __construct($scenario = 'insert')
    {
        parent::__construct($scenario = 'insert');

        $this->unique = uniqid();
    }

    public function collectionName()
    {
        return 'comment_items';
    }

    public function behaviors()
    {
        return [
            'CTimestampbehavior' => [
                'class' => 'EMongoTimestampBehaviour',
                'timestampExpression' => 'time()',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    public function rules()
    {
        return [
            ['video_id, image_id, user_sender_id, body, media, replay, update_at, create_at', 'safe'],
            ['body', 'required'],
        ];
    }

    /**
     * @param string $className
     * @return CommentItems
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        return parent::beforeSave();
    }

    public function init()
    {
        parent::init();
    }

    public function attributeLabels() {
        return [
            'body' => Yii::t('app', 'Текст'),
        ];
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param int $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return int
     */
    public function getImageId()
    {
        return (int)$this->image_id;
    }

    /**
     * @param int $image_id
     * @return $this
     */
    public function setImageId($image_id)
    {
        $this->image_id = (int)$image_id;
        return $this;
    }

    /**
     * @return Media[]
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param Media[] $media
     * @return $this
     */
    public function setMedia($media)
    {
        $this->media = $media;
        return $this;
    }

    /**
     * @param $media
     * @return $this
     */
    public function addMedia($media)
    {
        $this->media[] = $media;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param int $update_at
     * @return $this
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return int
     */
    public function getVideoId()
    {
        return (int)$this->video_id;
    }

    /**
     * @param int $video_id
     * @return $this
     */
    public function setVideoId($video_id)
    {
        $this->video_id = (int)$video_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserSenderId()
    {
        return $this->user_sender_id;
    }

    /**
     * @param int $user_sender_id
     * @return $this
     */
    public function setUserSenderId($user_sender_id)
    {
        $this->user_sender_id = $user_sender_id;
        return $this;
    }

    /**
     * @return null
     */
    public function getUnique()
    {
        return $this->unique;
    }

    /**
     * @param null $unique
     * @return $this
     */
    public function setUnique($unique)
    {
        $this->unique = $unique;
        return $this;
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param string $parent
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return array
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * @param array $parents
     * @return $this
     */
    public function setParents($parents)
    {
        $this->parents = $parents;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainParent()
    {
        if($this->main_parent === null)
            return $this->getPrimaryKey();

        return $this->main_parent;
    }

    /**
     * @param string $main_parent
     * @return $this
     */
    public function setMainParent($main_parent)
    {
        $this->main_parent = $main_parent;
        return $this;
    }

    public static function getSizes()
    {
        return [self::SIZE_TYPE_MIN => self::getSizesMin()];
    }

    public static function getSizesMin()
    {
        return ['w' => self::SIZE_MIN_WIDTH, 'h' => self::SIZE_MIN_HEIGHT];
    }

    public function afterFind()
    {
        parent::afterFind();

        $cache_id = get_class($this).'_comment_'.md5(serialize($this)).$this->getUnique();
        $cache = Yii::app()->cache->get($cache_id);
        //if($cache !== false) {
        //    $this->setAttributes($cache, false);
        //    return;
        //}

        $media = $this->media;
        $this->media = [];
        foreach ($media as $item) {
            if(empty($item))
                continue;

            switch ($item['type']) {
                case 'video':
                    $model = new VideoMedia();
                    break;
                case 'image':
                    $model = new ImageMedia();
                    break;
                default:
                    $model = new Media();
                    break;
            }
            $model->setAttributes($item, false);
            $model->setParentId($this->getPrimaryKey());
            $this->media[] = $model;
        }
        Yii::app()->cache->add($cache_id, $this->getAttributes());

        return;
    }

    public static function getTempImagePath($absolute = false, $checked = true)
    {
        $path = 'images/uploads/comment/temp';
        if($checked && !is_dir(Yii::app()->static->staticPath().'/'.$path))
            mkdir(Yii::app()->static->staticPath().'/'.$path, 0777, true);

        return $absolute ? Yii::app()->static->staticPath().'/'.$path : $path;
    }

    /**
     * @param $comment_id
     * @param string $type
     * @param bool $absolute
     * @param bool $checked
     * @return string
     */
    public static function getImagePath($comment_id, $type = self::SIZE_TYPE_ORIGIN, $absolute = false, $checked = true)
    {
        $path = sprintf('images/uploads/comment/image/%s/%s', $comment_id, $type);
        if($checked && !is_dir(Yii::app()->static->staticPath().'/'.$path))
            mkdir(Yii::app()->static->staticPath().'/'.$path, 0777, true);

        return $absolute ? Yii::app()->static->staticPath().'/'.$path : $path;
    }

    /**
     * @param $comment_id
     * @param string $type
     * @param bool $absolute
     * @param bool $checked
     * @return string
     */
    public static function getVideoImagePath($comment_id, $type = self::SIZE_TYPE_ORIGIN, $absolute = false, $checked = true)
    {
        $path = sprintf('images/uploads/comment/video/%s/%s', $comment_id, $type);
        if($checked && !is_dir(Yii::app()->static->staticPath().'/'.$path))
            mkdir(Yii::app()->static->staticPath().'/'.$path, 0777, true);

        return $absolute ? Yii::app()->static->staticPath().'/'.$path : $path;
    }

    public static function generateImageName($name = null)
    {
        return $name === null ? uniqid() : \common\helpers\StringHelper::doDir($name);
    }
} 