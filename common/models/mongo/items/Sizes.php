<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 11.10.2014
 * Time: 2:14
 */

class Sizes extends EMongoModel
{
    /** @var string */
    public $original_image_path;
    /** @var string */
    public $original_image_link;
    /** @var string */
    public $thumb_image_path;
    /** @var string */
    public $thumb_image_link;

    /**
     * @return string
     */
    public function getOriginalImageLink()
    {
        return $this->original_image_link;
    }

    /**
     * @param string $original_image_link
     * @return $this
     */
    public function setOriginalImageLink($original_image_link)
    {
        $this->original_image_link = $original_image_link;
        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalImagePath()
    {
        return $this->original_image_path;
    }

    /**
     * @param string $original_image_path
     * @return $this
     */
    public function setOriginalImagePath($original_image_path)
    {
        $this->original_image_path = $original_image_path;
        return $this;
    }

    /**
     * @return string
     */
    public function getThumbImageLink()
    {
        return $this->thumb_image_link;
    }

    /**
     * @param string $thumb_image_link
     * @return $this
     */
    public function setThumbImageLink($thumb_image_link)
    {
        $this->thumb_image_link = $thumb_image_link;
        return $this;
    }

    /**
     * @return string
     */
    public function getThumbImagePath()
    {
        return $this->thumb_image_path;
    }

    /**
     * @param string $thumb_image_path
     * @return $this
     */
    public function setThumbImagePath($thumb_image_path)
    {
        $this->thumb_image_path = $thumb_image_path;
        return $this;
    }
}