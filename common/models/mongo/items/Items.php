<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 30.09.2014
 * Time: 21:20
 */

class Items extends BaseMongo
{
    /** @var int */
    public $user_id;
    /** @var int */
    public $image_id;
    /** @var int */
    public $video_id;
    /** @var string */
    public $host_video_id;
    /** @var int */
    public $template_id;
    /** @var int */
    public $duration;
    /** @var int */
    public $view_count;
    /** @var int */
    public $host_view;
    /** @var int */
    public $host_create_at;
    /** @var string */
    public $host_author;
    /** @var string */
    public $title;
    /** @var string */
    public $description;
    /** @var int */
    public $view_role;
    /** @var int */
    public $status;
    /** @var array */
    public $filter = [];
    /** @var int */
    public $conference_id;
    /** @var string */
    public $conference;
    /** @var DateTime */
    public $update_at;
    /** @var DateTime */
    public $create_at;

    public function collectionName()
    {
        return 'items';
    }

    public function behaviors()
    {
        return [
            'CTimestampbehavior' => [
                'class' => 'EMongoTimestampBehaviour',
                'timestampExpression' => 'time()',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    /**
     * @param string $className
     * @return Items
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        return parent::beforeSave();
    }

    public function getPrimaryKey($value = null)
    {
        if($value === null)
            $value = $this->{$this->primaryKey()};
        return (string)$value;
    }

    /**
     * @return DateTime
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param DateTime $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     * @return $this
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return string
     */
    public function getHostAuthor()
    {
        return $this->host_author;
    }

    /**
     * @param string $host_author
     * @return $this
     */
    public function setHostAuthor($host_author)
    {
        $this->host_author = $host_author;
        return $this;
    }

    /**
     * @return int
     */
    public function getHostCreateAt()
    {
        return $this->host_create_at;
    }

    /**
     * @param int $host_create_at
     * @return $this
     */
    public function setHostCreateAt($host_create_at)
    {
        $this->host_create_at = $host_create_at;
        return $this;
    }

    /**
     * @return string
     */
    public function getHostVideoId()
    {
        return $this->host_video_id;
    }

    /**
     * @param string $host_video_id
     * @return $this
     */
    public function setHostVideoId($host_video_id)
    {
        $this->host_video_id = $host_video_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getHostView()
    {
        return $this->host_view;
    }

    /**
     * @param int $host_view
     * @return $this
     */
    public function setHostView($host_view)
    {
        $this->host_view = $host_view;
        return $this;
    }

    /**
     * @return int
     */
    public function getImageId()
    {
        return $this->image_id;
    }

    /**
     * @param int $image_id
     * @return $this
     */
    public function setImageId($image_id)
    {
        $this->image_id = $image_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getTemplateId()
    {
        return $this->template_id;
    }

    /**
     * @param int $template_id
     * @return $this
     */
    public function setTemplateId($template_id)
    {
        $this->template_id = $template_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param DateTime $update_at
     * @return $this
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getVideoId()
    {
        return $this->video_id;
    }

    /**
     * @param int $video_id
     * @return $this
     */
    public function setVideoId($video_id)
    {
        $this->video_id = $video_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getViewCount()
    {
        return $this->view_count;
    }

    /**
     * @param int $view_count
     * @return $this
     */
    public function setViewCount($view_count)
    {
        $this->view_count = $view_count;
        return $this;
    }

    /**
     * @return int
     */
    public function getViewRole()
    {
        return $this->view_role;
    }

    /**
     * @param int $view_role
     * @return $this
     */
    public function setViewRole($view_role)
    {
        $this->view_role = $view_role;
        return $this;
    }

    /**
     * @return array
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @param array $filter
     * @return $this
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
        return $this;
    }

    /**
     * @return int
     */
    public function getConferenceId()
    {
        return $this->conference_id;
    }

    /**
     * @param int $conference_id
     * @return $this
     */
    public function setConferenceId($conference_id)
    {
        $this->conference_id = $conference_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getConference()
    {
        return $this->conference;
    }

    /**
     * @param string $conference
     * @return $this
     */
    public function setConference($conference)
    {
        $this->conference = $conference;
        return $this;
    }
} 