<?php

Yii::import('common.models._base.BaseGymnasiumUser');

class GymnasiumUser extends BaseGymnasiumUser
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}