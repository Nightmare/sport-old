<?php

Yii::import('common.models._base.BaseFilterGroupPage');

class FilterGroupPage extends BaseFilterGroupPage
{
    /**
     * @param string $className
     * @return FilterGroupPage
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function behaviors()
    {
        return [
            // Password behavior strategy
            'CTimestampbehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    public function attributeLabels() {
        return [
            'filter_group_id' => null,
            'page_id' => Yii::t('app', 'Страница'),
            'update_at' => Yii::t('app', 'Update At'),
            'create_at' => Yii::t('app', 'Create At'),
            'filterGroup' => null,
        ];
    }

    public function relations() {
        return [
            'filterGroup' => [
                self::BELONGS_TO,
                'FilterGroup',
                'filter_group_id',
                'joinType' => 'inner join'
            ],
        ];
    }

    /**
     * @return int
     */
    public function getFilterGroupId()
    {
        return $this->filter_group_id;
    }

    /**
     * @param int $filter_group_id
     * @return $this
     */
    public function setFilterGroupId($filter_group_id)
    {
        $this->filter_group_id = $filter_group_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getPageId()
    {
        return $this->page_id;
    }

    /**
     * @param int $page_id
     * @return $this
     */
    public function setPageId($page_id)
    {
        $this->page_id = $page_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param int $update_at
     * @return $this
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param int $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }
}