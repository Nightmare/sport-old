<?php

Yii::import('common.models._base.BaseSimulator');

class Simulator extends BaseSimulator
{
    /**
     * @param string $className
     * @return Simulator
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function behaviors()
    {
        Yii::import('common.extensions.behaviors.password.*');
        return [
            // Password behavior strategy
            'CTimestampbehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param mixed $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHImageLink()
    {
        return $this->h_image_link;
    }

    /**
     * @param mixed $h_image_link
     * @return $this
     */
    public function setHImageLink($h_image_link)
    {
        $this->h_image_link = $h_image_link;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHImagePath()
    {
        return $this->h_image_path;
    }

    /**
     * @param mixed $h_image_path
     * @return $this
     */
    public function setHImagePath($h_image_path)
    {
        $this->h_image_path = $h_image_path;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMuscle()
    {
        return $this->muscle;
    }

    /**
     * @param mixed $muscle
     * @return $this
     */
    public function setMuscle($muscle)
    {
        $this->muscle = $muscle;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (int)$id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param mixed $update_at
     * @return $this
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVImageLink()
    {
        return $this->v_image_link;
    }

    /**
     * @param mixed $v_image_link
     * @return $this
     */
    public function setVImageLink($v_image_link)
    {
        $this->v_image_link = $v_image_link;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVImagePath()
    {
        return $this->v_image_path;
    }

    /**
     * @param mixed $v_image_path
     * @return $this
     */
    public function setVImagePath($v_image_path)
    {
        $this->v_image_path = $v_image_path;
        return $this;
    }

    /**
     * @return string
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * @param string $dir
     */
    public function setDir($dir)
    {
        $this->dir = $dir;
    }

    /**
     * @return string
     */
    public function getDirMain()
    {
        return $this->dir_main;
    }

    /**
     * @param string $dir_main
     */
    public function setDirMain($dir_main)
    {
        $this->dir_main = $dir_main;
    }

    /**
     * @return string
     */
    public function getHImageSmallLink()
    {
        return $this->h_image_small_link;
    }

    /**
     * @param string $h_image_small_link
     */
    public function setHImageSmallLink($h_image_small_link)
    {
        $this->h_image_small_link = $h_image_small_link;
    }
}