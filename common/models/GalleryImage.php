<?php

Yii::import('common.models._base.BaseGalleryImage');

/**
 * Class GalleryImage
 *
 * @method $this own()
 *
 * @property UserProfile $userProfile
 * @property int conference_id
 * @property int is_deleted
 * @property boolean from_admin
 * @property string filter_ids
 */
class GalleryImage extends BaseGalleryImage implements \common\interfaces\iSizesGalleryImage, \common\interfaces\iComment
{
    const STATUS_NEW = 0;

    public $image_filter_group_id;
    public $image_filter_id;
    protected $image_filter_info = [];

    /**
     * @param string $className
     * @return GalleryImage
     */
	public static function model($className=__CLASS__)
    {
		return parent::model($className);
	}

    public function init()
    {
        if($this->isNewRecord || $this->unique_id === null)
            $this->unique_id = md5(uniqid(rand(), true));
    }

    public function scopes()
    {
        $t = $this->getTableAlias(false, false);
        return [
            'own' => [
                'condition' => "`{$t}`.user_id = :{$t}_user_id",
                'params' => [":{$t}_user_id" => Yii::app()->user->id]
            ],
            'enable' => [
                'condition' => "`{$t}`.is_deleted = :{$t}_is_deleted",
                'params' => [":{$t}_is_deleted" => 0]
            ],
            'admin' => [
                'condition' => "`{$t}`.from_admin = :{$t}_from_admin",
                'params' => [":{$t}_from_admin" => 1]
            ]
        ];
    }

    public function behaviors()
    {
        Yii::import('common.extensions.behaviors.password.*');
        return [
            // Password behavior strategy
            'CTimestampbehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    public function relations() {
        return [
            'user' => [self::BELONGS_TO, 'User', 'user_id', 'joinType' => 'inner join'],
            'userProfile' => [self::BELONGS_TO, 'UserProfile', 'user_id', 'joinType' => 'inner join'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'album_id' => Yii::t('app', 'Album'),
            'user_id' => null,
            'title' => Yii::t('app', 'Название'),
            'description' => Yii::t('app', 'Описание'),
            'view_role' => Yii::t('app', 'View Role'),
            'status' => Yii::t('app', 'Status'),
            'update_at' => Yii::t('app', 'Дата обновления'),
            'create_at' => Yii::t('app', 'Дата создания'),
            'user' => null,
        ];
    }

    public function rules() {
        return [
            ['filter_ids, user_id', 'unsafe'],
            ['user_id', 'required'],
            ['title', 'required', 'on' => 'updateAction, createAction'],
            ['album_id, user_id, view_role, status, update_at, create_at', 'numerical', 'integerOnly'=>true],
            ['title', 'length', 'max'=>255],
            ['description', 'safe'],
            ['album_id, title, description, view_role, status, update_at, filter_ids', 'default', 'setOnEmpty' => true, 'value' => null],
            ['id, album_id, user_id, title, description, view_role, status, update_at, create_at, filter_ids', 'safe', 'on'=>'search'],
        ];
    }

    public function getCommentField()
    {
        return 'image_id';
    }

    public function getCommentSendLink()
    {
        return Yii::app()->createUrl("/comment/image/add", ["image_dir" => $this->getDir(), 'conference' => $this->getConferenceString()]);
    }

    public function getCommentListLink()
    {
        return Yii::app()->createUrl("/comment/image/get", ["image_dir" => $this->getDir(), 'conference' => $this->getConferenceString()]);
    }

    public function getCommentUploadImageLink()
    {
        return Yii::app()->createUrl("/comment/image/upload", ["image_dir" => $this->getDir(), 'conference' => $this->getConferenceString()]);
    }

    public function getCommentUploadVideoLink()
    {
        return Yii::app()->createUrl("/comment/image/video", ["image_dir" => $this->getDir(), 'conference' => $this->getConferenceString()]);
    }

    public function getCommentPageRoute()
    {
        return "/comment/image/get";
    }

    /**
     * @return mixed
     */
    public function getAlbumId()
    {
        return $this->album_id;
    }

    /**
     * @param mixed $album_id
     * @return $this
     */
    public function setAlbumId($album_id)
    {
        $this->album_id = $album_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param mixed $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (int)$id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param mixed $update_at
     * @return $this
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getViewRole()
    {
        return $this->view_role;
    }

    /**
     * @param mixed $view_role
     * @return $this
     */
    public function setViewRole($view_role)
    {
        $this->view_role = $view_role;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUniqueId()
    {
        return $this->unique_id;
    }

    /**
     * @param mixed $unique_id
     * @return $this
     */
    public function setUniqueId($unique_id)
    {
        $this->unique_id = $unique_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->image_name;
    }

    /**
     * @param $image_name
     * @return $this
     */
    public function setImageName($image_name)
    {
        $this->image_name = $image_name;
        return $this;
    }

    /**
     * @return int
     */
    public function getConferenceId()
    {
        return $this->conference_id;
    }

    /**
     * @param int $conference_id
     * @return $this
     */
    public function setConferenceId($conference_id)
    {
        $this->conference_id = $conference_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * @param string $dir
     * @return $this
     */
    public function setDir($dir)
    {
        $this->dir = $dir;
        return $this;
    }

    /**
     * @return string
     */
    public function getDirMain()
    {
        return $this->dir_main;
    }

    /**
     * @param string $dir_main
     * @return $this
     */
    public function setDirMain($dir_main)
    {
        $this->dir_main = $dir_main;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isFromAdmin()
    {
        return $this->from_admin;
    }

    /**
     * @param boolean $from_admin
     * @return $this
     */
    public function setFromAdmin($from_admin)
    {
        $this->from_admin = $from_admin;
        return $this;
    }

    /**
     * @return string|array
     */
    public function getFilterIds($unserialize = false)
    {
        if($unserialize === false)
            return $this->filter_ids;

        $r = unserialize($this->filter_ids);
        return $r === false ? [] : array_map(function($v){ return (int)$v; }, $r);
    }

    /**
     * @param string $filter_ids
     * @return $this
     */
    public function setFilterIds($filter_ids)
    {
        $this->filter_ids = $filter_ids;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageFilterGroupId()
    {
        return $this->image_filter_group_id;
    }

    /**
     * @param mixed $image_filter_group_id
     * @return $this
     */
    public function setImageFilterGroupId($image_filter_group_id)
    {
        $this->image_filter_group_id = $image_filter_group_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageFilterId()
    {
        return $this->image_filter_id;
    }

    /**
     * @param mixed $image_filter_id
     * @return $this
     */
    public function setImageFilterId($image_filter_id)
    {
        $this->image_filter_id = $image_filter_id;
        return $this;
    }

    /**
     * @return array
     */
    public function getImageFilterInfo()
    {
        return $this->image_filter_info;
    }

    /**
     * @param array $image_filter_info
     * @return $this
     */
    public function setImageFilterInfo($image_filter_info)
    {
        $this->image_filter_info = $image_filter_info;
        return $this;
    }

    /**
     * @param $id
     * @param $title
     * @return $this
     */
    public function addImageFilterInfo($id, $title)
    {
        $this->image_filter_info[$id] = $title;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param int $is_deleted
     * @return $this
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }

    /**
     * @return null
     */
    public function getConferenceString()
    {
        return \common\helpers\StreetHelper::getConferenceById($this->conference_id);
    }

    public function getMinImage()
    {
        return self::generateFilePath($this->getId(), self::SIZE_TYPE_MIN, false, false).'/'.$this->getImageName();
    }

    public function getOriginalImage()
    {
        return self::generateFilePath($this->getId(), self::SIZE_TYPE_ORIGIN, false, false).'/'.$this->getImageName();
    }

    public static function getSizes()
    {
        return [self::SIZE_TYPE_MIN => self::getSizesMin()];
    }

    public static function getSizesMin()
    {
        return ['w' => self::SIZE_MIN_WIDTH, 'h' => self::SIZE_MIN_HEIGHT];
    }

    /**
     * @param int $image_id
     * @param string $type
     * @param $absolute
     * @param $checked
     * @return string
     */
    public static function generateFilePath($image_id, $type = self::SIZE_TYPE_ORIGIN, $absolute = false, $checked = true)
    {
        $path = sprintf('images/uploads/gallery/image/%d/%s', $image_id, $type);
        if($checked && !is_dir(Yii::app()->static->staticPath().'/'.$path))
            mkdir(Yii::app()->static->staticPath().'/'.$path, 0777, true);

        return $absolute ? Yii::app()->static->staticPath().'/'.$path : $path;
    }

    /**
     * @param $image_id
     * @param string $type
     * @param bool $absolute
     * @param bool $checked
     * @return string
     */
    public static function getTempFilePath($image_id, $type = self::SIZE_TYPE_ORIGIN, $absolute = false, $checked = true)
    {
        $path = sprintf('images/uploads/gallery/temp/%s', $image_id, $type);
        if($checked && !is_dir(Yii::app()->static->staticPath().'/'.$path))
            mkdir(Yii::app()->static->staticPath().'/'.$path, 0777, true);

        return $absolute ? Yii::app()->static->staticPath().'/'.$path : $path;
    }

    public static function generateImageName($name = null)
    {
        return $name === null ? uniqid() : \common\helpers\StringHelper::doDir($name);
    }

    public $original;
    public $min;

    public function createAction()
    {
        $t = Yii::app()->db->beginTransaction();
        try {
            /** @var \common\components\CImageHandler $min */
            $min = new \common\components\CImageHandler();
            $min->load($this->min);
            /** @var \common\components\CImageHandler $origin */
            $origin = new \common\components\CImageHandler();
            $origin->load($this->original);

            $imageNameWithExt = sprintf('%s.%s', self::generateImageName($this->title), $min->getFormatType());

            $this
                ->setDir(\common\helpers\StringHelper::doDir($this->title))
                ->setDirMain(\common\helpers\StringHelper::doDir($this->title));
            $criteria = new CDbCriteria();
            $criteria->addCondition('dir_main = :dir_main');
            $criteria->params = [':dir_main' => $this->dir_main];
            $count = self::model()->count($criteria);
            if($count > 0)
                $this->dir .= '-'.$count;

            $this->setUserId(Yii::app()->user->id)
                ->setImageName($imageNameWithExt)
                ->setStatus(self::STATUS_NEW);
            if(!$this->save()) {
                Yii::app()->ajax->addErrors($this);
                throw new \Exception(Yii::t('errors', 'Возникла ошибка при сохранении информации.'));
            }

            /** @var Items $Item */
            $Item = Items::model()->findByPk($this->unique_id);
            if(!$Item) $Item = new Items();
            $Item
                ->setPrimaryKey($this->unique_id, false)
                ->setImageId($this->getId())
                ->setFilter($this->getFilterIds(true))
                ->setAttributes($this->getAttributes(), false);

            if(!$Item->validate()) {
                Yii::app()->ajax->addErrors($Item);
                throw new \Exception(Yii::t('errors', 'Возникла ошибка при добавлении изображения. Повторите позже'));
            }

            $min->save(self::generateFilePath($this->getId(), self::SIZE_TYPE_MIN, true).'/'.$imageNameWithExt);
            $origin->save(self::generateFilePath($this->getId(), self::SIZE_TYPE_ORIGIN, true).'/'.$imageNameWithExt);

            $Item->save(false);
            $t->commit();
            return true;
        } catch (Exception $ex) {
            $t->rollback();
            if($ex->getCode() === 0)
                Yii::app()->ajax->addErrors($ex->getMessage());

            return false;
        }
    }

    public function updateAction()
    {
        $t = Yii::app()->db->beginTransaction();
        try {

            /** @var Items $ItemMongo */
            $ItemMongo = Items::model()->findByPk($this->unique_id);
            if(!$ItemMongo)
                $ItemMongo = new Items();

            if(!$this->save())
                throw new \Exception();

            $ItemMongo
                ->setPrimaryKey($this->unique_id, false)
                ->setImageId($this->getId())
                ->setFilter($this->getFilterIds(true))
                ->setAttributes($this->getAttributes(), false);

            if(!$ItemMongo->save()) {
                Yii::app()->ajax->addErrors($ItemMongo);
                throw new \Exception();
            }


            $t->commit();
            return true;
        } catch (\Exception $ex) {
            $t->rollback();
            //if($ex->getCode() === 0)
                Yii::app()->ajax->addErrors($ex->getMessage());

            return false;
        }
    }

    /**
     * @return bool
     */
    public function deleteAction()
    {
        $t = Yii::app()->db->beginTransaction();
        try {

            /** @var Items $ItemMongo */
            $ItemMongo = Items::model()->findByPk($this->unique_id);
            if(!$ItemMongo)
                $ItemMongo = new Items();

            $this->setIsDeleted(true);
            if(!$this->save())
                throw new \Exception();

            $ItemMongo
                ->setPrimaryKey($this->unique_id, false)
                ->setImageId($this->getId())
                ->setFilter($this->getFilterIds(true))
                ->setAttributes($this->getAttributes(), false);

            if(!$ItemMongo->save()) {
                Yii::app()->ajax->addErrors($ItemMongo);
                throw new \Exception();
            }

            $t->commit();
            return true;
        } catch (\Exception $ex) {
            $t->rollback();
            //if($ex->getCode() === 0)
            Yii::app()->ajax->addErrors($ex->getMessage());

            return false;
        }
    }

    public function getFilterString()
    {
        $string = '';
        $string .= '<ul class="image-filter-list">';
        foreach ($this->getFilterIds(true) as $filter_id) {
            $Filter = Yii::app()->categories->getFilter($filter_id);
            if($Filter === null) continue;
            $string .= CHtml::tag('li', [], $Filter->getTitle());
        }

        $string .= '</ul>';

        return $string;
    }
}