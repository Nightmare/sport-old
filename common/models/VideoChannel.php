<?php

Yii::import('common.models._base.BaseVideoChannel');

/**
 * Class VideoChannel
 *
 */
class VideoChannel extends BaseVideoChannel implements \common\interfaces\iSizesVideoChannel
{
    /**
     * @param string $className
     * @return VideoChannel
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function scopes()
    {
        $t = $this->getTableAlias(false, false);
        return [
            'enable' => [
                'condition' => "`{$t}`.is_deleted = :{$t}_is_deleted",
                'params' => [":{$t}_is_deleted" => 0]
            ]
        ];
    }

    public function behaviors()
    {
        Yii::import('common.extensions.behaviors.password.*');
        return [
            // Password behavior strategy
            'CTimestampbehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    public function rules() {
        return [
            ['title, link, image_name', 'required', 'on' => 'createAction'],
            ['user_id, update_at, create_at', 'numerical', 'integerOnly'=>true],
            ['title, link, image_name, description', 'length', 'max'=>255],
            ['user_id, title, link, update_at, create_at, description', 'default', 'setOnEmpty' => true, 'value' => null],
            ['id, user_id, title, link, update_at, create_at, description', 'safe', 'on'=>'search'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'title' => Yii::t('app', 'Название'),
            'description' => Yii::t('app', 'Описание'),
            'link' => Yii::t('app', 'Ссылка'),
            'update_at' => Yii::t('app', 'Update At'),
            'create_at' => Yii::t('app', 'Create At'),
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return $this
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->image_name;
    }

    /**
     * @param string $image_name
     * @return $this
     */
    public function setImageName($image_name)
    {
        $this->image_name = $image_name;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param int $update_at
     * @return $this
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param int $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $is_deleted
     * @return $this
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * @param string $dir
     * @return $this
     */
    public function setDir($dir)
    {
        $this->dir = $dir;
        return $this;
    }

    /**
     * @return string
     */
    public function getDirMain()
    {
        return $this->dir_main;
    }

    /**
     * @param string $dir_main
     * @return $this
     */
    public function setDirMain($dir_main)
    {
        $this->dir_main = $dir_main;
        return $this;
    }

    public function getMinImage()
    {
        return self::generateFilePath($this->getId(), self::SIZE_TYPE_MIN, false, false).'/'.$this->getImageName();
    }

    public function getOriginalImage()
    {
        return self::generateFilePath($this->getId(), self::SIZE_TYPE_ORIGIN, false, false).'/'.$this->getImageName();
    }

    public static function getSizes()
    {
        return [self::SIZE_TYPE_MIN => self::getSizesMin()];
    }

    public static function getSizesMin()
    {
        return ['w' => self::SIZE_MIN_WIDTH, 'h' => self::SIZE_MIN_HEIGHT];
    }

    /**
     * @param int $image_id
     * @param string $type
     * @param $absolute
     * @param $checked
     * @return string
     */
    public static function generateFilePath($image_id, $type = self::SIZE_TYPE_ORIGIN, $absolute = false, $checked = true)
    {
        $path = sprintf('images/uploads/gallery/image/%d/%s', $image_id, $type);
        if($checked && !is_dir(Yii::app()->static->staticPath().'/'.$path))
            mkdir(Yii::app()->static->staticPath().'/'.$path, 0777, true);

        return $absolute ? Yii::app()->static->staticPath().'/'.$path : $path;
    }

    /**
     * @param $image_id
     * @param string $type
     * @param bool $absolute
     * @param bool $checked
     * @return string
     */
    public static function getTempFilePath($image_id, $type = self::SIZE_TYPE_ORIGIN, $absolute = false, $checked = true)
    {
        $path = sprintf('images/uploads/gallery/temp/%s', $image_id, $type);
        if($checked && !is_dir(Yii::app()->static->staticPath().'/'.$path))
            mkdir(Yii::app()->static->staticPath().'/'.$path, 0777, true);

        return $absolute ? Yii::app()->static->staticPath().'/'.$path : $path;
    }

    public static function generateImageName($name = null)
    {
        return $name === null ? uniqid() : \common\helpers\StringHelper::doDir($name);
    }

    public $min;
    public $origin;

    public function createAction()
    {
        $t = Yii::app()->db->beginTransaction();
        try {
            /** @var \common\components\CImageHandler $min */
            $min = new \common\components\CImageHandler();
            $min->load($this->min);
            /** @var \common\components\CImageHandler $origin */
            $origin = new \common\components\CImageHandler();
            $origin->load($this->origin);

            $imageNameWithExt = sprintf('%s.%s', self::generateImageName($this->getTitle()), $min->getFormatType());

            $this
                ->setDir(\common\helpers\StringHelper::doDir($this->title))
                ->setDirMain(\common\helpers\StringHelper::doDir($this->title));
            $criteria = new CDbCriteria();
            $criteria->addCondition('dir_main = :dir_main');
            $criteria->params = [':dir_main' => $this->dir_main];
            $count = self::model()->count($criteria);
            if($count > 0)
                $this->dir .= '-'.$count;

            $this->setUserId(Yii::app()->user->id)
                ->setImageName($imageNameWithExt);
            if(!$this->save()) {
                Yii::app()->ajax->addErrors($this);
                throw new \Exception(Yii::t('errors', 'Возникла ошибка при сохранении информации.'));
            }

            $min->save(self::generateFilePath($this->getId(), self::SIZE_TYPE_MIN, true).'/'.$imageNameWithExt);
            $origin->save(self::generateFilePath($this->getId(), self::SIZE_TYPE_ORIGIN, true).'/'.$imageNameWithExt);

            $t->commit();
            return true;
        } catch (Exception $ex) {
            $t->rollback();
            if($ex->getCode() === 0)
                Yii::app()->ajax->addErrors($ex->getMessage());

            return false;
        }
    }

    public function updateAction()
    {
        $t = Yii::app()->db->beginTransaction();
        try {
            $imageNameWithExt   = null;
            $min                = null;
            $origin             = null;
            if($this->min) {
                /** @var \common\components\CImageHandler $min */
                $min = new \common\components\CImageHandler();
                $min->load($this->min);
                $imageNameWithExt = sprintf('%s.%s', self::generateImageName($this->getTitle()), $min->getFormatType());
            }

            if($this->origin) {
                /** @var \common\components\CImageHandler $origin */
                $origin = new \common\components\CImageHandler();
                $origin->load($this->origin);
                $imageNameWithExt = sprintf('%s.%s', self::generateImageName($this->getTitle()), $origin->getFormatType());
            }
            if($imageNameWithExt)
                $this->setImageName($imageNameWithExt);


            if(!$this->save())
                throw new \Exception();

            if($this->min)
                $min->save(self::generateFilePath($this->getId(), self::SIZE_TYPE_MIN, true).'/'.$imageNameWithExt);
            if($this->origin)
                $origin->save(self::generateFilePath($this->getId(), self::SIZE_TYPE_ORIGIN, true).'/'.$imageNameWithExt);

            $t->commit();
            return true;
        } catch (\Exception $ex) {
            $t->rollback();
            //if($ex->getCode() === 0)
            Yii::app()->ajax->addErrors($ex->getMessage());

            return false;
        }
    }

    /**
     * @return bool
     */
    public function deleteAction()
    {
        $this->setIsDeleted(true);

        return $this->save();
    }
}