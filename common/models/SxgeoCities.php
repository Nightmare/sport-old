<?php

Yii::import('common.models._base.BaseSxgeoCities');

class SxgeoCities extends BaseSxgeoCities
{
    /**
     * @param string $className
     * @return SxgeoCities
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getRegionId()
    {
        return $this->region_id;
    }

    /**
     * @param int $region_id
     * @return $this
     */
    public function setRegionId($region_id)
    {
        $this->region_id = $region_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameRu()
    {
        return $this->name_ru;
    }

    /**
     * @param string $name_ru
     * @return $this
     */
    public function setNameRu($name_ru)
    {
        $this->name_ru = $name_ru;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameEn()
    {
        return $this->name_en;
    }

    /**
     * @param string $name_en
     * @return $this
     */
    public function setNameEn($name_en)
    {
        $this->name_en = $name_en;
        return $this;
    }

    /**
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param string $lat
     * @return $this
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * @param string $lon
     * @return $this
     */
    public function setLon($lon)
    {
        $this->lon = $lon;
        return $this;
    }

    /**
     * @return string
     */
    public function getOkato()
    {
        return $this->okato;
    }

    /**
     * @param string $okato
     * @return $this
     */
    public function setOkato($okato)
    {
        $this->okato = $okato;
        return $this;
    }
}