<?php

Yii::import('common.models._base.BasePocketVideoInfo');

/**
 * Class PocketVideoInfo
 *
 * @property boolean $is_deleted
 */
class PocketVideoInfo extends BasePocketVideoInfo
{
    /**
     * @param string $className
     * @return PocketVideoInfo
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function behaviors()
    {
        Yii::import('common.extensions.behaviors.password.*');
        return [
            // Password behavior strategy
            'CTimestampbehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    public function scopes()
    {
        $t = $this->getTableAlias(false, false);
        return [
            'enable' => [
                'condition' => "`{$t}`.is_deleted = :{$t}_is_deleted",
                'params' => [":{$t}_is_deleted" => 0]
            ]
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getPocketId()
    {
        return $this->pocket_id;
    }

    /**
     * @param int $pocket_id
     * @return $this
     */
    public function setPocketId($pocket_id)
    {
        $this->pocket_id = $pocket_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getStartTime()
    {
        return $this->start_time;
    }

    /**
     * @param int $start_time
     * @param boolean $prepare
     * @return $this
     */
    public function setStartTime($start_time, $prepare = false)
    {
        if(!$prepare)
            $this->start_time = $start_time;
        else {
            $temp = explode(':', $start_time);
            $this->start_time = $temp[0] * 3600 + $temp[1] * 60 + $temp[2];
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param int $update_at
     * @return $this
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param int $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return PocketVideo
     */
    public function getPocket()
    {
        return $this->pocket;
    }

    /**
     * @param PocketVideo $pocket
     * @return $this
     */
    public function setPocket($pocket)
    {
        $this->pocket = $pocket;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function getTimeView()
    {
        $time = new \DateTime('@'.$this->start_time);
        return $time->format('H:i:s');
    }

    public function createAction()
    {
        $this->setUserId(Yii::app()->user->id);

        return $this->save();
    }

    /**
     * @return boolean
     */
    public function isIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $is_deleted
     * @return $this
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }
}