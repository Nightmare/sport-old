<?php

Yii::import('common.models._base.BaseFilterFilter');

/**
 * Class FilterFilter
 *
 */
class FilterFilter extends BaseFilterFilter
{
    public $group_id;

    /**
     * @param string $className
     * @return FilterFilter
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function relations() {
        return [
            'filterAssign' => [
                self::BELONGS_TO,
                'Filter',
                'filter_assign_id',
                //'joinType' => 'inner join'
            ],
            'filter' => [
                self::BELONGS_TO,
                'Filter',
                'filter_id',
                'joinType' => 'inner join'
            ],
        ];
    }

    public function rules() {
        return [
            ['filter_id, filter_assign_id', 'required'],
            ['filter_id, filter_assign_id', 'numerical', 'integerOnly'=>true],
            ['filter_id, filter_assign_id', 'safe', 'on'=>'search'],
            ['filter_id', 'unsafe'],
        ];
    }

    public function attributeLabels() {
        return [
            'group_id' => 'Группа',
            'filter_id' => null,
            'filter_assign_id' => 'Фильтр',
            'filterAssign' => null,
            'filter' => null,
        ];
    }


    /**
     * @return int
     */
    public function getFilterId()
    {
        return $this->filter_id;
    }

    /**
     * @param int $filter_id
     * @return $this
     */
    public function setFilterId($filter_id)
    {
        $this->filter_id = $filter_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getFilterAssignId()
    {
        return $this->filter_assign_id;
    }

    /**
     * @param int $filter_assign_id
     * @return $this
     */
    public function setFilterAssignId($filter_assign_id)
    {
        $this->filter_assign_id = $filter_assign_id;
        return $this;
    }
}