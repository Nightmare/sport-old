<?php

Yii::import('common.models._base.BaseSxgeoRegions');

class SxgeoRegions extends BaseSxgeoRegions
{
    /**
     * @param string $className
     * @return SxgeoRegions
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getIso()
    {
        return $this->iso;
    }

    /**
     * @param string $iso
     * @return $this
     */
    public function setIso($iso)
    {
        $this->iso = $iso;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameRu()
    {
        return $this->name_ru;
    }

    /**
     * @param string $name_ru
     * @return $this
     */
    public function setNameRu($name_ru)
    {
        $this->name_ru = $name_ru;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameEn()
    {
        return $this->name_en;
    }

    /**
     * @param string $name_en
     * @return $this
     */
    public function setNameEn($name_en)
    {
        $this->name_en = $name_en;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     * @return $this
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * @return string
     */
    public function getOkato()
    {
        return $this->okato;
    }

    /**
     * @param string $okato
     * @return $this
     */
    public function setOkato($okato)
    {
        $this->okato = $okato;
        return $this;
    }
}