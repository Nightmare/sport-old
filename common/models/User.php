<?php
Yii::import('common.models._base.BaseUser');
/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property integer $id
 * @property string $password
 * @property string $salt
 * @property string $email
 * @property string $username
 * @property string $login_ip
 * @property integer $login_attempts
 * @property integer $login_time
 * @property string $validation_key
 * @property string $password_strategy
 * @property boolean $requires_new_password
 * @property integer $create_at
 * @property integer $update_at
 * @property integer $click_at
 * @property integer $status
 *
 * @method bool verifyPassword
 * @method $this own
 *
 * @package YiiBoilerplate\Models
 */
class User extends BaseUser
{
    const STATUS_BLOCKED = 100;

	/** @var string Field to hold a new password when user updates it. */
	public $newPassword;

	/** @var string Password confirmation. Is used only in validation on login. */
	public $passwordConfirm;

	/**
	 * Behaviors associated with this ActiveRecord.
     *
     * We are using the APasswordBehavior because it allows neat things
     * like changing the password hashing methods without rebuilding the whole user database.
     *
     * @see https://github.com/phpnode/YiiPassword
     *
	 * @return array Configuration for the behavior classes.
	 */
	public function behaviors()
	{
		Yii::import('common.extensions.behaviors.password.*');
		return [
            // Password behavior strategy
            "APasswordBehavior" => [
                "class" => "APasswordBehavior",
                "defaultStrategyName" => "bcrypt",
                "strategies" => [
                    "bcrypt" => [
                        "class" => "ABcryptPasswordStrategy",
                        "workFactor" => 14,
                        "minLength" => 8
                    ]
                ],
            ],
            'CTimestampbehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
	}

	/**
     * Validation rules for model attributes.
     *
     * @see http://www.yiiframework.com/wiki/56/
	 * @return array
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
            ['email, username, password, passwordConfirm', 'required', 'on' => 'registration'],
            ['email', 'email'],
            ['username, email', 'unique'],
            ['passwordConfirm', 'compare', 'compareAttribute' => 'newPassword', 'on' => 'change_pass', 'message' => Yii::t('validation', "Passwords don't match")],
            ['password, newPassword', 'length', 'min' => 8],
            ['email, password, salt', 'length', 'max' => 255],
            ['requires_new_password, login_attempts', 'numerical', 'integerOnly' => true],
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            ['id, username, email', 'safe', 'on' => 'search'],
            ['login_ip, login_attempts, login_time, validation_key, create_at, update_at, status, click_at', 'unsafe'],
            //update
            ['email, password, passwordConfirm, newPassword', 'unsafe', 'on' => 'updateAction']
        ];
	}

	/**
     * Customized attribute labels (attr=>label)
     *
	 * @return array
	 */
	public function attributeLabels()
	{
		return [
            'id' => 'ID',
            'username' => Yii::t('labels', 'Логин'),
            'password' => Yii::t('labels', 'Пароль'),
            'newPassword' => Yii::t('labels', 'Новый пароль'),
            'passwordConfirm' => Yii::t('labels', 'Подтверждение пароля'),
            'email' => Yii::t('labels', 'Email'),
            'update_at' => Yii::t('app', 'Update At'),
            'create_at' => Yii::t('app', 'Дата регистрации'),
        ];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
        $PARTIAL = true;

		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('username', $this->username, $PARTIAL);
		$criteria->compare('email', $this->email, $PARTIAL);

		return new CActiveDataProvider(get_class($this), compact('criteria'));
	}

	/**
	 * Generates a new validation key (additional security for cookie)
	 */
	public function regenerateValidationKey()
	{
        $validation_key = md5(mt_rand() . mt_rand() . mt_rand());
		$this->saveAttributes(compact('validation_key'));
	}

    /**
     * @return bool
     * @throws CDbException
     */
    public function registrationUser()
    {
        $t = Yii::app()->db->beginTransaction();
        $error = false;
        try {

            if($this->passwordConfirm != $this->password)
                $this->addError('password', Yii::t('validation', "Passwords don't match."));

            if(!$this->save()) {
                Yii::app()->ajax->addErrors($this);
                $error = true;
            }

            if(!$error) {
                $UserProfile = new UserProfile();
                $UserProfile->setUserId($this->id);
                if(!$UserProfile->save()) {
                    Yii::app()->ajax->addErrors($this);
                    $error = true;
                }
            }

            if(!$error)
                $t->commit();
            else
                $t->rollback();

            return !$error;

        } catch (Exception $ex) {
            $t->rollback();
        }

        return false;
    }

    public function relations() {
        return [
            'userProfile' => [self::HAS_ONE, 'UserProfile', 'user_id', 'joinType' => 'inner join'],
        ];
    }

    /**
	 * Returns the static model of the specified AR class.
     * Mandatory method for ActiveRecord descendants.
     *
     * @param string $className
	 * @return User the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return mixed
     */
    public function getClickAt()
    {
        return $this->click_at;
    }

    /**
     * @param mixed $click_at
     * @return $this
     */
    public function setClickAt($click_at)
    {
        $this->click_at = $click_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param mixed $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLoginAttempts()
    {
        return $this->login_attempts;
    }

    /**
     * @param mixed $login_attempts
     * @return $this
     */
    public function setLoginAttempts($login_attempts)
    {
        $this->login_attempts = $login_attempts;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLoginIp()
    {
        return $this->login_ip;
    }

    /**
     * @param mixed $login_ip
     * @return $this
     */
    public function setLoginIp($login_ip)
    {
        $this->login_ip = $login_ip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLoginTime()
    {
        return $this->login_time;
    }

    /**
     * @param mixed $login_time
     * @return $this
     */
    public function setLoginTime($login_time)
    {
        $this->login_time = $login_time;
        return $this;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     * @return $this
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getPasswordConfirm()
    {
        return $this->passwordConfirm;
    }

    /**
     * @param string $passwordConfirm
     * @return $this
     */
    public function setPasswordConfirm($passwordConfirm)
    {
        $this->passwordConfirm = $passwordConfirm;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param mixed $update_at
     * @return $this
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserProfile()
    {
        return $this->userProfile;
    }

    /**
     * @param mixed $userProfile
     * @return $this
     */
    public function setUserProfile($userProfile)
    {
        $this->userProfile = $userProfile;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function getOnline($string = true)
    {
        $Online = Yii::app()->cache->get('online');
        if($Online === false)
            $Online = [];

        if($string)
            return isset($Online[$this->getId()]) && (time() - $Online[$this->getId()]['update_at'] < 300) ? 'online' : 'offline';
        else
            return isset($Online[$this->getId()]) && (time() - $Online[$this->getId()]['update_at'] < 300);
    }
}
