<?php

Yii::import('common.models._base.BaseUserMessageList');

/**
 * Class UserMessageList
 *
 * @method UserMessageList own()
 */
class UserMessageList extends BaseUserMessageList
{
    /**
     * @param string $className
     * @return UserMessageList
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function scopes()
    {
        $t = $this->getTableAlias(false, false);
        return [
            'own' => [
                'condition' => "`{$t}`.owner_user_id = :{$t}_owner_user_id",
                'params' => [":{$t}_owner_user_id" => Yii::app()->user->id]
            ]
        ];
    }

    public function relations() {
        return [
            'ownerUser' => [self::BELONGS_TO, 'User', 'owner_user_id', 'joinType' => 'inner join'],
            'dialogUser' => [self::BELONGS_TO, 'User', 'dialog_user_id', 'joinType' => 'inner join'],
        ];
    }

    /**
     * @return mixed
     */
    public function getDialogUserId()
    {
        return $this->dialog_user_id;
    }

    /**
     * @param mixed $dialog_user_id
     * @return $this
     */
    public function setDialogUserId($dialog_user_id)
    {
        $this->dialog_user_id = $dialog_user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOwnerUserId()
    {
        return $this->owner_user_id;
    }

    /**
     * @param mixed $owner_user_id
     * @return $this
     */
    public function setOwnerUserId($owner_user_id)
    {
        $this->owner_user_id = $owner_user_id;
        return $this;
    }


}