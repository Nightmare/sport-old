<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 16.09.2014
 * Time: 1:34
 */

class LoginFormFrontend extends CFormModel
{
    public $username;
    public $email;
    public $password;
    public $rememberMe;

    private $_identity;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['email', 'email'],
            ['rememberMe', 'boolean'],
            ['username, email, password', 'safe'],
            ['password', 'authenticate'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('labels', 'Логин или email'),
            'password' => Yii::t('labels', 'Пароль'),
            'rememberMe' => Yii::t('labels', 'Запомнить?'),
        ];
    }

    public function authenticate($attribute,$params)
    {
        if(!$this->hasErrors())
        {
            $this->_identity=new UserIdentity($this->username, $this->password, $this->email);
            if(!$this->_identity->authenticate())
                $this->addError('password',Yii::t('errors', 'Неправельный логин или пароль'));
        }
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {
        if($this->_identity === null)
        {
            $this->_identity = new UserIdentity($this->username, $this->password, $this->email);
            $this->_identity->authenticate();
        }
        if($this->_identity->errorCode === UserIdentity::ERROR_NONE)
        {
            $duration = $this->rememberMe ? 3600*24*30 : 0; // 30 days
            $duration = 3600*24*30;
            Yii::app()->user->login($this->_identity,$duration);
            return true;
        }
        else
            return false;
    }
} 