<?php

Yii::import('common.models._base.BasePocketImage');

class PocketImage extends BasePocketImage
{
    /**
     * @param string $className
     * @return PocketImage
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function behaviors()
    {
        Yii::import('common.extensions.behaviors.password.*');
        return [
            // Password behavior strategy
            'CTimestampbehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    public function rules() {
        return [
            ['user_id, image_id', 'required'],
            ['user_id, image_id, update_at, create_at, is_deleted', 'numerical', 'integerOnly'=>true],
            ['title', 'length', 'max'=>255],
            ['description', 'safe'],
            ['title, description, update_at, create_at, is_deleted', 'default', 'setOnEmpty' => true, 'value' => null],
            ['id, user_id, image_id, title, description, update_at, create_at, is_deleted', 'safe', 'on'=>'search'],
        ];
    }

    public function relations() {
        return [
            'user' => [self::BELONGS_TO, 'User', 'user_id', 'joinType' => 'inner join'],
            'image' => [self::BELONGS_TO, 'GalleryImage', 'image_id', 'joinType' => 'inner join'],
        ];
    }

    public function scopes()
    {
        $t = $this->getTableAlias(false, false);
        return [
            'own' => [
                'condition' => "`{$t}`.user_id = :{$t}_user_id",
                'params' => [":{$t}_user_id" => Yii::app()->user->id]
            ],
            'enable' => [
                'condition' => "`{$t}`.is_deleted = :{$t}_is_deleted",
                'params' => [":{$t}_is_deleted" => 0]
            ]
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => null,
            'image_id' => null,
            'title' => Yii::t('app', 'Название'),
            'description' => Yii::t('app', 'Описание'),
            'update_at' => Yii::t('app', 'Update At'),
            'create_at' => Yii::t('app', 'Create At'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
            'image' => null,
            'user' => null,
        ];
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param mixed $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageId()
    {
        return $this->image_id;
    }

    /**
     * @param mixed $image_id
     * @return $this
     */
    public function setImageId($image_id)
    {
        $this->image_id = $image_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param int $is_deleted
     * @return $this
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }

    public function updateAction()
    {
        $t = Yii::app()->db->beginTransaction();
        try {
            if(!$this->save())
                throw new \Exception();

            $t->commit();
            return true;
        } catch (\Exception $ex) {
            $t->rollback();
            //if($ex->getCode() === 0)
            Yii::app()->ajax->addErrors($ex->getMessage());

            return false;
        }
    }
}