<?php

Yii::import('common.models._base.BasePocketVideo');

/**
 * Class PocketVideo
 *
 * Relations
 * @property GalleryVideo $video
 *
 * Property
 * @property boolean $is_deleted
 */
class PocketVideo extends BasePocketVideo
{
    /**
     * @param string $className
     * @return PocketVideo
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function behaviors()
    {
        Yii::import('common.extensions.behaviors.password.*');
        return [
            // Password behavior strategy
            'CTimestampbehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    public function relations() {
        return [
            'video' => [self::BELONGS_TO, 'GalleryVideo', 'video_id', 'joinType' => 'inner join'],
            'user' => [self::BELONGS_TO, 'User', 'user_id', 'joinType' => 'inner join'],
            'pocketVideoInfos' => [self::HAS_MANY, 'PocketVideoInfo', 'pocket_id'],
        ];
    }

    public function scopes()
    {
        $t = $this->getTableAlias(false, false);
        return [
            'own' => [
                'condition' => "`{$t}`.user_id = :{$t}_user_id",
                'params' => [":{$t}_user_id" => Yii::app()->user->id]
            ],
            'enable' => [
                'condition' => "`{$t}`.is_deleted = :{$t}_is_deleted",
                'params' => [":{$t}_is_deleted" => 0]
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param mixed $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVideoId()
    {
        return $this->video_id;
    }

    /**
     * @param mixed $video_id
     * @return $this
     */
    public function setVideoId($video_id)
    {
        $this->video_id = $video_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $is_deleted
     * @return $this
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
        return $this;
    }
}