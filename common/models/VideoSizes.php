<?php

Yii::import('common.models._base.BaseVideoSizes');

class VideoSizes extends BaseVideoSizes
{
    /**
     * @param string $className
     * @return VideoSizes
     */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    /**
     * @return mixed
     */
    public function getOriginalImageLink()
    {
        return $this->original_image_link;
    }

    /**
     * @param mixed $original_image_link
     * @return $this
     */
    public function setOriginalImageLink($original_image_link)
    {
        $this->original_image_link = $original_image_link;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOriginalImagePath()
    {
        return $this->original_image_path;
    }

    /**
     * @param mixed $original_image_path
     * @return $this
     */
    public function setOriginalImagePath($original_image_path)
    {
        $this->original_image_path = $original_image_path;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getThumbImageLink()
    {
        return $this->thumb_image_link;
    }

    /**
     * @param mixed $thumb_image_link
     * @return $this
     */
    public function setThumbImageLink($thumb_image_link)
    {
        $this->thumb_image_link = $thumb_image_link;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getThumbImagePath()
    {
        return $this->thumb_image_path;
    }

    /**
     * @param mixed $thumb_image_path
     * @return $this
     */
    public function setThumbImagePath($thumb_image_path)
    {
        $this->thumb_image_path = $thumb_image_path;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVideoId()
    {
        return $this->video_id;
    }

    /**
     * @param mixed $video_id
     * @return $this
     */
    public function setVideoId($video_id)
    {
        $this->video_id = $video_id;
        return $this;
    }
}