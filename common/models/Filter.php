<?php

Yii::import('common.models._base.BaseFilter');

/**
 * Class Filter
 *
 * @property int group_id
 * @property int status
 * @property string dir
 * @property string dir_main
 *
 * @property FilterFilter[] $filters
 * @property FilterGroup $filterGroup
 */
class Filter extends BaseFilter implements \common\interfaces\iStatus
{
    public $filter_id;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function behaviors()
    {
        return [
            // Password behavior strategy
            'CTimestampbehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_at',
                'updateAttribute' => 'update_at',
                'setUpdateOnCreate' => true
            ]
        ];
    }

    public function afterSave()
    {
        $filter = Yii::app()->cache->get('filterList');
        if($filter !== false) {
            $filter[$this->getId()] = $this;
            Yii::app()->cache->set('filterList', $filter);
        }
    }
    public function scopes()
    {
        $t = $this->getTableAlias(false, false);
        return [
            'enable' => [
                'condition' => "`{$t}`.status != :{$t}_deleted",
                'params' => [":{$t}_deleted" => self::STATUS_DELETE]
            ]
        ];
    }

    public function relations() {
        return [
            'filters' => [self::HAS_MANY, 'FilterFilter', 'filter_id'],
            'filterGroup' => [
                self::BELONGS_TO,
                'FilterGroup',
                'group_id',
                'joinType' => 'inner join'
            ],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Название'),
            'v_image_path' => Yii::t('app', 'V Image Path'),
            'v_image_link' => Yii::t('app', 'V Image Link'),
            'h_image_path' => Yii::t('app', 'H Image Path'),
            'h_image_link' => Yii::t('app', 'H Image Link'),
            'update_at' => Yii::t('app', 'Дата обновления'),
            'create_at' => Yii::t('app', 'Дата создания'),
            'group_id' => Yii::t('app', 'Группа'),
            'filter_id' => Yii::t('app', 'Фильтр'),
            'filterGroups' => null,
        ];
    }

    public function search($group_id = null) {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('v_image_path', $this->v_image_path, true);
        $criteria->compare('v_image_link', $this->v_image_link, true);
        $criteria->compare('h_image_path', $this->h_image_path, true);
        $criteria->compare('h_image_link', $this->h_image_link, true);
        $criteria->compare('update_at', $this->update_at);
        $criteria->compare('create_at', $this->create_at);
        if($group_id)
            $criteria->compare('group_id', $group_id);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
        ]);
    }

    public function rules() {
        return [
            ['title', 'required'],
            ['update_at, create_at', 'numerical', 'integerOnly'=>true],
            ['title, v_image_path, v_image_link, h_image_path, h_image_link', 'length', 'max'=>255],
            ['v_image_path, v_image_link, h_image_path, h_image_link, update_at, create_at', 'default', 'setOnEmpty' => true, 'value' => null],
            ['id, title, v_image_path, v_image_link, h_image_path, h_image_link, update_at, create_at', 'safe', 'on'=>'search'],
            ['group_id', 'unsafe']
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getVImagePath()
    {
        return $this->v_image_path;
    }

    /**
     * @param string $v_image_path
     * @return $this
     */
    public function setVImagePath($v_image_path)
    {
        $this->v_image_path = $v_image_path;
        return $this;
    }

    /**
     * @return string
     */
    public function getVImageLink()
    {
        return $this->v_image_link;
    }

    /**
     * @param string $v_image_link
     * @return $this
     */
    public function setVImageLink($v_image_link)
    {
        $this->v_image_link = $v_image_link;
        return $this;
    }

    /**
     * @return string
     */
    public function getHImageLink()
    {
        return $this->h_image_link;
    }

    /**
     * @param string $h_image_link
     * @return $this
     */
    public function setHImageLink($h_image_link)
    {
        $this->h_image_link = $h_image_link;
        return $this;
    }

    /**
     * @return string
     */
    public function getHImagePath()
    {
        return $this->h_image_path;
    }

    /**
     * @param string $h_image_path
     * @return $this
     */
    public function setHImagePath($h_image_path)
    {
        $this->h_image_path = $h_image_path;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param int $create_at
     * @return $this
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param int $update_at
     * @return $this
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * @param int $group_id
     * @return $this
     */
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;
        return $this;
    }

    public function createAction()
    {
        $this
            ->setDir(\common\helpers\StringHelper::doDir($this->title))
            ->setDirMain(\common\helpers\StringHelper::doDir($this->title));
        $criteria = new CDbCriteria();
        $criteria->addCondition('dir_main = :dir_main');
        $criteria->params = [':dir_main' => $this->dir_main];
        $count = self::model()->count($criteria);
        if($count > 0)
            $this->dir .= '-'.$count;

        return $this->save();
    }

    public function updateAction()
    {
        $this
            ->setDir(\common\helpers\StringHelper::doDir($this->title))
            ->setDirMain(\common\helpers\StringHelper::doDir($this->title));
        $criteria = new CDbCriteria();
        $criteria->addCondition('dir_main = :dir_main');
        $criteria->params = [':dir_main' => $this->dir_main];
        $count = self::model()->count($criteria);
        if($count > 0)
            $this->dir .= '-'.$count;

        return $this->save();
    }

    public function copyAction()
    {
        return $this->save();
    }

    /**
     * @return string
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * @param string $dir
     * @return $this
     */
    public function setDir($dir)
    {
        $this->dir = $dir;
        return $this;
    }

    /**
     * @return string
     */
    public function getDirMain()
    {
        return $this->dir_main;
    }

    /**
     * @param string $dir_main
     * @return $this
     */
    public function setDirMain($dir_main)
    {
        $this->dir_main = $dir_main;
        return $this;
    }

    /**
     * @param bool $absolute
     * @return string
     */
    public static function getImagePath($absolute = false)
    {
        $path = sprintf('images/uploads/filter');
        if(!is_dir(Yii::app()->static->staticPath().'/'.$path))
            mkdir(Yii::app()->static->staticPath().'/'.$path, 0777, true);

        return $absolute ? Yii::app()->static->staticPath().'/'.$path : $path;
    }

    public static function generateImageName($name = null)
    {
        return $name === null ? uniqid() : \common\helpers\StringHelper::doDir($name);
    }

    public static function getSizesByType($type)
    {
        switch($type) {
            case 'v_image':
                return ['w' => 94, 'h' => 130];
                break;
            default:
                return ['w' => 230, 'h' => 94];
                break;
        }
    }

    public function getAssignFilterListString()
    {
        $string = '';
        $string .= CHtml::tag(
            'div', ['class' => 'center'],
            CHtml::link(
                '+',
                Yii::app()->createUrl('/filter/filter/assign', ['filter_id' => $this->getId()]),
                ['class' => 'btn btn-primary sq', 'data-type' => 'ajax']
            )
        );
        if(!$this->hasRelated('filters') || !$this->filters)
            return $string.'Фильтры отсутствуют';

        $string .= '<ul class="filter-assign-list">';
        foreach ($this->filters as $filter) {
            if(!$filter->hasRelated('filterAssign') || !$filter->filterAssign) continue;

            $icon = CHtml::tag('i', [
                'class' => 'glyphicon glyphicon-trash pointer',
                'title' => 'Удалить связь',
                'data-toggle' => 'tooltip',
                'data-type' => 'ajax',
                'data-link' => Yii::app()->createUrl('/filter/filter/deleteAssign', [
                    'filter_id' => $filter->filter_id,
                    'filter_assign_id' => $filter->filter_assign_id,
                ])
            ], null);
            $string .= CHtml::tag('li', ['style' => 'white-space: nowrap;'], $filter->filterAssign->getTitle().' '.$icon);
        }
        $string .= '</ul>';

        return $string;
    }
}