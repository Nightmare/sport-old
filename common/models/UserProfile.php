<?php

Yii::import('common.models._base.BaseUserProfile');

/**
 * Class UserProfile
 *
 * @property string logo_name
 */
class UserProfile extends BaseUserProfile implements \common\interfaces\iSizesAvatar
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function rules() {
        return [
            ['user_id, is_image_croped, birthday, sex, zodiak, panel_status, country_id, region_id, city_id', 'numerical', 'integerOnly'=>true],
            ['first_name, middle_name, last_name', 'length', 'max'=>50],
            ['image_name, logo_name', 'length', 'max'=>255],
            ['user_id, first_name, middle_name, last_name, image_name, is_image_croped, birthday, sex, zodiak, panel_status, country_id, region_id, city_id', 'default', 'setOnEmpty' => true, 'value' => null],
            ['user_id, first_name, middle_name, last_name, image_name, is_image_croped, birthday, sex, zodiak, panel_status, country_id, region_id, city_id', 'safe', 'on'=>'search'],
            ['zodiak, image_name, user_id, panel_status, country_id, region_id, city_id', 'unsafe']
        ];
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     * @return $this
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     * @return $this
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageName()
    {
        return $this->image_name;
    }

    /**
     * @param mixed $image_name
     * @return $this
     */
    public function setImageName($image_name)
    {
        $this->image_name = $image_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsImageCroped()
    {
        return $this->is_image_croped;
    }

    /**
     * @param mixed $is_image_croped
     * @return $this
     */
    public function setIsImageCroped($is_image_croped)
    {
        $this->is_image_croped = $is_image_croped;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     * @return $this
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMiddleName()
    {
        return $this->middle_name;
    }

    /**
     * @param mixed $middle_name
     * @return $this
     */
    public function setMiddleName($middle_name)
    {
        $this->middle_name = $middle_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPanelStatus()
    {
        return $this->panel_status;
    }

    /**
     * @param mixed $panel_status
     * @return $this
     */
    public function setPanelStatus($panel_status)
    {
        $this->panel_status = $panel_status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param mixed $sex
     * @return $this
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZodiak()
    {
        return $this->zodiak;
    }

    /**
     * @param mixed $zodiak
     * @return $this
     */
    public function setZodiak($zodiak)
    {
        $this->zodiak = $zodiak;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogoName()
    {
        return $this->logo_name;
    }

    /**
     * @param string $logo_link
     * @return $this
     */
    public function setLogoName($logo_link)
    {
        $this->logo_name = $logo_link;
        return $this;
    }

    public function attributeLabels() {
        return [
            'user_id' => null,
            'first_name' => Yii::t('app', 'Имя'),
            'middle_name' => Yii::t('app', 'Отчество'),
            'last_name' => Yii::t('app', 'Фамилия'),
            'image_name' => Yii::t('app', 'Image Name'),
            'is_image_croped' => Yii::t('app', 'Is Image Croped'),
            'birthday' => Yii::t('app', 'Дата рождения'),
            'sex' => Yii::t('app', 'Пол'),
            'zodiak' => Yii::t('app', 'Zodiak'),
            'panel_status' => Yii::t('app', 'Panel Status'),
            'user' => null,
        ];
    }

    public static function getSexList()
    {
        return [
            0 => Yii::t('labels', 'Женский'),
            1 => Yii::t('labels', 'Мужской'),
        ];
    }

    public static function generateName($name = null)
    {
        return $name === null ? uniqid() : \common\helpers\StringHelper::doDir($name);
    }

    public static function getSizes()
    {
        return [
            self::SIZE_TYPE_MIN     => self::getSizesMin(),
            self::SIZE_TYPE_MIDDLE  => self::getSizesMiddle(),
            self::SIZE_TYPE_MAX     => self::getSizesMax(),
        ];
    }

    public static function getSizesMin()
    {
        return ['w' => self::SIZE_MIN_WIDTH, 'h' => self::SIZE_MIN_HEIGHT];
    }

    public static function getSizesMiddle()
    {
        return ['w' => self::SIZE_MIDDLE_WIDTH, 'h' => self::SIZE_MIDDLE_HEIGHT];
    }

    public static function getSizesMax()
    {
        return ['w' => self::SIZE_MAX_WIDTH, 'h' => self::SIZE_MAX_HEIGHT];
    }

    public static function generateAvatarPath($type = self::SIZE_TYPE_ORIGIN, $absolute = false, $checked = true)
    {
        $path = sprintf('images/uploads/avatars/%s', $type);
        if($checked && !is_dir(Yii::app()->static->staticPath().'/'.$path))
            mkdir(Yii::app()->static->staticPath().'/'.$path, 0777, true);

        return $absolute ? Yii::app()->static->staticPath().'/'.$path : $path;
    }

    public function getAvatar($type = self::SIZE_TYPE_ORIGIN)
    {
        $imageLink = self::generateAvatarPath($type, false, false).'/'.$this->image_name;
        if(!$this->image_name || !file_exists(Yii::app()->static->staticPath().'/'.$imageLink))
            return Yii::app()->static->setOwn()->imageLink('img/no-avatar.png', true);

        return Yii::app()->static->imageLink($imageLink);
    }

    public static function getLogoSizes()
    {
        return [
            self::SIZE_TYPE_MIN     => self::getLogoSizesMin(),
            self::SIZE_TYPE_MIDDLE  => self::getLogoSizesMiddle(),
            self::SIZE_TYPE_MAX     => self::getLogoSizesMax(),
        ];
    }

    public static function getLogoSizesMin()
    {
        return ['w' => self::SIZE_LOGO_MIN_WIDTH, 'h' => self::SIZE_LOGO_MIN_HEIGHT];
    }

    public static function getLogoSizesMiddle()
    {
        return ['w' => self::SIZE_LOGO_MIDDLE_WIDTH, 'h' => self::SIZE_LOGO_MIDDLE_HEIGHT];
    }

    public static function getLogoSizesMax()
    {
        return ['w' => self::SIZE_LOGO_MAX_WIDTH, 'h' => self::SIZE_LOGO_MAX_HEIGHT];
    }

    /**
     * @param string $type
     * @param bool $absolute
     * @param bool $checked
     * @return string
     */
    public static function generateLogoPath($type = self::SIZE_TYPE_ORIGIN, $absolute = false, $checked = true)
    {
        $path = sprintf('images/uploads/logo/%s', $type);
        if($checked && !is_dir(Yii::app()->static->staticPath().'/'.$path))
            mkdir(Yii::app()->static->staticPath().'/'.$path, 0777, true);

        return $absolute ? Yii::app()->static->staticPath().'/'.$path : $path;
    }

    public function getLogo($type = self::SIZE_TYPE_ORIGIN)
    {
        $imageLink = self::generateLogoPath($type, false, false).'/'.$this->logo_name;
        if(!$this->image_name || !file_exists(Yii::app()->static->staticPath().'/'.$imageLink))
            return Yii::app()->static->setOwn()->imageLink('img/no-logo.png', true);

        return Yii::app()->static->imageLink($imageLink);
    }
}