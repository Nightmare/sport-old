<?php

/**
 * This is the model base class for the table "muscle".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Muscle".
 *
 * Columns in table "muscle" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property string $title
 * @property string $dir
 * @property string $dir_main
 * @property string $v_image_path
 * @property string $v_image_link
 * @property string $h_image_path
 * @property string $h_image_link
 * @property integer $update_at
 * @property integer $create_at
 * @property string $simulator
 *
 */
abstract class BaseMuscle extends BaseModel {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'muscle';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Muscle|Muscles', $n);
	}

	public static function representingColumn() {
		return 'title';
	}

	public function rules() {
		return [
			['title, dir, dir_main, create_at', 'required'],
			['update_at, create_at', 'numerical', 'integerOnly'=>true],
			['title, dir, dir_main, v_image_path, v_image_link, h_image_path, h_image_link', 'length', 'max'=>255],
			['simulator', 'safe'],
			['v_image_path, v_image_link, h_image_path, h_image_link, update_at, simulator', 'default', 'setOnEmpty' => true, 'value' => null],
			['id, title, dir, dir_main, v_image_path, v_image_link, h_image_path, h_image_link, update_at, create_at, simulator', 'safe', 'on'=>'search'],
		];
	}

	public function relations() {
		return [
		];
	}

	public function pivotModels() {
		return [
		];
	}

	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'dir' => Yii::t('app', 'Dir'),
			'dir_main' => Yii::t('app', 'Dir Main'),
			'v_image_path' => Yii::t('app', 'V Image Path'),
			'v_image_link' => Yii::t('app', 'V Image Link'),
			'h_image_path' => Yii::t('app', 'H Image Path'),
			'h_image_link' => Yii::t('app', 'H Image Link'),
			'update_at' => Yii::t('app', 'Update At'),
			'create_at' => Yii::t('app', 'Create At'),
			'simulator' => Yii::t('app', 'Simulator'),
		];
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('dir', $this->dir, true);
		$criteria->compare('dir_main', $this->dir_main, true);
		$criteria->compare('v_image_path', $this->v_image_path, true);
		$criteria->compare('v_image_link', $this->v_image_link, true);
		$criteria->compare('h_image_path', $this->h_image_path, true);
		$criteria->compare('h_image_link', $this->h_image_link, true);
		$criteria->compare('update_at', $this->update_at);
		$criteria->compare('create_at', $this->create_at);
		$criteria->compare('simulator', $this->simulator, true);

		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
		]);
	}
}