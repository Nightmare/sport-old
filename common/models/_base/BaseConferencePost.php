<?php

/**
 * This is the model base class for the table "conference_post".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ConferencePost".
 *
 * Columns in table "conference_post" available as properties of the model,
 * followed by relations of table "conference_post" available as properties of the model.
 *
 * @property integer $id
 * @property integer $conference_id
 * @property integer $category_id
 * @property integer $user_id
 * @property string $dir
 * @property string $dir_main
 * @property string $title
 * @property string $description
 * @property integer $update_at
 * @property integer $create_at
 *
 * @property ConferenceCategory $category
 * @property Conference $conference
 * @property User $user
 */
abstract class BaseConferencePost extends BaseModel {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'conference_post';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'ConferencePost|ConferencePosts', $n);
	}

	public static function representingColumn() {
		return 'dir';
	}

	public function rules() {
		return [
			['conference_id, category_id, user_id, dir, dir_main, title, description', 'required'],
			['conference_id, category_id, user_id, update_at, create_at', 'numerical', 'integerOnly'=>true],
			['dir, dir_main, title', 'length', 'max'=>255],
			['update_at, create_at', 'default', 'setOnEmpty' => true, 'value' => null],
			['id, conference_id, category_id, user_id, dir, dir_main, title, description, update_at, create_at', 'safe', 'on'=>'search'],
		];
	}

	public function relations() {
		return [
			'category' => [self::BELONGS_TO, 'ConferenceCategory', 'category_id'],
			'conference' => [self::BELONGS_TO, 'Conference', 'conference_id'],
			'user' => [self::BELONGS_TO, 'User', 'user_id'],
		];
	}

	public function pivotModels() {
		return [
		];
	}

	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'conference_id' => null,
			'category_id' => null,
			'user_id' => null,
			'dir' => Yii::t('app', 'Dir'),
			'dir_main' => Yii::t('app', 'Dir Main'),
			'title' => Yii::t('app', 'Title'),
			'description' => Yii::t('app', 'Description'),
			'update_at' => Yii::t('app', 'Update At'),
			'create_at' => Yii::t('app', 'Create At'),
			'category' => null,
			'conference' => null,
			'user' => null,
		];
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('conference_id', $this->conference_id);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('dir', $this->dir, true);
		$criteria->compare('dir_main', $this->dir_main, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('update_at', $this->update_at);
		$criteria->compare('create_at', $this->create_at);

		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
		]);
	}
}