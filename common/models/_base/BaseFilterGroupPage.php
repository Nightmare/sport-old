<?php

/**
 * This is the model base class for the table "filter_group_page".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "FilterGroupPage".
 *
 * Columns in table "filter_group_page" available as properties of the model,
 * followed by relations of table "filter_group_page" available as properties of the model.
 *
 * @property integer $filter_group_id
 * @property integer $page_id
 * @property integer $update_at
 * @property integer $create_at
 *
 * @property FilterGroup $filterGroup
 */
abstract class BaseFilterGroupPage extends BaseModel {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'filter_group_page';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'FilterGroupPage|FilterGroupPages', $n);
	}


	public function rules() {
		return [
			['filter_group_id, page_id', 'required'],
			['filter_group_id, page_id, update_at, create_at', 'numerical', 'integerOnly'=>true],
			['update_at, create_at', 'default', 'setOnEmpty' => true, 'value' => null],
			['filter_group_id, page_id, update_at, create_at', 'safe', 'on'=>'search'],
		];
	}

	public function relations() {
		return [
			'filterGroup' => [self::BELONGS_TO, 'FilterGroup', 'filter_group_id'],
		];
	}

	public function pivotModels() {
		return [
		];
	}

	public function attributeLabels() {
		return [
			'filter_group_id' => null,
			'page_id' => Yii::t('app', 'Page'),
			'update_at' => Yii::t('app', 'Update At'),
			'create_at' => Yii::t('app', 'Create At'),
			'filterGroup' => null,
		];
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('filter_group_id', $this->filter_group_id);
		$criteria->compare('page_id', $this->page_id);
		$criteria->compare('update_at', $this->update_at);
		$criteria->compare('create_at', $this->create_at);

		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
		]);
	}
}