<?php

/**
 * This is the model base class for the table "sxgeo_country".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SxgeoCountry".
 *
 * Columns in table "sxgeo_country" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property string $iso
 * @property string $continent
 * @property string $name_ru
 * @property string $name_en
 * @property string $lat
 * @property string $lon
 * @property string $timezone
 *
 */
abstract class BaseSxgeoCountry extends BaseModel {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'sxgeo_country';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'SxgeoCountry|SxgeoCountries', $n);
	}

	public static function representingColumn() {
		return 'iso';
	}

	public function rules() {
		return [
			['id', 'required'],
			['id', 'numerical', 'integerOnly'=>true],
			['iso, continent', 'length', 'max'=>2],
			['name_ru, name_en', 'length', 'max'=>128],
			['lat, lon', 'length', 'max'=>6],
			['timezone', 'length', 'max'=>30],
			['iso, continent, name_ru, name_en, lat, lon, timezone', 'default', 'setOnEmpty' => true, 'value' => null],
			['id, iso, continent, name_ru, name_en, lat, lon, timezone', 'safe', 'on'=>'search'],
		];
	}

	public function relations() {
		return [
		];
	}

	public function pivotModels() {
		return [
		];
	}

	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'iso' => Yii::t('app', 'Iso'),
			'continent' => Yii::t('app', 'Continent'),
			'name_ru' => Yii::t('app', 'Name Ru'),
			'name_en' => Yii::t('app', 'Name En'),
			'lat' => Yii::t('app', 'Lat'),
			'lon' => Yii::t('app', 'Lon'),
			'timezone' => Yii::t('app', 'Timezone'),
		];
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('iso', $this->iso, true);
		$criteria->compare('continent', $this->continent, true);
		$criteria->compare('name_ru', $this->name_ru, true);
		$criteria->compare('name_en', $this->name_en, true);
		$criteria->compare('lat', $this->lat, true);
		$criteria->compare('lon', $this->lon, true);
		$criteria->compare('timezone', $this->timezone, true);

		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
		]);
	}
}