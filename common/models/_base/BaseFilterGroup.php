<?php

/**
 * This is the model base class for the table "filter_group".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "FilterGroup".
 *
 * Columns in table "filter_group" available as properties of the model,
 * followed by relations of table "filter_group" available as properties of the model.
 *
 * @property integer $id
 * @property string $title
 * @property integer $update_at
 * @property integer $create_at
 *
 * @property Filter[] $filters
 */
abstract class BaseFilterGroup extends BaseModel {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'filter_group';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'FilterGroup|FilterGroups', $n);
	}

	public static function representingColumn() {
		return 'title';
	}

	public function rules() {
		return [
            ['title', 'required'],
			['update_at, create_at', 'numerical', 'integerOnly'=>true],
			['title', 'length', 'max'=>255],
			['update_at, create_at', 'default', 'setOnEmpty' => true, 'value' => null],
			['id, title, update_at, create_at', 'safe', 'on'=>'search'],
		];
	}

	public function relations() {
		return [
			'filters' => [self::MANY_MANY, 'Filter', 'filter_assign_group(filter_group_id, filter_id)'],
		];
	}

	public function pivotModels() {
		return [
		];
	}

	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'update_at' => Yii::t('app', 'Update At'),
			'create_at' => Yii::t('app', 'Create At'),
			'filters' => null,
		];
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('update_at', $this->update_at);
		$criteria->compare('create_at', $this->create_at);

		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
		]);
	}
}