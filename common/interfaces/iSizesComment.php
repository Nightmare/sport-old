<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 21.01.2015
 * Time: 20:44
 */

namespace common\interfaces;


interface iSizesComment
{
    const SIZE_TYPE_ORIGIN   = 'origin';

    const SIZE_MIN_WIDTH     = 200;
    const SIZE_MIN_HEIGHT    = 113;
    const SIZE_TYPE_MIN      = 'min';

    public static function getSizes();

    public static function getSizesMin();
}