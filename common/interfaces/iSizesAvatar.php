<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 21.01.2015
 * Time: 20:44
 */

namespace common\interfaces;


interface iSizesAvatar
{
    const SIZE_TYPE_ORIGIN    = 'origin';

    const SIZE_MIN_WIDTH      = 54;
    const SIZE_MIN_HEIGHT     = 54;
    const SIZE_TYPE_MIN       = 'min';

    const SIZE_MIDDLE_WIDTH   = 100;
    const SIZE_MIDDLE_HEIGHT  = 100;
    const SIZE_TYPE_MIDDLE    = 'middle';

    const SIZE_MAX_WIDTH      = 200;
    const SIZE_MAX_HEIGHT     = 200;
    const SIZE_TYPE_MAX       = 'max';

    const SIZE_LOGO_MIN_WIDTH      = 24;
    const SIZE_LOGO_MIN_HEIGHT     = 24;

    const SIZE_LOGO_MIDDLE_WIDTH   = 48;
    const SIZE_LOGO_MIDDLE_HEIGHT  = 48;

    const SIZE_LOGO_MAX_WIDTH      = 96;
    const SIZE_LOGO_MAX_HEIGHT     = 96;

    public static function getSizes();

    public static function getSizesMin();
    public static function getSizesMiddle();
    public static function getSizesMax();

    public static function getLogoSizes();

    public static function getLogoSizesMin();
    public static function getLogoSizesMiddle();
    public static function getLogoSizesMax();
}