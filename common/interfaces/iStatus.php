<?php
/**
 * Created by PhpStorm.
 * User: Nick Nikitchenko (skype: quietasice)
 * Date: 03.03.2015
 * Time: 0:59
 */

namespace common\interfaces;


interface iStatus
{
    const STATUS_WAIT       = 0;
    const STATUS_ENABLE     = 1;
    const STATUS_DISABLE    = 2;
    const STATUS_DELETE     = 3;
}