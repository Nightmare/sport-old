<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 21.01.2015
 * Time: 20:44
 */

namespace common\interfaces;


interface iComment
{
    public function getId();
    public function getDir();
    public function getCommentField();
    public function getCommentSendLink();
    public function getCommentListLink();
    public function getCommentUploadImageLink();
    public function getCommentUploadVideoLink();
    public function getCommentPageRoute();
}