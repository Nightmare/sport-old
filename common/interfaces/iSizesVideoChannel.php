<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 21.01.2015
 * Time: 20:44
 */

namespace common\interfaces;


interface iSizesVideoChannel
{
    const SIZE_TYPE_ORIGIN   = 'origin';

    const SIZE_MIN_WIDTH     = 50;
    const SIZE_MIN_HEIGHT    = 50;
    const SIZE_TYPE_MIN      = 'min';

    public static function getSizes();

    public static function getSizesMin();
}