<?php
namespace common\extensions\pagination;

/**
 * Created by PhpStorm.
 * User: me
 * Date: 02.10.2014
 * Time: 16:25
 */

use CLinkPager;
use CHtml;
class Pagination extends CLinkPager
{
    public $nextPageLabel = '>';
    public $prevPageLabel = '<';
    public $firstPageLabel = '<<';
    public $lastPageLabel = '>>';

    public $firstPageCssClass = 'pager-item';
    public $lastPageCssClass = 'pager-item';
    public $previousPageCssClass = 'pager-item';
    public $nextPageCssClass = 'pager-item next-item';
    public $internalPageCssClass = 'pager-item';

    public $selectedPageCssClass = 'active';

    public $header = '';

    public function init()
    {
        if(!isset($this->htmlOptions['class']))
            $this->htmlOptions['class']='pager-list';

        parent::init();
    }

    protected function createPageButton($label,$page,$class,$hidden,$selected)
    {
        if($hidden || $selected)
            $class.=' '.($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);

        return '<li class="'.$class.'">'.CHtml::link($label, $this->createPageUrl($page), ['data-type' => 'ajax', 'data-link' => $this->createPageUrl($page)]).'</li>';
    }
}