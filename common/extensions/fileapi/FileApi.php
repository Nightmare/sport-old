<?php
namespace common\extensions\fileapi;
/**
 * Created by PhpStorm.
 * User: me
 * Date: 16.09.2014
 * Time: 23:09
 */

use CWidget;
use Yii;
class FileApi extends CWidget
{

    /**
     * @var
     */
    public $url;

    public $preview_selector;
    public $preview_height = 200;
    public $preview_width = 200;

    public $is_crop = false;

    public $view;

    private function registerSimple()
    {
        Yii::app()->static->setLibrary('fileapi')
            ->registerScriptFile2('FileAPI/FileAPI.min.js')
            ->registerScriptFile2('FileAPI/FileAPI.exif.js')
            ->registerScriptFile2('jquery.fileapi.min.js');
    }

    private function registerCrop()
    {
        Yii::app()->static->setLibrary('fileapi')
            ->registerScriptFile2('jcrop/jquery.Jcrop.min.js')
            ->registerCssFile2('jcrop/jquery.Jcrop.min.css');
    }

    public function run()
    {
        $this->registerSimple();
        if($this->is_crop)
            $this->registerCrop();


        $this->render($this->view);
    }
}