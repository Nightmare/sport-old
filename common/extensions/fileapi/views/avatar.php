<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 17.09.2014
 * Time: 0:21
 *
 * @var $this \common\extensions\fileapi\FileApi
 */ ?>

<div id="userpic" style="background-image: url('<?= Yii::app()->user->getAvatar(); ?>');width: <?= $this->preview_width ?>px;height: <?= $this->preview_height; ?>px;">
    <div class="js-preview" style="position: absolute;">

    </div>
    <div class="buttons">
        <a href="" data-type="file" data-for-name="filedata" class="btn btn-primary sq"><?= Yii::t('labels', 'Выбрать'); ?></a>
        <input type="file" name="filedata" class="hidden-file">
    </div>
</div>
<div class="modal fade" id="avatarModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= Yii::t('labels', 'Обработка'); ?></h4>
            </div>
            <div class="modal-body">
                <div id="avatar-crop"></div>
            </div>
            <div class="modal-footer buttons">
                <button type="button" class="btn btn-default sq" data-dismiss="modal"><?= Yii::t('labels', 'Отмена') ?></button>
                <button type="button" class="btn btn-primary sq js-upload"><?= Yii::t('labels', 'Сохранить') ?></button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $(document.body).on('click', '[data-type="file"]', function(event){
            event.preventDefault();

            var $self = $(this);
            var name = $self.attr('data-for-name');

            $('[name="'+name+'"]').trigger('click');
        });
    });
    $('#userpic').fileapi({
        url: '<?= $this->url ?>',
        accept: 'image/*',
        imageSize: { minWidth: <?= $this->preview_width ?>, minHeight: <?= $this->preview_height ?> },
        elements: {
            preview: {
                el: '.js-preview',
                width: <?= $this->preview_width ?>,
                height: <?= $this->preview_height ?>
            }
        },
        onSelect: function (evt, ui){
            var file = ui.files[0];

            $('#avatarModal').modal({
                backdrop: 'static'
            }).on('shown.bs.modal', function(e){
                $('#avatarModal').on('click', '.js-upload', function (){
                    $('.js-preview').attr('data-success', true);

                    $('#avatarModal').modal('hide');
                    $('#userpic').fileapi('upload');
                });

                var width = $('.modal-body').width();
                $('#avatar-crop', '#avatarModal').cropper({
                    file: file,
                    bgColor: '#fff',
                    maxSize: [width, width],
                    minSize: [<?= $this->preview_width ?>, <?= $this->preview_height ?>],
                    allowSelect: false,
                    allowResize: true,
                    allowMove : true,
                    aspectRatio: <?= $this->preview_width/$this->preview_height ?>,
                    setSelect: [ 0, 0, <?= $this->preview_width ?>, <?= $this->preview_height ?> ],
                    keySupport: false,
                    onSelect: function (coords){
                        $('#userpic').fileapi('crop', file, coords);
                    }
                });
            }).on('hide.bs.modal', function(e){
                if($('.js-preview').attr('data-success') != 'true') {
                    $('.js-preview').html('');
                }
            });
        },
        onFileComplete: function(evt, uiEvt){
            var error = uiEvt.error;
            var result = uiEvt.result; // server response

            if(result.ok !== undefined) {
                $('.js-preview').html('');
                $('#userpic').css('background-image', 'url("'+result.link+'")');
            }

            $ajax.hideLoader();
        },
        onUpload: function(evt, uiEvt) {
            $ajax.showLoader();
        }
    });
</script>