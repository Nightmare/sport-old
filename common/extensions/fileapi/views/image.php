<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 17.09.2014
 * Time: 0:21
 *
 * @var $this \common\extensions\fileapi\FileApi
 */
use \common\components\Ajax;
?>

<div id="image_uploader">
    <div id="imagepic" style="background-image: url('<?= Yii::app()->static->imageLink('www/img/muscles/big/1.jpg'); ?>');width: <?= $this->preview_width ?>px;height: <?= $this->preview_height; ?>px;">
        <form>
            <div class="js-preview" style="position: absolute;">

            </div>
            <div class="buttons">
                <a href="" data-type="file" data-for-name="image[]" class="btn btn-primary sq"><?= Yii::t('labels', 'Загрузить изображение'); ?></a>
                <input type="file" name="image[]" class="hidden-file">
            </div>
        </form>
    </div>
    <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?= Yii::t('labels', 'Обработка'); ?></h4>
                </div>
                <div class="modal-body">
                    <div id="image-crop"></div>
                </div>
                <div class="modal-footer buttons">
                    <button type="button" class="btn btn-default sq" data-dismiss="modal"><?= Yii::t('labels', 'Отмена') ?></button>
                    <button type="button" class="btn btn-primary sq js-upload"><?= Yii::t('labels', 'Сохранить') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('#imagepic').fileapi({
            url: '<?= $this->url ?>',
            accept: 'image/*',
            imageSize: { minWidth: <?= $this->preview_width ?>, minHeight: <?= $this->preview_height ?> },
            imageTransform: {
                'thumb': { width: <?= $this->preview_width ?>, height: <?= $this->preview_height ?> }
            },
            elements: {
                preview: {
                    el: '.js-preview',
                    width: <?= $this->preview_width ?>,
                    height: <?= $this->preview_height ?>
                }
            },
            onSelect: function (evt, ui){
                var file = ui.files[0];

                $('#imageModal').modal({
                    backdrop: 'static'
                }).on('shown.bs.modal', function(e){
                    $('#imageModal').on('click', '.js-upload', function (){
                        $('.js-preview').attr('data-success', true);
                        $('#imagepic').fileapi('upload');
                    });

                    var width = $('.modal-body').width();
                    $('#image-crop', '#imageModal').cropper({
                        file: file,
                        bgColor: '#fff',
                        maxSize: [width, width],
                        minSize: [<?= $this->preview_width ?>, <?= $this->preview_height ?>],
                        allowSelect: false,
                        allowResize: true,
                        allowMove : true,
                        aspectRatio: <?= $this->preview_width/$this->preview_height ?>,
                        setSelect: [ 0, 0, <?= $this->preview_width ?>, <?= $this->preview_height ?> ],
                        keySupport: false,
                        onSelect: function (coords){
                            $('#imagepic').fileapi('crop', file, coords);
                        }
                    });
                }).on('hide.bs.modal', function(e){
                    if($('.js-preview').attr('data-success') != 'true')
                        $('.js-preview').html('');
                });
            },
            onFileComplete: function(evt, uiEvt){
                var error = uiEvt.error;
                var result = uiEvt.result; // server response

                if(result.ok !== undefined) {
                    $('.js-preview').html('');

                    var data = {
                        'min': result.min,
                        'origin': result.origin
                    };
                    $ajax.send('<?= Yii::app()->createUrl('/gallery/image/create') ?>', data);
                } else {
                    $('#imageModal').modal('hide');
                    $ajax.hideLoader();
                }
            },
            onUpload: function(evt, uiEvt) {
                $ajax.showLoader();
            }
        });
    });
</script>