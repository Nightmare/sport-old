<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 08.10.2014
 * Time: 7:54
 */

namespace common\extensions\behaviors\cors;
\Yii::import('vendor.iachilles.cors-behavior.CorsBehavior');

use CorsBehavior as BaseCorsBehavior;
class CorsBehavior extends BaseCorsBehavior
{
    protected function setAllowOriginHeader($origin)
    {
        header('Access-Control-Allow-Origin: ' . $origin);
        header('Access-Control-Allow-Credentials: true');

        if(\Yii::app()->request->getParam('request_width'))
            $_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest';
    }
} 