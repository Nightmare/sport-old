<?php
/**
 * Created by PhpStorm.
 */

namespace common\extensions\egmap;

\Yii::import('vendor.2amigos.egmap.*');

use common\components\VarDumper;
use Yii;
use EGMap;
use CClientScript;
use CGoogleApi;

class MEGMap extends EGMap
{
    protected $plugins = [];
    protected  $_appendTo;
    protected $_containerId;
    protected $is_autocomplete = false;

    public function registerMapScript($afterInit=array(), $language = null, $region = null, $position = CClientScript::POS_LOAD)
    {
        // TODO: include support in the future
        $params = 'sensor=false';

        if ($language !== null)
            $params .= '&language=' . $language;
        if ($region !== null)
            $params .= '&region=' . $region;
        if($this->is_autocomplete)
            $params .= '&libraries=places';

        CGoogleApi::init();
        CGoogleApi::register('maps', '3', array('other_params' => $params));

        $this->registerPlugins();

        $js = '';

        $init_events = array();
        if (null !== $this->_appendTo)
        {
            $init_events[] = "$('{$this->getContainer()}').appendTo('{$this->_appendTo}');" . PHP_EOL;
        }
        $init_events[] = 'var mapOptions = ' . $this->encode($this->options) . ';' . PHP_EOL;
        $init_events[] = $this->getJsName() . ' = new google.maps.Map(document.getElementById("' . $this->getContainerId() . '"), mapOptions);' . PHP_EOL;


        // add some more events
        $init_events[] = $this->getEventsJs();
        $init_events[] = $this->getMarkersJs();
        $init_events[] = $this->getDirectionsJs();
        $init_events[] = $this->getPluginsJs();
        $init_events[] = $this->getPolygonsJs();
        $init_events[] = $this->getCirclesJs();
        $init_events[] = $this->getRectanglesJs();

        if (is_array($afterInit))
        {
            foreach ($afterInit as $ainit)
                $init_events[] = $ainit;
        }
        if ($this->getGlobalVariable($this->getJsName() . '_info_window'))
            $init_events[] = $this->getJsName() . '_info_window=new google.maps.InfoWindow();';
        if ($this->getGlobalVariable($this->getJsName() . '_info_box') && $this->resources->itemAt('infobox_config'))
            $init_events[] = $this->getJsName (). '_info_box=new InfoBox('.
                $this->resources->itemAt('infobox_config').');';
        if($this->is_autocomplete)
            $init_events[] = $this->getAutocompleteJs();

        // declare the Google Map Javascript object as global
        $this->addGlobalVariable($this->getJsName(), 'null');

        $js = $this->getGlobalVariables();

        Yii::app()->getClientScript()->registerScript('EGMap_' . $this->getJsName(), $js, CClientScript::POS_HEAD);

        $js = 'function ' . $this->_containerId . '_init(){' . PHP_EOL;
        foreach ($init_events as $init_event)
        {
            if ($init_event)
            {
                $js .= $init_event . PHP_EOL;
            }
        }
        $js .= '
			  } google.maps.event.addDomListener(window, "load",' . PHP_EOL . $this->_containerId . '_init);' . PHP_EOL;

        Yii::app()->getClientScript()->registerScript($this->_containerId . time(), $js, CClientScript::POS_END);
    }

    private function registerPlugins()
    {
        $assetDir = Yii::getPathOfAlias('vendor.2amigos.egmap') . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR;
        $assetUrl = Yii::app()->assetManager->publish($assetDir);


        $cs = Yii::app()->getClientScript();

        foreach ($this->plugins as $p)
        {

            if ($p['flag'])
            {
                foreach ($p['js'] as $js)
                    $cs->registerScriptFile($assetUrl . "/" . $js, CClientScript::POS_END);
            }
        }
    }

    public function getJsName($autoGenerate=true)
    {
        if ($this->js_name !== null)
            return $this->js_name;
        else if ($autoGenerate) {
            $function = new \ReflectionClass(get_class($this));
            return $this->js_name = $function->getShortName() . self::$_counter++;
        }
    }

    public function addAutocomplete($afterChanged = [], $inputId = null)
    {
        $this->is_autocomplete = true;
        //Yii::app()->getClientScript()->registerScriptFile('http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places');

        /*if ($inputId === null)
            $autocomplete = new EGMapAutocomplete();
        else
            $autocomplete = new EGMapAutocomplete($inputId);

        $this->resources->add('autocompletes', new \CTypedList('common\extensions\egmap\EGMapAutocomplete'));
        $this->resources->itemAt('autocompletes')->add($autocomplete);*/
    }

    public function getAutocompleteJs()
    {
        $return = '';
        if (null !== $this->resources->itemAt('autocompletes'))
        {
            foreach ($this->resources->itemAt('autocompletes') as $autocomplete)
            {
                $return .= $autocomplete->toJs($this->getJsName());
                $return .= "\n      ";
            }
        }

        return $return;
    }
}