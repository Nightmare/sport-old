<?php
namespace common\extensions\egmap;

\Yii::import('vendor.2amigos.egmap.*');

use EGMapBase;
use Yii;

/**
 * Created by PhpStorm.
 */

class EGMapAutocomplete extends EGMapBase
{
    //String $url url of image
    protected $inputId;
    protected $marker_object = 'google.maps.places.Autocomplete';

    const GMAP_LIBRARY = 'http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false';

    public function __construct( $inputId = 'searchTextField',$js_name = 'autocomplete')
    {
        $this->inputId = $inputId;
        $this->setJsName($js_name);
    }

    /**
     *
     * @return inputId
     */
    public function getInputId()
    {
        return $this->inputId;
    }

    /**
     *
     * @return string js code to create the autoComplete
     */
    public function toJs($map_js_name = 'map')
    {
        $this->options['map'] = $map_js_name;
        $return = "var input = document.getElementById('".\EGMap::encode($this->inputId)."');". PHP_EOL;
        $return .= "var ac_options = {
                      types: ['address'] //this should work !
                    };";
        $return .= $this->getJsName() . ' = new ' . $this->marker_object . '(input, ac_options);' . PHP_EOL;
        $return .= $this->getJsName().".bindTo('bounds', ".$map_js_name.");" . PHP_EOL;

        $event = new \EGMapEvent('place_changed', "function() {
          var place = autocomplete.getPlace();
          if (place.geometry.viewport) {
            ".$map_js_name.".fitBounds(place.geometry.viewport);
          } else {
            ".$map_js_name.".setCenter(place.geometry.location);
            ".$map_js_name.".setZoom(17);
          }
      }",false);

        $return .= $event->getEventJs($this->getJsName()) . PHP_EOL;

        return $return;
    }

}