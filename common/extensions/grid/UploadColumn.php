<?php
namespace common\extensions\grid;
/**
 * Created by PhpStorm.
 * User: Nick Nikitchenko (skype: quietasice)
 * Date: 03.03.2015
 * Time: 16:25
 */

use Yii;
use CDataColumn;

Yii::import('zii.widgets.grid.CDataColumn');

class UploadColumn extends CDataColumn
{
    public $uploadLink;
    private $_blockId;

    protected function renderDataCellContent($row, $data)
    {
        $this->_blockId = sprintf('%s-userpic-%s', $row, $this->name);

        $value = $this->getValue($row, $data);
        $style = 'background:url("'.Yii::app()->static->imageLink($value).'")';

        $url = $this->getLink($row, $data);

        $link = \CHtml::link(
            '+',
            $url,
            [
                'data-type' => 'file2',
                'data-for-name' => sprintf('Image[%s][%d]', $this->name, $row),
                'class' => 'btn btn-primary sq',
                'data-file-block' => $this->_blockId
            ]
        );
        $file = \CHtml::fileField(sprintf('Image[%s][%d]', $this->name, $row), null, ['id' => sprintf('Image_%s_%d_', $this->name, $row), 'class' => 'hidden-file']);
        echo \CHtml::tag('div', ['class' => 'filter-block filter-'.$this->name, 'id' => $this->_blockId, 'style' => $style], $link . $file);

        $this->registerJS($url);
    }

    private function getValue($row, $data)
    {
        $value = null;
        if($this->value !== null)
            $value = $this->evaluateExpression($this->value, array('data' => $data, 'row' => $row));
        elseif($this->name !== null)
            $value = \CHtml::value($data, $this->name);

        return $value === null ? $this->grid->nullDisplay : $this->grid->getFormatter()->format($value,$this->type);
    }

    private function getLink($row, $data)
    {
        $value = null;
        if($this->uploadLink !== null)
            $value = $this->evaluateExpression($this->uploadLink, array('data' => $data, 'row' => $row));
        elseif($this->name !== null)
            $value = \CHtml::value($data, $this->name);

        return $value === null ? $this->grid->nullDisplay : $this->grid->getFormatter()->format($value,$this->type);
    }

    private function registerJS($uploadLink)
    {
        Yii::app()->static->setLibrary('fileapi')
            ->registerScriptFile2('FileAPI/FileAPI.min.js')
            ->registerScriptFile2('FileAPI/FileAPI.exif.js')
            ->registerScriptFile2('jquery.fileapi.min.js');

        $script = "$('#{$this->_blockId}').fileapi({";
        $script .= "url: '{$uploadLink}',";
        $script .= "accept: 'image/*',";
        $script .= "autoUpload: true,";
        $script .= 'onFileComplete: function(evt, uiEvt){
            var error = uiEvt.error;
            var result = uiEvt.result; // server response

            if(result.ok !== undefined) {
                $(this).css({"background": "url("+result.link+")"});
            }
            $ajax.hideLoader();
        },';
        $script .= 'onUpload: function(evt, uiEvt) {$ajax.showLoader();}';
        $script .= "});";

        //Yii::app()->clientScript->registerScript(uniqid(), $script);
    }
}