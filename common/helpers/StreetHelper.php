<?php
namespace common\helpers;
/**
 * Created by PhpStorm.
 * User: me
 * Date: 10.12.2014
 * Time: 2:36
 */

use Yii;
class StreetHelper
{
    const PAGE_SIMULATORS           = 1;
    const PAGE_STADIUM_TEORIA       = 2;
    const PAGE_RACK_STREET_WORKOUT  = 3;
    const PAGE_RACK_TURNIK          = 4;
    const PAGE_RACK_BRUSYA          = 5;

    const STREET_RACK                   = 'rack';
    const STREET_RACK_STREET_WORKOUT    = 'rack-street-workout';
    const STREET_RACK_TURNIK            = 'rack-turnik';
    const STREET_RACK_BRUSYA            = 'rack-brus';


    const STREET_SHOP                   = 'shop';
    const STREET_ARENA                  = 'arena';

    const STREET_STADIUM                = 'stadium';
    const STREET_STADIUM_TEORIA         = 'stadium-teoria';

    private static $conferenceById = [
        1 => self::STREET_STADIUM,
        2 => self::STREET_ARENA,
        3 => self::STREET_SHOP,
        4 => self::STREET_RACK,
        5 => self::STREET_RACK_STREET_WORKOUT,
        6 => self::STREET_RACK_TURNIK,
        7 => self::STREET_RACK_BRUSYA,
        8 => self::STREET_STADIUM_TEORIA,
    ];

    private static $conferenceLink = [
        self::STREET_STADIUM    => '/site/reception',
        self::STREET_ARENA      => '',
        self::STREET_SHOP       => '',
        self::STREET_RACK       => '/rack/index',
    ];

    private static $conferenceTitle = [
        self::STREET_STADIUM                => 'СпортКомплекс',
        self::STREET_ARENA                  => 'Арена',
        self::STREET_SHOP                   => 'Магазин',
        self::STREET_RACK                   => 'Турники',
    ];

    private static $treeConference = [
        self::STREET_STADIUM => [
            self::STREET_STADIUM_TEORIA         => 'Теория',
        ],
        self::STREET_RACK => [
            self::STREET_RACK_STREET_WORKOUT    => 'Стрит Воркаут',
            self::STREET_RACK_TURNIK            => 'Турники',
            self::STREET_RACK_BRUSYA            => 'Брусья',
        ]
    ];

    public static function getConferenceList()
    {
        return self::$conferenceTitle;
    }

    public static function getConferenceSubList($conferenceName)
    {
        return isset(self::$treeConference[$conferenceName]) ? self::$treeConference[$conferenceName] : [];
    }

    public static function getConferenceById($id)
    {
        return isset(self::$conferenceById[$id]) ? self::$conferenceById[$id] : null;
    }

    public static function getConferenceByName($name)
    {
        return ($id = array_search($name, self::$conferenceById)) ? $id : null;
    }

    public static function getConferenceLink($name)
    {
        return isset(self::$conferenceLink[$name]) ? Yii::app()->createUrl(self::$conferenceLink[$name], ['conference' => $name]) : 'javascript:void(0)';
    }

    public static function getConferenceTitle($name)
    {
        if(isset(self::$conferenceTitle[$name]))
            return Yii::t('street', self::$conferenceTitle[$name]);
        else {
            foreach (self::$treeConference as $id => $values) {
                if(isset($values[$name]))
                    return Yii::t('street', $values[$name]);
            }
        }

        return null;
    }

    public static function getConferenceTitleById($id)
    {
        $name = self::getConferenceById($id);
        return self::getConferenceTitle($name);
    }

    public static function pages()
    {
        return [
            self::PAGE_SIMULATORS           => 'Страница тренажеров',
            self::PAGE_STADIUM_TEORIA       => 'Теория',
            self::PAGE_RACK_STREET_WORKOUT  => 'Стрит Воркаут',
            self::PAGE_RACK_TURNIK          => 'Турники',
            self::STREET_RACK_BRUSYA        => 'Брусья',
        ];
    }

    public static function getPageTitle($id)
    {
        $pages = self::pages();

        return isset($pages[$id]) ? $pages[$id] : null;
    }

    public static function getConferenceTree()
    {
        $returned = [];
        foreach (self::$conferenceTitle as $id => $title) {
            $returned[$title] = [self::getConferenceByName($id) => $title];
            if(!isset(self::$treeConference[$id]))
                continue;

            foreach (self::$treeConference[$id] as $treeId => $treeTitle)
                $returned[$title][self::getConferenceByName($treeId)] = $treeTitle;
        }

        return $returned;
    }

    public static function getConferenceTreeId($conference_name, $with_parent = true)
    {
        $returned = [];
        if($with_parent)
            $returned[] = self::getConferenceByName($conference_name);

        if(isset(self::$treeConference[$conference_name]))
            foreach (self::$treeConference[$conference_name] as $name => $title)
                $returned[] = self::getConferenceByName($name);

        return $returned;
    }
}