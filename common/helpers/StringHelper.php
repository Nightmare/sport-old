<?php
namespace common\helpers;
/**
 * Created by JetBrains PhpStorm.
 * User: Николай
 * Date: 28.08.13
 * Time: 16:23
 * To change this template use File | Settings | File Templates.
 */

use common\components\VarDumper;
use ForceUTF8;
class StringHelper
{
    private static $encryptKey = '!@#$%^&*';

    public static function substr($string, $length = 300, $after='...')
    {
        if(mb_strlen($string)>$length)
        {
            return mb_substr($string, 0, $length).$after;
        }
        return $string;
    }

    public static function transliterate($string)
    {
        $table = ['А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO',
            'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'N' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'CSH', 'Ь' => '', 'Ы' => 'Y', 'Ъ' => '', 'Э' => 'E','Ю' => 'YU', 'Я' => 'YA',

            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
            'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'csh', 'ь' => '', 'ы' => 'y', 'ъ' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya'];

        $output = str_replace(array_keys($table), array_values($table), $string);

        return $output;
    }

    public static function doDir($string)
    {
        $string = trim(preg_replace('/[^a-zа-я0-9]/ui', '-', $string), ' -');
        $string = str_replace('--', '-', $string);

        return self::transliterate(mb_strtolower($string));
    }

    public static function encrypt($string)
    {
        $r = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( self::$encryptKey . $string ), $string, MCRYPT_MODE_CBC, md5( md5( $salt . $pass . $pepper ))));
        var_dump($r);die;
        return $r;
    }

    public static function decrypt($string)
    {
        return mcrypt_decrypt(MCRYPT_RIJNDAEL_128, self::$encryptKey, $string, MCRYPT_MODE_ECB);
    }
}