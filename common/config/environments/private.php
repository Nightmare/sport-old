<?php
/**
 * Overrides for all entry points on local development workstations.
 * Note that this is NOT your personal overrides like the passwords.
 * Such changes should end in `/common/overrides/local.php`
 */
return [
    'import' => [
        'common.extensions.gii.giix-components.*', // giix components
    ],
    'modules' => [
        'gii' => [
            'class' => 'system.gii.GiiModule',
            'password' => '123',
            'ipFilters' => array('*'),
            'generatorPaths'=>array(
                'common.extensions.gii.giix-core', // giix generators
                'vendor.clevertech.yii-booster.src.gii',
            ),
        ]
    ],
    'components' => [
        'clientScript' => [
            'mergeCss' => false, //def:true
            'compressMergedCss' => false, //def:false
            'mergeJs' => false,
            'compressMergedJs' => false,
        ],
        'db' => [
            'connectionString' => 'mysql:host=192.168.133.129;dbname=sport',
            'username' => 'root',
            'password' => 'root',
        ],
        'user' => [
            'loginUrl' => 'http://sport.local/login',
            'identityCookie' => [
                'path' => '/',
                'domain' => '.sport.local',
            ]
        ],
        'session' => [
            'class' => 'redis_package.ARedisSession',
            'cookieMode' => 'allow',
            'cookieParams' => [
                'path' => '/',
                'domain' => '.sport.local'
            ],
        ],
        'mongodb' => [
            'class' => 'EMongoClient',
            'server' => 'mongodb://192.168.133.129:27017',
            'db' => 'sport'
        ],
        'redis' => [
            "class" => "redis_package.ARedisConnection",
            "database" => 1,
            "prefix" => "Yii.redis.",
            "hostname" => "192.168.133.129",
            "port" => 6379,
        ],
    ],
    'params' => [
        'static_domain' => 'http://static.sport.loc'
    ]
];