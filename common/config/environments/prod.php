<?php
/**
 * Overrides for all entry points on local development workstations.
 * Note that this is NOT your personal overrides like the passwords.
 * Such changes should end in `/common/overrides/local.php`
 */
return [
    'id' => 'sportime.ru',
    'import' => [
        'common.extensions.gii.giix-components.*', // giix components
    ],
    'modules' => [

    ],
    'components' => [
        'clientScript' => [
            'mergeCss' => false, //def:true
            'compressMergedCss' => false, //def:false
            'mergeJs' => false,
            'compressMergedJs' => false,
        ],
        'db' => [
            'connectionString' => 'mysql:host=10.10.10.8;dbname=sport',
            'username' => 'sport',
            'password' => '8yPaglgFz1yNpWFuuPps',
            'enableProfiling' => true,
        ],
        'redis' => [
            "class" => "redis_package.ARedisConnection",
            "database" => 1,
            "prefix" => "Yii.redis.",
            "hostname" => "10.10.10.9",
            "port" => 6379,
        ],
        'mongodb' => [
            'class' => 'EMongoClient',
            'server' => 'mongodb://10.10.10.4:27017',
            'db' => 'sport'
        ],
        'ih' => [
            'engine' => 'GD',
            'engineIMConvert' => '/usr/bin/convert',
            'engineIMComposite' => '/usr/bin/composite',
            'checkIM' => false,
        ],
        "cache" => [
            "class" => "redis_package.ARedisCache"
        ],
        'user' => [
            'loginUrl' => 'http://sportime.ru/login',
            'identityCookie' => [
                'path' => '/',
                'domain' => '.sportime.ru',
            ]
        ],
        'session' => [
            'class' => 'redis_package.ARedisSession',
            'cookieMode' => 'allow',
            'cookieParams' => [
                'path' => '/',
                'domain' => '.sportime.ru',
            ],
        ],
        'urlManager' => [
            'rules' => [

            ]
        ],
        'log' => [
            'class' => 'CLogRouter',
            'routes' => [
                'logFile' => [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'filter' => 'CLogFilter'
                ],
                [
                    'class' => 'EMongoLogRoute',
                    'connectionId' => 'mongodb', // optional, defaults to 'mongodb'
                    'logCollectionName' => 'YiiLog', // optional, defaults to 'YiiLog'
                ]
            ]
        ],
    ],
    'params' => [
        'static_domain' => 'http://static.sportime.ru'
    ]
];
