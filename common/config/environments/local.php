<?php
/**
 * Overrides for all entry points on local development workstations.
 * Note that this is NOT your personal overrides like the passwords.
 * Such changes should end in `/common/overrides/local.php`
 */
return [
    'id' => 'sport.loc',
    'import' => [
        'common.extensions.gii.giix-components.*', // giix components
    ],
    'modules' => [
        'gii' => [
            'class' => 'system.gii.GiiModule',
            'password' => false,
            'ipFilters' => ['*'],
            'generatorPaths' => [
                'common.extensions.gii.giix-core', // giix generators
                'vendor.clevertech.yii-booster.src.gii',
            ],
        ]
    ],
    'components' => [
        'clientScript' => [
//            'mergeCss' => false, //def:true
//            'compressMergedCss' => false, //def:false
//            'mergeJs' => false,
//            'compressMergedJs' => false,
        ],
        'db' => [
            'connectionString' => 'mysql:host=192.168.133.129;dbname=sport',
            'username' => 'root',
            'password' => 'root',
            'enableParamLogging' => true,
            'enableProfiling' => true,
        ],
        'mongodb' => [
            'class' => 'EMongoClient',
            'server' => 'mongodb://192.168.133.129:27017',
            'db' => 'sport'
        ],
        'ih' => [
            'engine' => 'GD',
            'engineIMConvert' => '/usr/bin/convert',
            'engineIMComposite' => '/usr/bin/composite',
            'checkIM' => true,
        ],
        "cache" => [
            //"class" => "redis_package.ARedisCache"
            "class" => "CDummyCache"
        ],
        'redis' => [
            "class" => "redis_package.ARedisConnection",
            "database" => 1,
            "prefix" => "Yii.redis.",
            "hostname" => "192.168.133.129",
            "port" => 6379,
        ],
        'user' => [
            'loginUrl' => 'http://sport.loc/login',
            'identityCookie' => [
                'path' => '/',
                'domain' => '.sport.loc',
            ]
        ],
        'session' => [
            'class' => 'redis_package.ARedisSession',
            'cookieMode' => 'allow',
            'cookieParams' => [
                'path' => '/',
                'domain' => '.sport.loc'
            ],
        ],
        'urlManager' => [
            'cacheID' => false,
            'rules' => [

            ]
        ],
        'log' => [
            'class' => 'CLogRouter',
            'routes' => [
                'logFile' => [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'filter' => 'CLogFilter'
                ],
                /*[
                    'class' => 'vendor.malyshev.yii-debug-toolbar.YiiDebugToolbarRoute',
                    'ipFilters' => ['127.0.0.1', '192.168.1.215', '*'],
                ],*/
                [
                    'class' => 'EMongoLogRoute',
                    'connectionId' => 'mongodb', // optional, defaults to 'mongodb'
                    'logCollectionName' => 'YiiLog', // optional, defaults to 'YiiLog'
                ]
            ]
        ],
    ],
    'params' => [
        'static_domain' => 'http://static.sport.loc'
    ]
];
