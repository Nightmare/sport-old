<?php
/**
 * Configuration parameters common to all entry points.
 */
return [
    'preload' => ['log', 'booster'],
    'aliases' => [
        'booster' => 'vendor.clevertech.yii-booster.src',
        'redis_package' => 'vendor.codemix.yiiredis'
    ],
    'import' => [
        'common.lib.*',
        'common.components.*',
        'common.extensions.gii.giix-components.*',
        'common.components.url.*',
        'common.models.*',
        'common.models.mongo.*',
        'common.models.mongo.message.*',
        'common.models.mongo.comment.*',
        'common.models.mongo.media.*',
        'common.models.mongo.items.*',
        'common.models.withoutAR.*',
        // The following two imports are polymorphic and will resolve against wherever the `basePath` is pointing to.
        // We have components and models in all entry points anyway
        'application.components.*',
        'application.models.*',
        'vendor.zhdanovartur.yii-easyimage.EasyImage',
        'vendor.sammaye.mongoyii.*',
        'vendor.sammaye.mongoyii.validators.*',
        'vendor.sammaye.mongoyii.behaviors.*',
        'vendor.sammaye.mongoyii.util.*'
    ],
    'sourceLanguage' => 'ru_RU',
    'language' => 'ru',
    'components' => [
        'themeManager' => [
            'themeClass' => '\common\components\Theme'
        ],
        'clientScript' => [
//            'class' => 'common.extensions.minify.NLSClientScript',
            'scriptMap' => [
                'jquery.js' => false,
                'jquery.min.js' => false,
            ],
        ],
        'request' => [
            'class' => '\common\components\Request'
        ],
        'static' => [
            'class' => '\common\components\StaticContent'
        ],
        'categories' => [
            'class' => '\common\components\Categories'
        ],
        'easyImage' => [
            'class' => 'vendor.zhdanovartur.yii-easyimage.EasyImage',
            //'driver' => 'GD',
            //'quality' => 100,
            //'cachePath' => '/assets/easyimage/',
            //'cacheTime' => 2592000,
            //'retinaSupport' => false,
        ],
        'ih' => [
            'class' => '\common\components\CImageHandler',
        ],
        'booster' => [
            'class' => 'booster.components.Booster',
            'enableNotifierJS' => false,
            'enableCdn' => true,
            'enableBootboxJS' => false,
            'enablePopover' => false,
            'enableTooltip' => false,
            //'coreCss' => false,
            //'bootstrapCss' => false,
            //'responsiveCss' => false,
            //'yiiCss' => false,
        ],
        'user' => [
            'allowAutoLogin' => true,
            'autoRenewCookie' => true,
            'returnUrl'=> ['/site/index'],
            'class' => '\common\components\WebUser',
        ],
        'imageUpload' => [
            'class' => '\common\components\ImageUpload'
        ],
        'ajax' => [
            'class' => '\common\components\Ajax'
        ],
        'breadcrumbs' => [
            'class' => '\common\components\Breadcrumbs'
        ],
        'curl' => [
            'class' => 'vendor.hackerone.curl.Curl'
        ],
        'youtube' => [
            'class' => '\common\components\video\Youtube',
            'api_link' => 'https://www.googleapis.com/youtube/v3/videos',
            'api_key' => 'AIzaSyCGDJ1qpJIvd-79tVJU-LJoSyDKdYKvjCM'
        ],
        'db' => [
            'schemaCachingDuration' => PRODUCTION_MODE ? 86400000 : 0, // 86400000 == 60*60*24*1000 seconds == 1000 days
            'enableParamLogging' => !PRODUCTION_MODE,
            'charset' => 'utf8',
            'tablePrefix' => ''
        ],
        'session' => [
            'sessionName' => 'sportime_ru',
        ],
        'authManager' => [
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
            'defaultRoles' => ['authenticated', 'guest'],
        ],
        'urlManager' => [
            'class' => 'UrlManager',
            'urlFormat' => 'path',
            'showScriptName' => false,
            'urlSuffix' => '.html',
            'rules' => [
                ['site/index',                  'pattern' => '/',                           'sitePart' => 'frontend', 'urlSuffix' => ''],
                ['users/user/login',            'pattern' => '/login',                      'sitePart' => 'frontend'],
                ['users/user/registration',     'pattern' => '/registration',               'sitePart' => 'frontend'],
                ['users/user/logout',           'pattern' => '/logout',                     'sitePart' => 'frontend'],
                ['users/profile/index',         'pattern' => '/',                           'sitePart' => 'profile'],
                ['users/profile/panel',         'pattern' => '/users/profile/panel',        'sitePart' => 'profile'],

                ['location/country',            'pattern' => '/country',                    'sitePart' => 'frontend'],
                ['location/region',             'pattern' => '/region',                     'sitePart' => 'frontend'],
                ['location/city',               'pattern' => '/city',                       'sitePart' => 'frontend'],

                //Галерея
                ['gallery/image/view',          'pattern' => '/<conference:.*>/gallery/image/<image_dir:.*>/view',      'sitePart' => 'frontend'],
                ['gallery/video/view',          'pattern' => '/<conference:.*>/gallery/video/<video_dir:.*>/view',      'sitePart' => 'frontend'],
                ['gallery/image/list',          'pattern' => '/<conference:.*>/images',                                 'sitePart' => 'frontend'],
                ['gallery/video/list',          'pattern' => '/<conference:.*>/videos',                                 'sitePart' => 'frontend'],

                //Сообщения
                ['message/profile/index',       'pattern' => '/messages',                   'sitePart' => 'profile'],
                ['message/profile/message',     'pattern' => '/message/profile/message',    'sitePart' => 'profile'],

                //Карман
                ['pocket/all/get',              'pattern' => '/pocket/get', 'sitePart' => 'profile', 'urlSuffix' => '.json'],
                ['pocket/image/index',          'pattern' => '/pocket/image', 'sitePart' => 'profile'],
                ['pocket/video/index',          'pattern' => '/pocket/video', 'sitePart' => 'profile'],

                //Сейвер
                ['saver/all/get',               'pattern' => '/saver/get',  'sitePart' => 'profile', 'urlSuffix' => '.json'],

                //Админ
                ['admin/index', 'pattern' => '/', 'sitePart' => 'backend',  'urlSuffix' => ''],
            ]
        ],
        'messages' => [
            'basePath' => 'common/messages'
        ],
        'log' => [
            'class' => 'CLogRouter',
            'routes' => [
                'logFile' => [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'filter' => 'CLogFilter'
                ],
                /*[
                    'class' => 'vendor.malyshev.yii-debug-toolbar.YiiDebugToolbarRoute',
                    'ipFilters' => ['127.0.0.1', '192.168.1.215', '178.151.80.59'],
                ],*/
                /*[
                    'class' => 'EMongoLogRoute',
                    'connectionId' => 'mongodb', // optional, defaults to 'mongodb'
                    'logCollectionName' => 'YiiLog', // optional, defaults to 'YiiLog'
                ]*/
            ]
        ],
    ],
    'params' => [
        'pages' => [
            'image' => 40,
            'video' => 40
        ],
        'sizes' => [
            'avatar' => [
                'preview' => [
                    'width' => 200,
                    'height' => 200
                ]
            ],
            'image' => [
                'preview' => [
                    'width' => 230,
                    'height' => 130
                ]
            ],
            'video' => [
                'preview' => [
                    'width' => 230,
                    'height' => 130
                ]
            ],
        ]
    ]
];
