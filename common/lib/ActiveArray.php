<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 27.04.14
 * Time: 19:50
 */

class ActiveArray
{
    protected $array;

    public function __construct(&$array) {
        $this->array = &$array;
    }

    public function __set($name, $value) {
        $this->array[$name] = $value;
    }

    public function __get($name) {
        if(!isset($this->array[$name]))
            return null;

        if (is_array($this->array[$name]))
            return new self($this->array[$name]);
        else
            return $this->array[$name];
    }
} 