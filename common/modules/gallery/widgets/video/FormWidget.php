<?php
namespace common\modules\gallery\widgets\video;

/**
 * Created by PhpStorm.
 * User: me
 * Date: 02.10.2014
 * Time: 18:29
 */

use CWidget;
class FormWidget extends CWidget
{
    public function run()
    {
        $this->render('form');
    }
} 