<?php
namespace common\modules\comment\components;
/**
 * Created by PhpStorm.
 * User: me
 * Date: 10.10.2014
 * Time: 4:41
 */

use CommentItems;
use Media;
class Tree
{
    /** @var array|CommentItems[]  */
    private $_parents = [];
    /** @var array|CommentItems[]  */
    private $_children = [];

    /** @var array|Media */
    private $_media = [];

    /** @var array|CommentItems[] */
    private $_tree = [];

    /** @var int  */
    private $_deep = 5;

    /**
     * @param CommentItems[] $parents
     * @param CommentItems[] $children
     */
    public function __construct($parents, $children)
    {
        $this->_parents = $parents;
        $this->_children = $children;

        if(!empty($this->_parents) && !empty($this->_children))
            $this->run();
    }

    protected function run()
    {
        $children = [];
        $this->_children->rewind();
        if ($this->_children->valid())
            $children = iterator_to_array($this->_children);

        foreach ($this->_parents as $parent) {
            $this->_media = \CMap::mergeArray($this->_media, $parent->getMedia());
            $this->_tree[] = [
                'comment_id' => $parent->getPrimaryKey().'',
                'comment' => $parent,
                'children' => $this->buildTree($parent->getPrimaryKey(), $children)
            ];
        }
    }

    /**
     * @param $parentId
     * @param CommentItems[] $list
     * @param int $deep
     * @return array
     */
    protected function buildTree($parentId, $list, $deep = -1)
    {
        $deep++;
        if($deep == $this->_deep)
            return [];

        $tree = [];
        foreach ($list as $key => $item)
        {
            if($item->getParent() == $parentId)
            {
                $this->_media = \CMap::mergeArray($this->_media, $item->getMedia());

                unset($list[$key]);

                $tree[] = [
                    'comment_id' => $item->getPrimaryKey().'',
                    'comment' => $item,
                    'children' => $this->buildTree($item->getPrimaryKey(), $list, $deep)
                ];
            }
        }

        return $tree;
    }

    public function getTree()
    {
        return $this->_tree;
    }

    public function getMedia()
    {
        return $this->_media;
    }

    public function getDeep()
    {
        return $this->_deep;
    }
} 