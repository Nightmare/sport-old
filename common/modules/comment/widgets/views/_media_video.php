<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 09.10.2014
 * Time: 17:47
 *
 * @var VideoMedia $model
 */ ?>

<li class="video-item" data-unique-source="<?= $model->getUnique(); ?>">
    <div class="image-item-inner">
        <a href="<?= $model->getVideoLink(); ?>"><img src="<?= Yii::app()->static->imageLink($model->getThumb()); ?>" alt="holder"></a>
    </div>
</li>