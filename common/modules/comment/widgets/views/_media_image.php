<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 09.10.2014
 * Time: 17:47
 *
 * @var ImageMedia $model
 */ ?>

<li class="image-item" data-unique-source="<?= $model->getUnique(); ?>">
    <div class="image-item-inner">
        <a href="<?= Yii::app()->static->imageLink($model->getOrigin()); ?>"><img src="<?= Yii::app()->static->imageLink($model->getThumb()); ?>" alt="holder"></a>
    </div>
</li>