<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 08.10.2014
 * Time: 22:48
 *
 * @var int $level
 * @var int $deep
 * @var CommentItems $model
 * @var User $user
 */ ?>

<li data-unique="<?= $model->getUnique(); ?>" class="comment-item comment-item-reply-level-<?= $level; ?>">
    <img src="<?= $user->userProfile->getAvatar(UserProfile::SIZE_TYPE_MIN) ?>" alt="<?= $user->getUsername() ?>">
    <div class="comment-info">
        <a href="javascript:void(0);" class="username"><?= $user->getUsername() ?></a>
        <time class="time" datetime="<?= date('Y-m-d H:i:s', $model->getCreateAt()) ?>"><?= date('H:i d.m.Y', $model->getCreateAt()) ?></time>
        <span class="show_tree" data-toggle="tooltip" title="Показать только эту ветку" data-main="<?= $model->getUnique(); ?>"></span>
        <?php if ($model->getParent() !== null): ?><span class="tree_top" data-toggle="tooltip" title="Подняться в корень ветки" data-source="<?= $model->getMainParent(); ?>">↑</span><?php endif; ?>
        <?php foreach ($model->getMedia() as $media): ?>
            <span data-unique-link="<?= $media->getUnique(); ?>" class="attach"><a class="attach-<?= $media->getType(); ?>" href=""><?= $media->getType(); ?></a></span>
        <?php endforeach; ?>

        <div class="text"><?= $model->getBody() ?></div>
        <?= $level != $deep ? '<a href="javascript:void(0);" data-id="'.$model->getPrimaryKey().'" class="reply">Добавить ответ</a>' : ''; ?>
    </div>
</li>