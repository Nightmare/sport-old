<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 08.10.2014
 * Time: 21:13
 *
 * @var \common\modules\comment\widgets\ListViewWidget $this
 * @var \common\extensions\pagination\Pagination $pages
 * @var array $liList
 * @var User[] $users
 * @var Media[] $media
 */
?>

<div class="comment_container" id="replacement">
    <div class="container comment_box">
        <ul class="comment-list" id="comment_list">
            <?php if(empty($liList)): ?>
                <li class="empty_text">
                    <?= Yii::t('labels', 'Комментарии отсутствуют') ?>
                </li>
            <?php endif; ?>
            <li class="pagerWrapper">
                <?php $this->widget('\common\extensions\pagination\Pagination', [
                    'pages' => $pages,
                    'cssFile' => Yii::app()->static->setOwn()->getLink('css/paging.css')
                ]); ?>
            </li>
            <?php foreach ($liList as $li): ?>
                <?= $li; ?>
            <?php endforeach; ?>
            <li class="pagerWrapper">
                <?php $this->widget('\common\extensions\pagination\Pagination', [
                    'pages' => $pages,
                    'cssFile' => Yii::app()->static->setOwn()->getLink('css/paging.css')
                ]); ?>
            </li>
        </ul>
    </div>
</div><!-- .container-->