<?php
namespace common\modules\comment\widgets;

use common\components\VarDumper;
use common\interfaces\iComment;
use common\modules\comment\components\Tree;
use CWidget;
use Yii;
use EMongoCriteria;
use EMongoPagination;
use CommentItems;
use CDbCriteria;
use User;
use Media;
use CDbCacheDependency;
use CMap;
/**
 * Class MessageListWidget
 *
 * @package application.widgets.messageList
 */
class ListViewWidget extends CWidget
{
    /** @var iComment */
    public $model = null;

    public $controller = false;

    public $comment_id = null;

    private $_assets = null;

    public $view = 'list_view_index';

    /** @var User[] */
    private $_users = [];

    public function init()
    {
        if($this->controller)
            return;

        Yii::app()->static->setLibrary('fileapi')
            ->registerScriptFile2('FileAPI/FileAPI.min.js')
            ->registerScriptFile2('FileAPI/FileAPI.exif.js')
            ->registerScriptFile2('jquery.fileapi.min.js');

        parent::init();

        Yii::app()->static->setWidget('comment')
            ->registerScriptFile('script.js')
            ->registerCssFile('style.css');

        $registry = Yii::app()->clientScript;
        $registry
            ->registerScript(uniqid(), "var sendCommentLink = '".$this->model->getCommentSendLink()."';", \CClientScript::POS_END)
            ->registerScript(uniqid(), "var getCommentLink = '".$this->model->getCommentListLink()."';", \CClientScript::POS_END)
            ->registerScript(uniqid(), "var uploadCommentTemp = '".$this->model->getCommentUploadImageLink()."';", \CClientScript::POS_END)
            ->registerScript(uniqid(), "var videoCommentTemp = '".$this->model->getCommentUploadVideoLink()."';", \CClientScript::POS_END);
    }

    public function run()
    {
        $criteria = new EMongoCriteria();
        $criteria->addCondition($this->model->getCommentField(), (int)$this->model->getId());
        $criteria->sort = ['create_at' => 'desc'];
        if($this->comment_id !== null)
            $criteria->addCondition('unique', $this->comment_id);
        else
            $criteria->addCondition('parent', null);

        $Pages = new EMongoPagination(CommentItems::model()->count($criteria));
        $Pages->pageSize = 5;
        $Pages->route = $this->model->getCommentPageRoute();
        $Pages->applyLimit($criteria);

        $userIds = $ids = [];
        /** @var CommentItems[] $List */
        $List = CommentItems::model()->find($criteria);
        foreach ($List as $Comment) {
            $ids[] = $Comment->getPrimaryKey();
            $userIds[] = $Comment->getUserSenderId();
        }

        /* get tree */
        $criteria = new EMongoCriteria();
        $condition =  ['parents' => ['$in' => $ids]];
        $criteria->setCondition($condition);
        $criteria->sort = ['create_at' => 'asc'];
        //var_dump($criteria);die;
        /** @var CommentItems[] $Children */
        $Children = CommentItems::model()->find($criteria);
        foreach ($Children as $Comment) {
            if(!in_array($Comment->getUserSenderId(), $userIds))
                $userIds[] = $Comment->getUserSenderId();
        }

        $Tree = new Tree($List, $Children);

        $dependency = new CDbCacheDependency('SELECT MAX(update_at) FROM {{user}}');
        $dependency->reuseDependentData = true;

        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $userIds);
        $criteria->with = ['userProfile'];
        /** @var User[] $Users */
        $Users = User::model()->cache(10000, $dependency)->findAll($criteria);
        foreach ($Users as $User)
            $this->_users[$User->getId()] = $User;

        $liList = $this->drawItem($Tree->getTree(), $Tree->getDeep());
        $this->render($this->view, ['liList' => $liList, 'media' => $Tree->getMedia(), 'pages' => $Pages]);
    }

    /**
     * @param CommentItems[] $list
     * @param int $deep
     * @param int $level
     * @return array
     * @throws \CException
     */
    private function drawItem($list, $deep, $level = -1)
    {
        $level++;
        $li = [];
        foreach ($list as $item) {
            /** @var CommentItems $model */
            $model = $item['comment'];
            $li[] = $this->render('_list_view_item', [
                'model' => $model,
                'user'  => $this->_users[$model->getUserSenderId()],
                'level' => $level,
                'deep'  => $deep
            ], true);

            $_l = $this->drawItem($item['children'], $deep, $level);
            $li = CMap::mergeArray($li, $_l);
        }

        return $li;
    }
}